namespace :setup do
  desc "import globalnetsis admin to database"
  task :make_admin do
    on roles(:app) do
      within "#{current_path}" do
        with rails_env: :production do
          execute :rake, "db:seed"
        end
      end
    end
  end
end
