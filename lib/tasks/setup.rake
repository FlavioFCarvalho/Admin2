require 'yaml'
require 'stage/generator.rb'

namespace :system do
  desc "Setup and Initialize application"
  task setup: :environment do
    images_path = Rails.root.join('public', 'system')
    puts "\n💎  Initializing basic setup for the system 💎 \n"
    puts "\n✘ DROPING databases ... \n"
    puts "#{%x(rake db:drop)} \n"
    sleep(1)
    puts "\n✘ DELETING images from PUBLIC folder ... #{%x(rm -rf #{images_path})} \n\n"
    sleep(2)
    puts "\n✔ CREATING development database ... \n#{%x(rake db:create RAILS_ENV=development)}\n"
    puts "✚ IMPORTING migration files ... \n\n"
    puts"#{%x(rake db:migrate)}"
    puts "✚ IMPORTING seed file ... \n"
    sleep(1)
    puts "#{%x(rake db:seed)}"
    sleep(2)
    puts "\n💎 DEPLOYING GEAR UP ... 💎"
    sleep(1)
    puts "\n💎 ... "
    puts "\n#{%x(rake system:service_samples)}"
    puts "\n#{%x(rake system:bank_samples)}"
    puts "\n#{%x(rake system:obra_samples)}"
    puts "\n#{%x(rake system:make_admin)}"
    sleep(2)
    puts "\n 💎 SETUP IS COMPLETE 💎\n"
    puts "\n 💎 run rails s to run the server 💎\n"
  end

  desc "import globalnetsis admin to database"
  task make_admin: :environment do
    Admin.create(email: "globalnetsis@globalnetsis.com.br", password: Setup::Secret.make)
    puts "-*- ADMIN CREATED  -*-\n"
    puts "➤ USE globalnetsis@globalnetsis.com.br to LOG-IN the system"
  end

  desc "import service samples to database"
  task service_samples: :environment do
    10.times do
      @service = Service.new do |f|
        f.name= Faker::Commerce.department(2, true)
      end
      @service.save(validate: false)
    end
  end

  desc "import product samples to database"
  task service_samples: :environment do
    10.times do
      @product = Produto.new do |f|
        f.nome= Faker::Commerce.department(2, true)
      end
      @service.save(validate: false)
    end
  end

  desc "import bank samples to database"
  task obra_samples: :environment do
    10.times do
      @obra = Obra.new do |f|
        f.nome_local= Faker::Ancient.god,
        f.codigo    = Faker::Commerce.promotion_code
      end
      @obra.save(validate: false)
    end
  end

  desc "import bank samples to database"
  task bank_samples: :environment do
    10.times do
      @banco = Banco.new do |f|
        f.nome=Faker::Bank.name
      end
      @banco.save(validate: false)
    end
  end
end
