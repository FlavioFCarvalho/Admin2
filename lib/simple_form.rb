module SimpleForm
  module Components
    module CustomErrors
      include SimpleForm::Helpers::HasErrors

      def custom_error
        enabled_error
      end

      def error_tag
        options[:error_tag] || SimpleForm.error_tag
      end

      def error_text
        if options[:error_prefix]
          options[:error_prefix] + " " + errors.send(error_method)
        else
          errors.send(error_method)
        end
      end

      def error_method
        options[:error_method] || SimpleForm.error_method
      end

      def error_html_options
        html_options_for(:error, [SimpleForm.error_class])
      end

      protected

      def enabled_error
        # We need '<span title="Blah!"></span>'
        template.content_tag(error_tag, '', error_html_options.merge({:title => error_text})) if has_errors?
      end

      def disabled_error
        nil
      end

      def errors
        @errors ||= (errors_on_attribute + errors_on_association).compact
      end

      def errors_on_attribute
        object.errors[attribute_name]
      end

      def errors_on_association
        reflection ? object.errors[reflection.name] : []
      end
    end
  end

  module Inputs
    class Base
      include SimpleForm::Components::CustomErrors
    end
  end
end
