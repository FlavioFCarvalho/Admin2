class ChangeColumnStatusFromProdutos < ActiveRecord::Migration[5.0]
  def change
     change_column :produtos, :status, :integer, default: 0
  end
end
