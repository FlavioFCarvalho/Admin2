class ChangeColumnUpdatedValueFromEstoqueEstoqueParcelamentos < ActiveRecord::Migration[5.0]
  def change
	rename_column :estoque_estoque_parcelamentos, :updated_value, :bloqueado
  end
end
