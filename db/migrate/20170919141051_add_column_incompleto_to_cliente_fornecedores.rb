class AddColumnIncompletoToClienteFornecedores < ActiveRecord::Migration[5.0]
  def change
    add_column :cliente_fornecedores, :dados, :integer, default: 0
    ClienteFornecedor.update_all(dados: 0)
  end
end
