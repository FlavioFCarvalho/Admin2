class AddUserIdToComodos < ActiveRecord::Migration[5.0]
  def change
    add_column :comodos, :user_id, :integer
  end
end
