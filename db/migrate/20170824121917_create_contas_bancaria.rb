class CreateContasBancaria < ActiveRecord::Migration[5.0]
  def change
    create_table :contas_bancaria do |t|
      t.integer :banco_id
      t.string :agencia
      t.string :conta
      t.string :tipo
      t.integer :cliente_fornecedor_id

      t.timestamps
    end
  end
end
