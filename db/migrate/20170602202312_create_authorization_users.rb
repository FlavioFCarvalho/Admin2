class CreateAuthorizationUsers < ActiveRecord::Migration[5.0]
  def change
    create_table :authorization_users do |t|
      t.belongs_to :user, foreign_key: true
      t.belongs_to :authorization, foreign_key: true

      t.timestamps
    end
  end
end
