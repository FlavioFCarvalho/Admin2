class AddStatusToContasBancaria < ActiveRecord::Migration[5.0]
  def change
    add_column :contas_bancaria, :status, :boolean, default: 1
  end
end
