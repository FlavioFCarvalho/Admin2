class AddFieldsToObras < ActiveRecord::Migration[5.0]
  def change
    add_column :obras, :endereco_local, :string
    add_column :obras, :bairro, :string
    add_column :obras, :cep, :string
    add_column :obras, :cidade_id, :integer
    add_column :obras, :estado_id, :integer
    add_column :obras, :endereco_obra, :string
    add_column :obras, :quadra, :string
    add_column :obras, :lote, :string
    add_column :obras, :cei, :string
    add_column :obras, :peticao, :string
    add_column :obras, :aprovacao, :string
  end
end
