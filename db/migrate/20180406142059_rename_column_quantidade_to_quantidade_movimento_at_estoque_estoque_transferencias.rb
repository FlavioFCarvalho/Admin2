class RenameColumnQuantidadeToQuantidadeMovimentoAtEstoqueEstoqueTransferencias < ActiveRecord::Migration[5.0]
  def change
    rename_column :estoque_estoque_transferencias, :quantidade, :quantidade_movimento
  end
end
