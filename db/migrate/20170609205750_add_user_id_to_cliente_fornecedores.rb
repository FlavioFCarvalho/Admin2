class AddUserIdToClienteFornecedores < ActiveRecord::Migration[5.0]
  def change
    add_reference :cliente_fornecedores, :user, foreign_key: true
  end
end
