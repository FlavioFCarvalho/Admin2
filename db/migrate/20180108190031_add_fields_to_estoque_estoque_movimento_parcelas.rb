class AddFieldsToEstoqueEstoqueMovimentoParcelas < ActiveRecord::Migration[5.0]
  def change
    add_column :estoque_estoque_movimento_parcelas, :data_vencimento, :date
    add_column :estoque_estoque_movimento_parcelas, :data_pagamento, :date
    add_column :estoque_estoque_movimento_parcelas, :periodo, :string, default: "NÃO MENCIONADO"
    add_column :estoque_estoque_movimento_parcelas, :status_pagamento, :string, default: "A PAGAR"
  end
end
