class AddColumnQuantidadeAtualEQuantidadeAnteriorToEstoqueEstoqueTransferencias < ActiveRecord::Migration[5.0]
  def change
    add_column :estoque_estoque_transferencias, :quantidade_anterior, :integer, default: 0
    add_column :estoque_estoque_transferencias, :quantidade_atual, :integer, default: 0
  end
end
