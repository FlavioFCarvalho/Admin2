class ChangeColumnBancoAtContas < ActiveRecord::Migration[5.0]
  def change
    rename_column :contas, :nome_banco, :banco_id
    change_column :contas, :banco_id, :integer
  end
end
