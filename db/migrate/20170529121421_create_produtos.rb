class CreateProdutos < ActiveRecord::Migration[5.0]
  def change
    create_table :produtos do |t|
      t.string :status
      t.string :nome
      t.string :unidade
      t.text :observacoes
      t.timestamps
    end
  end
end
