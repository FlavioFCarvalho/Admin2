class AddColumnStatusToObras < ActiveRecord::Migration[5.0]
  def change
    add_column :obras, :status, :string
  end
end
