class AddAttachmentPictureToReportConfigurations < ActiveRecord::Migration
  def self.up
    change_table :report_configurations do |t|
      t.attachment :picture
    end
  end

  def self.down
    remove_attachment :report_configurations, :picture
  end
end
