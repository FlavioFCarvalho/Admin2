class ChangeColumnClienteFornecedorAtClienteFornecedor < ActiveRecord::Migration[5.0]
  def change
    change_column :cliente_fornecedores, :cliente_fornecedor, :string, limit: 50
  end
end
