class AddColumnValidaPorAtEstoqueEstoqueMovimentos < ActiveRecord::Migration[5.0]
  def change
    add_column :estoque_estoque_movimentos, :valida_por, :string, default: "total_estoque"
  end
end
