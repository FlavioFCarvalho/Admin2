class ChangeColumnValorAtEstoqueEstoqueTransferencia < ActiveRecord::Migration[5.0]
  def change
    change_column :estoque_estoque_transferencias, :valor_total, :decimal, :precision => 14, :scale => 2
    change_column :estoque_estoque_transferencias, :valor_produto, :decimal, :precision => 14, :scale => 2

  end
end
