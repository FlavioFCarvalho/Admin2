class AddColumnsToEstoqueMovimentos < ActiveRecord::Migration[5.0]
  def change
    add_column :estoque_estoque_movimentos, :data_transferencia, :date
    add_column :estoque_estoque_movimentos, :transferencia_tipo, :string
  end
end
