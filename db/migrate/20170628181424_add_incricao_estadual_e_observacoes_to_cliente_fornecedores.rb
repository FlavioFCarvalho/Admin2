class AddIncricaoEstadualEObservacoesToClienteFornecedores < ActiveRecord::Migration[5.0]
  def change
    add_column :cliente_fornecedores, :inscricao_estadual, :string
    add_column :cliente_fornecedores, :observacao, :text
    add_column :cliente_fornecedores, :id_nome, :string
    add_column :cliente_fornecedores, :datafaker, :string
    add_column :cliente_fornecedores, :data_cadastro, :string
  end
end
