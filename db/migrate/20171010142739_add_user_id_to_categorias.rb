class AddUserIdToCategorias < ActiveRecord::Migration[5.0]
  def change
    add_column :categorias, :user_id, :integer
  end
end
