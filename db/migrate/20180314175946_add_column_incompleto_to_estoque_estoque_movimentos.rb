class AddColumnIncompletoToEstoqueEstoqueMovimentos < ActiveRecord::Migration[5.0]
  def change
    add_column :estoque_estoque_movimentos, :incompleto, :boolean, default: 1
  end
end
