class RenameColumnValorParcelaAtEstoqueEstoqueParcelamentos < ActiveRecord::Migration[5.0]
  def change
	rename_column :estoque_estoque_parcelamentos, :valor_parcela, :valor
  end
end
