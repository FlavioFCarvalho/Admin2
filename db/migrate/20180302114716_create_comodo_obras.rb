class CreateComodoObras < ActiveRecord::Migration[5.0]
  def change
    create_table :comodo_obras do |t|
      t.references :obra
      t.references :comodo

      t.timestamps
    end
  end
end
