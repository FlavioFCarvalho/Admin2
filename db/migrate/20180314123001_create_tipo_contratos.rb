class CreateTipoContratos < ActiveRecord::Migration[5.0]
  def change
    create_table :tipo_contratos do |t|
      t.string :name

      t.timestamps
    end
  end
end
