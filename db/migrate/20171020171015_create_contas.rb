class CreateContas < ActiveRecord::Migration[5.0]
  def change
    create_table :contas do |t|
      t.string :nome_banco
      t.string :agencia
      t.string :numero
      t.string :tipo_conta
      t.string :operacao
      t.string :titular
      t.string :cpf_cnpj
      t.timestamps
    end
  end
end
