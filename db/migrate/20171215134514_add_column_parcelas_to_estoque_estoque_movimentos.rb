class AddColumnParcelasToEstoqueEstoqueMovimentos < ActiveRecord::Migration[5.0]
  def change
    add_column :estoque_estoque_movimentos, :parcelas, :integer, default: 1
  end
end
