class ChangeColumnsAtEstoqueEstoqueProdutos < ActiveRecord::Migration[5.0]
  def change
    rename_column :estoque_estoque_produtos, :quantidade, :quantidade_movimento
    change_column :estoque_estoque_produtos, :quantidade_movimento, :integer, default: 0
    add_column    :estoque_estoque_produtos, :quantidade_anterior, :integer, default: 0
    add_column    :estoque_estoque_produtos, :quantidade_atual, :integer, default: 0
  end
end
