class AddColumnsToAuthorizationUsers < ActiveRecord::Migration[5.0]
  def change
    add_column :authorization_users, :_create,   :boolean, default: 0
    add_column :authorization_users, :_retrieve, :boolean, default: 0
    add_column :authorization_users, :_update,   :boolean, default: 0
    add_column :authorization_users, :_delete,   :boolean, default: 0
  end
end
