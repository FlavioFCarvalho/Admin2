class AddFieldsToEstoque < ActiveRecord::Migration[5.0]
  def change
    add_column :estoque_estoque_movimentos, :acrescimo, :string, limit: 255
    add_column :estoque_estoque_movimentos, :desconto, :string, limit: 255
    add_column :estoque_estoque_movimentos, :valor_total_produto, :decimal, :precision => 14, :scale => 2
  end
end


# :valor_total = :valor_total_produto + :acrescimo - :desconto