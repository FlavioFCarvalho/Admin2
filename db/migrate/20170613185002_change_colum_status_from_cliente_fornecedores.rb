class ChangeColumStatusFromClienteFornecedores < ActiveRecord::Migration[5.0]
  def change
     change_column :cliente_fornecedores, :status, :integer, default: 0
  end
end
