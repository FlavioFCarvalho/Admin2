class CreateEstoqueEstoqueParcelamentos < ActiveRecord::Migration[5.0]
  def change
    create_table :estoque_estoque_parcelamentos do |t|
      t.integer :estoque_movimento_id, null: false
      t.integer :estoque_produto_id
      t.integer :estoque_transferencia_id
      t.decimal :valor_parcela, :precision => 14, :scale => 2

      t.timestamps
    end
  end
end
