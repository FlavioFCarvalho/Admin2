class CreateFornecedorProdutos < ActiveRecord::Migration[5.0]
  def change
    create_table :fornecedor_produtos do |t|
      t.integer :produto_id
      t.integer :fornecedor_id

      t.timestamps
    end
  end
end
