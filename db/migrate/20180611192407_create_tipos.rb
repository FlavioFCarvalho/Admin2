class CreateTipos < ActiveRecord::Migration[5.0]
  def change
    create_table :tipos do |t|
      t.string :name
      t.integer :status
      t.integer :user_id

      t.timestamps
    end
  end
end
