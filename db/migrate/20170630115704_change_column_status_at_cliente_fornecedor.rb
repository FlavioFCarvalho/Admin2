class ChangeColumnStatusAtClienteFornecedor < ActiveRecord::Migration[5.0]
  def change
    change_column :cliente_fornecedores, :status, :string, limit: 20
  end
end
