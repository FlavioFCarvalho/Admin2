class AddUserIdToBancos < ActiveRecord::Migration[5.0]
  def change
    add_reference :bancos, :user, foreign_key: true
  end
end
