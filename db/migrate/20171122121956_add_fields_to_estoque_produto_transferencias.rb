class AddFieldsToEstoqueProdutoTransferencias < ActiveRecord::Migration[5.0]
  def change
    # => Colunas adicionais para criar parcelamento de itens em EstoqueProduto
    add_column :estoque_estoque_produtos, :parcelas, :integer

    # => Colunas adicionais para criar parcelamento de itens em EstoqueTransferencia
    add_column :estoque_estoque_transferencias, :parcelas, :integer
  end
end
