class CreateEstados < ActiveRecord::Migration[5.0]
  def change
    create_table :estados do |t|
      t.string :sigla
      t.string :nome
      t.integer :capital_id
      t.timestamps
    end
  end
end
