class ChangeColumnUnidadeFromSubItensObra < ActiveRecord::Migration[5.0]
  def change
    change_column :sub_itens_obra, :unidade, :string
  end
end
