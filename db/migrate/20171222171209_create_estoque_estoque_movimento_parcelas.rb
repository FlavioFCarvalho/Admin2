class CreateEstoqueEstoqueMovimentoParcelas < ActiveRecord::Migration[5.0]
  def change
    create_table :estoque_estoque_movimento_parcelas do |t|
      t.decimal :valor, :precision => 14, :scale => 2
      t.references :estoque_movimento
      t.boolean :bloqueado, default: 0
      t.timestamps
    end
  end
end
