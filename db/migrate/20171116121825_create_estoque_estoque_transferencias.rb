class CreateEstoqueEstoqueTransferencias < ActiveRecord::Migration[5.0]
  def change
    create_table :estoque_estoque_transferencias do |t|
      t.integer :estoque_movimento_id
      t.integer :produto_id
      t.integer :obra_anterior_id
      t.integer :obra_posterior_id
      t.integer :item_obra_id
      t.integer :sub_item_obra_id
      t.integer :quantidade
      t.decimal :valor_produto
      t.decimal :valor_total
      t.belongs_to :item, foreign_key: true

      t.timestamps
    end
  end
end
