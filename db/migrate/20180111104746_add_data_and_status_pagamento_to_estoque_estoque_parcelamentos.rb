class AddDataAndStatusPagamentoToEstoqueEstoqueParcelamentos < ActiveRecord::Migration[5.0]
  def change
    add_column :estoque_estoque_parcelamentos, :data_vencimento, :date
    add_column :estoque_estoque_parcelamentos, :status_pagamento, :string, default: "A PAGAR"
    add_column :estoque_estoque_parcelamentos, :periodo, :string
  end
end
