class AddEtapaSubEtapaColumnsToEstoqueEstoqueProdutos < ActiveRecord::Migration[5.0]
  def change
    add_column :estoque_estoque_produtos, :item_obra_id, :integer
    add_column :estoque_estoque_produtos, :sub_item_obra_id, :integer
  end
end
