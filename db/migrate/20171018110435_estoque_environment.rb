class EstoqueEnvironment < ActiveRecord::Migration[5.0]
  def change
    '''
      * CRIANDO A TABELA ESTOQUE *
    '''
    create_table :estoque_estoque_movimentos do |t|
      t.string  :tipo_movimentacao
      t.integer :fornecedor_id
      t.integer :user_id
      t.string :nota_fiscal_entrada
      t.string :recibo
      t.string :cupom_fiscal
      t.string :numero_pedido
      t.string :numero_da_os
      t.string :nota_fiscal_saida
      t.date :data_entrada
      t.date :data_vencimento_entrada
      t.date :data_saida
      t.date :data_vencimento_saida
      t.date :data_devolucao
      t.text :justificativa_devolucao
      t.decimal :total_estoque, :precision => 14, :scale => 2
      t.timestamps
    end

    '''
      * CRIANDO A TABELA ESTOQUE PRODUTOS *
    '''
    create_table :estoque_estoque_produtos do |t|
      t.integer :produto_id
      t.integer :estoque_movimento_id
      t.integer :obra_id
      t.integer :quantidade, default: 1
      t.decimal :valor_produto, :precision => 14, :scale => 2
      t.decimal :valor_total, :precision => 14, :scale => 2

      t.timestamps
    end
  end
end
