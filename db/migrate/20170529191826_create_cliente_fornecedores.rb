class CreateClienteFornecedores < ActiveRecord::Migration[5.0]
  def change
    create_table :cliente_fornecedores do |t|
      t.string :status
      t.string :tipo_pessoa, limit: 20
      t.string :cliente_fornecedor, limit: 2
      t.string :nome
      t.string :razao_social
      t.string :cpf, limit: 20
      t.string :cnpj, limit: 20
      t.string :rg, limit: 20
      t.string :sexo, limit: 20
      t.string :endereco
      t.string :numero, limit: 10
      t.string :complemento
      t.string :cep, limit: 20
      t.string :bairro
      t.belongs_to :cidade, index: true, foreign_key: true
      t.belongs_to :estado, index: true, foreign_key: true
      t.string :contato1_nome
      t.string :contato1_ddd, limit: 2
      t.string :contato1_telefone
      t.string :contato1_tipo_telefone, limit: 20
      t.string :contato1_email1, limit: 80
      t.string :contato1_email2, limit: 80
      t.string :contato2_nome
      t.string :contato2_ddd, limit: 2
      t.string :contato2_telefone
      t.string :contato2_tipo_telefone, limit: 20
      t.string :contato2_email1, limit: 80
      t.string :contato2_email2, limit: 80
      t.string :contato3_nome
      t.string :contato3_ddd, limit: 2
      t.string :contato3_telefone
      t.string :contato3_tipo_telefone, limit: 20
      t.string :contato3_email1, limit: 80
      t.string :contato3_email2, limit: 80
      t.timestamps
    end
  end
end
