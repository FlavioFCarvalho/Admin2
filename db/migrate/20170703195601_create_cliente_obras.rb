class CreateClienteObras < ActiveRecord::Migration[5.0]
  def change
    create_table :cliente_obras do |t|
      t.integer :cliente_id
      t.integer :obra_id

      t.timestamps
    end
  end
end
