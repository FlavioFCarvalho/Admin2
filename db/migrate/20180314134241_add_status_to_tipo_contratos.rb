class AddStatusToTipoContratos < ActiveRecord::Migration[5.0]
  def change
    add_column :tipo_contratos, :status, :integer
  end
end
