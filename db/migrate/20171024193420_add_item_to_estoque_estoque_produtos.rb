class AddItemToEstoqueEstoqueProdutos < ActiveRecord::Migration[5.0]
  def change
    add_reference :estoque_estoque_produtos, :item, foreign_key: true
  end
end
