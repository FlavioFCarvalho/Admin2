class RemoveColumnComodoIdFromEstoqueEstoqueProdutos < ActiveRecord::Migration[5.0]
  def change
    remove_foreign_key :estoque_estoque_produtos, :comodos

    remove_index :estoque_estoque_produtos, :comodo_id

    remove_column :estoque_estoque_produtos, :comodo_id, :integer
  end
end
