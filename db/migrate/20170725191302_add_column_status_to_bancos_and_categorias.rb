class AddColumnStatusToBancosAndCategorias < ActiveRecord::Migration[5.0]
  def change
    add_column :bancos, :status, :string, limit: 20
    add_column :categorias, :status, :string, limit: 20
  end
end
