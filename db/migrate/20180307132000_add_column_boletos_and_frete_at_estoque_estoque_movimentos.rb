class AddColumnBoletosAndFreteAtEstoqueEstoqueMovimentos < ActiveRecord::Migration[5.0]
  def change
    add_column :estoque_estoque_movimentos, :boleto, :string, limit: 255
    add_column :estoque_estoque_movimentos, :frete, :decimal, :precision => 14, :scale => 2
  end
end
