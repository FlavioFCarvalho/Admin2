class AddColumnUpdatedValueToEstoqueParcelamentos < ActiveRecord::Migration[5.0]
  def change
    add_column :estoque_estoque_parcelamentos, :updated_value, :boolean, default: 0
  end
end
