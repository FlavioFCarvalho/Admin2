class AddTiposToClienteFornecedores < ActiveRecord::Migration[5.0]
  def change
    add_column :cliente_fornecedores, :tipo_cliente,    :string
    add_column :cliente_fornecedores, :tipo_fornecedor, :string
    add_column :cliente_fornecedores, :tipo_investidor, :string
  end
end
