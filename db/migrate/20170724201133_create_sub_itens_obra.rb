class CreateSubItensObra < ActiveRecord::Migration[5.0]
  def change
    create_table :sub_itens_obra do |t|
      t.string :sub_item
      t.integer :item_obra_id
      t.string :sub_tipo_servico
      t.integer :unidade

      t.timestamps
    end
  end
end
