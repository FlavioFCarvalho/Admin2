class CreateFornecedorServicos < ActiveRecord::Migration[5.0]
  def change
    create_table :fornecedor_servicos do |t|
      t.integer :fornecedor_id
      t.belongs_to :service, foreign_key: true

      t.timestamps
    end
  end
end
