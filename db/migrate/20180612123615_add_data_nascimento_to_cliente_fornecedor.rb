class AddDataNascimentoToClienteFornecedor < ActiveRecord::Migration[5.0]
  def change
    add_column :cliente_fornecedores, :data_nascimento, :date
  end
end
