class AddColumnQuantidadeToProdutos < ActiveRecord::Migration[5.0]
  def change
    add_column :produtos, :quantidade, :integer, default: 0
  end
end
