class AddUserIdToObras < ActiveRecord::Migration[5.0]
  def change
    add_reference :obras, :user, foreign_key: true
  end
end
