class CreateProdutoCategorias < ActiveRecord::Migration[5.0]
  def change
    create_table :produto_categorias do |t|
      t.belongs_to :produto, index: true, foreign_key: true
      t.belongs_to :categoria, index: true, foreign_key: true

      t.timestamps
    end
  end
end
