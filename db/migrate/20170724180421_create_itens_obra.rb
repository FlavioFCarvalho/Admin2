class CreateItensObra < ActiveRecord::Migration[5.0]
  def change
    create_table :itens_obra do |t|
      t.string :item
      t.string :tipo_servico

      t.timestamps
    end
  end
end
