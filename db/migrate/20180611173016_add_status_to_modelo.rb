class AddStatusToModelo < ActiveRecord::Migration[5.0]
  def change
    add_column :modelos, :status, :integer
  end
end
