class CreateReportConfigurations < ActiveRecord::Migration[5.0]
  def change
    create_table :report_configurations do |t|
      t.string :company
      t.string :cnpj
      t.string :phone
      t.string :address
      t.integer :state_id
      t.integer :city_id
      t.string :email
      t.string :neighborhood

      t.timestamps
    end
  end
end
