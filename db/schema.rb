# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20180612123615) do


  create_table "admins", id: :bigint, force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "email",                  default: "", null: false
    t.string   "encrypted_password",     default: "", null: false
    t.string   "password_salt",          default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.datetime "created_at",                          null: false
    t.datetime "updated_at",                          null: false
    t.string   "username"
    t.index ["email"], name: "index_admins_on_email", unique: true, using: :btree
    t.index ["reset_password_token"], name: "index_admins_on_reset_password_token", unique: true, using: :btree
  end

  create_table "authorization_users", id: :bigint, force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.bigint   "user_id"
    t.bigint   "authorization_id"
    t.datetime "created_at",                       null: false
    t.datetime "updated_at",                       null: false
    t.boolean  "_create",          default: false
    t.boolean  "_retrieve",        default: false
    t.boolean  "_update",          default: false
    t.boolean  "_delete",          default: false
    t.index ["authorization_id"], name: "index_authorization_users_on_authorization_id", using: :btree
    t.index ["user_id"], name: "index_authorization_users_on_user_id", using: :btree
  end

  create_table "authorizations", id: :bigint, force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "name"
    t.string   "description"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  create_table "average_caches", id: :bigint, force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=latin1" do |t|
    t.bigint   "rater_id"
    t.string   "rateable_type"
    t.bigint   "rateable_id"
    t.float    "avg",           limit: 24, null: false
    t.datetime "created_at",               null: false
    t.datetime "updated_at",               null: false
    t.index ["rateable_type", "rateable_id"], name: "index_average_caches_on_rateable_type_and_rateable_id", using: :btree
    t.index ["rater_id"], name: "index_average_caches_on_rater_id", using: :btree
  end

  create_table "bancos", id: :bigint, force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "nome"
    t.datetime "created_at",            null: false
    t.datetime "updated_at",            null: false
    t.bigint   "user_id"
    t.string   "status",     limit: 20
    t.index ["user_id"], name: "index_bancos_on_user_id", using: :btree
  end

  create_table "categorias", id: :bigint, force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=latin1" do |t|
    t.string   "nome"
    t.datetime "created_at",            null: false
    t.datetime "updated_at",            null: false
    t.string   "status",     limit: 20
    t.integer  "user_id"
  end

  create_table "cidades", id: :bigint, force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "nome"
    t.integer  "estado_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "cliente_fornecedores", id: :bigint, force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "status",                 limit: 20,    default: "0"
    t.string   "tipo_pessoa",            limit: 20
    t.string   "cliente_fornecedor",     limit: 50
    t.string   "nome"
    t.string   "razao_social"
    t.string   "cpf",                    limit: 20
    t.string   "cnpj",                   limit: 20
    t.string   "rg",                     limit: 20
    t.string   "sexo",                   limit: 20
    t.string   "endereco"
    t.string   "numero",                 limit: 10
    t.string   "complemento"
    t.string   "cep",                    limit: 20
    t.string   "bairro"
    t.bigint   "cidade_id"
    t.bigint   "estado_id"
    t.string   "contato1_nome"
    t.string   "contato1_ddd",           limit: 2
    t.string   "contato1_telefone"
    t.string   "contato1_tipo_telefone", limit: 20
    t.string   "contato1_email1",        limit: 80
    t.string   "contato1_email2",        limit: 80
    t.string   "contato2_nome"
    t.string   "contato2_ddd",           limit: 2
    t.string   "contato2_telefone"
    t.string   "contato2_tipo_telefone", limit: 20
    t.string   "contato2_email1",        limit: 80
    t.string   "contato2_email2",        limit: 80
    t.string   "contato3_nome"
    t.string   "contato3_ddd",           limit: 2
    t.string   "contato3_telefone"
    t.string   "contato3_tipo_telefone", limit: 20
    t.string   "contato3_email1",        limit: 80
    t.string   "contato3_email2",        limit: 80
    t.datetime "created_at"
    t.datetime "updated_at"
    t.bigint   "user_id"
    t.string   "inscricao_estadual"
    t.text     "observacao",             limit: 65535
    t.string   "id_nome"
    t.string   "datafaker"
    t.string   "data_cadastro"
    t.string   "tipo_cliente"
    t.string   "tipo_fornecedor"
    t.string   "tipo_investidor"
    t.integer  "dados",                                default: 0
    t.date     "DataNascimento"
    t.date     "data_nascimento"
    t.index ["cidade_id"], name: "index_cliente_fornecedores_on_cidade_id", using: :btree
    t.index ["estado_id"], name: "index_cliente_fornecedores_on_estado_id", using: :btree
    t.index ["user_id"], name: "index_cliente_fornecedores_on_user_id", using: :btree
  end

  create_table "cliente_obras", id: :bigint, force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=latin1" do |t|
    t.integer  "cliente_id"
    t.bigint   "obra_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["obra_id"], name: "index_cliente_obras_on_obra_id", using: :btree
  end

  create_table "comodo_obras", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=latin1" do |t|
    t.integer  "obra_id"
    t.integer  "comodo_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["comodo_id"], name: "index_comodo_obras_on_comodo_id", using: :btree
    t.index ["obra_id"], name: "index_comodo_obras_on_obra_id", using: :btree
  end

  create_table "comodos", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=latin1" do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer  "user_id"
  end

  create_table "contas", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=latin1" do |t|
    t.integer  "banco_id"
    t.string   "agencia"
    t.string   "numero"
    t.string   "tipo_conta"
    t.string   "operacao"
    t.string   "titular"
    t.string   "cpf_cnpj"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "contas_bancaria", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=latin1" do |t|
    t.integer  "banco_id"
    t.string   "agencia"
    t.string   "conta"
    t.string   "tipo"
    t.integer  "cliente_fornecedor_id"
    t.datetime "created_at",                           null: false
    t.datetime "updated_at",                           null: false
    t.boolean  "status",                default: true
  end

  create_table "estados", id: :bigint, force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "sigla"
    t.string   "nome"
    t.integer  "capital_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "estoque_estoque_movimento_parcelas", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=latin1" do |t|
    t.decimal  "valor",                precision: 14, scale: 2
    t.integer  "estoque_movimento_id"
    t.boolean  "bloqueado",                                     default: false
    t.datetime "created_at",                                                               null: false
    t.datetime "updated_at",                                                               null: false
    t.date     "data_vencimento"
    t.date     "data_pagamento"
    t.string   "periodo",                                       default: "NÃO MENCIONADO"
    t.string   "status_pagamento",                              default: "A PAGAR"
    t.integer  "parcela"
    t.index ["estoque_movimento_id"], name: "index_estoque_estoque_movimento_parcelas_on_estoque_movimento_id", using: :btree
  end

  create_table "estoque_estoque_movimentos", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=latin1" do |t|
    t.string   "tipo_movimentacao"
    t.integer  "fornecedor_id"
    t.integer  "user_id"
    t.string   "nota_fiscal_entrada"
    t.string   "recibo"
    t.string   "cupom_fiscal"
    t.string   "numero_pedido"
    t.string   "numero_da_os"
    t.string   "nota_fiscal_saida"
    t.date     "data_entrada"
    t.date     "data_vencimento_entrada"
    t.date     "data_saida"
    t.date     "data_vencimento_saida"
    t.date     "data_devolucao"
    t.text     "justificativa_devolucao", limit: 65535
    t.decimal  "total_estoque",                         precision: 14, scale: 2
    t.datetime "created_at",                                                                               null: false
    t.datetime "updated_at",                                                                               null: false
    t.integer  "obra_id"
    t.integer  "produto_id"
    t.date     "data_transferencia"
    t.string   "transferencia_tipo"
    t.integer  "parcelas",                                                       default: 1
    t.string   "periodo"
    t.string   "acrescimo"
    t.string   "desconto"
    t.decimal  "valor_total_produto",                   precision: 14, scale: 2
    t.string   "boleto"
    t.decimal  "frete",                                 precision: 14, scale: 2
    t.text     "observacoes",             limit: 65535
    t.boolean  "incompleto",                                                     default: true
    t.string   "valida_por",                                                     default: "total_estoque"
    t.integer  "destinatario_tomador_id"
    t.string   "pronto_pagamento"
  end

  create_table "estoque_estoque_parcelamentos", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=latin1" do |t|
    t.integer  "estoque_movimento_id",                                                  null: false
    t.integer  "estoque_produto_id"
    t.integer  "estoque_transferencia_id"
    t.decimal  "valor",                    precision: 14, scale: 2
    t.datetime "created_at",                                                            null: false
    t.datetime "updated_at",                                                            null: false
    t.boolean  "bloqueado",                                         default: false
    t.date     "data_vencimento"
    t.string   "status_pagamento",                                  default: "A PAGAR"
    t.string   "periodo"
    t.integer  "parcela"
  end

  create_table "estoque_estoque_produtos", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=latin1" do |t|
    t.integer  "produto_id"
    t.integer  "estoque_movimento_id"
    t.integer  "obra_id"
    t.integer  "quantidade_movimento",                                        default: 0
    t.decimal  "valor_produto",                      precision: 14, scale: 2
    t.decimal  "valor_total",                        precision: 14, scale: 2
    t.datetime "created_at",                                                              null: false
    t.datetime "updated_at",                                                              null: false
    t.integer  "item_id"
    t.integer  "item_obra_id"
    t.integer  "sub_item_obra_id"
    t.integer  "parcelas"
    t.integer  "comodo_id"
    t.text     "observacoes",          limit: 65535
    t.integer  "quantidade_anterior",                                         default: 0
    t.integer  "quantidade_atual",                                            default: 0
    t.index ["item_id"], name: "index_estoque_estoque_produtos_on_item_id", using: :btree
  end

  create_table "estoque_estoque_transferencias", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=latin1" do |t|
    t.integer  "estoque_movimento_id"
    t.integer  "produto_id"
    t.integer  "obra_anterior_id"
    t.integer  "obra_posterior_id"
    t.integer  "item_obra_id"
    t.integer  "sub_item_obra_id"
    t.integer  "quantidade_movimento"
    t.decimal  "valor_produto",        precision: 14, scale: 2
    t.decimal  "valor_total",          precision: 14, scale: 2
    t.integer  "item_id"
    t.datetime "created_at",                                                null: false
    t.datetime "updated_at",                                                null: false
    t.integer  "parcelas"
    t.integer  "comodo_id"
    t.integer  "quantidade_anterior",                           default: 0
    t.integer  "quantidade_atual",                              default: 0
    t.index ["item_id"], name: "index_estoque_estoque_transferencias_on_item_id", using: :btree
  end

  create_table "fornecedor_produtos", id: :bigint, force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=latin1" do |t|
    t.bigint   "produto_id"
    t.integer  "fornecedor_id"
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
    t.index ["produto_id"], name: "index_fornecedor_produtos_on_produto_id", using: :btree
  end

  create_table "fornecedor_servicos", id: :bigint, force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=latin1" do |t|
    t.integer  "fornecedor_id"
    t.bigint   "service_id"
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
    t.index ["service_id"], name: "index_fornecedor_servicos_on_service_id", using: :btree
  end

  create_table "itens", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=latin1" do |t|
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "itens_obra", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=latin1" do |t|
    t.string   "item"
    t.string   "tipo_servico"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
  end

  create_table "modelos", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=latin1" do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer  "status"
    t.integer  "user_id"
  end

  create_table "obras", id: :bigint, force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "nome_local"
    t.string   "codigo"
    t.datetime "created_at",     null: false
    t.datetime "updated_at",     null: false
    t.bigint   "user_id"
    t.string   "endereco_local"
    t.string   "bairro"
    t.string   "cep"
    t.bigint   "cidade_id"
    t.bigint   "estado_id"
    t.string   "endereco_obra"
    t.string   "quadra"
    t.string   "lote"
    t.string   "cei"
    t.string   "peticao"
    t.string   "aprovacao"
    t.string   "status"
    t.index ["cidade_id"], name: "index_obras_on_cidade_id", using: :btree
    t.index ["estado_id"], name: "index_obras_on_estado_id", using: :btree
    t.index ["user_id"], name: "index_obras_on_user_id", using: :btree
  end

  create_table "overall_averages", id: :bigint, force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=latin1" do |t|
    t.string   "rateable_type"
    t.bigint   "rateable_id"
    t.float    "overall_avg",   limit: 24, null: false
    t.datetime "created_at",               null: false
    t.datetime "updated_at",               null: false
    t.index ["rateable_type", "rateable_id"], name: "index_overall_averages_on_rateable_type_and_rateable_id", using: :btree
  end

  create_table "produto_categorias", id: :bigint, force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=latin1" do |t|
    t.bigint   "produto_id"
    t.bigint   "categoria_id"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
    t.index ["categoria_id"], name: "index_produto_categorias_on_categoria_id", using: :btree
    t.index ["produto_id"], name: "index_produto_categorias_on_produto_id", using: :btree
  end

  create_table "produtos", id: :bigint, force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.integer  "status",                                             default: 0
    t.string   "nome"
    t.string   "unidade"
    t.text     "observacoes", limit: 65535
    t.datetime "created_at",                                                     null: false
    t.datetime "updated_at",                                                     null: false
    t.bigint   "user_id"
    t.decimal  "valor",                     precision: 14, scale: 2
    t.integer  "quantidade",                                         default: 0
    t.index ["user_id"], name: "index_produtos_on_user_id", using: :btree
  end

  create_table "rates", id: :bigint, force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=latin1" do |t|
    t.bigint   "rater_id"
    t.string   "rateable_type"
    t.bigint   "rateable_id"
    t.float    "stars",         limit: 24, null: false
    t.string   "dimension"
    t.datetime "created_at",               null: false
    t.datetime "updated_at",               null: false
    t.index ["rateable_id", "rateable_type"], name: "index_rates_on_rateable_id_and_rateable_type", using: :btree
    t.index ["rateable_type", "rateable_id"], name: "index_rates_on_rateable_type_and_rateable_id", using: :btree
    t.index ["rater_id"], name: "index_rates_on_rater_id", using: :btree
  end

  create_table "rating_caches", id: :bigint, force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=latin1" do |t|
    t.string   "cacheable_type"
    t.bigint   "cacheable_id"
    t.float    "avg",            limit: 24, null: false
    t.integer  "qty",                       null: false
    t.string   "dimension"
    t.datetime "created_at",                null: false
    t.datetime "updated_at",                null: false
    t.index ["cacheable_id", "cacheable_type"], name: "index_rating_caches_on_cacheable_id_and_cacheable_type", using: :btree
    t.index ["cacheable_type", "cacheable_id"], name: "index_rating_caches_on_cacheable_type_and_cacheable_id", using: :btree
  end

  create_table "report_configurations", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=latin1" do |t|
    t.string   "company"
    t.string   "cnpj"
    t.string   "phone"
    t.string   "address"
    t.integer  "state_id"
    t.integer  "city_id"
    t.string   "email"
    t.string   "neighborhood"
    t.datetime "created_at",           null: false
    t.datetime "updated_at",           null: false
    t.string   "picture_file_name"
    t.string   "picture_content_type"
    t.integer  "picture_file_size"
    t.datetime "picture_updated_at"
  end

  create_table "roles", id: :bigint, force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "name"
    t.string   "resource_type"
    t.bigint   "resource_id"
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
    t.index ["name", "resource_type", "resource_id"], name: "index_roles_on_name_and_resource_type_and_resource_id", using: :btree
    t.index ["name"], name: "index_roles_on_name", using: :btree
    t.index ["resource_type", "resource_id"], name: "index_roles_on_resource_type_and_resource_id", using: :btree
  end

  create_table "services", id: :bigint, force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=latin1" do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.bigint   "user_id"
    t.integer  "status"
    t.index ["user_id"], name: "index_services_on_user_id", using: :btree
  end

  create_table "sub_itens_obra", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=latin1" do |t|
    t.string   "sub_item"
    t.integer  "item_obra_id"
    t.string   "sub_tipo_servico"
    t.string   "unidade"
    t.datetime "created_at",       null: false
    t.datetime "updated_at",       null: false
  end

  create_table "tipo_contratos", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=latin1" do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer  "status"
    t.integer  "user_id"
  end

  create_table "tipos", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=latin1" do |t|
    t.string   "name"
    t.integer  "status"
    t.integer  "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "user_profiles", id: :bigint, force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "name"
    t.bigint   "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["user_id"], name: "index_user_profiles_on_user_id", using: :btree
  end

  create_table "users", id: :bigint, force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "username",               default: "", null: false
    t.string   "email",                  default: "", null: false
    t.string   "encrypted_password",     default: "", null: false
    t.string   "password_salt",          default: "", null: false
    t.integer  "role",                   default: 0,  null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.integer  "failed_attempts",        default: 0,  null: false
    t.string   "unlock_token"
    t.datetime "locked_at"
    t.datetime "created_at",                          null: false
    t.datetime "updated_at",                          null: false
    t.index ["email"], name: "index_users_on_email", unique: true, using: :btree
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree
    t.index ["unlock_token"], name: "index_users_on_unlock_token", unique: true, using: :btree
  end

  create_table "users_roles", id: false, force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.bigint "user_id"
    t.bigint "role_id"
    t.index ["role_id"], name: "index_users_roles_on_role_id", using: :btree
    t.index ["user_id", "role_id"], name: "index_users_roles_on_user_id_and_role_id", using: :btree
    t.index ["user_id"], name: "index_users_roles_on_user_id", using: :btree
  end

  add_foreign_key "authorization_users", "authorizations"
  add_foreign_key "authorization_users", "users"
  add_foreign_key "bancos", "users"
  add_foreign_key "cliente_fornecedores", "cidades"
  add_foreign_key "cliente_fornecedores", "estados"
  add_foreign_key "cliente_fornecedores", "users"
  add_foreign_key "cliente_obras", "obras"
  add_foreign_key "estoque_estoque_produtos", "itens"
  add_foreign_key "estoque_estoque_transferencias", "itens"
  add_foreign_key "fornecedor_produtos", "produtos"
  add_foreign_key "fornecedor_servicos", "services"
  add_foreign_key "obras", "cidades"
  add_foreign_key "obras", "estados"
  add_foreign_key "obras", "users"
  add_foreign_key "produto_categorias", "categorias"
  add_foreign_key "produto_categorias", "produtos"
  add_foreign_key "produtos", "users"
  add_foreign_key "services", "users"
  add_foreign_key "user_profiles", "users"
end
