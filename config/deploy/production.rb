set :port, 2552
set :user, 'brant'
set :deploy_via, :remote_cache
set :use_sudo, false

server 'brantgabrysistemas.com.br',
  roles: [:web, :app, :db],
  port: fetch(:port),
  user: fetch(:user),
  primary: true

set :ssh_options, {
  forward_agent: true,
  auth_methods: %w(publickey),
  user: fetch(:user),
}
