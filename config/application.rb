require_relative 'boot'

require 'rails/all'

# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(*Rails.groups)

# require 'sprockets/processing'
# extend Sprockets::Processing
# register_preprocessor 'text/css', SprocketsExtension

module Brant
  class Application < Rails::Application
    # Use the responders controller from the responders gem
    config.app_generators.scaffold_controller :responders_controller

    # Initialize configuration defaults for originally generated Rails version.
    # config.load_defaults 5.1

    # Settings in config/environments/* take precedence over those specified here.
    # Application configuration should go into files in config/initializers
    # -- all .rb files in that directory are automatically loaded.
    config.encoding ="UTF-8"
    config.generators do |g|
      g.orm             :active_record
      g.assets          false
      g.template_engine :haml
      g.stylesheets     false
      g.helper          false
      g.test_framework  :test_unit, fixture: false
    end

    config.time_zone = 'Brasilia'
    config.i18n.default_locale = :"pt-BR"
    config.active_record.schema_format = :ruby
    config.autoload_paths += %W( #{config.root}/lib/ )
    config.active_record.time_zone_aware_types = [:datetime]

    config.eager_load = true
    config.eager_load_namespaces = true

    console do
      # this block is called only when running console,
      # so we can safely require pry here
      require "pry"
      config.console = Pry
    end
  end
end
