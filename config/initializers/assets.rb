# Be sure to restart your server when you modify this file.

# Version of your assets, change this if you want to expire all your assets.
Rails.application.config.assets.version = '1.0'

# Add additional assets to the asset load path.
# Rails.application.config.assets.paths << Emoji.images_path
# Add Yarn node_modules folder to the asset load path.
Rails.application.config.assets.paths << Rails.root.join('node_modules')

# Precompile additional assets.
# application.js, application.css, and all non-JS/CSS in the app/assets
# folder are already added.
# Rails.application.config.assets.precompile += %w( admin.js admin.css )

Rails.application.config.assets.precompile += %w( backstage.coffee backstage.sass )
Rails.application.config.assets.precompile += %w( user_session.coffee user_session.sass )
Rails.application.config.assets.precompile += %w( _custom.scss )

# => Files on Global folder Precompile
Rails.application.config.assets.precompile += %w( global/user.coffee )
Rails.application.config.assets.precompile += %w( global/user_permissions.coffee )

# => Clientes & Fornecedores Precompile
Rails.application.config.assets.precompile += %w( brant/clientes_&_fornecedores/cliente_fornecedores_cidades.coffee )
Rails.application.config.assets.precompile += %w( brant/clientes_&_fornecedores/cliente_fornecedores_tipo_pessoa.coffee )
Rails.application.config.assets.precompile += %w( brant/clientes_&_fornecedores/create.coffee )
Rails.application.config.assets.precompile += %w( brant/clientes_&_fornecedores/update.coffee )
Rails.application.config.assets.precompile += %w( brant/clientes_&_fornecedores/fornecedor_services.coffee )
Rails.application.config.assets.precompile += %w( brant/clientes_&_fornecedores/fornecedor_produtos.coffee )
Rails.application.config.assets.precompile += %w( brant/clientes_&_fornecedores/cliente_obras.coffee )
Rails.application.config.assets.precompile += %w( brant/clientes_&_fornecedores/bank_account.coffee )
Rails.application.config.assets.precompile += %w( brant/itens_obra/form.coffee )
Rails.application.config.assets.precompile += %w( brant/obras/obras_cidades.coffee )
Rails.application.config.assets.precompile += %w( brant/obras/obra_comodo.coffee )
Rails.application.config.assets.precompile += %w( brant/obras/comodo.coffee )

Rails.application.config.assets.precompile += %w( brant/relobp/form.coffee )

# => Estados Cidades de Configuracao Relatorio
Rails.application.config.assets.precompile += %w( brant/report_configuration/state_city.coffee )

# => Estoques
Rails.application.config.assets.precompile += %w( brant/estoques/tipo_movimento.coffee )
Rails.application.config.assets.precompile += %w( brant/estoques/link_alter_payday_date.coffee )
Rails.application.config.assets.precompile += %w( brant/estoques/new_form.coffee )
Rails.application.config.assets.precompile += %w( brant/estoques/edit_form.coffee )
Rails.application.config.assets.precompile += %w( brant/estoques/produto.coffee )
Rails.application.config.assets.precompile += %w( brant/estoques/filtro.coffee )
Rails.application.config.assets.precompile += %w( brant/estoques/produto_etapas_da_obra.coffee )
Rails.application.config.assets.precompile += %w( brant/estoques/transferencia_etapas_da_obra.coffee )
Rails.application.config.assets.precompile += %w( brant/estoques/parcelamento.coffee )
Rails.application.config.assets.precompile += %w( brant/estoques/estoque_produto.coffee )
Rails.application.config.assets.precompile += %w( brant/estoques/estoque_transferencia.coffee )
Rails.application.config.assets.precompile += %w( brant/estoques/obra_comodo_group_collection.coffee )
Rails.application.config.assets.precompile += %w( brant/estoques/obra_anterior_comodo_transferencia_collection.coffee )
Rails.application.config.assets.precompile += %w( brant/estoques/obra_posterior_comodo_transferencia_collection.coffee )
