# Be sure to restart your server when you modify this file.

# Add new inflection rules using the following format. Inflections
# are locale specific, and you may define rules for as many different
# locales as you wish. All of these examples are active by default:
# ActiveSupport::Inflector.inflections(:en) do |inflect|
#   inflect.plural /^(ox)$/i, '\1en'
#   inflect.singular /^(ox)en/i, '\1'
#   inflect.irregular 'person', 'people'
#   inflect.uncountable %w( fish sheep )
# end

# These inflection rules are supported but not enabled by default:
# ActiveSupport::Inflector.inflections(:en) do |inflect|
#   inflect.acronym 'RESTful'
# end
ActiveSupport::Inflector.inflections(:en) do |inflect|
  inflect.irregular 'fornecedor', 'fornecedores'
  inflect.irregular 'categoria', 'categorias'
  inflect.irregular 'item_obra', 'itens_obra'
  inflect.irregular 'sub_item_obra', 'sub_itens_obra'
  inflect.irregular 'conta', 'contas'
  inflect.irregular 'item', 'itens'
  inflect.irregular 'estoque_movimento', 'estoque_movimentos'
  inflect.irregular 'estoque_transferencia', 'estoque_transferencias'
end
