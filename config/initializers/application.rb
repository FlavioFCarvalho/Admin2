Rails.application do |config|
  config.encoding ="UTF-8"
  config.generators do |g|
    g.orm             :active_record
    g.assets          false
    g.template_engine :haml
    g.stylesheets     false
    g.helper          false
    g.test_framework  :test_unit, fixture: false
  end

  config.time_zone = 'Brasilia'
  config.i18n.default_locale = :"pt-BR"
  config.active_record.schema_format = :ruby
  config.autoload_paths += %W( #{config.root}/lib/ )
  config.assets.precompile << /\.(?:svg|eot|woff|ttf)\z/
  config.assets.paths << Rails.root.join("app", "assets", "fonts")

  config.eager_load = true
  config.eager_load_namespaces = true

  console do
    # this block is called only when running console,
    # so we can safely require pry here
    require "pry"
    config.console = Pry
  end
end
