Rails.application.routes.draw do

  resources :modelos
  resources :tipos
  resources :relobps, only: [:index]

  resources :tipo_contratos
  resources :comodos
  resources :contas
  get 'contas_bancaria/update'

  resources :itens
  namespace :estoques do
    put 'data_vencimento/:id', to: "estoque_data_vencimento#update", as: :alter_data_vencimento
    put 'parcelamento/:id', to: "estoque_parcelamentos#update",as: :estoque_estoque_parcelamento
    put 'estoque-movimento/parcelamento/:id', to: "estoque_movimento_parcelas#update",as: :estoque_estoque_movimento_parcela
    get '/parcelas/resetar', to: "estoque_movimento_parcelas#reset",as: :reset_estoque_movimento_parcela
    post 'parcelamentos', to: "estoque_parcelamentos#find_and_create_portions",as: :estoque_estoque_parcelamento_find_and_create_portions
    get '/:estoque_movimentaco_id/desbloquear-valor', to: "estoque_movimento_parcelas#unlock_item", as: :unlock_estoque_movimento_parcela
    
    resources :estoque_movimentacoes do
      get 'parcelamentos', to: "estoque_parcelamentos#index"
      get 'buscar_parcelamentos', to: "estoque_parcelamentos#router", as: :router_parcelamento
      get 'editar_parcelamentos', to: "estoque_parcelamentos#editar_parcelamentos", as: :edit_parcelamentos
      get 'resetar-parcelas', to: "estoque_parcelamentos#reset_quotations", as: :reset_quotations
      post 'parcelas/reiniciar-valores', to: "estoque_movimento_parcelas#reload",as: :reload_estoque_movimento_parcela

      member do
        get 'lock_and_unlock_page'
        get 'unlock_through_validate_by'
      end

      resources :estoque_parcelamentos, except: [:index, :new, :create, :update] do
        member do
          put 'atualizar_parcelas'
          get 'unlock_value'
        end
      end
      resources :estoque_transferencias, except: [:show, :edit]
      resources :estoque_produtos, except: [:show, :edit]
    end
  end

  resources :itens_obra do
    member do
      get 'report_individual'
    end
  end
  resources :bancos
  resources :categorias
  resources :cliente_fornecedores, except: [:destroy]

  resources :obras do
    member do
      delete 'destroy_comodo'
    end
  end
  resources :produto_categorias
  resources :produtos
  resources :services

  # get 'itens_obras/', to: "itens_obra#index", as: :itens_obras
  # post 'sub_itens_obras/search', to: "itens_obra#search", as: :search_sub_itens_obras
  # post 'itens_obras/search', to: "itens_obra#search_itens", as: :search_itens_obras

  devise_for :users,
              patch: 'brant/users',
              controllers: { sessions: "brant/users/sessions" },
              path_names:  { sign_in: "signin", sign_out: "signout" },
              skip: [:registrations]
  as :user do
    get 'empresa/brant/users/sign-up', to: 'backstage/users/registrations#new', as: :new_user_registration
    get 'empresa/brant/users/edit/:id', to: 'backstage/users/registrations#edit', as: :edit_user_registration
    post 'empresa/brant/users/sign-up', to: 'backstage/users/registrations#create', as: :create_user_registration
    put 'empresa/brant/users/edit/:id/update', to: 'backstage/users/registrations#update', as: :update_user_registration
    delete 'empresa/brant/users/delete/:id', to: 'backstage/users/registrations#destroy', as: :destroy_user_registration
  end

  devise_for :admins,
              path: 'brant/admins',
              controllers: { sessions: "backstage/admin/sessions" },
              path_names:  { sign_in: "signin", sign_out: "signout" }


  namespace :backstage do
    resources :authorizations
    resources :dashboard, only: [:index]
    get 'user/:id/authorizations/', to: "users/permissions#index", as: :user_permissions
    post'user/:id/authorizations/', to: "users/permissions#update", as: :add_user_permissions
    get 'empresa/brant', to: "dashboard#brant"
    get 'empresa/brant/models', to: "dashboard#models"
    get 'empresa/brant/admins', to: "dashboard#admins"
    get 'empresa/brant/users', to: "dashboard#users"
  end

  authenticated :admin do
    root 'backstage/dashboard#index', as: :dashboard_root
  end

  devise_scope :user do
    unauthenticated do
      root "brant/home#index"
    end
  end

  namespace :brant do
    get 'home/index', as: :index_root
    namespace :users do
      resources :accounts
      resources :user_authorizations
    end
    resources :report_configurations

    get 'itens_obra/report_all', to: "reports#report_all_etapas", as: :report_all_item_obra
    get 'print_individual/cliente_fornecedores', to: "reports#print_individual_cliente_fornecedor", as: :print_individual_cliente_fornecedor
    get 'print_page/cliente_fornecedores', to: "reports#print_page_cliente_fornecedor", as: :print_page_cliente_fornecedor
  end

  get 'resquests/create_banco_through_ajax', to: "brant/requests#create_banco_through_ajax"
  get 'resquests/create_comodo_through_ajax', to: "brant/requests#create_comodo_through_ajax"
  get 'resquests/create_categoria_through_ajax', to: "brant/requests#create_categoria_through_ajax"
  get 'resquests/create_service_through_ajax', to: "brant/requests#create_service_through_ajax"
  get 'resquests/create_product_through_ajax', to: "brant/requests#create_product_through_ajax"
  get 'resquests/create_work_through_ajax', to: "brant/requests#create_work_through_ajax"
  get 'resquests/find_resource',                to: "brant/requests#find_resource"
  get 'resquests/collect_fornecedor_services', to: "brant/requests#collect_fornecedor_services"
  get 'resquests/collect_fornecedor_products', to: "brant/requests#collect_fornecedor_products"
  get 'resquests/collect_cliente_obras', to: "brant/requests#collect_cliente_obras"
  get 'resquests/find_client_obra_and_destroy', to: "brant/requests#destroy_cliente_obras"
  get 'resquests/find_fornecedor_service_and_destroy', to: "brant/requests#destroy_fornecedor_services"
  get 'resquests/find_fornecedor_product_and_destroy', to: "brant/requests#destroy_fornecedor_products"
  get 'resquests/collect_contas_bancarias', to: "brant/requests#collect_contas_bancarias"


  root "brant/home#index"
end