require 'test_helper'

class TipoContratosControllerTest < ActionDispatch::IntegrationTest
  setup do
    @tipo_contrato = tipo_contratos(:one)
  end

  test "should get index" do
    get tipo_contratos_url
    assert_response :success
  end

  test "should get new" do
    get new_tipo_contrato_url
    assert_response :success
  end

  test "should create tipo_contrato" do
    assert_difference('TipoContrato.count') do
      post tipo_contratos_url, params: { tipo_contrato: { name: @tipo_contrato.name } }
    end

    assert_redirected_to tipo_contrato_url(TipoContrato.last)
  end

  test "should show tipo_contrato" do
    get tipo_contrato_url(@tipo_contrato)
    assert_response :success
  end

  test "should get edit" do
    get edit_tipo_contrato_url(@tipo_contrato)
    assert_response :success
  end

  test "should update tipo_contrato" do
    patch tipo_contrato_url(@tipo_contrato), params: { tipo_contrato: { name: @tipo_contrato.name } }
    assert_redirected_to tipo_contrato_url(@tipo_contrato)
  end

  test "should destroy tipo_contrato" do
    assert_difference('TipoContrato.count', -1) do
      delete tipo_contrato_url(@tipo_contrato)
    end

    assert_redirected_to tipo_contratos_url
  end
end
