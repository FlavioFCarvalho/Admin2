require 'test_helper'

class ComodosControllerTest < ActionDispatch::IntegrationTest
  setup do
    @comodo = comodos(:one)
  end

  test "should get index" do
    get comodos_url
    assert_response :success
  end

  test "should get new" do
    get new_comodo_url
    assert_response :success
  end

  test "should create comodo" do
    assert_difference('Comodo.count') do
      post comodos_url, params: { comodo: { name: @comodo.name } }
    end

    assert_redirected_to comodo_url(Comodo.last)
  end

  test "should show comodo" do
    get comodo_url(@comodo)
    assert_response :success
  end

  test "should get edit" do
    get edit_comodo_url(@comodo)
    assert_response :success
  end

  test "should update comodo" do
    patch comodo_url(@comodo), params: { comodo: { name: @comodo.name } }
    assert_redirected_to comodo_url(@comodo)
  end

  test "should destroy comodo" do
    assert_difference('Comodo.count', -1) do
      delete comodo_url(@comodo)
    end

    assert_redirected_to comodos_url
  end
end
