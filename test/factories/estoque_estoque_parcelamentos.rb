# == Schema Information
#
# Table name: estoque_estoque_parcelamentos
#
#  id                       :integer          not null, primary key
#  estoque_movimento_id     :integer          not null
#  estoque_produto_id       :integer
#  estoque_transferencia_id :integer
#  valor                    :decimal(14, 2)
#  created_at               :datetime         not null
#  updated_at               :datetime         not null
#  bloqueado                :boolean          default("0")
#

FactoryGirl.define do
  factory :estoque_estoque_parcelamento, class: 'Estoque::EstoqueParcelamento' do
    estoque_movimento_id 1
    estoque_produto_id 1
    estoque_transferencia_id 1
    valor_parcela "9.99"
  end
end
