# == Schema Information
#
# Table name: estoque_estoque_transferencias
#
#  id                   :integer          not null, primary key
#  estoque_movimento_id :integer
#  produto_id           :integer
#  obra_anterior_id     :integer
#  obra_posterior_id    :integer
#  item_obra_id         :integer
#  sub_item_obra_id     :integer
#  quantidade           :integer
#  valor_produto        :decimal(10, )
#  valor_total          :decimal(10, )
#  item_id              :integer
#  created_at           :datetime         not null
#  updated_at           :datetime         not null
#  parcelas             :integer
#

FactoryGirl.define do
  factory :estoque_estoque_transferencia, class: 'Estoque::EstoqueTransferencia' do
    estoque_movimento_id 1
    produto_id 1
    item_obra_id 1
    sub_item_obra_id 1
    quantidade 1
    valor_produto "9.99"
    valor_total "9.99"
    item nil
  end
end
