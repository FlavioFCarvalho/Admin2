# == Schema Information
#
# Table name: estoque_estoque_movimento_parcelas
#
#  id                   :integer          not null, primary key
#  valor                :decimal(14, 2)
#  estoque_movimento_id :integer
#  bloqueado            :boolean          default("0")
#  created_at           :datetime         not null
#  updated_at           :datetime         not null
#  data_vencimento      :date
#  data_pagamento       :date
#  periodo              :string(255)      default("NÃO MENCIONADO")
#  status_pagamento     :string(255)      default("A PAGAR")
#

FactoryGirl.define do
  factory :estoque_estoque_movimento_parcela, class: 'Estoque::EstoqueMovimentoParcela' do
    valor "9.99"
  end
end
