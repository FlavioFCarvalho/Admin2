if @estoque_movimento_parcela
  json.notify_message t('flash.actions.update.success_fem', resource_name: "Data de Vencimento")
  json.url edit_estoques_estoque_movimentaco_path(@estoque_movimento_parcela.estoque_movimento)
end