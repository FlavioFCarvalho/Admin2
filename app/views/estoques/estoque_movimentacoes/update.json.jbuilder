require 'json'
unless @estoque.valid?
  json.set! :valid, @estoque.valid?
  @error_messages=[]
  @messages = JSON.parse(@estoque.errors.messages.to_json)
  @messages.map do |_key, value|
    @error_messages << "#{value.to_s.gsub("[", "").gsub("]", "").gsub('"', "")}"
  end.join
  json.set! :messages, @error_messages
else
  json.set! :valid, @estoque.valid?
  json.set! :notify_message, t('flash.actions.update.success', resource_name:  "Estoque")

  # if @estoque.tipo_movimentacao.eql?("Transferência")
  #   json.url new_estoques_estoque_movimentaco_estoque_transferencia_url(@estoque)
  # else
  #   json.url new_estoques_estoque_movimentaco_estoque_produto_url(@estoque)
  # end

  json.url edit_estoques_estoque_movimentaco_url(@estoque)
end
