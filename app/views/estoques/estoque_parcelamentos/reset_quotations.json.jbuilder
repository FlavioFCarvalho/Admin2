if !@estoque_parcelamentos.nil?
  json.set! :notify_message, t('flash.actions.update.success', resource_name:  "Parcelas")
  if @estoque_parcelamentos.last.estoque_produto.present?
    json.url estoques_estoque_movimentaco_parcelamentos_path(@estoque_parcelamentos.last.estoque_produto.estoque_movimento_id, estoque_produto_id: @estoque_parcelamentos.last.estoque_produto_id)
  end
  if @estoque_parcelamentos.last.estoque_transferencia.present?
    json.url estoques_estoque_movimentaco_parcelamentos_path(@estoque_parcelamentos.last.estoque_transferencia.estoque_movimento_id, estoque_transferencia_id: @estoque_parcelamentos.last.estoque_transferencia_id)
  end
end