require 'json'
unless @obra.valid?
  json.set! :valid, @obra.valid?
  @error_messages=[]
  @messages = JSON.parse(@obra.errors.messages.to_json)
  @messages.map do |_key, value|
    @error_messages << "#{value.to_s.gsub("[", "").gsub("]", "").gsub('"', "")}"
  end.join
  json.set! :messages, @error_messages
else
  json.set! :valid, @obra.valid?
  json.set! :notify_message, t('flash.actions.update.success', resource_name:  "Obra")
  json.url edit_obra_url(@obra)
end
