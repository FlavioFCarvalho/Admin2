if @deleted
  json.set! :deleted, true
  json.set! :id, @before_destroy_id
  json.set! :message, t('flash.actions.destroy.success', resource_name: "Produto")
else
  json.set! :deleted, false
  json.set! :message, t('flash.actions.destroy.alert', resource_name: "Produto")
end
