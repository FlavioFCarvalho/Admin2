unless @produto.valid?
  json.set! :valid, false
  json.set! :message, t('flash.actions.create.alert', resource_name: @produto.nome)
else
  json.set! :valid, true
  json.set! :message, t('flash.actions.create.success', resource_name: @produto.nome)
  json.set! :value, @produto.id
  json.set! :name, @produto.nome
end
