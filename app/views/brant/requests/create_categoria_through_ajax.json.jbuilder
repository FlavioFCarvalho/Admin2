unless @categoria.valid?
  json.set! :valid, false
  json.set! :message, t('flash.actions.create.alert', resource_name: "A Categoria #{@categoria.nome}")
else
  json.set! :valid, true
  json.set! :message, t('flash.actions.create.success', resource_name: "A Categoria #{@categoria.nome}")
  json.set! :value, @categoria.id
  json.set! :name, @categoria.nome
end
