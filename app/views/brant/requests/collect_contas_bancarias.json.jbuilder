json.array! @cliente_fornecedor_contas_bancaria do |account|
  json.id     account.id
  json.cliente_fornecedor_id account.cliente_fornecedor_id
  json.banco_nome   account.banco.nome
  json.banco_id   account.banco_id
  json.agencia   account.agencia
  json.conta   account.conta
  json.tipo   account.tipo
end
