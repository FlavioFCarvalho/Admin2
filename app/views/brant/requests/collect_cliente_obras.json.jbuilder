json.array! @cliente_fornecedor_obras do |cliobra|
  json.id     cliobra.id
  json.cliente_id cliobra.cliente_id
  json.name   cliobra.obra.name
  json.status cliobra.obra.status
  json.codigo cliobra.obra.codigo
end
