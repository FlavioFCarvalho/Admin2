unless @obra.valid?
  json.set! :valid, false
  json.set! :message, t('flash.actions.create.alert', resource_name: @obra.nome_local)
else
  json.set! :valid, true
  json.set! :message, t('flash.actions.create.success', resource_name: @obra.nome_local)
  json.set! :value, @obra.id
  json.set! :name, @obra.nome_local
  json.set! :codigo, @obra.codigo
end
