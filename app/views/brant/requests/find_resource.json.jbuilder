json.set! :id, @resource.id
json.set! :class_name, @resource.class.name.downcase.pluralize

if @resource.has_attribute?(:name)
  json.set! :name, @resource.name
end

if @resource.has_attribute?(:status)
  json.set! :status do
    if @resource.status.eql?("active") || @resource.status.eql?("inactive")
      json.set! :label, translate_i18n_status(@resource.status)
    else
      json.set! :label, @resource.status
    end
  end
end

if @resource.class.name.eql?("Obra")
  json.set! :codigo, @resource.codigo 
  json.set! :nome_local, @resource.nome_local
end

if @resource.class.name.eql?("Comodo")
  json.set! :id, @resource.id
  json.set! :name, @resource.name 
end

if @resource.class.name.eql?("Produto")
  json.set! :value, number_to_currency(@resource.valor).gsub("R$", '').delete(' ')
elsif @resource.class.name.eql?("ContasBancaria")
  json.set! :agency, @resource.agencia
  json.set! :account, @resource.conta
  json.set! :account_type, @resource.tipo
  json.set! :bank_id, @resource.banco_id
  json.set! :bank_name, @resource.banco_nome
elsif @resource.class.name.eql?("ClienteFornecedor")
  json.set! :nome,        @resource.nome
  json.set! :tipo_pessoa, @resource.tipo_pessoa

  if @resource.fisic_person?
    json.set! :rg,          @resource.rg
    json.set! :cpf,         @resource.cpf
    if ((@resource.rg.eql?("-") || @resource.rg.nil?) || (@resource.cpf.eql?("-") || @resource.cpf.nil?))
      json.set! :uncompleted_fields, true
      json.set! :errors_found, true
    end
  end

  if @resource.juridic_person?
    json.set! :cnpj,        @resource.cnpj
    json.set! :razao_social,@resource.razao_social
    json.set! :inscricao_estadual,@resource.inscricao_estadual
    if (@resource.cnpj.eql?("-") || @resource.cnpj.eql?("") || @resource.cnpj.nil?) ||
      (@resource.razao_social.eql?("-") || @resource.razao_social.eql?("") || @resource.razao_social.nil?) ||
      (@resource.inscricao_estadual.eql?("-") || @resource.inscricao_estadual.eql?("") || @resource.inscricao_estadual.nil?)
      json.set! :uncompleted_fields, true
      json.set! :errors_found, true
    end
  end

  json.set! :cliente do
    if @resource.tipo_cliente.nil?
      json.set! :tipo_cliente_founded, false
      json.set! :uncompleted_fields, true
      json.set! :errors_found, true
      json.set! :tipo_cliente, ""
    else
      if @resource.tipo_cliente.eql?("Não")
        json.set! :tipo_cliente_founded, true
        json.set! :tipo_cliente, "Não"
        json.set! :errors_found, false
      end
      if @resource.tipo_cliente.eql?("Sim")
        json.set! :tipo_cliente_founded, true
        json.set! :tipo_cliente, "Sim"
        if @resource.cliente_works.last.nil?
          json.set! :work_tab, true
          json.set! :errors_found, true
          json.set! :uncompleted_tab, true
        else
          json.set! :errors_found, false
          json.set! :work_tab, true
          json.set! :cliente_work_founded, true
          json.set! :uncompleted_tab, false
        end
      end
    end
  end

  json.set! :fornecedor do
    if @resource.tipo_fornecedor.nil?
      json.set! :tipo_fornecedor_founded, false
      json.set! :uncompleted_fields, true
      json.set! :errors_found, true
    else
      if @resource.tipo_fornecedor.eql?("Sim")
        json.set! :tipo_fornecedor_founded, true
        json.set! :tipo_fornecedor, "Sim"
        if (@resource.fornecedor_services.last.nil? || @resource.fornecedor_products.last.nil?)
          json.set! :errors_found,    true
          json.set! :service_tab,     true
          json.set! :product_tab,     true
          json.set! :uncompleted_tab, true
        else
          json.set! :errors_found,    false
          json.set! :service_tab,     true
          json.set! :product_tab,     true
          json.set! :uncompleted_tab, false
        end
      end

      if @resource.tipo_fornecedor.eql?("Não")
        json.set! :tipo_fornecedor_founded, true
        json.set! :tipo_fornecedor, "Não"
        json.set! :errors_found, false
      end
    end
  end

  json.set! :investidor do
    if @resource.tipo_investidor.nil?
      json.set! :tipo_investidor_founded, false
      json.set! :uncompleted_fields, true
      json.set! :errors_found, true
      json.set! :tipo_investidor, ""
    else
      if @resource.tipo_investidor.eql?("Não")
        json.set! :tipo_investidor_founded, true
        json.set! :tipo_investidor, "Não"
        json.set! :errors_found, false
      end
      if @resource.tipo_investidor.eql?("Sim")
        json.set! :tipo_investidor_founded, true
        json.set! :tipo_investidor, "Sim"
        if @resource.cliente_works.last.nil?
          json.set! :work_tab, true
          json.set! :errors_found, true
          json.set! :uncompleted_tab, true
        else
          json.set! :errors_found, false
          json.set! :work_tab, true
          json.set! :investidor_work_founded, true
          json.set! :uncompleted_tab, false
        end
      end
    end
  end
elsif @resource.class.name.eql?("Estoque")
  json.estoque_produtos do
    json.array! @resource.estoque_produtos do |estoque_produto|
      json.id estoque_produto.id
      json.nome estoque_produto.produto.nome
      json.quantidade estoque_produto.quantidade
      json.valor_produto number_to_currency(estoque_produto.valor_produto)
      json.valor_total   number_to_currency(estoque_produto.valor_total)
    end
  end
end
