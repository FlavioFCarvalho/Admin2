json.array! @cliente_fornecedor_services do |forservice|
  json.id     forservice.id
  json.fornecedor_id forservice.fornecedor_id
  json.name   forservice.service.name
  json.status translate_i18n_status(forservice.service.status)
end
