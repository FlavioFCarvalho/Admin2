unless @banco.valid?
  json.set! :valid, false
  json.set! :message, t('flash.actions.create.alert', resource_name: @banco.nome)
else
  json.set! :valid, true
  json.set! :message, t('flash.actions.create.success', resource_name: @banco.nome)
  json.set! :value, @banco.id
  json.set! :name, @banco.nome
end
