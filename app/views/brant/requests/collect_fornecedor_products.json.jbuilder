json.array! @cliente_fornecedor_products do |forproduct|
  json.id     forproduct.id
  json.fornecedor_id forproduct.fornecedor_id
  json.name   forproduct.produto.nome
  json.status translate_i18n_status(forproduct.produto.status)
end
