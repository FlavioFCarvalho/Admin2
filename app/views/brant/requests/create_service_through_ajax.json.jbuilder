unless @service.valid?
  json.set! :valid, false
  json.set! :message, t('flash.actions.create.alert', resource_name: @service.name)
else
  json.set! :valid, true
  json.set! :message, t('flash.actions.create.success', resource_name: @service.name)
  json.set! :value, @service.id
  json.set! :name, @service.name
end
