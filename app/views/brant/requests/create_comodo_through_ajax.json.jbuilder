unless @comodo.valid?
  json.set! :valid, false
  json.set! :message, t('flash.actions.create.alert', resource_name: @comodo.name)
else
  json.set! :valid, true
  json.set! :message, t('flash.actions.create.success', resource_name: @comodo.name)
  json.set! :value, @comodo.id
  json.set! :name, @comodo.name
end