require 'json'
unless @cliente_fornecedor.valid?
  json.set! :valid, @cliente_fornecedor.valid?
  @error_messages=[]
  @messages = JSON.parse(@cliente_fornecedor.errors.messages.to_json)
  @messages.map do |_key, value|
    @error_messages << "#{value.to_s.gsub("[", "").gsub("]", "").gsub('"', "")}"
  end.join
  json.set! :messages, @error_messages
else
  json.set! :valid, @cliente_fornecedor.valid?
  json.set! :notify_message, t('flash.actions.update.success', resource_name:  "Registro")
  json.url cliente_fornecedor_path(@cliente_fornecedor)
end
