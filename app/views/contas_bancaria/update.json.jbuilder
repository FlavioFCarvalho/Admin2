json.set! :bank_account do
  json.set! :agency, @conta_bancaria.agencia
  json.set! :account, @conta_bancaria.conta
  json.set! :account_type, @conta_bancaria.tipo
  json.set! :bank_id, @conta_bancaria.banco_id
  json.set! :bank_name, @conta_bancaria.banco_nome
end

