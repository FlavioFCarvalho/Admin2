json.extract! comodo, :id, :name, :created_at, :updated_at
json.url comodo_url(comodo, format: :json)
