# == Schema Information
#
# Table name: categorias
#
#  id         :integer          not null, primary key
#  nome       :string(255)
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  status     :string(20)
#  user_id    :integer
#

class Categoria < ApplicationRecord
  has_many :produto_categorias, :dependent => :destroy
  has_many :produtos, through: :produto_categorias

  validates :nome, presence: true
  validates :nome, uniqueness: true

  after_initialize :set_default_status, :if => :new_record?

  def set_default_status
    self.status = "Ativo"
  end

  RANSACKABLE_ATTRIBUTES = ["nome"]

  def self.ransackable_attributes auth_object = nil
    (RANSACKABLE_ATTRIBUTES) + _ransackers.keys
  end
end
