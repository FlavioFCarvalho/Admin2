# == Schema Information
#
# Table name: authorization_users
#
#  id               :integer          not null, primary key
#  user_id          :integer
#  authorization_id :integer
#  created_at       :datetime         not null
#  updated_at       :datetime         not null
#  _create          :boolean          default("0")
#  _retrieve        :boolean          default("0")
#  _update          :boolean          default("0")
#  _delete          :boolean          default("0")
#

class AuthorizationUser < ApplicationRecord
  belongs_to :user
  belongs_to :authorization
end
