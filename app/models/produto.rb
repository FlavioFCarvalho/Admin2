# == Schema Information
#
# Table name: produtos
#
#  id          :integer          not null, primary key
#  status      :integer          default("0")
#  nome        :string(255)
#  unidade     :string(255)
#  observacoes :text(65535)
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#  user_id     :integer
#  valor       :decimal(14, 2)
#  quantidade  :integer          default("0")
#

class Produto < ApplicationRecord
  attr_accessor :categoria_id
  belongs_to :user
  has_many :produto_categorias, :dependent => :destroy
  has_many :categorias, through: :produto_categorias
  accepts_nested_attributes_for :produto_categorias, allow_destroy: true

  attr_accessor :_status
  STATUS = [:active, :inactive]

  validates :status, :nome, :unidade, :valor, presence: true
  validates :nome, uniqueness: true

  usar_como_dinheiro :valor

  enum status: STATUS

  after_initialize :set_default_status, :if => :new_record?

  scope :by_the_last_one, -> { order("id desc") }
  scope :active, -> { where(status: 0).by_the_last_one }

  def set_default_status
    self.status ||= STATUS[0].to_s
  end

  def active?
    self.status.eql?(STATUS[0].to_s)
  end

  def name
    nome
  end

  def descricao
    "#{nome.upcase} - #{unidade}"
  end

  RANSACKABLE_ATTRIBUTES = ["status", "nome", "unidade"]

  def self.ransackable_attributes auth_object = nil
    (RANSACKABLE_ATTRIBUTES) + _ransackers.keys
  end
end
