# == Schema Information
#
# Table name: users
#
#  id                     :integer          not null, primary key
#  username               :string(255)      default(""), not null
#  email                  :string(255)      default(""), not null
#  encrypted_password     :string(255)      default(""), not null
#  password_salt          :string(255)      default(""), not null
#  role                   :integer          default("0"), not null
#  reset_password_token   :string(255)
#  reset_password_sent_at :datetime
#  remember_created_at    :datetime
#  sign_in_count          :integer          default("0"), not null
#  current_sign_in_at     :datetime
#  last_sign_in_at        :datetime
#  current_sign_in_ip     :string(255)
#  last_sign_in_ip        :string(255)
#  failed_attempts        :integer          default("0"), not null
#  unlock_token           :string(255)
#  locked_at              :datetime
#  created_at             :datetime         not null
#  updated_at             :datetime         not null
#

class User < ApplicationRecord
  # => Form field Accessors
  attr_accessor :login, :steps
  attr_accessor :type, :create, :retrieve, :update, :delete, :authorization_id

  # => Devise
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable,
         :trackable, :validatable, :encryptable,
         :timeoutable, :authentication_keys => [:login]

  after_initialize :set_default_role, :if => :new_record?
  before_validation :ensure_username_in_lowercase

  ROLES = [:user, :admin]

  enum role: ROLES

  def set_default_role
    self.role ||= ROLES[0].to_s
  end

  def add_role=(resource)
    self.role=resource.to_i
  end

  def admin?
    self.role.eql?(ROLES[1].to_s)
  end

  def user?
    self.role.eql?(ROLES[0].to_s)
  end

  has_many :bancos
  has_many :categorias
  has_many :comodos
  has_many :produtos
  has_many :obras
  has_many :services
  has_many :authorization_users
  has_many :cliente_fornecedores
  has_many :estoques_movimentos, class_name: "Estoque::EstoqueMovimento"
  has_many :tipo_contratos
  has_many :modelos
  has_many :tipos 


  cattr_accessor :steps do
    %w(permission)
  end

  scope :with_unlimited_access,  ->  { where(role: ROLES[1]) }
  scope :with_restricted_access, ->  { where(role: ROLES[0]) }

  has_one  :profile, class_name: "UserProfile", dependent: :destroy
  accepts_nested_attributes_for :profile, allow_destroy: true
  delegate :name,
           to: :profile,
           allow_nil: true

  validates :email, :username, presence: true
  validates :email, :username, uniqueness: true

  # => Importa Permissoes de Modulo
  def import_authorizations(resource)
    authorization_users.delete_all #necessario para atualizar ou adicionar novas permissoes
    resource.each do |_key, single|
      authorization_users.build(authorization_id: single[:authorization_id].to_i, _create: single[:create].to_b,
                        _update: single[:update].to_b, _retrieve: single[:retrieve].to_b, _delete: single[:delete].to_b)
    end
  end

  def login=(login)
    @login = login
  end

  def login
    @login || self.username || self.email
  end

  # This is for ActiveRecord
  def self.find_for_database_authentication(warden_conditions)
    conditions = warden_conditions.dup
    if login = conditions.delete(:login)
      where(conditions.to_hash).where(["lower(username) = :value OR lower(email) = :value", { :value => login.downcase }]).first
    elsif conditions.has_key?(:username) || conditions.has_key?(:email)
      where(conditions.to_hash).first
    end
  end

  private
    def ensure_username_in_lowercase
      self.username.downcase
    end
end
