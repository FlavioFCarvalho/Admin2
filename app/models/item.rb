# == Schema Information
#
# Table name: itens
#
#  id         :integer          not null, primary key
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

# => Modelo que guarda os indices dos Produtos do Estoque no Banco
class Item < ApplicationRecord
  has_many :estoque_produtos, class_name: "Estoque::EstoqueProduto", dependent: :destroy
  has_many :estoque_transferencias, class_name: "Estoque::EstoqueTransferencia", dependent: :destroy

  def add_produto_to_estoque(resource)
    
    current_item = estoque_produtos.find_by(id: resource[:id], estoque_movimento_id: resource[:estoque_movimento_id])

    if current_item
      # current_item.produto_id=resource[:produto_id] if resource[:produto_id] != 0
      # current_item.obra_id= resource[:obra_id] if resource[:obra_id] != 0
      # current_item.item_obra_id= resource[:item_obra_id] if resource[:item_obra_id] != 0
      # current_item.sub_item_obra_id= resource[:sub_item_obra_id] if resource[:sub_item_obra_id] != 0
      # current_item.comodo_id = (resource[:comodo_id].to_i) if resource[:comodo_id] != 0
      # current_item.quantidade = (resource[:quantidade].to_i)
      # current_item.valor_produto=(resource[:valor_produto].to_f)
      # @novo_valor_total = current_item.valor_total.to_f
      # @novo_valor_total = resource[:valor_total].to_f
      # current_item.valor_total=(@novo_valor_total)
      # current_item.observacoes=(resource[:observacoes])
    else
      current_item = estoque_produtos.build(estoque_movimento_id: resource[:estoque_movimento_id],
                                            produto_id: resource[:produto_id],
                                            obra_id: resource[:obra_id],
                                            item_obra_id: resource[:item_obra_id],
                                            sub_item_obra_id: resource[:sub_item_obra_id],
                                            quantidade_movimento: resource[:quantidade_movimento].to_i,
                                            valor_produto: resource[:valor_produto].gsub(".", "").gsub(",", ".").to_f,
                                            valor_total: resource[:valor_total].gsub(".", "").gsub(",", ".").to_f,
                                            comodo_id: resource[:comodo_id],
                                            observacoes: resource[:observacoes])
    end
    current_item
  end

  def add_transferencia_produto(kind, resource)
    current_item = nil
    if kind.eql?("ENTRADA")
      current_item = estoque_transferencias.where(estoque_movimento_id: resource[:estoque_movimento_id],
                                                  produto_id: resource[:produto_id],
                                                  item_obra_id: resource[:item_obra_id],
                                                  sub_item_obra_id: resource[:sub_item_obra_id],
                                                  comodo_id: resource[:comodo_id]).
                                                  joins(:obra_posterior).
                                                  where("obras.codigo = ?", "BRANT").last
    end

    if kind.eql?("SAÍDA")
      current_item = estoque_transferencias.where(estoque_movimento_id: resource[:estoque_movimento_id],
                                                  produto_id: resource[:produto_id],
                                                  item_obra_id: resource[:item_obra_id],
                                                  sub_item_obra_id: resource[:sub_item_obra_id],
                                                  comodo_id: resource[:comodo_id]).
                                                  joins(:obra_anterior).
                                                  where("obras.codigo = ?", "BRANT").last
    end


    if current_item
      current_item.comodo_id += (resource[:comodo_id].to_i)
      current_item.quantidade += (resource[:quantidade].to_i)
      current_item.valor_produto=(resource[:valor_produto].gsub(".", "").gsub(",", ".").to_f)
      @novo_valor_total = current_item.valor_total.to_f
      @novo_valor_total += resource[:valor_total].gsub(".", "").gsub(",", ".").to_f
      current_item.valor_total=(@novo_valor_total)
    else
      current_item = estoque_transferencias.build(estoque_movimento_id: resource[:estoque_movimento_id],
                                                  produto_id: resource[:produto_id],
                                                  obra_anterior_id: resource[:obra_anterior_id],
                                                  obra_posterior_id: resource[:obra_posterior_id],
                                                  item_obra_id: resource[:item_obra_id],
                                                  sub_item_obra_id: resource[:sub_item_obra_id],
                                                  quantidade_movimento: resource[:quantidade_movimento].to_i,
                                                  comodo_id: resource[:comodo_id],
                                                  valor_produto: resource[:valor_produto].gsub(".", "").gsub(",", ".").to_f,
                                                  valor_total: resource[:valor_total].gsub(".", "").gsub(",", ".").to_f)
    end
    current_item.estoque_movimento.valor_total_produto += current_item.valor_total.to_f
    current_item.estoque_movimento.total_estoque += current_item.valor_total.to_f
    current_item.estoque_movimento.save
    return current_item
  end
end
