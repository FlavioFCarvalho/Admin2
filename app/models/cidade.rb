# == Schema Information
#
# Table name: cidades
#
#  id         :integer          not null, primary key
#  nome       :string(255)
#  estado_id  :integer
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class Cidade < ApplicationRecord
  belongs_to :estado
  has_many :cliente_fornecedores
  has_many :obras

  def to_s
    "#{nome}"
  end

  def cidade_params
    params.require(:cidade).permit(:nome, :estado_id)
  end

  RANSACKABLE_ATTRIBUTES = ["nome"]

  def self.ransackable_attributes auth_object = nil
    (RANSACKABLE_ATTRIBUTES) + _ransackers.keys
  end
end
