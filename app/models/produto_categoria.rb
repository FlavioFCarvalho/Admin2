# == Schema Information
#
# Table name: produto_categorias
#
#  id           :integer          not null, primary key
#  produto_id   :integer
#  categoria_id :integer
#  created_at   :datetime         not null
#  updated_at   :datetime         not null
#

class ProdutoCategoria < ApplicationRecord
  belongs_to :categoria
  belongs_to :produto
end
