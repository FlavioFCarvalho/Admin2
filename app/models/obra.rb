# == Schema Information
#
# Table name: obras
#
#  id             :integer          not null, primary key
#  nome_local     :string(255)
#  codigo         :string(255)
#  created_at     :datetime         not null
#  updated_at     :datetime         not null
#  user_id        :integer
#  endereco_local :string(255)
#  bairro         :string(255)
#  cep            :string(255)
#  cidade_id      :integer
#  estado_id      :integer
#  endereco_obra  :string(255)
#  quadra         :string(255)
#  lote           :string(255)
#  cei            :string(255)
#  peticao        :string(255)
#  aprovacao      :string(255)
#  status         :string(255)
#

class Obra < ApplicationRecord
  after_initialize :set_default_status, :if => :new_record?

  validates :nome_local, presence: { message: I18n.t('obras.messages.nome_local_must_be_present')}
  validates :codigo, presence: { message: I18n.t('obras.messages.codigo_must_be_present')}
  validates :codigo, uniqueness: { message: I18n.t('obras.messages.codigo_must_be_unique')}

  belongs_to :user
  belongs_to :estado
  belongs_to :cidade

  with_options dependent: :destroy do
    has_many :cliente_works, class_name: "ClienteObra", foreign_key: "obra_id"
    has_many :comodo_obras
  end

  has_many :clientes, through: :cliente_works
  has_many :comodos, through: :comodo_obras
  
  has_many :estoque_produtos, class_name: "Estoque::EstoqueProduto", foreign_key: "obra_id"

  accepts_nested_attributes_for :comodo_obras, reject_if: ->(attributes){ attributes['comodo_id'].blank? }, allow_destroy: true
  accepts_nested_attributes_for :comodos, reject_if: ->(attributes){ attributes['name'].blank? }
  
  before_save :upcased_name_attribute

  def set_default_status
    self.status = "Ativo"
  end

  def name
    nome_local
  end

  class << self
    def return_distinct_job_code(resource)
      find_by_sql("SELECT `obras`.`codigo`, SUM(`estoque_estoque_parcelamentos`.`valor`) AS valor_total
                    FROM `obras` 
                    INNER JOIN `estoque_estoque_produtos` ON `estoque_estoque_produtos`.`obra_id` = `obras`.`id`
                    INNER JOIN `estoque_estoque_parcelamentos` ON `estoque_estoque_produtos`.`id` = `estoque_estoque_parcelamentos`.`estoque_produto_id`
                    WHERE `estoque_estoque_produtos`.`obra_id` = `obras`.`id` 
                    AND (`estoque_estoque_parcelamentos`.`data_vencimento` BETWEEN '#{Date.parse(resource[:data_inicio])}' AND '#{Date.parse(resource[:data_fim])}')
                    GROUP BY `obras`.`id`	
                    ORDER BY `obras`.`codigo` ASC")
    end
  end

  RANSACKABLE_ATTRIBUTES = ["nome_local", "codigo"]

  def self.ransackable_attributes auth_object = nil
    (RANSACKABLE_ATTRIBUTES) + _ransackers.keys
  end

  def estado_cidade
    "#{cidade.nome} - #{estado.nome}"
  end

  def upcased_name_attribute
    nome_local.upcase!
  end
end
