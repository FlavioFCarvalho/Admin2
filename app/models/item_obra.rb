# == Schema Information
#
# Table name: itens_obra
#
#  id           :integer          not null, primary key
#  item         :string(255)
#  tipo_servico :string(255)
#  created_at   :datetime         not null
#  updated_at   :datetime         not null
#

class ItemObra < ApplicationRecord
  has_many :sub_itens_obras, class_name: "SubItemObra", foreign_key: "item_obra_id", dependent: :destroy
  accepts_nested_attributes_for :sub_itens_obras,
                                 reject_if: :reject_sub_itens?,
                                allow_destroy: true
  validates :item, :tipo_servico, presence: true, uniqueness: true

  paginates_per QT_PER_PAGE

  scope :alphabetic_order, -> { order(tipo_servico: :asc) }
  # Ex:- scope :active, -> {where(:active => true)}


  RANSACKABLE_ATTRIBUTES = ["item", "tipo_servico"]

  def self.ransackable_attributes auth_object = nil
    (RANSACKABLE_ATTRIBUTES) + _ransackers.keys
  end

  def item_obra_descricao
    "#{tipo_servico} - #{item}"
  end

  class << self
    def search(resource)
      where("item LIKE ? AND tipo_servico LIKE ?", "%#{resource[:item]}%", "%#{resource[:tipo_servico]}%")
    end
  end

  private
    def reject_sub_itens?(att)
      return if att['sub_tipo_servico'].blank? && new_record?
    end
end
