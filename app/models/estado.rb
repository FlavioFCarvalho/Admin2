# == Schema Information
#
# Table name: estados
#
#  id         :integer          not null, primary key
#  sigla      :string(255)
#  nome       :string(255)
#  capital_id :integer
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class Estado < ApplicationRecord
  scope :pelo_nome, -> {order("nome ASC")}

  belongs_to :capital, class_name: 'Cidade', foreign_key: :capital_id
  has_many :cidades
  has_many :cliente_fornecedores
  has_many :obras

  def to_s
    "#{nome} - #{sigla}"
  end

  def estado_params
    params.require(:estado).permit(:nome, :sigla, :capital_id)
  end

  RANSACKABLE_ATTRIBUTES = ["nome"]

  def self.ransackable_attributes auth_object = nil
    (RANSACKABLE_ATTRIBUTES) + _ransackers.keys
  end
end
