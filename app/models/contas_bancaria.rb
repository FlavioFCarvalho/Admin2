# == Schema Information
#
# Table name: contas_bancaria
#
#  id                    :integer          not null, primary key
#  banco_id              :integer
#  agencia               :string(255)
#  conta                 :string(255)
#  tipo                  :string(255)
#  cliente_fornecedor_id :integer
#  created_at            :datetime         not null
#  updated_at            :datetime         not null
#  status                :boolean          default("1")
#

class ContasBancaria < ApplicationRecord
  belongs_to :banco
  belongs_to :cliente_fornecedor
  attr_accessor :name_bank

  delegate :nome, to: :banco, prefix: true
end


