# == Schema Information
#
# Table name: comodos
#
#  id         :integer          not null, primary key
#  name       :string(255)
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  user_id    :integer
#

#-*- coding: utf-8 -*-
class Comodo < ApplicationRecord
  has_many :comodo_obras
  has_many :obras, through: :comodo_obras

  validates_presence_of :name

  before_create :upcase_name_before_create

  paginates_per 20

  def self.search(source)
    where('name LIKE ?', "%#{source[:name]}%").
    order(id: :desc).distinct
  end

  def upcase_name_before_create
    name.upcase!
  end
end
