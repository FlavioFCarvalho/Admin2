# == Schema Information
#
# Table name: contas
#
#  id         :integer          not null, primary key
#  banco_id   :integer
#  agencia    :string(255)
#  numero     :string(255)
#  tipo_conta :string(255)
#  operacao   :string(255)
#  titular    :string(255)
#  cpf_cnpj   :string(255)
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class Conta < ApplicationRecord
  belongs_to :banco

  validates :banco_id, :agencia, :numero, :tipo_conta, :titular, :cpf_cnpj, presence: true
  validates_associated :banco

  RANSACKABLE_ATTRIBUTES = ["agencia", "numero", "tipo_conta", "operacao", "titular", "cpf_cnpj"]

  def self.ransackable_attributes auth_object = nil
    (RANSACKABLE_ATTRIBUTES) + _ransackers.keys
  end

  def banco_pagador
    banco.nome
  end

  def descricao
    "#{banco_pagador} - #{agencia} / #{numero}"
  end

  def account_description
    "#{agencia} / #{numero}"
  end
  
  
end
