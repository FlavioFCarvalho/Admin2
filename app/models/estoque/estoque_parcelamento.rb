# == Schema Information
#
# Table name: estoque_estoque_parcelamentos
#
#  id                       :integer          not null, primary key
#  estoque_movimento_id     :integer          not null
#  estoque_produto_id       :integer
#  estoque_transferencia_id :integer
#  valor                    :decimal(14, 2)
#  created_at               :datetime         not null
#  updated_at               :datetime         not null
#  bloqueado                :boolean          default("0")
#  data_vencimento          :date
#  status_pagamento         :string(255)      default("A PAGAR")
#  periodo                  :string(255)
#  parcela                  :integer
#

#-*- coding: utf-8 -*-

class Estoque::EstoqueParcelamento < ApplicationRecord

  belongs_to :estoque_movimento, class_name: "Estoque::EstoqueMovimento", foreign_key: "estoque_movimento_id"
  belongs_to :estoque_produto, class_name: "Estoque::EstoqueProduto"
  belongs_to :estoque_transferencia, class_name: "Estoque::EstoqueTransferencia"

  validates :valor, presence: {  message: I18n.t('estoques.messages.valor_total_must_be_present') }
  validates :parcela, presence: {  message: I18n.t('estoques.messages.parcelas_must_be_present') }

  class << self
    def divide_valor_proporcional(_valor_restante, _parcelas_restantes)
      return (_valor_restante / _parcelas_restantes)
    end
  end

  '''
    Verifica se existe um proximo elemento depois
    caso este não esteja bloqueado
  '''
  def next
    self.class.where("id > ? AND (estoque_produto_id = ? OR estoque_transferencia_id = ?) AND updated_value = ?", id, estoque_produto_id, estoque_transferencia_id, false).last
  end
    
  '''
    Verifica se existe um elemento anterior
    caso este não esteja bloqueado
  '''
  def previous
    self.class.where("id < ? AND (estoque_produto_id = ? OR estoque_transferencia_id = ?) AND updated_value = ?", id, estoque_produto_id, estoque_transferencia_id, false).first
  end
end
