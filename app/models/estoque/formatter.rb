class Estoque::Formatter
  attr_accessor :tipo_movimentacao, :estoque_movimento, :fornecedor, :data_entrada, :data_vencimento_entrada, :data_devolucao, :data_transferencia, :total
  attr_accessor :entrada, :devolucao, :transferencia_entrada, :transferencia_saida
  attr_reader :result, :source

  def initialize(search_param, resource)
    @total=@entrada=@devolucao=@transferencia_entrada=@transferencia_saida = 0
    @tipo_movimentacao =search_param
    @fornecedor = []
    @source = resource
    _build_line_
    return @result
  end

  def _build_line_
    @source.each do |resource|
      @entrada += resource.quantidade if resource.tipo_movimentacao.eql?("Entrada")
      @devolucao += resource.quantidade if resource.tipo_movimentacao.eql?("Devolução para Fornecedor")

      if resource.tipo_movimentacao.eql?("Transferência")
        @transferencia_entrada += resource.quantidade if resource.transferencia_tipo.eql?("ENTRADA")
        @transferencia_saida += resource.quantidade   if resource.transferencia_tipo.eql?("SAÍDA")
      end

      @estoque_movimento = Estoque::EstoqueMovimento.find_by_id(resource.id) if !resource.id.nil?
      @fornecedor << ClienteFornecedor.find_by_id(resource.fornecedor_id).nome.truncate(27) if !resource.fornecedor_id.nil?
      @data_entrada = resource.data_entrada if !resource.data_entrada.nil?
      @data_vencimento_entrada = resource.data_vencimento_entrada if !resource.data_vencimento_entrada.nil?
      @data_devolucao = resource.data_devolucao if !resource.data_devolucao.nil?
      @data_transferencia = resource.data_transferencia if !resource.data_transferencia.nil?
    end
    @total= (@entrada - @devolucao - @transferencia_saida + @transferencia_entrada)
  end
end
