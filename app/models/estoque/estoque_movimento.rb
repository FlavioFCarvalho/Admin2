# == Schema Information
#
# Table name: estoque_estoque_movimentos
#
#  id                      :integer          not null, primary key
#  tipo_movimentacao       :string(255)
#  fornecedor_id           :integer
#  user_id                 :integer
#  nota_fiscal_entrada     :string(255)
#  recibo                  :string(255)
#  cupom_fiscal            :string(255)
#  numero_pedido           :string(255)
#  numero_da_os            :string(255)
#  nota_fiscal_saida       :string(255)
#  data_entrada            :date
#  data_vencimento_entrada :date
#  data_saida              :date
#  data_vencimento_saida   :date
#  data_devolucao          :date
#  justificativa_devolucao :text(65535)
#  total_estoque           :decimal(14, 2)
#  created_at              :datetime         not null
#  updated_at              :datetime         not null
#  obra_id                 :integer
#  produto_id              :integer
#  data_transferencia      :date
#  transferencia_tipo      :string(255)
#  parcelas                :integer          default("1")
#  periodo                 :string(255)
#  acrescimo               :string(255)
#  desconto                :string(255)
#  valor_total_produto     :decimal(14, 2)
#  boleto                  :string(255)
#  frete                   :decimal(14, 2)
#  observacoes             :text(65535)
#  incompleto              :boolean          default("1")
#  valida_por              :string(255)      default("total_estoque")
#

#-*- coding: utf-8 -*-
class Estoque::EstoqueMovimento < ApplicationRecord
  include Estoque::Filtros::TodosConcern
  include Estoque::Filtros::EntradaConcern
  include Estoque::Filtros::DevolucaoConcern
  include Estoque::Filtros::TransferenciaEntradaConcern
  include Estoque::Filtros::TransferenciaSaidaConcern

  belongs_to :user, class_name: "User", foreign_key: "user_id"
  belongs_to :fornecedor, class_name: "ClienteFornecedor", foreign_key: "fornecedor_id"
  belongs_to :destinatario_tomador, class_name: "ClienteFornecedor", foreign_key: "destinatario_tomador"

  with_options dependent: :destroy do
    has_many   :estoque_produtos, class_name: "Estoque::EstoqueProduto"
    has_many   :estoque_transferencias, class_name: "Estoque::EstoqueTransferencia"
    has_many   :estoque_produto_parcelamentos, class_name: "Estoque::EstoqueParcelamento", through: :estoque_produtos
    has_many   :estoque_transferencia_parcelamentos, class_name: "Estoque::EstoqueParcelamento", through: :estoque_transferencias
    has_many   :estoque_movimento_parcelas, class_name: "Estoque::EstoqueMovimentoParcela", foreign_key: "estoque_movimento_id"
  end

  has_many :produtos,   through: :estoque_produtos
  has_many :categorias, through: :produtos
  
  # => Criando campos para produto na aba produtos
  accepts_nested_attributes_for :produtos,
                                :reject_if => :all_blank,
                                allow_destroy: true

  # => Criando campos para categoria na aba produtos
  accepts_nested_attributes_for :categorias,
                                :reject_if => :all_blank,
                                allow_destroy: true

  accepts_nested_attributes_for :estoque_movimento_parcelas, reject_if: ->(attributes){ attributes['valor'].blank? }, allow_destroy: true

  validates :tipo_movimentacao, presence: { message: I18n.t('estoques.messages.tipo_movimentacao_must_be_present')}
  validates :fornecedor_id, presence: { if: :tipo_movimentacao_entrada?, message: I18n.t('estoques.messages.fornecedor_must_be_present')}
  validates :data_entrada, presence: { if: :tipo_movimentacao_entrada?, message: I18n.t('estoques.messages.data_entrada_must_be_present')}
  validates :data_vencimento_entrada, presence: { if: :tipo_movimentacao_entrada?, message: I18n.t('estoques.messages.data_vencimento_must_be_present')}
  validates :data_devolucao, presence: { if: :tipo_movimentacao_devolucao?, message: I18n.t('estoques.messages.data_devolucao_must_be_present')}
  validates :fornecedor_id, presence: { if: :tipo_movimentacao_devolucao?, message: I18n.t('estoques.messages.fornecedor_must_be_present')}
  validates :parcelas, presence: {  message: I18n.t('estoques.messages.parcelas_must_be_present') }

  before_create :set_valor_total

  scope :order_by_incompleto, ->{ order(incompleto: :desc) }
  paginates_per 20

  STATUS_VALIDOS =[
    'TODOS',
    'ENTRADA',
    'DEVOLUÇÃO PARA FORNECEDOR',
    'TRANSFERÊNCIA DE ENTRADA',
    'TRANSFERÊNCIA DE SAÍDA'
  ]

  TRANSFERENCIA = ["ENTRADA", "SAÍDA"]
  VALIDA_POR = ["total_estoque", "total_produtos"]

  def n_documento
    return "#{nota_fiscal_entrada}" if nota_fiscal_entrada.present?
    return "#{nota_fiscal_saida}" if nota_fiscal_saida.present?
    return "#{cupom_fiscal}" if cupom_fiscal.present?
    return "#{recibo}" if recibo.present?
    return "#{numero_pedido}" if numero_pedido.present?
    return "#{boleto}" if boleto.present?
  end

  def confronta_valores_de_produtos
    _valor_total_produtos = 0.0
    if self.estoque_produtos.empty?
      self.incompleto= true
    else
      self.estoque_produtos.each do |eprodutos|
        _valor_total_produtos += eprodutos.valor_total.to_f
      end
      
      if self.valida_por.eql?("total_produtos") and _valor_total_produtos.eql?(self.valor_total_produto.to_f)
        self.incompleto= false
      elsif self.valida_por.eql?("total_estoque") and _valor_total_produtos.eql?(self.total_estoque.to_f)
        self.incompleto= false
      else
        self.incompleto= true
      end
    end
    self.save
  end
  
  def confronta_valores_de_transferencia
    _valor_total_estoque_transferencias = 0.0
    if self.estoque_transferencias.empty?
      self.incompleto = true
    else
      self.estoque_transferencias.each do |eprodutostransferencia|
        _valor_total_estoque_transferencias += eprodutostransferencia.valor_total.to_f
      end

      if self.valida_por.eql?("total_produtos") and _valor_total_estoque_transferencias.eql?(self.valor_total_produto.to_f)
        self.incompleto= false
      elsif self.valida_por.eql?("total_estoque") and _valor_total_estoque_transferencias.eql?(self.total_estoque.to_f)
        self.incompleto= false
      else
        self.incompleto= true
      end
    end
    self.save
  end

  class << self
    STATUS_VALIDOS.each do |status_validos|
      define_method "#{status_validos}" do
        status_validos
      end
    end

    def search_movimento(resource)
      where(tipo_movimentacao: resource[:tipo_movimentacao_eq]).distinct
    end

    def generate_order(resource)
      joins(:fornecedor).
      joins(:estoque_produtos).
      joins(estoque_produtos: :obra).
      joins(estoque_produtos: :estoque_produto_parcelamentos).
      where(tipo_movimentacao: STATUS_VALIDOS[1].downcase.capitalize).
      where("`estoque_estoque_parcelamentos`.`data_vencimento` BETWEEN '#{Date.parse(resource[:data_inicio])}' AND '#{Date.parse(resource[:data_fim])}'").
      order("`estoque_estoque_parcelamentos`.`data_vencimento` ASC").
      select("`estoque_estoque_parcelamentos`.`data_vencimento` AS vencimento").
      select("`cliente_fornecedores`.`nome`").
      select("`estoque_estoque_movimentos`.`nota_fiscal_entrada`").
      select("`estoque_estoque_movimentos`.`recibo`").
      select("`estoque_estoque_movimentos`.`cupom_fiscal`").
      select("`estoque_estoque_movimentos`.`numero_pedido`").
      select("`estoque_estoque_movimentos`.`boleto`").
      select("`estoque_estoque_parcelamentos`.`parcela`").
      select("`estoque_estoque_parcelamentos`.`valor`").
      select("`obras`.`codigo`").
      select("`estoque_estoque_produtos`.`observacoes`")
    end
  end

  def add_transferencia_tipo=(resource)
    if resource.eql?("true")
      self.transferencia_tipo=TRANSFERENCIA[0].to_s
    else
      self.transferencia_tipo=TRANSFERENCIA[1].to_s
    end
  end

  def set_valida_por=(resource)
    if resource.eql?("true")
      self.valida_por=VALIDA_POR[0].to_s
    else
      self.valida_por=VALIDA_POR[1].to_s
    end
  end

  def entrada?
    self.tipo_movimentacao.eql?(STATUS_VALIDOS[1].to_s)
  end

  def devolucao?
    self.tipo_movimentacao.eql?(STATUS_VALIDOS[2].to_s)
  end
  
  def transferencia?
    self.tipo_movimentacao.eql?(STATUS_VALIDOS[3].to_s) || self.tipo_movimentacao.eql?(STATUS_VALIDOS[4].to_s)
  end

  def transferencia_tipo_entrada?
    self.transferencia_tipo.eql?(TRANSFERENCIA[0].to_s)
  end

  def transferencia_tipo_saida?
    self.transferencia_tipo.eql?(TRANSFERENCIA[1].to_s)
  end

  def transferencia_descricao
    "#{tipo_movimentacao.upcase} - #{transferencia_tipo}"
  end

  def data_vencimento
    return data_vencimento_entrada if data_vencimento_entrada.present?
    return data_vencimento_saida if data_vencimento_saida.present?
  end

  def return_total_produtos
    _valor_total = 0
    estoque_produtos.each do |ep|
      _valor_total += ep.valor_total
    end
    return _valor_total.to_f
  end

  def return_total_produtos_transferencia
    _valor_total = 0
    estoque_transferencias.each do |ep|
      _valor_total += ep.valor_total
    end
    return _valor_total.to_f
  end

  private
    def set_valor_total
      self.total_estoque=(0.0) if self.total_estoque.nil?
    end
    
    def tipo_movimentacao_entrada?
      return true if tipo_movimentacao.eql?("Entrada")
    end

    def tipo_movimentacao_devolucao?
      return true if tipo_movimentacao.eql?("Devolução para Fornecedor")
    end
end
