# == Schema Information
#
# Table name: estoque_estoque_produtos
#
#  id                   :integer          not null, primary key
#  produto_id           :integer
#  estoque_movimento_id :integer
#  obra_id              :integer
#  quantidade_movimento :integer          default("0")
#  valor_produto        :decimal(14, 2)
#  valor_total          :decimal(14, 2)
#  created_at           :datetime         not null
#  updated_at           :datetime         not null
#  item_id              :integer
#  item_obra_id         :integer
#  sub_item_obra_id     :integer
#  parcelas             :integer
#  comodo_id            :integer
#  observacoes          :text(65535)
#  quantidade_anterior  :integer          default("0")
#  quantidade_atual     :integer          default("0")
#

#-*- coding: utf-8 -*-
class Estoque::EstoqueProduto < ApplicationRecord
  include Estoque::Produtos::BuildConcern
  include Estoque::Parcelamentos::PagamentoConcern
  
  belongs_to :estoque_movimento, class_name: "Estoque::EstoqueMovimento"
  belongs_to :produto,           class_name: "Produto",  foreign_key: "produto_id"
  belongs_to :obra,              class_name: "Obra",     foreign_key: "obra_id"
  belongs_to :item_obra,         class_name: "ItemObra", foreign_key: "item_obra_id"
  belongs_to :sub_item_obra,     class_name: "SubItemObra", foreign_key: "sub_item_obra_id"
  has_many   :estoque_produto_parcelamentos, class_name: "Estoque::EstoqueParcelamento", foreign_key: "estoque_produto_id", dependent: :destroy  
  
  # => Obs: linha importante a seguir para indice dos produtos no estoque
  belongs_to :item

  validates :produto_id, presence: { message: I18n.t('estoques.messages.produto_must_be_present')}
  validates :obra_id, presence: { message: I18n.t('estoques.messages.obra_must_be_present')}
  validates :item_obra_id, presence: { message: I18n.t('estoques.messages.item_obra_must_be_present')}, if: :validate_etapa?
  validates :sub_item_obra_id, presence: { message: I18n.t('estoques.messages.sub_item_obra_must_be_present')}, if: :validate_sub_etapa?
  validates :quantidade_movimento, presence: { message: I18n.t('estoques.messages.quantidade_must_be_present')}
  validates :valor_produto, presence: { message: I18n.t('estoques.messages.valor_produto_must_be_present')}
  validates :valor_total, presence: { message: I18n.t('estoques.messages.valor_total_must_be_present')}

  before_destroy :reajustar_quantidade_no_produto

  delegate :nome,
           :unidade,
           :quantidade,
           :valor,
           to: :produto,
           prefix:  true,
           allow_nil: true
  paginates_per 10

  def validate_etapa?
    unless estoque_movimento.tipo_movimentacao.upcase.eql?("DEVOLUÇÃO PARA FORNECEDOR")
      if !item_obra_id.present?
        return true
      end
    end
  end

  def validate_sub_etapa?
    unless estoque_movimento.tipo_movimentacao.upcase.eql?("DEVOLUÇÃO PARA FORNECEDOR")
      if !sub_item_obra_id.present?
        return true
      end
    end
  end
end
