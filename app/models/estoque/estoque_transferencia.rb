# == Schema Information
#
# Table name: estoque_estoque_transferencias
#
#  id                   :integer          not null, primary key
#  estoque_movimento_id :integer
#  produto_id           :integer
#  obra_anterior_id     :integer
#  obra_posterior_id    :integer
#  item_obra_id         :integer
#  sub_item_obra_id     :integer
#  quantidade_movimento :integer
#  valor_produto        :decimal(14, 2)
#  valor_total          :decimal(14, 2)
#  item_id              :integer
#  created_at           :datetime         not null
#  updated_at           :datetime         not null
#  parcelas             :integer
#  comodo_id            :integer
#  quantidade_anterior  :integer          default("0")
#  quantidade_atual     :integer          default("0")
#

class Estoque::EstoqueTransferencia < ApplicationRecord
  include Estoque::Parcelamentos::PagamentoConcern
  include Estoque::EstoqueTransferencia::ParcelamentoConcern

  belongs_to :estoque_movimento, class_name: "Estoque::EstoqueMovimento", foreign_key: "estoque_movimento_id"
  belongs_to :produto,   class_name: "Produto",  foreign_key: "produto_id"
  belongs_to :obra_anterior, class_name: "Obra", foreign_key: "obra_anterior_id"
  belongs_to :obra_posterior, class_name: "Obra", foreign_key: "obra_posterior_id"
  belongs_to :item_obra, class_name: "ItemObra", foreign_key: "item_obra_id"
  belongs_to :sub_item_obra, class_name: "SubItemObra", foreign_key: "sub_item_obra_id"
  has_many   :estoque_transferencia_parcelamentos, class_name: "Estoque::EstoqueParcelamento", foreign_key: "estoque_transferencia_id",dependent: :destroy

  # => Obs: linha importante a seguir para indice dos produtos no estoque
  belongs_to :item

  validates :produto_id, presence: { message: I18n.t('estoques.messages.produto_must_be_present')}
  validates :obra_anterior_id, presence: { message: I18n.t('estoques.messages.obra_must_be_present')}
  validates :obra_posterior_id, presence: { message: I18n.t('estoques.messages.obra_must_be_present')}
  validates :quantidade_movimento, presence: { message: I18n.t('estoques.messages.quantidade_must_be_present')}
  validates :valor_produto, presence: { message: I18n.t('estoques.messages.valor_produto_must_be_present')}
  validates :valor_total, presence: { message: I18n.t('estoques.messages.valor_total_must_be_present')}

  def verify_last_stock
    if Estoque::EstoqueTransferencia.where(produto_id: self.produto_id).last
      @ultimo_estoque_deste_produto = Estoque::EstoqueTransferencia.where(produto_id: self.produto_id).last
      self.quantidade_anterior = @ultimo_estoque_deste_produto.quantidade_atual
    else
      self.quantidade_anterior = 0
    end
  end

  def add_quantity_to_estoque_transferencia
    if !self.quantidade_movimento.nil?
      if self.quantidade_anterior == 0
        self.quantidade_atual = self.quantidade_movimento
      else
        self.quantidade_atual = (self.quantidade_anterior + self.quantidade_movimento)
      end
    end
  end

  def verify_changes_on_main_fields(resource)
    self.produto_id=resource[:produto_id]
    self.obra_anterior=resource[:obra_anterior_id] if resource[:obra_anterior_id].present?
    self.obra_posterior=resource[:obra_posterior_id] if resource[:obra_posterior_id].present?
    self.item_obra_id=resource[:item_obra_id]
    self.sub_item_obra_id=resource[:sub_item_obra_id]
    self.quantidade_movimento=resource[:quantidade_movimento]

    if self.estoque_movimento.estoque_transferencias.last.quantidade_anterior.eql?(0)
      self.quantidade_atual=resource[:quantidade_movimento]
    else
      self.quantidade_atual = self.quantidade_anterior + self.quantidade_movimento
    end
  end

  def simula_valor_total_de_produtos(_total_)
    self.valor_produto = _total_.gsub('.','').gsub(',','.').to_f
    self.valor_total = (self.valor_produto.to_f * self.quantidade_movimento)
  end

  class << self
    def build_parcelamento_from_source(source, qtParcelas)
      @estoque_transferencia = source
      @estoque_transferencia.update_attributes(parcelas: qtParcelas)
      @estoque_transferencia.estoque_transferencia_parcelamentos.delete_all

      data_acumulada = @estoque_transferencia.estoque_movimento.data_vencimento_entrada unless @estoque_transferencia.estoque_movimento.data_vencimento_entrada.nil?
      data_acumulada = @estoque_transferencia.estoque_movimento.data_devolucao unless @estoque_transferencia.estoque_movimento.data_devolucao.nil?
      data_acumulada = @estoque_transferencia.estoque_movimento.data_transferencia unless @estoque_transferencia.estoque_movimento.data_transferencia.nil?

      data_inicial   = data_acumulada 
      @valor_parcial  = Estoque::EstoqueParcelamento.divide_valor_proporcional(@estoque_transferencia.valor_total.to_f, qtParcelas)
      _total_parcial=(@valor_parcial.round(2) * qtParcelas)

      for x in 1..qtParcelas
        @@parcelamento = @estoque_transferencia.estoque_transferencia_parcelamentos.build(estoque_movimento_id: @estoque_transferencia.estoque_movimento_id, valor: @valor_parcial, periodo: @estoque_transferencia.estoque_movimento.periodo, parcela: x)
        @@parcelamento.data_vencimento = data_acumulada

        case @@parcelamento.periodo.downcase
          when 'semanal'
            if @@parcelamento.data_vencimento.day.eql?(30) and data_acumulada.month.eql?(2) and data_acumulada.day.eql?(28)
              data_acumulada = data_acumulada.advance days: 2
            else
              data_acumulada = data_acumulada.advance days: self::Periodo.semanal()
            end
          when 'quinzenal'
            data_acumulada = data_acumulada.advance days: self::Periodo.quinzenal()
          when 'mensal'
            if data_inicial.day.eql?(29) and @@parcelamento.data_vencimento.day.eql?(28) and @@parcelamento.data_vencimento.month.eql?(2)
              data_acumulada = data_acumulada.advance days: 29
            elsif data_inicial.day.eql?(30) and @@parcelamento.data_vencimento.day.eql?(28) and @@parcelamento.data_vencimento.month.eql?(2)
              data_acumulada = data_acumulada.advance days: 30
            elsif data_inicial.day.eql?(31) and @@parcelamento.data_vencimento.day.eql?(28) and @@parcelamento.data_vencimento.month.eql?(2)
              data_acumulada = data_acumulada.advance days: 31
            else
              data_acumulada = data_acumulada.advance months: self::Periodo.mensal()
            end
          when 'anual'
            data_acumulada = data_acumulada.advance years: self::Periodo.anual()
        end

        if @@parcelamento.save
          true
        else
          false
        end
      end

      if (@estoque_transferencia.valor_total > _total_parcial)
        _diferenca_de=(_total_parcial - @estoque_transferencia.valor_total)
      else
        _diferenca_de=(_total_parcial - @estoque_transferencia.valor_total)
      end

      _valor_diferenciado_parcela=(@valor_parcial - _diferenca_de)
      @estoque_transferencia.estoque_transferencia_parcelamentos.first.update_attributes(valor: _valor_diferenciado_parcela)
      
      return @estoque_transferencia.estoque_transferencia_parcelamentos.any?
    end
  end
end
