# == Schema Information
#
# Table name: estoque_estoque_movimento_parcelas
#
#  id                   :integer          not null, primary key
#  valor                :decimal(14, 2)
#  estoque_movimento_id :integer
#  bloqueado            :boolean          default("0")
#  created_at           :datetime         not null
#  updated_at           :datetime         not null
#  data_vencimento      :date
#  data_pagamento       :date
#  periodo              :string(255)      default("NÃO MENCIONADO")
#  status_pagamento     :string(255)      default("A PAGAR")
#  parcela              :integer
#

#-*- coding:utf-8 -*-
class Estoque::EstoqueMovimentoParcela < ApplicationRecord
    include Estoque::Parcelamentos::PagamentoConcern

    belongs_to :estoque_movimento, class_name: "Estoque::EstoqueMovimento", foreign_key: "estoque_movimento_id"
    validates :valor, presence: {  message: I18n.t('estoques.messages.valor_total_must_be_present') }
    validates :parcela, presence: {  message: I18n.t('estoques.messages.parcelas_must_be_present') }

    class << self
        def build_parcelamento(estoque_movimento_id, valor, data_pagamento, periodo, parcelas, diferenca)
            data_acumulada = data_pagamento
            data_inicial   = data_pagamento

            for x in 1..parcelas
                @@parcelamento = self.new(estoque_movimento_id: estoque_movimento_id, valor: valor, periodo: periodo, parcela: x)
                @@parcelamento.data_vencimento = data_acumulada
                case @@parcelamento.periodo.downcase
                    when 'semanal'
                        if @@parcelamento.data_vencimento.day.eql?(30) and data_acumulada.month.eql?(2) and data_acumulada.day.eql?(28)
                            data_acumulada = data_acumulada.advance days: 2
                        else
                            data_acumulada = data_acumulada.advance days: self::Periodo.semanal()
                        end
                    when 'quinzenal'
                        data_acumulada = data_acumulada.advance days: self::Periodo.quinzenal()
                    when 'mensal'
                        if data_inicial.day.eql?(29) and @@parcelamento.data_vencimento.day.eql?(28) and @@parcelamento.data_vencimento.month.eql?(2)
                            data_acumulada = data_acumulada.advance days: 29
                        elsif data_inicial.day.eql?(30) and @@parcelamento.data_vencimento.day.eql?(28) and @@parcelamento.data_vencimento.month.eql?(2)
                            data_acumulada = data_acumulada.advance days: 30
                        elsif data_inicial.day.eql?(31) and @@parcelamento.data_vencimento.day.eql?(28) and @@parcelamento.data_vencimento.month.eql?(2)
                            data_acumulada = data_acumulada.advance days: 31
                        else
                            data_acumulada = data_acumulada.advance months: self::Periodo.mensal()
                        end
                    when 'anual'
                        data_acumulada = data_acumulada.advance years: self::Periodo.anual()
                end

                if @@parcelamento.save
                    true
                else
                    false
                end
            end

            _valor_ultima_parcela=(valor - diferenca)
            @estoque = Estoque::EstoqueMovimento.find(estoque_movimento_id)
            @estoque.estoque_movimento_parcelas.last.update_attributes(valor: _valor_ultima_parcela)
        end
    end
end
