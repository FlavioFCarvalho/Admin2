# == Schema Information
#
# Table name: user_profiles
#
#  id         :integer          not null, primary key
#  name       :string(255)
#  user_id    :integer
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class UserProfile < ApplicationRecord
  belongs_to :user

  validates :name, length: { minimum: 4 }
  validates :name, presence: true
  validates :name, uniqueness: true
  validates_associated :user
end
