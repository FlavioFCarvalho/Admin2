module ClienteFornecedorModelConcern
  extend ActiveSupport::Concern

  included do
    after_initialize :set_default_status, if: :new_record?
  end

  # => adiciona serviços ao cliente_fornecedor
  def build_services(resource)
    resource.each do |_key, value|
      self.fornecedor_services.build(service_id: value[:service_id])
    end
  end

  # => adiciona produtos ao cliente_fornecedor
  def build_products(resource)
    resource.each do |_key, value|
      self.fornecedor_products.build(produto_id: value[:product_id])
    end
  end

  # => adiciona produtos ao cliente_fornecedor
  def build_works(resource)
    resource.each do |_key, value|
      self.cliente_works.build(obra_id: value[:obra_id])
    end
  end

  def build_bank_accounts(resource)
    resource.each do |_key, value|
      if value[:id] != ''
        @conta = self.contas_bancaria.find(value[:id])
        @conta.update_attributes(agencia: value[:agencia], banco_id: value[:banco_id], conta: value[:conta], tipo: value[:tipo])
      else
        self.contas_bancaria.build(agencia: value[:agencia], banco_id: value[:banco_id], conta: value[:conta], tipo: value[:tipo])
      end
    end
  end

  private
    def set_default_status
      self.status = "Ativo"
    end

    def cliente_fornecedor_not_present?
      return !self.cliente_fornecedor.present?
    end

    def name_not_present?
      return !self.nome.present?
    end

    def tipo_pessoa_not_present?
      return !self.tipo_pessoa.present?
    end
end
