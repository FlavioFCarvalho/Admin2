'''
  MODULO RESPONSAVEL APENAS POR CONTROLAR
  FILTRO TODOS DO ESTOQUE
'''

module Estoque::Filtros::TodosConcern
  extend ActiveSupport::Concern

  included do
    class << self
      def search_todos_pelo_produto(resource)
        find_by_sql(
          "SELECT DISTINCT `estoque_estoque_movimentos`.id,
              `estoque_estoque_movimentos`.tipo_movimentacao,
              `estoque_estoque_movimentos`.fornecedor_id,
              `estoque_estoque_movimentos`.data_entrada,
              `estoque_estoque_movimentos`.data_vencimento_entrada,
              `estoque_estoque_movimentos`.data_devolucao,
              `estoque_estoque_movimentos`.data_transferencia,
              `estoque_estoque_movimentos`.transferencia_tipo,
              `estoque_estoque_produtos`.produto_id,
              `estoque_estoque_produtos`.quantidade
            FROM `estoque_estoque_movimentos`
              INNER JOIN `estoque_estoque_produtos`
                ON `estoque_estoque_produtos`.`estoque_movimento_id` = `estoque_estoque_movimentos`.`id`
            WHERE   estoque_estoque_movimentos.tipo_movimentacao = 'Entrada'
              AND `estoque_estoque_produtos`.produto_id='#{resource[:produto_id_eq]}'
              AND (`estoque_estoque_movimentos`.`data_entrada` BETWEEN '#{resource[:data_entrada_eq].to_date}' AND '#{resource[:data_vencimento_entrada_eq].to_date}')
          UNION
            SELECT DISTINCT `estoque_estoque_movimentos`.id,
              `estoque_estoque_movimentos`.tipo_movimentacao,
              `estoque_estoque_movimentos`.fornecedor_id,
              `estoque_estoque_movimentos`.data_entrada,
              `estoque_estoque_movimentos`.data_vencimento_entrada,
              `estoque_estoque_movimentos`.data_devolucao,
              `estoque_estoque_movimentos`.data_transferencia,
              `estoque_estoque_movimentos`.transferencia_tipo,
              `estoque_estoque_produtos`.produto_id,
              `estoque_estoque_produtos`.quantidade
            FROM `estoque_estoque_movimentos`
              INNER JOIN `estoque_estoque_produtos`
                ON `estoque_estoque_produtos`.`estoque_movimento_id` = `estoque_estoque_movimentos`.`id`
            WHERE   estoque_estoque_movimentos.tipo_movimentacao = 'Devolução para Fornecedor'
              AND `estoque_estoque_produtos`.produto_id='#{resource[:produto_id_eq]}'
              AND (`estoque_estoque_movimentos`.`data_devolucao` BETWEEN '#{resource[:data_entrada_eq].to_date}' AND '#{resource[:data_vencimento_entrada_eq].to_date}')
          UNION
            SELECT DISTINCT `estoque_estoque_movimentos`.id,
              `estoque_estoque_movimentos`.tipo_movimentacao,
              `estoque_estoque_movimentos`.fornecedor_id,
              `estoque_estoque_movimentos`.data_entrada,
              `estoque_estoque_movimentos`.data_vencimento_entrada,
              `estoque_estoque_movimentos`.data_devolucao,
              `estoque_estoque_movimentos`.data_transferencia,
              `estoque_estoque_movimentos`.transferencia_tipo,
              `estoque_estoque_transferencias`.produto_id,
              `estoque_estoque_transferencias`.quantidade
            FROM `estoque_estoque_movimentos`
              INNER JOIN `estoque_estoque_transferencias`
                ON `estoque_estoque_transferencias`.`estoque_movimento_id` = `estoque_estoque_movimentos`.`id`
            WHERE estoque_estoque_movimentos.tipo_movimentacao = 'Transferência'
              AND `estoque_estoque_transferencias`.produto_id='#{resource[:produto_id_eq]}'
              AND (`estoque_estoque_movimentos`.`data_transferencia` BETWEEN '#{resource[:data_entrada_eq].to_date}' AND '#{resource[:data_vencimento_entrada_eq].to_date}')"
        )
      end

      def search_todos_pelo_produto_e_obra(resource)
        find_by_sql(
          "SELECT DISTINCT `estoque_estoque_movimentos`.id,
              `estoque_estoque_movimentos`.tipo_movimentacao,
              `estoque_estoque_movimentos`.fornecedor_id,
              `estoque_estoque_movimentos`.data_entrada,
              `estoque_estoque_movimentos`.data_vencimento_entrada,
              `estoque_estoque_movimentos`.data_devolucao,
              `estoque_estoque_movimentos`.data_transferencia,
              `estoque_estoque_movimentos`.transferencia_tipo,
              `estoque_estoque_produtos`.produto_id,
              `estoque_estoque_produtos`.quantidade
            FROM `estoque_estoque_movimentos`
              INNER JOIN `estoque_estoque_produtos`
                ON `estoque_estoque_produtos`.`estoque_movimento_id` = `estoque_estoque_movimentos`.`id`
            WHERE   estoque_estoque_movimentos.tipo_movimentacao = 'Entrada'
              AND `estoque_estoque_produtos`.produto_id='#{resource[:produto_id_eq]}'
              AND `estoque_estoque_produtos`.obra_id='#{resource[:obra_id_eq]}'
              AND (`estoque_estoque_movimentos`.`data_entrada` BETWEEN '#{resource[:data_entrada_eq].to_date}' AND '#{resource[:data_vencimento_entrada_eq].to_date}')
          UNION
            SELECT DISTINCT `estoque_estoque_movimentos`.id,
              `estoque_estoque_movimentos`.tipo_movimentacao,
              `estoque_estoque_movimentos`.fornecedor_id,
              `estoque_estoque_movimentos`.data_entrada,
              `estoque_estoque_movimentos`.data_vencimento_entrada,
              `estoque_estoque_movimentos`.data_devolucao,
              `estoque_estoque_movimentos`.data_transferencia,
              `estoque_estoque_movimentos`.transferencia_tipo,
              `estoque_estoque_produtos`.produto_id,
              `estoque_estoque_produtos`.quantidade
            FROM `estoque_estoque_movimentos`
              INNER JOIN `estoque_estoque_produtos`
                ON `estoque_estoque_produtos`.`estoque_movimento_id` = `estoque_estoque_movimentos`.`id`
            WHERE   estoque_estoque_movimentos.tipo_movimentacao = 'Devolução para Fornecedor'
              AND `estoque_estoque_produtos`.produto_id='#{resource[:produto_id_eq]}'
              AND `estoque_estoque_produtos`.obra_id='#{resource[:obra_id_eq]}'
              AND (`estoque_estoque_movimentos`.`data_devolucao` BETWEEN '#{resource[:data_entrada_eq].to_date}' AND '#{resource[:data_vencimento_entrada_eq].to_date}')
          UNION
            SELECT DISTINCT `estoque_estoque_movimentos`.id,
              `estoque_estoque_movimentos`.tipo_movimentacao,
              `estoque_estoque_movimentos`.fornecedor_id,
              `estoque_estoque_movimentos`.data_entrada,
              `estoque_estoque_movimentos`.data_vencimento_entrada,
              `estoque_estoque_movimentos`.data_devolucao,
              `estoque_estoque_movimentos`.data_transferencia,
              `estoque_estoque_movimentos`.transferencia_tipo,
              `estoque_estoque_transferencias`.produto_id,
              `estoque_estoque_transferencias`.quantidade
            FROM `estoque_estoque_movimentos`
              INNER JOIN `estoque_estoque_transferencias`
                ON `estoque_estoque_transferencias`.`estoque_movimento_id` = `estoque_estoque_movimentos`.`id`
            WHERE estoque_estoque_movimentos.tipo_movimentacao = 'Transferência'
              AND `estoque_estoque_transferencias`.produto_id='#{resource[:produto_id_eq]}'
              AND `estoque_estoque_transferencias`.obra_anterior_id='#{resource[:obra_id_eq]}' OR `estoque_estoque_transferencias`.obra_posterior_id='#{resource[:obra_id_eq]}'
              AND (`estoque_estoque_movimentos`.`data_transferencia` BETWEEN '#{resource[:data_entrada_eq].to_date}' AND '#{resource[:data_vencimento_entrada_eq].to_date}')"
        )
      end

      def search_todos_pela_obra(resource)
        find_by_sql(
          "SELECT DISTINCT `estoque_estoque_movimentos`.id,
              `estoque_estoque_movimentos`.tipo_movimentacao,
              `estoque_estoque_movimentos`.fornecedor_id,
              `estoque_estoque_movimentos`.data_entrada,
              `estoque_estoque_movimentos`.data_vencimento_entrada,
              `estoque_estoque_movimentos`.data_devolucao,
              `estoque_estoque_movimentos`.data_transferencia,
              `estoque_estoque_movimentos`.transferencia_tipo,
              `estoque_estoque_produtos`.produto_id,
              `estoque_estoque_produtos`.quantidade
            FROM `estoque_estoque_movimentos`
              INNER JOIN `estoque_estoque_produtos`
                ON `estoque_estoque_produtos`.`estoque_movimento_id` = `estoque_estoque_movimentos`.`id`
            WHERE   estoque_estoque_movimentos.tipo_movimentacao = 'Entrada'
              AND `estoque_estoque_produtos`.obra_id='#{resource[:obra_id_eq]}'
              AND (`estoque_estoque_movimentos`.`data_entrada` BETWEEN '#{resource[:data_entrada_eq].to_date}' AND '#{resource[:data_vencimento_entrada_eq].to_date}')
          UNION
            SELECT DISTINCT `estoque_estoque_movimentos`.id,
              `estoque_estoque_movimentos`.tipo_movimentacao,
              `estoque_estoque_movimentos`.fornecedor_id,
              `estoque_estoque_movimentos`.data_entrada,
              `estoque_estoque_movimentos`.data_vencimento_entrada,
              `estoque_estoque_movimentos`.data_devolucao,
              `estoque_estoque_movimentos`.data_transferencia,
              `estoque_estoque_movimentos`.transferencia_tipo,
              `estoque_estoque_produtos`.produto_id,
              `estoque_estoque_produtos`.quantidade
            FROM `estoque_estoque_movimentos`
              INNER JOIN `estoque_estoque_produtos`
                ON `estoque_estoque_produtos`.`estoque_movimento_id` = `estoque_estoque_movimentos`.`id`
            WHERE   estoque_estoque_movimentos.tipo_movimentacao = 'Devolução para Fornecedor'
              AND `estoque_estoque_produtos`.obra_id='#{resource[:obra_id_eq]}'
              AND (`estoque_estoque_movimentos`.`data_devolucao` BETWEEN '#{resource[:data_entrada_eq].to_date}' AND '#{resource[:data_vencimento_entrada_eq].to_date}')
          UNION
            SELECT DISTINCT `estoque_estoque_movimentos`.id,
              `estoque_estoque_movimentos`.tipo_movimentacao,
              `estoque_estoque_movimentos`.fornecedor_id,
              `estoque_estoque_movimentos`.data_entrada,
              `estoque_estoque_movimentos`.data_vencimento_entrada,
              `estoque_estoque_movimentos`.data_devolucao,
              `estoque_estoque_movimentos`.data_transferencia,
              `estoque_estoque_movimentos`.transferencia_tipo,
              `estoque_estoque_transferencias`.produto_id,
              `estoque_estoque_transferencias`.quantidade
            FROM `estoque_estoque_movimentos`
              INNER JOIN `estoque_estoque_transferencias`
                ON `estoque_estoque_transferencias`.`estoque_movimento_id` = `estoque_estoque_movimentos`.`id`
            WHERE estoque_estoque_movimentos.tipo_movimentacao = 'Transferência'
              AND `estoque_estoque_transferencias`.obra_anterior_id='#{resource[:obra_id_eq]}' OR `estoque_estoque_transferencias`.obra_posterior_id='#{resource[:obra_id_eq]}'
              AND (`estoque_estoque_movimentos`.`data_transferencia` BETWEEN '#{resource[:data_entrada_eq].to_date}' AND '#{resource[:data_vencimento_entrada_eq].to_date}')"
        )
      end

      def search_todos_pelo_fornecedor(resource)
        find_by_sql(
          "SELECT DISTINCT `estoque_estoque_movimentos`.id,
              `estoque_estoque_movimentos`.tipo_movimentacao,
              `estoque_estoque_movimentos`.fornecedor_id,
              `estoque_estoque_movimentos`.data_entrada,
              `estoque_estoque_movimentos`.data_vencimento_entrada,
              `estoque_estoque_movimentos`.data_devolucao,
              `estoque_estoque_movimentos`.data_transferencia,
              `estoque_estoque_movimentos`.transferencia_tipo,
              `estoque_estoque_produtos`.produto_id,
              `estoque_estoque_produtos`.quantidade
            FROM `estoque_estoque_movimentos`
              INNER JOIN `estoque_estoque_produtos`
                ON `estoque_estoque_produtos`.`estoque_movimento_id` = `estoque_estoque_movimentos`.`id`
            WHERE   estoque_estoque_movimentos.tipo_movimentacao = 'Entrada'
              AND estoque_estoque_movimentos.fornecedor_id='#{resource[:fornecedor_id_eq]}'
              AND (`estoque_estoque_movimentos`.`data_entrada` BETWEEN '#{resource[:data_entrada_eq].to_date}' AND '#{resource[:data_vencimento_entrada_eq].to_date}')
          UNION
            SELECT DISTINCT `estoque_estoque_movimentos`.id,
              `estoque_estoque_movimentos`.tipo_movimentacao,
              `estoque_estoque_movimentos`.fornecedor_id,
              `estoque_estoque_movimentos`.data_entrada,
              `estoque_estoque_movimentos`.data_vencimento_entrada,
              `estoque_estoque_movimentos`.data_devolucao,
              `estoque_estoque_movimentos`.data_transferencia,
              `estoque_estoque_movimentos`.transferencia_tipo,
              `estoque_estoque_produtos`.produto_id,
              `estoque_estoque_produtos`.quantidade
            FROM `estoque_estoque_movimentos`
              INNER JOIN `estoque_estoque_produtos`
                ON `estoque_estoque_produtos`.`estoque_movimento_id` = `estoque_estoque_movimentos`.`id`
            WHERE   estoque_estoque_movimentos.tipo_movimentacao = 'Devolução para Fornecedor'
              AND estoque_estoque_movimentos.fornecedor_id='#{resource[:fornecedor_id_eq]}'
              AND (`estoque_estoque_movimentos`.`data_devolucao` BETWEEN '#{resource[:data_entrada_eq].to_date}' AND '#{resource[:data_vencimento_entrada_eq].to_date}')"
        )
      end

      def search_todos_pelo_fornecedor_com_obra(resource)
        find_by_sql(
          "SELECT DISTINCT `estoque_estoque_movimentos`.id,
              `estoque_estoque_movimentos`.tipo_movimentacao,
              `estoque_estoque_movimentos`.fornecedor_id,
              `estoque_estoque_movimentos`.data_entrada,
              `estoque_estoque_movimentos`.data_vencimento_entrada,
              `estoque_estoque_movimentos`.data_devolucao,
              `estoque_estoque_movimentos`.data_transferencia,
              `estoque_estoque_movimentos`.transferencia_tipo,
              `estoque_estoque_produtos`.produto_id,
              `estoque_estoque_produtos`.quantidade
            FROM `estoque_estoque_movimentos`
              INNER JOIN `estoque_estoque_produtos`
                ON `estoque_estoque_produtos`.`estoque_movimento_id` = `estoque_estoque_movimentos`.`id`
            WHERE   estoque_estoque_movimentos.tipo_movimentacao = 'Entrada'
              AND estoque_estoque_movimentos.fornecedor_id='#{resource[:fornecedor_id_eq]}'
              AND `estoque_estoque_produtos`.obra_id='#{resource[:obra_id_eq]}'
              AND (`estoque_estoque_movimentos`.`data_entrada` BETWEEN '#{resource[:data_entrada_eq].to_date}' AND '#{resource[:data_vencimento_entrada_eq].to_date}')
          UNION
            SELECT DISTINCT `estoque_estoque_movimentos`.id,
              `estoque_estoque_movimentos`.tipo_movimentacao,
              `estoque_estoque_movimentos`.fornecedor_id,
              `estoque_estoque_movimentos`.data_entrada,
              `estoque_estoque_movimentos`.data_vencimento_entrada,
              `estoque_estoque_movimentos`.data_devolucao,
              `estoque_estoque_movimentos`.data_transferencia,
              `estoque_estoque_movimentos`.transferencia_tipo,
              `estoque_estoque_produtos`.produto_id,
              `estoque_estoque_produtos`.quantidade
            FROM `estoque_estoque_movimentos`
              INNER JOIN `estoque_estoque_produtos`
                ON `estoque_estoque_produtos`.`estoque_movimento_id` = `estoque_estoque_movimentos`.`id`
            WHERE   estoque_estoque_movimentos.tipo_movimentacao = 'Devolução para Fornecedor'
              AND estoque_estoque_movimentos.fornecedor_id='#{resource[:fornecedor_id_eq]}'
              AND `estoque_estoque_produtos`.obra_id='#{resource[:obra_id_eq]}'
              AND (`estoque_estoque_movimentos`.`data_devolucao` BETWEEN '#{resource[:data_entrada_eq].to_date}' AND '#{resource[:data_vencimento_entrada_eq].to_date}')"
        )
      end

      def search_todos(resource)
        find_by_sql(
          "SELECT DISTINCT `estoque_estoque_movimentos`.id,
              `estoque_estoque_movimentos`.tipo_movimentacao,
              `estoque_estoque_movimentos`.fornecedor_id,
              `estoque_estoque_movimentos`.data_entrada,
              `estoque_estoque_movimentos`.data_vencimento_entrada,
              `estoque_estoque_movimentos`.data_devolucao,
              `estoque_estoque_movimentos`.data_transferencia,
              `estoque_estoque_movimentos`.transferencia_tipo,
              `estoque_estoque_produtos`.produto_id,
              `estoque_estoque_produtos`.quantidade
            FROM `estoque_estoque_movimentos`
              INNER JOIN `estoque_estoque_produtos`
                ON `estoque_estoque_produtos`.`estoque_movimento_id` = `estoque_estoque_movimentos`.`id`
            WHERE   estoque_estoque_movimentos.tipo_movimentacao = 'Entrada'
              AND estoque_estoque_movimentos.fornecedor_id='#{resource[:fornecedor_id_eq]}'
              AND `estoque_estoque_produtos`.obra_id='#{resource[:obra_id_eq]}'
              AND `estoque_estoque_produtos`.produto_id='#{resource[:produto_id_eq]}'
              AND (`estoque_estoque_movimentos`.`data_entrada` BETWEEN '#{resource[:data_entrada_eq].to_date}' AND '#{resource[:data_vencimento_entrada_eq].to_date}')
          UNION
            SELECT DISTINCT `estoque_estoque_movimentos`.id,
              `estoque_estoque_movimentos`.tipo_movimentacao,
              `estoque_estoque_movimentos`.fornecedor_id,
              `estoque_estoque_movimentos`.data_entrada,
              `estoque_estoque_movimentos`.data_vencimento_entrada,
              `estoque_estoque_movimentos`.data_devolucao,
              `estoque_estoque_movimentos`.data_transferencia,
              `estoque_estoque_movimentos`.transferencia_tipo,
              `estoque_estoque_produtos`.produto_id,
              `estoque_estoque_produtos`.quantidade
            FROM `estoque_estoque_movimentos`
              INNER JOIN `estoque_estoque_produtos`
                ON `estoque_estoque_produtos`.`estoque_movimento_id` = `estoque_estoque_movimentos`.`id`
            WHERE   estoque_estoque_movimentos.tipo_movimentacao = 'Devolução para Fornecedor'
              AND estoque_estoque_movimentos.fornecedor_id='#{resource[:fornecedor_id_eq]}'
              AND `estoque_estoque_produtos`.obra_id='#{resource[:obra_id_eq]}'
              AND `estoque_estoque_produtos`.produto_id='#{resource[:produto_id_eq]}'
              AND (`estoque_estoque_movimentos`.`data_devolucao` BETWEEN '#{resource[:data_entrada_eq].to_date}' AND '#{resource[:data_vencimento_entrada_eq].to_date}')"
        )
      end

      def search_todos_fornecedor_e_produto(resource)
        find_by_sql(
          "SELECT DISTINCT `estoque_estoque_movimentos`.id,
              `estoque_estoque_movimentos`.tipo_movimentacao,
              `estoque_estoque_movimentos`.fornecedor_id,
              `estoque_estoque_movimentos`.data_entrada,
              `estoque_estoque_movimentos`.data_vencimento_entrada,
              `estoque_estoque_movimentos`.data_devolucao,
              `estoque_estoque_movimentos`.data_transferencia,
              `estoque_estoque_movimentos`.transferencia_tipo,
              `estoque_estoque_produtos`.produto_id,
              `estoque_estoque_produtos`.quantidade
            FROM `estoque_estoque_movimentos`
              INNER JOIN `estoque_estoque_produtos`
                ON `estoque_estoque_produtos`.`estoque_movimento_id` = `estoque_estoque_movimentos`.`id`
            WHERE   estoque_estoque_movimentos.tipo_movimentacao = 'Entrada'
              AND estoque_estoque_movimentos.fornecedor_id='#{resource[:fornecedor_id_eq]}'
              AND `estoque_estoque_produtos`.produto_id='#{resource[:produto_id_eq]}'
              AND (`estoque_estoque_movimentos`.`data_entrada` BETWEEN '#{resource[:data_entrada_eq].to_date}' AND '#{resource[:data_vencimento_entrada_eq].to_date}')
          UNION
            SELECT DISTINCT `estoque_estoque_movimentos`.id,
              `estoque_estoque_movimentos`.tipo_movimentacao,
              `estoque_estoque_movimentos`.fornecedor_id,
              `estoque_estoque_movimentos`.data_entrada,
              `estoque_estoque_movimentos`.data_vencimento_entrada,
              `estoque_estoque_movimentos`.data_devolucao,
              `estoque_estoque_movimentos`.data_transferencia,
              `estoque_estoque_movimentos`.transferencia_tipo,
              `estoque_estoque_produtos`.produto_id,
              `estoque_estoque_produtos`.quantidade
            FROM `estoque_estoque_movimentos`
              INNER JOIN `estoque_estoque_produtos`
                ON `estoque_estoque_produtos`.`estoque_movimento_id` = `estoque_estoque_movimentos`.`id`
            WHERE   estoque_estoque_movimentos.tipo_movimentacao = 'Devolução para Fornecedor'
              AND estoque_estoque_movimentos.fornecedor_id='#{resource[:fornecedor_id_eq]}'
              AND `estoque_estoque_produtos`.produto_id='#{resource[:produto_id_eq]}'
              AND (`estoque_estoque_movimentos`.`data_devolucao` BETWEEN '#{resource[:data_entrada_eq].to_date}' AND '#{resource[:data_vencimento_entrada_eq].to_date}')"
        )
      end
    end
  end
end
