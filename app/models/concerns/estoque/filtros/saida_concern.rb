'''
  MODULO RESPONSAVEL APENAS POR CONTROLAR
  FILTRO DE SAIDA DO ESTOQUE
'''

module Estoque::Filtros::SaidaConcern
  extend ActiveSupport::Concern

  included do
    class << self
      # -------------------------- INICIO SAIDA -------------------------------------
      # => 1º Possibilidade de Filtro
      def search_saida_completa(resource)
        where(tipo_movimentacao: resource[:tipo_movimentacao_eq]).
        where(data_saida: Date.parse(resource[:data_saida_eq])..Date.parse(resource[:data_vencimento_saida_eq])).
        joins(:estoque_produtos).
        where("estoque_estoque_produtos.obra_id = ?", "#{resource[:obra_id_eq]}").
        where("estoque_estoque_produtos.produto_id = ?", "#{resource[:produto_id_eq]}").distinct
      end

      # => 2º Possibilidade de Filtro
      def search_saida_com_obra_sem_data_fornecedor_e_produto(resource)
        where(tipo_movimentacao: resource[:tipo_movimentacao_eq]).
        joins(:estoque_produtos).
        where("estoque_estoque_produtos.obra_id = ?", "#{resource[:obra_id_eq]}").distinct
      end

      # => 3º Possibilidade de Filtro
      def search_saida_com_produto_sem_obra_data_e_fornecedor(resource)
        where(tipo_movimentacao: resource[:tipo_movimentacao_eq]).
        joins(:estoque_produtos).
        where("estoque_estoque_produtos.produto_id = ?", "#{resource[:produto_id_eq]}").distinct
      end

      # => 4º Possibilidade de Filtro
      def search_saida_com_data_sem_fornecedor_obra_e_produto(resource)
        where(tipo_movimentacao: resource[:tipo_movimentacao_eq]).
        where(data_saida: Date.parse(resource[:data_saida_eq])..Date.parse(resource[:data_vencimento_saida_eq])).distinct
      end

      # => 5º Possibilidade de Filtro
      def search_saida_com_data_obra_sem_fornecedor_e_produto(resource)
        where(tipo_movimentacao: resource[:tipo_movimentacao_eq]).
        where(data_saida: Date.parse(resource[:data_saida_eq])..Date.parse(resource[:data_vencimento_saida_eq])).
        joins(:estoque_produtos).
        where("estoque_estoque_produtos.obra_id = ?", "#{resource[:obra_id_eq]}").distinct
      end

      # => 6º Possibilidade de Filtro
      def search_saida_data_produto_sem_fornecedor_e_obra(resource)
        where(tipo_movimentacao: resource[:tipo_movimentacao_eq]).
        where(data_saida: Date.parse(resource[:data_saida_eq])..Date.parse(resource[:data_vencimento_saida_eq])).
        joins(:estoque_produtos).
        where("estoque_estoque_produtos.produto_id = ?", "#{resource[:produto_id_eq]}").distinct
      end

      def search_saida_obra_produto_sem_fornecedor_e_data(resource)
        where(tipo_movimentacao: resource[:tipo_movimentacao_eq]).
        joins(:estoque_produtos).
        where("estoque_estoque_produtos.produto_id = ?", "#{resource[:produto_id_eq]}").
        where("estoque_estoque_produtos.obra_id = ?", "#{resource[:obra_id_eq]}").distinct
      end

      def search_saida_obra_produto_sem_data_e_fornecedor(resource)
        where(tipo_movimentacao: resource[:tipo_movimentacao_eq]).
        joins(:estoque_produtos).
        where("estoque_estoque_produtos.produto_id = ?", "#{resource[:produto_id_eq]}").distinct
      end
      # -------------------------- FIM SAIDA -------------------------------------
    end
  end
end
