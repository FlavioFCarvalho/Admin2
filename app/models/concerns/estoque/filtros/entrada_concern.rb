'''
  MODULO RESPONSAVEL APENAS POR CONTROLAR
  FILTRO DE ENTRADA DO ESTOQUE
'''

module Estoque::Filtros::EntradaConcern
  extend ActiveSupport::Concern

  included do
    class << self
      # -------------------------- INICIO ENTRADA -------------------------------------
      # => 1º Possibilidade de Filtro
      def search_entrada_completa(resource)
        where(tipo_movimentacao: resource[:tipo_movimentacao_eq]).
        where(fornecedor_id: resource[:fornecedor_id_eq]).
        where(data_entrada: Date.parse(resource[:data_entrada_eq])..Date.parse(resource[:data_vencimento_entrada_eq])).
        joins(:estoque_produtos).
        where("estoque_estoque_produtos.obra_id = ?", "#{resource[:obra_id_eq]}").
        where("estoque_estoque_produtos.produto_id = ?", "#{resource[:produto_id_eq]}").distinct.order(id: :desc)
      end

      # => 2º Possibilidade de Filtro
      def search_entrada_com_fornecedor_sem_obra_produto_e_data(resource)
        where(tipo_movimentacao: resource[:tipo_movimentacao_eq]).
        where(fornecedor_id: resource[:fornecedor_id_eq]).distinct.order(id: :desc)
      end

      # => 3º Possibilidade de Filtro
      def search_entrada_com_obra_sem_data_fornecedor_e_produto(resource)
        where(tipo_movimentacao: resource[:tipo_movimentacao_eq]).
        joins(:estoque_produtos).
        where("estoque_estoque_produtos.obra_id = ?", "#{resource[:obra_id_eq]}").distinct.order(id: :desc)
      end

      # => 4º Possibilidade de Filtro
      def search_entrada_com_produto_sem_obra_data_e_fornecedor(resource)
        where(tipo_movimentacao: resource[:tipo_movimentacao_eq]).
        joins(:estoque_produtos).
        where("estoque_estoque_produtos.produto_id = ?", "#{resource[:produto_id_eq]}").distinct.order(id: :desc)
      end

      # => 5º Possibilidade de Filtro
      def search_entrada_com_fornecedor_obra_e_sem_produto_e_data(resource)
        where(tipo_movimentacao: resource[:tipo_movimentacao_eq]).
        where(fornecedor_id: resource[:fornecedor_id_eq]).
        joins(:estoque_produtos).
        where("estoque_estoque_produtos.obra_id = ?", "#{resource[:obra_id_eq]}").distinct.order(id: :desc)
      end

      # => 6º Possibilidade de Filtro
      def search_entrada_com_fornecedor_produto_sem_obra_e_data(resource)
        where(tipo_movimentacao: resource[:tipo_movimentacao_eq]).
        where(fornecedor_id: resource[:fornecedor_id_eq]).
        joins(:estoque_produtos).
        where("estoque_estoque_produtos.produto_id = ?", "#{resource[:produto_id_eq]}").distinct.order(id: :desc)
      end

      # => 7º Possibilidade de Filtro
      def search_entrada_com_fornecedor_obra_produto_e_sem_data(resource)
        where(tipo_movimentacao: resource[:tipo_movimentacao_eq]).
        where(fornecedor_id: resource[:fornecedor_id_eq]).
        joins(:estoque_produtos).
        where("estoque_estoque_produtos.obra_id = ?", "#{resource[:obra_id_eq]}").
        where("estoque_estoque_produtos.produto_id = ?", "#{resource[:produto_id_eq]}").distinct.order(id: :desc)
      end

      # => 8º Possibilidade de Filtro
      def search_entrada_com_data_sem_fornecedor_obra_e_produto(resource)
        where(tipo_movimentacao: resource[:tipo_movimentacao_eq]).
        where(data_entrada: Date.parse(resource[:data_entrada_eq])..Date.parse(resource[:data_vencimento_entrada_eq])).distinct.order(id: :desc)
      end

      # => 9º Possibilidade de Filtro
      def search_entrada_com_data_fornecedor_sem_obra_e_produto(resource)
        where(tipo_movimentacao: resource[:tipo_movimentacao_eq]).
        where(fornecedor_id: resource[:fornecedor_id_eq]).
        where(data_entrada: Date.parse(resource[:data_entrada_eq])..Date.parse(resource[:data_vencimento_entrada_eq])).distinct.order(id: :desc)
      end

      # => 10º Possibilidade de Filtro
      def search_entrada_com_data_obra_sem_fornecedor_e_produto(resource)
        where(tipo_movimentacao: resource[:tipo_movimentacao_eq]).
        where(data_entrada: Date.parse(resource[:data_entrada_eq])..Date.parse(resource[:data_vencimento_entrada_eq])).
        joins(:estoque_produtos).
        where("estoque_estoque_produtos.obra_id = ?", "#{resource[:obra_id_eq]}").distinct.order(id: :desc)
      end

      # => 11º Possibilidade de Filtro
      def search_entrada_data_produto_sem_fornecedor_e_obra(resource)
        where(tipo_movimentacao: resource[:tipo_movimentacao_eq]).
        where(data_entrada: Date.parse(resource[:data_entrada_eq])..Date.parse(resource[:data_vencimento_entrada_eq])).
        joins(:estoque_produtos).
        where("estoque_estoque_produtos.produto_id = ?", "#{resource[:produto_id_eq]}").distinct.order(id: :desc)
      end

      # => 12º Possibilidade de Filtro
      def search_entrada_data_fornecedor_obra_sem_produto(resource)
        where(tipo_movimentacao: resource[:tipo_movimentacao_eq]).
        where(fornecedor_id: resource[:fornecedor_id_eq]).
        where(data_entrada: Date.parse(resource[:data_entrada_eq])..Date.parse(resource[:data_vencimento_entrada_eq])).
        joins(:estoque_produtos).
        where("estoque_estoque_produtos.obra_id = ?", "#{resource[:obra_id_eq]}").distinct.order(id: :desc)
      end

      # => 13º Possibilidade de Filtro
      def search_entrada_data_fornecedor_produto_sem_obra(resource)
        where(tipo_movimentacao: resource[:tipo_movimentacao_eq]).
        where(fornecedor_id: resource[:fornecedor_id_eq]).
        where(data_entrada: Date.parse(resource[:data_entrada_eq])..Date.parse(resource[:data_vencimento_entrada_eq])).
        joins(:estoque_produtos).
        where("estoque_estoque_produtos.produto_id = ?", "#{resource[:produto_id_eq]}").distinct.order(id: :desc)
      end

      # => 14º Possibilidade de Filtro
      def search_entrada_obra_produto_sem_data_e_fornecedor(resource)
        where(tipo_movimentacao: resource[:tipo_movimentacao_eq]).
        joins(:estoque_produtos).
        where("estoque_estoque_produtos.obra_id = ?", "#{resource[:obra_id_eq]}").
        where("estoque_estoque_produtos.produto_id = ?", "#{resource[:produto_id_eq]}").distinct.order(id: :desc)
      end
      # -------------------------- FIM ENTRADA -------------------------------------
    end
  end
end
