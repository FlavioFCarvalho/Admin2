'''
  MODULO RESPONSAVEL APENAS POR CONTROLAR
  FILTRO DE TRANSFERENCIA DE ENTRADA DO ESTOQUE
'''

module Estoque::Filtros::TransferenciaEntradaConcern
  extend ActiveSupport::Concern

  included do
    class << self
      def search_transferencia_entrada(resource)
        where(transferencia_tipo: "ENTRADA").distinct.order(id: :desc)
        # where(data_entrada: Date.parse(resource[:data_entrada_eq])..Date.parse(resource[:data_vencimento_entrada_eq]))
        # where(fornecedor_id: resource[:fornecedor_id_eq]).
        # joins(:estoque_produtos).
        # where("estoque_estoque_produtos.obra_id = ?", "#{resource[:obra_id_eq]}").
        # where("estoque_estoque_produtos.produto_id = ?", "#{resource[:produto_id_eq]}").distinct.order(id: :desc)
      end

      def search_transferencia_com_data(resource)
        where(transferencia_tipo: "ENTRADA").
        where(data_transferencia: Date.parse(resource[:data_entrada_eq])..Date.parse(resource[:data_vencimento_entrada_eq])).distinct.order(id: :desc)
      end

      def search_transferencia_com_obra(resource)
        where(transferencia_tipo: "ENTRADA").
        joins(:estoque_transferencias).
        where("estoque_estoque_transferencias.obra_anterior_id = ? OR estoque_estoque_transferencias.obra_posterior_id = ?", "#{resource[:obra_id_eq]}" , "#{resource[:obra_id_eq]}").distinct.order(id: :desc)
      end

      def search_transferencia_com_produto(resource)
        where(transferencia_tipo: "ENTRADA").
        joins(:estoque_transferencias).
        where("estoque_estoque_transferencias.produto_id = ?", "#{resource[:produto_id_eq]}").distinct.order(id: :desc)
      end

      def search_transferencia_com_obra_e_data(resource)
        where(transferencia_tipo: "ENTRADA").
        joins(:estoque_transferencias).
        where(data_transferencia: Date.parse(resource[:data_entrada_eq])..Date.parse(resource[:data_vencimento_entrada_eq])).
        where("estoque_estoque_transferencias.obra_anterior_id = ? OR estoque_estoque_transferencias.obra_posterior_id = ?", "#{resource[:obra_id_eq]}" , "#{resource[:obra_id_eq]}").distinct.order(id: :desc)
      end

      def search_transferencia_com_obra_e_produto(resource)
        where(transferencia_tipo: "ENTRADA").
        joins(:estoque_transferencias).
        where("estoque_estoque_transferencias.produto_id = ?", "#{resource[:produto_id_eq]}").
        where("estoque_estoque_transferencias.obra_anterior_id = ? OR estoque_estoque_transferencias.obra_posterior_id = ?", "#{resource[:obra_id_eq]}" , "#{resource[:obra_id_eq]}").distinct.order(id: :desc)
      end

      def search_transferencia_entrada_completa(resource)
        where(transferencia_tipo: "ENTRADA").
        joins(:estoque_transferencias).
        where(data_transferencia: Date.parse(resource[:data_entrada_eq])..Date.parse(resource[:data_vencimento_entrada_eq])).
        where("estoque_estoque_transferencias.produto_id = ?", "#{resource[:produto_id_eq]}").
        where("estoque_estoque_transferencias.obra_anterior_id = ? OR estoque_estoque_transferencias.obra_posterior_id = ?", "#{resource[:obra_id_eq]}" , "#{resource[:obra_id_eq]}").distinct.order(id: :desc)
      end
    end
  end
end
