module Estoque::Parcelamentos::PagamentoConcern
    extend ActiveSupport::Concern

    included do
        module Periodo
            def Periodo.semanal()
                return 7
            end

            def Periodo.quinzenal()
                return 15
            end

            def Periodo.mensal()
                return 1
            end

            def Periodo.anual()
                return 1
            end
        end
    end
end