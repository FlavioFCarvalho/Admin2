module Estoque::Produtos::BuildConcern
  extend ActiveSupport::Concern

  included do
    ZERO=0

    def verify_last_stock
      if Estoque::EstoqueProduto.where(produto_id: self.produto_id).last
        @ultimo_estoque_deste_produto = Estoque::EstoqueProduto.where(produto_id: self.produto_id).last
        self.quantidade_anterior = @ultimo_estoque_deste_produto.quantidade_atual
      else
        self.quantidade_anterior = 0
      end
    end

    def add_or_remove_through_stock_moviment
      case self.estoque_movimento.tipo_movimentacao
        when "Entrada"
          add_quantity_to_estoque_produto
        when "Devolução para Fornecedor"
          remove_item_from_estoque_produto
        when "Descarte"
          remove_item_from_estoque_produto
        when "Transferência"
          add_quantity_to_estoque_produto
      end
    end

    def add_quantity_to_estoque_produto
      if !self.quantidade_movimento.nil?
        if self.quantidade_anterior == 0
          self.quantidade_atual = self.quantidade_movimento
        else
          self.quantidade_atual = (self.quantidade_anterior + self.quantidade_movimento)
        end
      end
    end

    def remove_item_from_estoque_produto
      self.quantidade_atual = (self.quantidade_anterior - self.quantidade_movimento) if !self.quantidade_movimento.nil?
    end

    def save_product_at_provider_if_not_exists
      @fornecedor_produto = FornecedorProduto.find_by(produto_id: self.produto_id, fornecedor_id: self.estoque_movimento.fornecedor_id)
      if @fornecedor_produto == nil
        @fornecedor_produto = FornecedorProduto.new
        @fornecedor_produto.produto_id = self.produto_id
        @fornecedor_produto.fornecedor_id = self.estoque_movimento.fornecedor_id
        @fornecedor_produto.save
      end
    end

    def verify_changes_on_main_fields(resource)
      self.produto_id=resource[:produto_id]
      self.obra_id=resource[:obra_id]
      self.item_obra_id=resource[:item_obra_id]
      self.sub_item_obra_id=resource[:sub_item_obra_id]
      self.quantidade_movimento=resource[:quantidade_movimento]

      if self.estoque_movimento.estoque_produtos.last.quantidade_anterior.eql?(0)
        self.quantidade_atual=resource[:quantidade_movimento]
      else
        self.quantidade_atual = self.quantidade_anterior + self.quantidade_movimento
      end
    end

    def simula_valor_total_de_produtos(_total_)
      self.valor_produto = _total_.gsub('.','').gsub(',','.').to_f
      self.valor_total = (self.valor_produto * self.quantidade_movimento)
    end

    class << self
      def build_parcelamento_from_source(source, qtParcelas)
        @estoque_produto = source
        @estoque_produto.update_attributes(parcelas: qtParcelas)
        @estoque_produto.estoque_produto_parcelamentos.delete_all

        data_acumulada = @estoque_produto.estoque_movimento.data_vencimento_entrada unless @estoque_produto.estoque_movimento.data_vencimento_entrada.nil?
        data_acumulada = @estoque_produto.estoque_movimento.data_devolucao unless @estoque_produto.estoque_movimento.data_devolucao.nil?
        data_acumulada = @estoque_produto.estoque_movimento.data_transferencia unless @estoque_produto.estoque_movimento.data_transferencia.nil?
        
        data_inicial   = data_acumulada 

        @valor_parcial  = Estoque::EstoqueParcelamento.divide_valor_proporcional(@estoque_produto.valor_total, qtParcelas)
        _total_parcial=(@valor_parcial.round(2) * qtParcelas)

        for x in 1..qtParcelas
          @@parcelamento = @estoque_produto.estoque_produto_parcelamentos.build(estoque_movimento_id: @estoque_produto.estoque_movimento_id, valor: @valor_parcial, periodo: @estoque_produto.estoque_movimento.periodo, parcela: x)
          @@parcelamento.data_vencimento = data_acumulada

          case @@parcelamento.periodo.downcase
            when 'semanal'
              if @@parcelamento.data_vencimento.day.eql?(30) and data_acumulada.month.eql?(2) and data_acumulada.day.eql?(28)
                data_acumulada = data_acumulada.advance days: 2
              else
                data_acumulada = data_acumulada.advance days: self::Periodo.semanal()
              end
            when 'quinzenal'
              data_acumulada = data_acumulada.advance days: self::Periodo.quinzenal()
            when 'mensal'
              if data_inicial.day.eql?(29) and @@parcelamento.data_vencimento.day.eql?(28) and @@parcelamento.data_vencimento.month.eql?(2)
                data_acumulada = data_acumulada.advance days: 29
              elsif data_inicial.day.eql?(30) and @@parcelamento.data_vencimento.day.eql?(28) and @@parcelamento.data_vencimento.month.eql?(2)
                data_acumulada = data_acumulada.advance days: 30
              elsif data_inicial.day.eql?(31) and @@parcelamento.data_vencimento.day.eql?(28) and @@parcelamento.data_vencimento.month.eql?(2)
                data_acumulada = data_acumulada.advance days: 31
              else
                data_acumulada = data_acumulada.advance months: self::Periodo.mensal()
              end
            when 'anual'
              data_acumulada = data_acumulada.advance years: self::Periodo.anual()
          end

          if @@parcelamento.save
            true
          else
            false
          end
        end

        if (@estoque_produto.valor_total > _total_parcial)
          _diferenca_de=(_total_parcial - @estoque_produto.valor_total)
        else
          _diferenca_de=(_total_parcial - @estoque_produto.valor_total)
        end

        _valor_diferenciado_parcela=(@valor_parcial - _diferenca_de)
        @estoque_produto.estoque_produto_parcelamentos.first.update_attributes(valor: _valor_diferenciado_parcela)

        return @estoque_produto.estoque_produto_parcelamentos.any?
      end
    end
  end
end