# == Schema Information
#
# Table name: authorizations
#
#  id          :integer          not null, primary key
#  name        :string(255)
#  description :string(255)
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#

class Authorization < ApplicationRecord
  validates :name, :description, presence: true
  validates :name, :description, uniqueness: true
end
