# == Schema Information
#
# Table name: sub_itens_obra
#
#  id               :integer          not null, primary key
#  sub_item         :string(255)
#  item_obra_id     :integer
#  sub_tipo_servico :string(255)
#  unidade          :string(255)
#  created_at       :datetime         not null
#  updated_at       :datetime         not null
#

class SubItemObra < ApplicationRecord
  belongs_to :item_obra

  validates :sub_item, :sub_tipo_servico, presence: true, uniqueness: true

  paginates_per QT_PER_PAGE

  RANSACKABLE_ATTRIBUTES = ["sub_item", "sub_tipo_servico"]

  def sub_item_obra_descricao
    "#{sub_tipo_servico} - #{sub_item}"
  end

  def self.ransackable_attributes auth_object = nil
    (RANSACKABLE_ATTRIBUTES) + _ransackers.keys
  end

  class << self
    def search(resource)
      where("sub_item LIKE ? AND sub_tipo_servico LIKE ? AND unidade LIKE ?", "%#{resource[:sub_item]}%", "%#{resource[:sub_tipo_servico]}%", "%#{resource[:unidade]}%")
    end
  end
end
