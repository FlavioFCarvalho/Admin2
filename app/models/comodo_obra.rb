# == Schema Information
#
# Table name: comodo_obras
#
#  id         :integer          not null, primary key
#  obra_id    :integer
#  comodo_id  :integer
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class ComodoObra < ApplicationRecord
  belongs_to :obra
  belongs_to :comodo

  delegate :name, to: :comodo, prefix: true
end
