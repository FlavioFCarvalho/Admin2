# == Schema Information
#
# Table name: bancos
#
#  id         :integer          not null, primary key
#  nome       :string(255)
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  user_id    :integer
#  status     :string(20)
#

class Banco < ApplicationRecord
  belongs_to :user
  has_many :contas

  validates :nome, presence: true
  validates :nome, uniqueness: true

  after_initialize :set_default_status, :if => :new_record?

  def set_default_status
    self.status = "Ativo"
  end

  def created_by
    user.name
  end

  RANSACKABLE_ATTRIBUTES = ["nome"]

  def self.ransackable_attributes auth_object = nil
    (RANSACKABLE_ATTRIBUTES) + _ransackers.keys
  end
end
