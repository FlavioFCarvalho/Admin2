# == Schema Information
#
# Table name: fornecedor_servicos
#
#  id            :integer          not null, primary key
#  fornecedor_id :integer
#  service_id    :integer
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#

class FornecedorServico < ApplicationRecord
  belongs_to :service
  belongs_to :fornecedor, class_name: "ClienteFornecedor", foreign_key: "fornecedor_id"
end
