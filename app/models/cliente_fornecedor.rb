# == Schema Information
#
# Table name: cliente_fornecedores
#
#  id                     :integer          not null, primary key
#  status                 :string(20)       default("0")
#  tipo_pessoa            :string(20)
#  cliente_fornecedor     :string(50)
#  nome                   :string(255)
#  razao_social           :string(255)
#  cpf                    :string(20)
#  cnpj                   :string(20)
#  rg                     :string(20)
#  sexo                   :string(20)
#  endereco               :string(255)
#  numero                 :string(10)
#  complemento            :string(255)
#  cep                    :string(20)
#  bairro                 :string(255)
#  cidade_id              :integer
#  estado_id              :integer
#  contato1_nome          :string(255)
#  contato1_ddd           :string(2)
#  contato1_telefone      :string(255)
#  contato1_tipo_telefone :string(20)
#  contato1_email1        :string(80)
#  contato1_email2        :string(80)
#  contato2_nome          :string(255)
#  contato2_ddd           :string(2)
#  contato2_telefone      :string(255)
#  contato2_tipo_telefone :string(20)
#  contato2_email1        :string(80)
#  contato2_email2        :string(80)
#  contato3_nome          :string(255)
#  contato3_ddd           :string(2)
#  contato3_telefone      :string(255)
#  contato3_tipo_telefone :string(20)
#  contato3_email1        :string(80)
#  contato3_email2        :string(80)
#  created_at             :datetime
#  updated_at             :datetime
#  user_id                :integer
#  inscricao_estadual     :string(255)
#  observacao             :text(65535)
#  id_nome                :string(255)
#  datafaker              :string(255)
#  data_cadastro          :string(255)
#  tipo_cliente           :string(255)
#  tipo_fornecedor        :string(255)
#  tipo_investidor        :string(255)
#  dados                  :integer          default("0")
#

class ClienteFornecedor < ApplicationRecord
  include ActiveModel::Validations
  include ClienteFornecedorModelConcern

  enum dados: { incompleto: 0, completo: 1 }

  belongs_to :user
  belongs_to :estado
  belongs_to :cidade
  has_many :fornecedor_services, inverse_of: :fornecedor, class_name: "FornecedorServico", dependent: :destroy, foreign_key: "fornecedor_id"
  has_many :fornecedor_products, inverse_of: :fornecedor, class_name: "FornecedorProduto", dependent: :destroy, foreign_key: "fornecedor_id"
  has_many :cliente_works, inverse_of: :cliente, class_name: "ClienteObra", dependent: :destroy, foreign_key: "cliente_id"
  has_many :services, through: :fornecedor_services
  has_many :produtos, through: :fornecedor_products
  has_many :obras, through: :cliente_works
  has_many :contas_bancaria, class_name: "ContasBancaria"

  attr_accessor :banco_id, :name_bank, :agencia, :conta, :tipo

  attr_accessor :name_services
  accepts_nested_attributes_for :services,
                                :reject_if => :all_blank,
                                allow_destroy: true

  attr_accessor :name_products
  accepts_nested_attributes_for :produtos,
                                :reject_if => :all_blank,
                                allow_destroy: true

  attr_accessor :name_works
  accepts_nested_attributes_for :obras,
                                :reject_if => :all_blank,
                                allow_destroy: true

  validates :status, presence: true
  # validates :tipo_cliente, presence: { message: I18n.t('cliente_fornecedores.messages.type_client_must_be_selected')}
  # validates :tipo_fornecedor, presence: { message: I18n.t('cliente_fornecedores.messages.type_provider_must_be_selected')}
  # validates :tipo_investidor, presence: { message: I18n.t('cliente_fornecedores.messages.type_investor_must_be_selected')}

  #validates :cpf, presence: {if: :fisic_person?, message: I18n.t('cliente_fornecedores.messages.cpf_must_be_present')}
  #validates :rg, presence: {if: :fisic_person?, message: I18n.t('cliente_fornecedores.messages.rg_must_be_present')}
  #validates :cnpj, presence: {if: :juridic_person?, message: I18n.t('cliente_fornecedores.messages.cnpj_must_be_present')}
  #validates :razao_social, presence: {if: :juridic_person?, message: I18n.t('cliente_fornecedores.messages.razao_social_must_be_present')}
  #validates :inscricao_estadual, presence: {if: :juridic_person?, message: I18n.t('cliente_fornecedores.messages.inscricao_estadual_must_be_present')}
  #validates :nome, presence: { if: :name_not_present?, message: I18n.t('cliente_fornecedores.messages.name_must_be_present') }
  #validates :nome, uniqueness: { if: :name_not_present?, message: I18n.t('cliente_fornecedores.messages.name_must_be_unique') }
  #validates :tipo_pessoa, presence: { if: :tipo_pessoa_not_present?, message: I18n.t('cliente_fornecedores.messages.tipo_pessoa_must_be_present') }
  #validates :cliente_fornecedor, presence: { if: :cliente_fornecedor_not_present?, message: I18n.t('cliente_fornecedores.messages.cliente_fornecedor_must_be_present') }

  validates :tipo_pessoa, presence: { if: :tipo_pessoa_not_present?, message: I18n.t('cliente_fornecedores.messages.tipo_pessoa_must_be_present') }
  validates :nome, presence: { if: :name_not_present?, message: I18n.t('cliente_fornecedores.messages.name_must_be_present') }
  validates :nome, uniqueness: { if: :name_not_present?, message: I18n.t('cliente_fornecedores.messages.name_must_be_unique') }

  def document
    cpf unless cpf.nil?
    cnpj unless cnpj.nil?
  end

  def email
    if (contato1_email2.nil? || contato1_email2.eql?("-")) || (contato2_email1.nil? || contato2_email1.eql?("-")) || (contato2_email2.nil? || contato2_email2.eql?("-")) || (contato3_email1.nil? || contato3_email1.eql?("-")) || (contato3_email2.nil? || contato3_email2.eql?("-"))
      return contato1_email1
    elsif (contato1_email1.nil? || contato1_email1.eql?("-")) || (contato2_email1.nil? || contato2_email1.eql?("-")) || (contato2_email2.nil? || contato2_email2.eql?("-")) || (contato3_email1.nil? || contato3_email1.eql?("-")) || (contato3_email2.nil? || contato3_email2.eql?("-"))
      return contato1_email2
    elsif (contato1_email1.nil? || contato1_email1.eql?("-")) || (contato1_email1.nil? || contato1_email1.eql?("-")) || (contato2_email2.nil? || contato2_email2.eql?("-")) || (contato3_email1.nil? || contato3_email1.eql?("-")) || (contato3_email2.nil? || contato3_email2.eql?("-"))
      return contato2_email1
    elsif (contato1_email1.nil? || contato1_email1.eql?("-")) || (contato1_email1.nil? || contato1_email1.eql?("-")) || (contato2_email1.nil? || contato2_email1.eql?("-")) || (contato3_email1.nil? || contato3_email1.eql?("-")) || (contato3_email2.nil? || contato3_email2.eql?("-"))
      return contato2_email2
    elsif (contato1_email1.nil? || contato1_email1.eql?("-")) || (contato1_email1.nil? || contato1_email1.eql?("-")) || (contato2_email1.nil? || contato2_email1.eql?("-")) || (contato2_email2.nil? || contato2_email2.eql?("-")) || (contato3_email2.nil? || contato3_email2.eql?("-"))
      return contato3_email1
    elsif (contato1_email1.nil? || contato1_email1.eql?("-")) || (contato1_email1.nil? || contato1_email1.eql?("-")) || (contato2_email1.nil? || contato2_email1.eql?("-")) || (contato2_email2.nil? || contato2_email2.eql?("-")) || (contato3_email1.nil? || contato3_email1.eql?("-"))
      return contato3_email2
    end
  end

  def pdf_file_name
    "Fornecedor - #{nome}"
  end

  def phone
    "(#{contato1_ddd}) - #{contato1_telefone}"
  end

  def ativo?
    self.status.eql?("Ativo")
  end

  def fisic_person?
    self.tipo_pessoa.eql?("Fisica")
  end

  def juridic_person?
    self.tipo_pessoa.eql?("Juridica")
  end

  RANSACKABLE_ATTRIBUTES = [ "nome", "status", "cpf", "cnpj", "rg", "endereco", "bairro"]

  def self.ransackable_attributes auth_object = nil
    (RANSACKABLE_ATTRIBUTES) + _ransackers.keys
  end

  private

    def inscricao_estadual_content_should_not_be_an_dash
      if juridic_person?
        errors.add(:inscricao_estadual, I18n.t('helpers.empty_field', field: "Inscrição Estadual")) if inscricao_estadual.eql?("-")
      end
    end

    def razao_social_content_should_not_be_an_dash
      if juridic_person?
        errors.add(:razao_social, I18n.t('helpers.empty_field', field: "Razão Social")) if razao_social.eql?("-")
      end
    end

    def rg_content_should_not_be_an_dash
      if fisic_person?
        errors.add(:rg, I18n.t('helpers.empty_field', field: "RG")) if rg.eql?("-") or rg.eql?("..-")
      end
    end

    def cpf_content_should_not_be_an_dash
      if fisic_person?
        errors.add(:cpf, I18n.t('helpers.empty_field', field: "CPF")) if cpf.eql?("-") or cpf.eql?("..-")
      end
    end
end
