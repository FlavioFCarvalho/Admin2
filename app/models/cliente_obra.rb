# == Schema Information
#
# Table name: cliente_obras
#
#  id         :integer          not null, primary key
#  cliente_id :integer
#  obra_id    :integer
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class ClienteObra < ApplicationRecord
  belongs_to :obra
  belongs_to :cliente, class_name: "ClienteFornecedor", foreign_key: "cliente_id"
end
