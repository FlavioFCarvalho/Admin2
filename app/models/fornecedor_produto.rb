# == Schema Information
#
# Table name: fornecedor_produtos
#
#  id            :integer          not null, primary key
#  produto_id    :integer
#  fornecedor_id :integer
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#

class FornecedorProduto < ApplicationRecord
  belongs_to :produto
  belongs_to :fornecedor, class_name: "ClienteFornecedor", foreign_key: "fornecedor_id"
end
