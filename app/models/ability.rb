class Ability
  include CanCan::Ability

  def initialize(user)
    user ||= User.new
    if user.admin?
      can :manage, :all
    else
      user.authorization_users.each do |f|
        can :create,  f.authorization.name.constantize if f._create?
        can :read,    f.authorization.name.constantize if f._retrieve?
        can :update,  f.authorization.name.constantize if f._update?
        can :destroy, f.authorization.name.constantize if f._delete?
      end
    end
  end
end
