# == Schema Information
#
# Table name: roles
#
#  id            :integer          not null, primary key
#  name          :string(255)
#  resource_type :string(255)
#  resource_id   :integer
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#

class Role < ApplicationRecord
  OPTIONS = {
    create:  "create",
    retrieve:"retrieve",
    update:  "update",
    delete:  "delete"
  }

  def self.availables
    OPTIONS.map do |key, value|
      key.to_s
    end
  end

  has_and_belongs_to_many :bancos, :join_table => :bancos_roles
  has_and_belongs_to_many :obras, :join_table => :obras_roles
  has_and_belongs_to_many :produtos, :join_table => :produtos_roles
  has_and_belongs_to_many :users, :join_table => :users_roles

  belongs_to :resource,
             :polymorphic => true,
             :optional => true

  validates :resource_type,
            :inclusion => { :in => Rolify.resource_types },
            :allow_nil => true

  scopify
end
