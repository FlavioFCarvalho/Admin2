class Tipo < ApplicationRecord
    paginates_per 10
    attr_accessor :_status
  
    STATUS = [:active, :inactive]
  
    enum status: STATUS
  
    validates :name, presence: true
    validates :name, uniqueness: true
    belongs_to :user
  
    after_initialize :set_default_status, :if => :new_record?
  
    scope :by_the_last_one, -> { order("id desc") }
    scope :active, -> { where(status: STATUS[0].to_s).by_the_last_one }
    scope :inactive, -> { where(status: STATUS[1].to_s).by_the_last_one }
  
    def set_default_status
      self.status ||= STATUS[0].to_s
    end
  
    def active?
      self.status.eql?(STATUS[0].to_s)
    end
  
    def inactive?
      self.status.eql?(STATUS[1].to_s)
    end
  
    RANSACKABLE_ATTRIBUTES = ["name", "status"]
  
    def self.ransackable_attributes auth_object = nil
      (RANSACKABLE_ATTRIBUTES) + _ransackers.keys
    end
end
