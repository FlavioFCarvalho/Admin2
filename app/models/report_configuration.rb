# == Schema Information
#
# Table name: report_configurations
#
#  id                   :integer          not null, primary key
#  company              :string(255)
#  cnpj                 :string(255)
#  phone                :string(255)
#  address              :string(255)
#  state_id             :integer
#  city_id              :integer
#  email                :string(255)
#  neighborhood         :string(255)
#  created_at           :datetime         not null
#  updated_at           :datetime         not null
#  picture_file_name    :string(255)
#  picture_content_type :string(255)
#  picture_file_size    :integer
#  picture_updated_at   :datetime
#

class ReportConfiguration < ApplicationRecord
  belongs_to :state, class_name: "Estado", foreign_key: "state_id"
  belongs_to :city, class_name: "Cidade", foreign_key: "city_id"

  validates :company, :email, :cnpj, :state_id, :city_id, :phone, :address, :neighborhood, presence: true

  has_attached_file :picture, styles: { medium: "300x300>", thumb: "100x100>" }
  validates_attachment_content_type :picture, content_type: /\Aimage\/.*\z/


  def city_state
    "#{city}, #{state.sigla}"
  end
end
