class ObraPolicy < ManagerPolicy
  def new?
    user.admin? || authorized_to_create_subject?
  end

  def create?
    create?
  end

  def edit?
    user.admin? || authorized_to_update_subject?
  end

  def update?
    edit?
  end
end
