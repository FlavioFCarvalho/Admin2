class ProdutoPolicy < ManagerPolicy
  def new?
    user.admin? || authorized_to_create_subject?
  end

  def show?
    user.admin? || authorized_to_view_subject?
  end

  def create?
    new?
  end

  def edit?
    user.admin? || authorized_to_update_subject?
  end

  def update?
    edit?
  end

  class Scope < Scope
    def resolve
      if user.admin?
        scope.all
      else
        scope.active
      end
    end
  end
end
