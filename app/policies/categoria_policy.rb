class CategoriaPolicy < ManagerPolicy
  class Scope < Scope
    def resolve
      scope
    end
  end

  def show?
    user.admin? || authorized_to_view_subject?
  end

  def new?
    user.admin? || authorized_to_create_subject?
  end

  def create?
    new?
  end

  def edit?
    user.admin? || authorized_to_update_subject?
  end

  def update?
    edit?
  end
end
