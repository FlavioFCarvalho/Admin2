class ItemObraPolicy < ManagerPolicy
  class Scope < Scope
    def resolve
      if user.admin?
        scope.all.order(id: :DESC)
      else
        scope.all
      end
    end
  end

  def new?
    user.admin? || authorized_to_create_subject?
  end

  def show?
    user.admin? || authorized_to_view_subject?
  end

  def create?
    new?
  end

  def edit?
    user.admin? || authorized_to_update_subject?
  end

  def update?
    edit?
  end

  def destroy?
    user.admin? || authorized_to_update_subject?
  end
end
