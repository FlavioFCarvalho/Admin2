class ManagerPolicy < ApplicationPolicy
  private
    def authorized_to_create_subject?
      if user.user?
        @authorized_user = user.authorization_users.find_by(authorization_id: Authorization.find_by_name(@object.class.name.to_s).id, _create: true)
      end
      return true if @authorized_user
    end

    def authorized_to_update_subject?
      if user.user?
        @authorized_user = user.authorization_users.find_by(authorization_id: Authorization.find_by_name(@object.class.name.to_s).id, _update: true)
      end
      return true if @authorized_user
    end

    def authorized_to_view_subject?
      if user.user?
        @authorized_user = user.authorization_users.find_by(authorization_id: Authorization.find_by_name(@object.class.name.to_s).id, _retrieve: true)
      end
      return true if @authorized_user
    end
end
