class UserPolicy < ApplicationPolicy
  class Scope < Scope
    def resolve
      if user.admin?
        scope.all
      else
        scope.with_restricted_access
      end
    end
  end

  def new?
    user.admin?
  end

  def edit?
    user.admin? || user_is_owner_of_record?
  end

  def create?
    user.admin?
  end

  def update?
    user.admin? || user_is_owner_of_record?
  end

  private
    def user_is_owner_of_record?
      @user == @object
    end
end
