require "application_responder"
class ApplicationController < ActionController::Base
  # self.responder = ApplicationResponder
  before_action :set_locale
  protect_from_forgery with: :exception


  def get_query(cookie_key)
    cookies.delete(cookie_key) if params[:clear]
    cookies[cookie_key] = params[:q].to_json if params[:q]
    @query = params[:q].presence || JSON.load(cookies[cookie_key])
  end

  def logged_in_user?
    @logged_in_user = admin_signed_in? || user_signed_in?
  end

  def user
    @logged_in_user = current_admin || current_user
  end

  def default_url_options
    { locale: I18n.locale }
  end

  def set_locale
    I18n.locale = params[:locale] || I18n.default_locale
  end
end
