class ContasBancariaController < ApplicationController
  def update
    binding.pry
    @conta_bancaria = ContasBancaria.find(params[:conta_bancaria][:id])
    @conta_bancaria.update(account_params)
  end



  private
    def account_params
      params.require(:contas_bancaria).permit(:id, :agencia, :conta, :tipo, :banco_id)
    end
end
