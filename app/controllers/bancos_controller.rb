class BancosController < BrantController
  before_action :set_banco, only: [:show, :edit, :update, :destroy]

  respond_to :html

  def index
    @search = Banco.ransack(params[:q])
    @bancos = @search.result.order(:nome).page(params[:page]).per(10)
    @search.build_condition if @search.conditions.empty?
  end

  def show
    respond_with(@banco)
  end

  def new
    @banco = user.bancos.build
    authorize @banco
    respond_with(@banco)
  end

  def edit
  end

  def create
    @banco = user.bancos.build(resource_params)
    if @banco.save
      redirect_to new_banco_path, notice: t("flash.actions.#{__method__}.success", resource_name: t('activerecord.models.bank'))
    else
      flash[:error] = t("flash.actions.#{__method__}.alert", resource_name: t('activerecord.models.bank'))
      render :new
    end
  end

  def update
    @banco.user=current_user
    if @banco.update(resource_params)
      flash[:info]= t("flash.actions.#{__method__}.success", resource_name: t('activerecord.models.bank'))
      redirect_to bancos_url
    else
      flash[:error] = t("flash.actions.#{__method__}.alert", resource_name: t('activerecord.models.bank'))
      render :edit
    end
  end

  def destroy
    if @banco.destroy
      flash[:info] = t("flash.actions.#{__method__}.notice", resource_name: t('activerecord.models.bank'))
    else
      flash[:error] = t("flash.actions.#{__method__}.alert", resource_name: t('activerecord.models.bank'))
    end
    redirect_to bancos_url
  end

  private
    def set_banco
      @banco = Banco.find(params[:id])
      authorize @banco
    end

    def resource_params
      params.require(:banco).permit(:nome, :user_id, :status)
    end
end
