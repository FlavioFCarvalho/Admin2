class ModelosController < BrantController
  
    before_action :set_modelo, only: [:show, :edit, :update, :destroy]
  
    respond_to :html
  
    def index
      session[:page] = params[:page]
      @search = Modelo.ransack(params[:q])
      @modelos = @search.result.order(:name).page(params[:page]).per(10)
      @search.build_condition if @search.conditions.empty?
    end
  
    def new
      @modelo = user.modelos.build
      authorize @modelo
      respond_with(@modelo)
    end
  
    def create
      @modelo = user.modelos.build(resource_params)
      if @modelo.save
        redirect_to new_modelo_path, notice: t("flash.actions.#{__method__}.notice", resource_name: t('activerecord.models.modelo'))
      else
        flash[:error] = t("flash.actions.#{__method__}.alert", resource_name: t('activerecord.models.modelo'))
        render :edit
      end
    end
  
    def show
    end
  
    def edit
    end
  
    def update
      @modelo.status=(resource_params[:_status])
      @modelo.user=(user) if !@modelo.user.present?
  
      if @modelo.update(resource_params)
        flash[:info] = t("flash.actions.#{__method__}.notice", resource_name: t('activerecord.models.modelo'))
        redirect_to modelos_url
      else
        flash[:error] = t("flash.actions.#{__method__}.alert", resource_name: t('activerecord.models.modelo'))
        render :edit
      end
    end
  
    def destroy
      if @modelo.destroy
        flash[:info] = t("flash.actions.#{__method__}.notice", resource_name: t('activerecord.models.modelo'))
      else
        flash[:error] = t("flash.actions.#{__method__}.alert", resource_name: t('activerecord.models.modelo'))
      end
      redirect_to modelos_url
    end
  
    private
      def set_modelo
        @modelo = Modelo.find(params[:id])
        authorize @modelo
      end
  
      def resource_params
        params.require(:modelo).permit(:name, :user_id, :_status)
      end
  end

