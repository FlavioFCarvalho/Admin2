class ServicesController < BrantController
  before_action :set_service, only: [:show, :edit, :update, :destroy]

  respond_to :html

  def index
    # @services = policy_scope(Service).page params[:page]
    session[:page] = params[:page]
    @search = Service.ransack(params[:q])
    @services = @search.result.order(:name).page(params[:page]).per(10)
    @search.build_condition if @search.conditions.empty?
  end

  def new
    @service = user.services.build
    authorize @service
    respond_with(@service)
  end

  def create
    @service = user.services.build(resource_params)
    if @service.save
      redirect_to new_service_path, notice: t("flash.actions.#{__method__}.notice", resource_name: t('activerecord.models.service'))
    else
      flash[:error] = t("flash.actions.#{__method__}.alert", resource_name: t('activerecord.models.service'))
      render :edit
    end
  end

  def show
  end

  def edit
  end

  def update
    @service.status=(resource_params[:_status])
    @service.user=(user) if !@service.user.present?

    if @service.update(resource_params)
      flash[:info] = t("flash.actions.#{__method__}.notice", resource_name: t('activerecord.models.service'))
      redirect_to services_url
    else
      flash[:error] = t("flash.actions.#{__method__}.alert", resource_name: t('activerecord.models.service'))
      render :edit
    end
  end

  def destroy
    if @service.destroy
      flash[:info] = t("flash.actions.#{__method__}.notice", resource_name: t('activerecord.models.service'))
    else
      flash[:error] = t("flash.actions.#{__method__}.alert", resource_name: t('activerecord.models.service'))
    end
    redirect_to services_url
  end

  private
    def set_service
      @service = Service.find(params[:id])
      authorize @service
    end

    def resource_params
      params.require(:service).permit(:name, :user_id, :_status)
    end
end
