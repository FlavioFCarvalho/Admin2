class ProdutoCategoriasController < ApplicationController
  # before_action :set_produto_categoria, only: [:show, :edit, :update, :destroy]
  # load_and_authorize_resource :produto
  # load_and_authorize_resource :categoria, through: :produto
  respond_to :html, :json, :js

  def new
    @produto = Produto.find(session[:produto_id])
    unless ProdutoCategoria.where(produto_id: @produto.id).empty?
      @produto_categorias = ProdutoCategoria.where(produto_id: @produto.id)
    end
    @produto_categoria = ProdutoCategoria.new
    respond_with(@produto_categoria)
  end

  def create
    @produto = Produto.find(session[:produto_id])
    @produto_categoria = ProdutoCategoria.new
    # @produto_categoria = ProdutoCategoria.create(produto_categoria_params)
    @produto_categoria = @produto.produto_categorias.build(produto_categoria_params)
    if @produto_categoria.save
      respond_to &:js
    else
      flash[:error] = t("flash.actions.create.alert", resource_name: "Produto Categoria")
      render :new
    end
  end

  private
    def set_produto_categoria
      @produto_categoria = ProdutoCategoria.find(params[:id])
    end

    def produto_categoria_params
      params.require(:produto_categoria).permit(:produto_id,
                                                :categoria_id)
    end
end
