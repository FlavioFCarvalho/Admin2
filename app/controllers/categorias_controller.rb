class CategoriasController < BrantController
  before_action :set_categoria, only: [:show, :edit, :update, :destroy]

  respond_to :html

  def index
    @search = Categoria.ransack(params[:q])
    @categorias = @search.result.order(:nome).page(params[:page]).per(10)
    @search.build_condition if @search.conditions.empty?
  end

  def show
    respond_with(@categoria)
  end

  def new
    @categoria = Categoria.new
    authorize @categoria
    respond_with(@categoria)
  end

  def edit
  end

  def create
    @categoria = Categoria.new(categoria_params)
    if @categoria.save
      redirect_to new_categoria_path, notice: t("flash.actions.#{__method__}.success_fem", resource_name: t('activerecord.models.category'))
    else
      flash[:error] = t("flash.actions.#{__method__}.alert_fem", resource_name: t('activerecord.models.category'))
      render :new
    end
  end

  def update
    if @categoria.update(categoria_params)
      flash[:info]= t("flash.actions.#{__method__}.success_fem", resource_name: t('activerecord.models.category'))
      redirect_to categorias_url
    else
      flash[:error] = t("flash.actions.#{__method__}.alert_fem", resource_name: t('activerecord.models.category'))
      render :edit
    end
  end

  def destroy
    if @categoria.destroy
      flash[:info] = t("flash.actions.#{__method__}.success_fem", resource_name: t('activerecord.models.category'))
    else
      flash[:error] = t("flash.actions.#{__method__}.alert_fem", resource_name: t('activerecord.models.category'))
    end
    redirect_to categorias_url
  end

  private
    def set_categoria
      @categoria = Categoria.find(params[:id])
      authorize @categoria
    end

    def categoria_params
      params.require(:categoria).permit(:nome, :status)
    end
end
