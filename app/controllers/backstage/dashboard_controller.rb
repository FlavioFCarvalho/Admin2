class Backstage::DashboardController < BackstageController
  def index
  end

  def brant
    @users = User.all.order(id: :desc)
  end

  def models
    @authorizations = Authorization.all.order(id: :desc)
  end
end
