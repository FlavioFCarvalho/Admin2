'''
  * Controller para cadastrar os Models
  com seus respectivos nomes de Exibição
  e nome exato das classes criadas
'''
class Backstage::AuthorizationsController < BackstageController
  before_action :find_authorization, only: [:edit, :update, :destroy]

  def new
    @authorization = Authorization.new
  end

  def create
    @authorization = Authorization.new(resource_params)
    if @authorization.save
      flash[:success] =  t("flash.actions.create.notice", resource_name: "Módulo")
      redirect_to backstage_empresa_brant_models_path
    else
      flash[:alert] =  t("flash.actions.create.alert", resource_name: "Módulo")
      render :new
    end
  end

  def edit
  end

  def update
    if @authorization.update(resource_params)
      flash[:success] =  t("flash.actions.create.notice", resource_name: "Módulo")
      redirect_to backstage_empresa_brant_models_path
    else
      flash[:alert] =  t("flash.actions.create.alert", resource_name: "Módulo")
      render :edit
    end
  end

  def destroy
    if @authorization.destroy
      flash[:warning] =  t("flash.actions.destroy.notice", resource_name: "Módulo")
      redirect_to backstage_empresa_brant_models_path
    else
      flash[:alert] =  t("flash.actions.destroy.error", resource_name: "Módulo")
      redirect_to :back
    end
  end

  private
    def find_authorization
      @authorization = Authorization.find(params[:id])
    end

    def resource_params
      params.require(:authorization).permit(:name, :description)
    end
end
