class Backstage::Users::RegistrationsController < BackstageController
  before_action :find_user, only: [:edit, :update, :destroy]

  def new
    @user = User.new
    @user.build_profile if @user.profile.blank?
  end

  def create
    @user = User.new(resource_params)
    @user.add_role=(resource_params[:type])
    if @user.save
      flash[:success] = t("flash.actions.create.notice", resource_name: "Usuário")
      redirect_to backstage_empresa_brant_path
    else
      flash[:alert] = t("flash.actions.create.alert", resource_name: "Usuário")
      render :new
    end
  end

  def edit
    @user.build_profile if @user.profile.blank?
  end

  def update
    @user.add_role=(resource_params[:type])
    if password_blank?
      @user.update_without_password(resource_params)
      flash[:success] = t("flash.actions.update.notice", resource_name: "Usuário")
      redirect_to backstage_empresa_brant_path
    elsif @user.update_attributes(resource_params)
      flash[:success] = t("flash.actions.update.notice", resource_name: "Usuário")
      redirect_to backstage_empresa_brant_path
    else
      flash.now[:alert] = t("flash.actions.update.alert", resource_name: "Usuário")
      render :edit
    end
  end

  def destroy
    if @user.destroy
      flash[:warinig] = t("flash.actions.destroy.success", resource_name: "Usuário")
    else
      flash[:error] = t("flash.actions.destroy.alert", resource_name: "Usuário")
    end
    redirect_to backstage_empresa_brant_path
  end


  private
    def find_user
      @user = User.find(params[:id])
    end

    def resource_params
      params.require(:user).permit(:id, :username, :email, :role, :type, :password, :password_confirmation, :profile_attributes => [:id, :user_id, :name, :_destroy])
    end

    def password_blank?
      params[:user][:password].blank? &&
      params[:user][:password_confirmation].blank?
    end
end
