module ParcelamentoConcern
  extend ActiveSupport::Concern

  private
    # => Constroi parcelas com valores informados no 'params'
    def _create_parcelas_
      @estoque.estoque_movimento_parcelas.delete_all if !@estoque.estoque_movimento_parcelas.empty?

      if params[:valor_total] != nil
        _valor_total = params[:valor_total].to_money.to_f
        _total_parcelas = params[:parcelas].to_i
      else
        _valor_total = @estoque.total_estoque.to_money.to_f
        _total_parcelas = @estoque.parcelas
      end
      
      @valor_parcial  = Estoque::EstoqueParcelamento.divide_valor_proporcional(_valor_total, _total_parcelas)
      _total_parcial=(@valor_parcial.round(2) * _total_parcelas)

      if (_valor_total > _total_parcial)
        _diferenca_de=(_valor_total - _total_parcial)
      else
        _diferenca_de=(_total_parcial - _valor_total)
      end

      data_vencimento = ""
      data_vencimento = @estoque.data_vencimento_entrada unless @estoque.data_vencimento_entrada.nil?
      data_vencimento = @estoque.data_devolucao unless @estoque.data_devolucao.nil?
      data_vencimento = @estoque.data_transferencia unless @estoque.data_transferencia.nil?

      Estoque::EstoqueMovimentoParcela.build_parcelamento(@estoque.id, @valor_parcial, data_vencimento, @estoque.periodo, _total_parcelas, _diferenca_de)
    end

    # => Retorna o somatorio de todas as parcelas nao alteradas
    def _somatorio_valores_nao_bloqueados(resource)
      valor =0
      resource.where(bloqueado: false).each do |parcela|
        valor += parcela.valor
      end
      return valor
    end

    # => Retorna o somatorio de todos as parcelas já alterados
    def _somatorio_valores_bloqueados(resource)
      valor =0
      resource.where(bloqueado: true).each do |parcela|
        valor += parcela.valor
      end
      return valor
    end

    # => Retorna o valor da ultima parcela
    def _ultima_parcela_valor
      totalparcela =0
      @estoque.estoque_movimento_parcelas.where(bloqueado: false).each do |parcela|
        if parcela.eql? @estoque.estoque_movimento_parcelas.last
          totalparcela = parcela.valor
        end
      end
      return totalparcela
    end

    # => Funcionando com valores menores que a parcela
    def _calcular_diferenca_com_valores_menores(_total_parcial, _novo_total)
      if (_total_parcial.to_money.to_f > _novo_total.to_money.to_f)
        result=(_total_parcial.to_money.to_f - _novo_total.to_money.to_f)
      else
        result=(_total_parcial.to_money.to_f - _novo_total.to_money.to_f)
      end
      return result
    end

    
    def _calcular_diferenca_com_valores_maiores(_total_parcial, _novo_total)
      if (_total_parcial.to_money.to_f > _novo_total.to_money.to_f)
        result=(_total_parcial.to_money.to_f - _novo_total.to_money.to_f)
      else
        result=(_total_parcial.to_money.to_f - _novo_total.to_money.to_f)
      end
      return result
    end

    def _parcelamento_do_produto(_novo_valor, _valor_parcela)
      _valor_restante       = 0
      _parcelas_restantes   = 0

      # => Atribuindo valores inicial para variaveis de calculo
      _valor_restante = 0
      _valores_bloqueados = 0
      _parcelas_bloqueadas = false
      @estoque_produto   = @estoque_parcelamento.estoque_produto
      
      # => Descobrindo quantitativo de parcelas
      _parcelas_restantes = @estoque_produto.estoque_produto_parcelamentos.where.not(id: @estoque_parcelamento.id).where(bloqueado: false).count
      # => Soma todos os valores nao alterados
      _valor_restante = _somatorio_valores_nao_bloqueados(@estoque_produto.estoque_produto_parcelamentos)

      # => Verifica se existem valores alterados
      _valores_bloqueados = _somatorio_valores_bloqueados(@estoque_produto.estoque_produto_parcelamentos)
      if _valores_bloqueados != 0
        _parcelas_bloqueadas = true
      end

      # => Descobrindo novo valor a partir do novo valor para a parcela atualmente alterada
      _novo_total = (_valor_restante - _novo_valor)

      # => Descobrindo os valores parciais do novo valor
      @valor_parcial  = Estoque::EstoqueParcelamento.divide_valor_proporcional(_novo_total, _parcelas_restantes)
      _total_parcial=(@valor_parcial.round(2) * _parcelas_restantes)

      # => Condições para descobrir a diferença da parcela
      if not _parcelas_bloqueadas.eql?(true)
        if _novo_valor < _valor_parcela
          _resultado_diferenca = _calcular_diferenca_com_valores_menores(_total_parcial, _novo_total)
        else
          _resultado_diferenca = _calcular_diferenca_com_valores_maiores(_total_parcial, _novo_total)
        end
      else
        _resultado_diferenca = (_total_parcial.to_f - _novo_total.to_f)
      end

      # => Calculando o valor da ultima parcela
      _valor_ultima_parcela=(@valor_parcial - _resultado_diferenca.round(2))

      if _novo_valor <= _valor_restante
        # => Momento em que os valores são devidamente atualizados
        @estoque_parcelamento.update_attributes(valor: _novo_valor, bloqueado: true)
        @estoque_produto.estoque_produto_parcelamentos.where.not(id: @estoque_parcelamento.id).where(bloqueado: false).each do |parcela|
          if parcela.eql?(@estoque_produto.estoque_produto_parcelamentos.where.not(id: @estoque_parcelamento.id).where(bloqueado: false).first) 
            parcela.update_attributes(valor: _valor_ultima_parcela)
          else
            parcela.update_attributes(valor: @valor_parcial)
          end
        end
        return true
      else
        return false
      end
    end

    def _parcelamento_da_transferencia(_novo_valor, _valor_parcela)
      _valor_restante       = 0
      _parcelas_restantes   = 0

      # => Atribuindo valores inicial para variaveis de calculo
      _valor_restante = 0
      _valores_bloqueados = 0
      _parcelas_bloqueadas = false
      @estoque_transferencia   = @estoque_parcelamento.estoque_transferencia
      
      # => Descobrindo quantitativo de parcelas
      _parcelas_restantes = @estoque_transferencia.estoque_transferencia_parcelamentos.where.not(id: @estoque_parcelamento.id).where(bloqueado: false).count
      # => Soma todos os valores nao alterados
      _valor_restante = _somatorio_valores_nao_bloqueados(@estoque_transferencia.estoque_transferencia_parcelamentos)

      # => Verifica se existem valores alterados
      _valores_bloqueados = _somatorio_valores_bloqueados(@estoque_transferencia.estoque_transferencia_parcelamentos)
      if _valores_bloqueados != 0
        _parcelas_bloqueadas = true
      end

      # => Descobrindo novo valor a partir do novo valor para a parcela atualmente alterada
      _novo_total = (_valor_restante - _novo_valor)

      # => Descobrindo os valores parciais do novo valor
      @valor_parcial  = Estoque::EstoqueParcelamento.divide_valor_proporcional(_novo_total, _parcelas_restantes)
      _total_parcial=(@valor_parcial.round(2) * _parcelas_restantes)

      # => Condições para descobrir a diferença da parcela
      if not _parcelas_bloqueadas.eql?(true)
        if _novo_valor < _valor_parcela
          _resultado_diferenca = _calcular_diferenca_com_valores_menores(_total_parcial, _novo_total)
        else
          _resultado_diferenca = _calcular_diferenca_com_valores_maiores(_total_parcial, _novo_total)
        end
      else
        _resultado_diferenca = (_total_parcial.to_f - _novo_total.to_f)
      end

      # => Calculando o valor da ultima parcela
      _valor_ultima_parcela=(@valor_parcial - _resultado_diferenca.round(2))

      if _novo_valor <= _valor_restante
        # => Momento em que os valores são devidamente atualizados
        @estoque_parcelamento.update_attributes(valor: _novo_valor, bloqueado: true)
        @estoque_transferencia.estoque_transferencia_parcelamentos.where.not(id: @estoque_parcelamento.id).where(bloqueado: false).each do |parcela|
          if parcela.eql?(@estoque_transferencia.estoque_transferencia_parcelamentos.where.not(id: @estoque_parcelamento.id).where(bloqueado: false).first) 
            parcela.update_attributes(valor: _valor_ultima_parcela)
          else
            parcela.update_attributes(valor: @valor_parcial)
          end
        end
        return true
      else
        return false
      end
    end
end