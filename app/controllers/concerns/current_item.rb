module CurrentItem
  private
    def set_item
      @item = Item.find(session[:item_id])
    rescue ActiveRecord::RecordNotFound
      @item = Item.create
      session[:item_id] = @item.id
    end
end
