module EstoqueMovimentoConcern
  extend ActiveSupport::Concern
  included do
    before_action :find_estoque, only: [:show, :edit, :update]
    respond_to :html
  end

  private
    def inicializa_estoque
      @estoque = user.estoques_movimentos.build(resource_params)
    end

    def verifica_se_ha_transferencia
      if @estoque.tipo_movimentacao.eql?("Transferência")
        @estoque.add_transferencia_tipo=resource_params[:transferencia_tipo]
        @estoque.save
      end
    end

    def dados_salvos
      @estoque.save
    end

    def atualiza_dados_movimentacao
      @estoque.update(resource_params)
    end

    def find_estoque
      @id = check_params
      @estoque = Estoque::EstoqueMovimento.find_by(id: @id.call(params[:id], params[:estoque_movimentaco_id]))
    end

    def resource_params
      params.require(:estoque_estoque_movimento).permit(:id,
                                                        :tipo_movimentacao,
                                                        :fornecedor_id,
                                                        :user_id,
                                                        :nota_fiscal_entrada,
                                                        :numero_pedido,
                                                        :recibo,
                                                        :cupom_fiscal,
                                                        :numero_da_os,
                                                        :nota_fiscal_saida,
                                                        :data_entrada,
                                                        :data_vencimento_entrada,
                                                        :data_vencimento_saida,
                                                        :data_saida,
                                                        :data_devolucao,
                                                        :data_transferencia,
                                                        :justificativa_devolucao,
                                                        :transferencia_tipo,
                                                        :total_estoque,
                                                        :parcelas,
                                                        :periodo,
                                                        :valor_total_produto,
                                                        :acrescimo,
                                                        :boleto,
                                                        :frete,
                                                        :desconto,
                                                        :observacoes, :valida_por)
    end
end
