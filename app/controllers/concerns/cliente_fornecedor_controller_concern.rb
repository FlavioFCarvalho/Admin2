module ClienteFornecedorControllerConcern
  extend ActiveSupport::Concern
  included do
    before_action :set_cliente_fornecedor, only: [:show, :edit, :update, :destroy]
    respond_to :html
  end

  def load_and_update_if_there_are_uncompleted_fields_for(source)
    source.each do |resource|
      # binding.pry
      begin
        # => Logica de Pessoa Juridica
        if resource.tipo_cliente.eql?("") and
          resource.tipo_fornecedor.eql?("") and
          resource.tipo_investidor.eql?("") and
          resource.juridic_person? and
          ((resource.cnpj.nil? or resource.cnpj.eql?("") or resource.cnpj.eql?("-")) or
          ( resource.razao_social.nil? or resource.razao_social.eql?("") or resource.razao_social.eql?("-")) or
          ( resource.inscricao_estadual.nil? or resource.inscricao_estadual.eql?("") or resource.inscricao_estadual.eql?("-")))
          resource.dados=(0)
          resource.save(validate: false)
        elsif !resource.tipo_cliente.eql?("") and
          !resource.tipo_fornecedor.eql?("") and
          !resource.tipo_investidor.eql?("") and
          resource.juridic_person?
          if ((resource.cnpj.nil? or resource.cnpj.eql?("") or resource.cnpj.eql?("-")) or
            ( resource.razao_social.nil? or resource.razao_social.eql?("") or resource.razao_social.eql?("-")) or
            ( resource.inscricao_estadual.nil? or resource.inscricao_estadual.eql?("") or resource.inscricao_estadual.eql?("-")))
            if resource.tipo_cliente.eql?("Sim")  and resource.tipo_fornecedor.eql?("Não") and resource.tipo_investidor.eql?("Não")
              resource.dados=(0)
              resource.save(validate: false)
            end

            if resource.tipo_cliente.eql?("Sim")  and resource.tipo_fornecedor.eql?("Sim") and resource.tipo_investidor.eql?("Não")
              resource.dados=(0)
              resource.save(validate: false)
            end

            if (resource.tipo_cliente.eql?("Sim") and resource.tipo_investidor.eql?("Sim")) and resource.tipo_fornecedor.eql?("Sim")
              resource.dados=(0)
              resource.save(validate: false)
            end

            if (resource.tipo_cliente.eql?("Não") and resource.tipo_investidor.eql?("Não")) and resource.tipo_fornecedor.eql?("Sim")
              resource.dados=(0)
              resource.save(validate: false)
            end
          end
          if ((!resource.cnpj.nil? or !resource.cnpj.eql?("") or !resource.cnpj.eql?("-")) or
            ( !resource.razao_social.nil? or !resource.razao_social.eql?("") or !resource.razao_social.eql?("-")) or
            ( !resource.inscricao_estadual.nil? or !resource.inscricao_estadual.eql?("") or !resource.inscricao_estadual.eql?("-")))

            if (resource.tipo_cliente.eql?("Sim") and resource.tipo_investidor.eql?("Sim") and resource.cliente_works.last.nil?) and (resource.tipo_fornecedor.eql?("Sim") and (!resource.fornecedor_services.last.nil? or !resource.fornecedor_products.last.nil?))
              resource.dados=(0)
              resource.save(validate: false)
            end

            if (resource.tipo_cliente.eql?("Sim") and resource.tipo_investidor.eql?("Sim") and resource.cliente_works.last.nil?) and (resource.tipo_fornecedor.eql?("Sim") and (resource.fornecedor_services.last.nil? or resource.fornecedor_products.last.nil?))
              resource.dados=(0)
              resource.save(validate: false)
            end

            if (resource.tipo_cliente.eql?("Sim") and resource.tipo_investidor.eql?("Sim") and !resource.cliente_works.last.nil?) and (resource.tipo_fornecedor.eql?("Sim") and (!resource.fornecedor_services.last.nil? or !resource.fornecedor_products.last.nil?))
              resource.dados=(1)
              resource.save(validate: false)
            end

            if (resource.tipo_cliente.eql?("Não") and resource.tipo_investidor.eql?("Não")) and (resource.tipo_fornecedor.eql?("Sim") and (!resource.fornecedor_services.last.nil? or !resource.fornecedor_products.last.nil?))
              resource.dados=(1)
              resource.save(validate: false)
            end

            if (resource.tipo_cliente.eql?("Sim") and !resource.cliente_works.last.nil?) and resource.tipo_fornecedor.eql?("Não") and resource.tipo_investidor.eql?("Não")
              resource.dados=(1)
              resource.save(validate: false)
            end

            if resource.tipo_cliente.eql?("Não") and resource.tipo_fornecedor.eql?("Não") and resource.tipo_investidor.eql?("Não")
              resource.dados=(1)
              resource.save(validate: false)
            end
          end
        end

        # => Logica para pessoa fisica
        if resource.tipo_cliente.eql?("") and
          resource.tipo_fornecedor.eql?("") and
          resource.tipo_investidor.eql?("") and
          resource.fisic_person? and
          (( resource.rg.nil? or  resource.rg.eql?("-")) and
          (  resource.cpf.nil? or resource.cpf.eql?("-")))
          resource.dados=(0)
          resource.save(validate: false)
        elsif !resource.tipo_cliente.eql?("") and
          !resource.tipo_fornecedor.eql?("") and
          !resource.tipo_investidor.eql?("") and
          resource.fisic_person?
          if ( resource.rg.nil? or  resource.rg.eql?("-")) and
            (resource.cpf.nil? or resource.cpf.eql?("-"))
            if ((resource.tipo_cliente.eql?("Sim") and resource.tipo_investidor.eql?("Sim")) and resource.cliente_works.last.nil?) and resource.tipo_fornecedor.eql?("Não")
              resource.dados=(0)
              resource.save(validate: false)
            end

            if ((resource.tipo_cliente.eql?("Sim") and resource.tipo_investidor.eql?("Sim")) and !resource.cliente_works.last.nil?) and resource.tipo_fornecedor.eql?("Não")
              resource.dados=(0)
              resource.save(validate: false)
            end

            if (resource.tipo_cliente.eql?("Não") and resource.tipo_investidor.eql?("Não") and resource.tipo_fornecedor.eql?("Não"))
              resource.dados=(0)
              resource.save(validate: false)
            end

            if (resource.tipo_cliente.eql?("Não") and resource.tipo_investidor.eql?("Não")) and (resource.tipo_fornecedor.eql?("Sim") and (resource.fornecedor_services.last.nil? or resource.fornecedor_products.last.nil?))
              resource.dados=(0)
              resource.save(validate: false)
            end

            if (resource.tipo_cliente.eql?("Não") and resource.tipo_investidor.eql?("Não")) and (resource.tipo_fornecedor.eql?("Sim") and (!resource.fornecedor_services.last.nil? or !resource.fornecedor_products.last.nil?))
              resource.dados=(0)
              resource.save(validate: false)
            end
          end

          if ( !resource.rg.nil? or  !resource.rg.eql?("-")) and
            (!resource.cpf.nil? or !resource.cpf.eql?("-"))
            if ((resource.tipo_cliente.eql?("Sim") and resource.tipo_investidor.eql?("Sim")) and resource.cliente_works.last.nil?) and resource.tipo_fornecedor.eql?("Não")
              resource.dados=(0)
              resource.save(validate: false)
            end

            if ((resource.tipo_cliente.eql?("Sim") and resource.tipo_investidor.eql?("Sim")) and !resource.cliente_works.last.nil?) and resource.tipo_fornecedor.eql?("Não")
              resource.dados=(1)
              resource.save(validate: false)
            end

            if (resource.tipo_cliente.eql?("Não") and resource.tipo_investidor.eql?("Não") and resource.tipo_fornecedor.eql?("Não"))
              resource.dados=(1)
              resource.save(validate: false)
            end

            if (resource.tipo_cliente.eql?("Não") and resource.tipo_investidor.eql?("Não")) and (resource.tipo_fornecedor.eql?("Sim") and (resource.fornecedor_services.last.nil? or resource.fornecedor_products.last.nil?))
              resource.dados=(0)
              resource.save(validate: false)
            end

            if (resource.tipo_cliente.eql?("Não") and resource.tipo_investidor.eql?("Não")) and (resource.tipo_fornecedor.eql?("Sim") and (!resource.fornecedor_services.last.nil? or resource.fornecedor_products.last.nil?))
              resource.dados=(0)
              resource.save(validate: false)
            end

            if (resource.tipo_cliente.eql?("Não") and resource.tipo_investidor.eql?("Não")) and (resource.tipo_fornecedor.eql?("Sim") and (resource.fornecedor_services.last.nil? or !resource.fornecedor_products.last.nil?))
              resource.dados=(0)
              resource.save(validate: false)
            end


            if (resource.tipo_cliente.eql?("Não") and resource.tipo_investidor.eql?("Não")) and (resource.tipo_fornecedor.eql?("Sim") and (!resource.fornecedor_services.last.nil? or !resource.fornecedor_products.last.nil?))
              resource.dados=(1)
              resource.save(validate: false)
            end
          end
        end
      rescue Exception => e
        puts e.message
        puts e.backtrace.inspect
      end
    end
  end

  private
    def _validates_aba_obras(resource)
      if resource.tipo_cliente.eql?("Sim")
        resource.dados=(0) if resource.cliente_works.last.nil?
        resource.save(validate: false)
      else
        resource.dados=(1)
        resource.save(validate: false)
      end
    end

    def _validates_abas_servico_e_produtos(resource)
      if resource.tipo_fornecedor.eql?("Sim")
        if resource.fornecedor_services.last.nil? or resource.fornecedor_products.last.nil?
          resource.dados=(0)
          resource.save(validate: false)
        end
      else
        resource.dados=(1)
        resource.save(validate: false)
      end
    end


    def set_cliente_fornecedor
      @cliente_fornecedor = ClienteFornecedor.find(params[:id])
      authorize @cliente_fornecedor
    end
end
