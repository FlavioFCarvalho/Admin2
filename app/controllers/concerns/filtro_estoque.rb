module FiltroEstoque
  # => Metodo para o filtro base TODOS no Estoque
  def filtro_de_todos
    if params[:q][:tipo_movimentacao_eq].eql?("TODOS")
      if params[:q][:fornecedor_id_eq] == "" and params[:q][:obra_id_eq] == "" and params[:q][:produto_id_eq] != "" and params[:q][:data_entrada_eq] != "" || params[:q][:data_vencimento_entrada_eq] != ""
        @source = Estoque::EstoqueMovimento.search_todos_pelo_produto(params[:q])
        build_resource
      end

      if params[:q][:fornecedor_id_eq] == "" and params[:q][:obra_id_eq] != "" and params[:q][:produto_id_eq] != "" and params[:q][:data_entrada_eq] != "" || params[:q][:data_vencimento_entrada_eq] != ""
        @source = Estoque::EstoqueMovimento.search_todos_pelo_produto_e_obra(params[:q])
        build_resource
      end

      if params[:q][:fornecedor_id_eq] == "" and params[:q][:obra_id_eq] != "" and params[:q][:produto_id_eq] == "" and params[:q][:data_entrada_eq] != "" || params[:q][:data_vencimento_entrada_eq] != ""
        @source = Estoque::EstoqueMovimento.search_todos_pela_obra(params[:q])
        build_resource
      end

      if params[:q][:fornecedor_id_eq] != "" and params[:q][:obra_id_eq] == "" and params[:q][:produto_id_eq] == "" and params[:q][:data_entrada_eq] != "" || params[:q][:data_vencimento_entrada_eq] != ""
        @source = Estoque::EstoqueMovimento.search_todos_pelo_fornecedor(params[:q])
        build_resource
      end

      if params[:q][:fornecedor_id_eq] != "" and params[:q][:obra_id_eq] != "" and params[:q][:produto_id_eq] == "" and params[:q][:data_entrada_eq] != "" || params[:q][:data_vencimento_entrada_eq] != ""
        @source = Estoque::EstoqueMovimento.search_todos_pelo_fornecedor_com_obra(params[:q])
        build_resource
      end

      if params[:q][:fornecedor_id_eq] != "" and params[:q][:obra_id_eq] != "" and params[:q][:produto_id_eq] != "" and params[:q][:data_entrada_eq] != "" || params[:q][:data_vencimento_entrada_eq] != ""
        @source = Estoque::EstoqueMovimento.search_todos(params[:q])
        build_resource
      end

      if params[:q][:fornecedor_id_eq] != "" and params[:q][:obra_id_eq] == "" and params[:q][:produto_id_eq] != "" and params[:q][:data_entrada_eq] != "" || params[:q][:data_vencimento_entrada_eq] != ""
        @source = Estoque::EstoqueMovimento.search_todos_fornecedor_e_produto(params[:q])
        build_resource
      end
    end
  end

  # => Metodo para o filtro do Tipo de Movimento Entrada
  def filtro_de_entrada
    if params[:q][:tipo_movimentacao_eq].eql?("ENTRADA")
      if params[:q][:fornecedor_id_eq] == "" and params[:q][:obra_id_eq] == "" and params[:q][:produto_id_eq] == "" and params[:q][:data_entrada_eq] == "" || params[:q][:data_vencimento_entrada_eq] == ""
        @estoques = Estoque::EstoqueMovimento.search_movimento(params[:q]).page params[:page]
      elsif params[:q][:fornecedor_id_eq] != "" and params[:q][:obra_id_eq] != "" and params[:q][:produto_id_eq] != "" and params[:q][:data_entrada_eq] != "" || params[:q][:data_vencimento_entrada_eq] != ""
        @estoques = Estoque::EstoqueMovimento.search_entrada_completa(params[:q]).page params[:page]
      elsif params[:q][:fornecedor_id_eq] != "" and params[:q][:obra_id_eq] == "" and params[:q][:produto_id_eq] == "" and params[:q][:data_entrada_eq] == "" || params[:q][:data_vencimento_entrada_eq] == ""
        @estoques = Estoque::EstoqueMovimento.search_entrada_com_fornecedor_sem_obra_produto_e_data(params[:q]).page params[:page]
      elsif params[:q][:fornecedor_id_eq] == "" and params[:q][:obra_id_eq] != "" and params[:q][:produto_id_eq] == "" and params[:q][:data_entrada_eq] == "" || params[:q][:data_vencimento_entrada_eq] == ""
        @estoques = Estoque::EstoqueMovimento.search_entrada_com_obra_sem_data_fornecedor_e_produto(params[:q]).page params[:page]
      elsif params[:q][:fornecedor_id_eq] == "" and params[:q][:obra_id_eq] == "" and params[:q][:produto_id_eq] != "" and params[:q][:data_entrada_eq] == "" || params[:q][:data_vencimento_entrada_eq] == ""
        @estoques = Estoque::EstoqueMovimento.search_entrada_com_produto_sem_obra_data_e_fornecedor(params[:q]).page params[:page]
      elsif params[:q][:fornecedor_id_eq] != "" and params[:q][:obra_id_eq] != "" and params[:q][:produto_id_eq] == "" and params[:q][:data_entrada_eq] == "" || params[:q][:data_vencimento_entrada_eq] == ""
        @estoques = Estoque::EstoqueMovimento.search_entrada_com_fornecedor_obra_e_sem_produto_e_data(params[:q]).page params[:page]
      elsif params[:q][:fornecedor_id_eq] != "" and params[:q][:obra_id_eq] == "" and params[:q][:produto_id_eq] != "" and params[:q][:data_entrada_eq] == "" || params[:q][:data_vencimento_entrada_eq] == ""
        @estoques = Estoque::EstoqueMovimento.search_entrada_com_fornecedor_produto_sem_obra_e_data(params[:q]).page params[:page]
      elsif params[:q][:fornecedor_id_eq] != "" and params[:q][:obra_id_eq] != "" and params[:q][:produto_id_eq] != "" and params[:q][:data_entrada_eq] == "" || params[:q][:data_vencimento_entrada_eq] == ""
        @estoques = Estoque::EstoqueMovimento.search_entrada_com_fornecedor_obra_produto_e_sem_data(params[:q]).page params[:page]
      elsif params[:q][:fornecedor_id_eq] == "" and params[:q][:obra_id_eq] == "" and params[:q][:produto_id_eq] == "" and params[:q][:data_entrada_eq] != "" || params[:q][:data_vencimento_entrada_eq] != ""
        @estoques = Estoque::EstoqueMovimento.search_entrada_com_data_sem_fornecedor_obra_e_produto(params[:q]).page params[:page]
      elsif params[:q][:fornecedor_id_eq] != "" and params[:q][:obra_id_eq] == "" and params[:q][:produto_id_eq] == "" and params[:q][:data_entrada_eq] != "" || params[:q][:data_vencimento_entrada_eq] != ""
        @estoques = Estoque::EstoqueMovimento.search_entrada_com_data_fornecedor_sem_obra_e_produto(params[:q]).page params[:page]
      elsif params[:q][:fornecedor_id_eq] == "" and params[:q][:obra_id_eq] != "" and params[:q][:produto_id_eq] == "" and params[:q][:data_entrada_eq] != "" || params[:q][:data_vencimento_entrada_eq] != ""
        @estoques = Estoque::EstoqueMovimento.search_entrada_com_data_obra_sem_fornecedor_e_produto(params[:q]).page params[:page]
      elsif params[:q][:fornecedor_id_eq] == "" and params[:q][:obra_id_eq] == "" and params[:q][:produto_id_eq] != "" and params[:q][:data_entrada_eq] != "" || params[:q][:data_vencimento_entrada_eq] != ""
        @estoques = Estoque::EstoqueMovimento.search_entrada_data_produto_sem_fornecedor_e_obra(params[:q]).page params[:page]
      elsif params[:q][:fornecedor_id_eq] != "" and params[:q][:obra_id_eq] != "" and params[:q][:produto_id_eq] == "" and params[:q][:data_entrada_eq] != "" || params[:q][:data_vencimento_entrada_eq] != ""
        @estoques = Estoque::EstoqueMovimento.search_entrada_data_fornecedor_obra_sem_produto(params[:q]).page params[:page]
      elsif params[:q][:fornecedor_id_eq] != "" and params[:q][:obra_id_eq] == "" and params[:q][:produto_id_eq] != "" and params[:q][:data_entrada_eq] != "" || params[:q][:data_vencimento_entrada_eq] != ""
        @estoques = Estoque::EstoqueMovimento.search_entrada_data_fornecedor_produto_sem_obra(params[:q]).page params[:page]
      elsif params[:q][:fornecedor_id_eq] == "" and params[:q][:obra_id_eq] != "" and params[:q][:produto_id_eq] != "" and params[:q][:data_entrada_eq] == "" || params[:q][:data_vencimento_entrada_eq] == ""
        @estoques = Estoque::EstoqueMovimento.search_entrada_obra_produto_sem_data_e_fornecedor(params[:q]).page params[:page]
      end
    end
  end

  # => Metodo para o filtro do Tipo de Movimento Saida
  # def filtro_de_saida
  #   if params[:q][:tipo_movimentacao_eq].eql?("SAÍDA")
  #     if params[:q][:obra_id_eq] == "" and params[:q][:produto_id_eq] == "" and params[:q][:data_entrada_eq] == "" || params[:q][:data_vencimento_entrada_eq] == ""
  #       @estoques = Estoque::EstoqueMovimento.search_movimento(params[:q]).page params[:page]
  #     elsif params[:q][:obra_id_eq] != "" and params[:q][:produto_id_eq] != "" and params[:q][:data_entrada_eq] != "" || params[:q][:data_vencimento_entrada_eq] != ""
  #       @estoques = Estoque::EstoqueMovimento.search_saida_completa(params[:q]).page params[:page]
  #     elsif params[:q][:obra_id_eq] != "" and params[:q][:produto_id_eq] == "" and params[:q][:data_saida_eq] == "" || params[:q][:data_vencimento_saida_eq] == ""
  #       @estoques = Estoque::EstoqueMovimento.search_saida_com_obra_sem_data_fornecedor_e_produto(params[:q]).page params[:page]
  #     elsif params[:q][:obra_id_eq] == "" and params[:q][:produto_id_eq] != "" and params[:q][:data_saida_eq] == "" || params[:q][:data_vencimento_saida_eq] == ""
  #       @estoques = Estoque::EstoqueMovimento.search_saida_com_produto_sem_obra_data_e_fornecedor(params[:q]).page params[:page]
  #     elsif params[:q][:obra_id_eq] == "" and params[:q][:produto_id_eq] == "" and params[:q][:data_saida_eq] != "" || params[:q][:data_vencimento_saida_eq] != ""
  #       @estoques = Estoque::EstoqueMovimento.search_saida_com_data_sem_fornecedor_obra_e_produto(params[:q]).page params[:page]
  #     elsif params[:q][:obra_id_eq] != "" and params[:q][:produto_id_eq] == "" and params[:q][:data_saida_eq] != "" || params[:q][:data_vencimento_saida_eq] != ""
  #       @estoques = Estoque::EstoqueMovimento.search_saida_com_data_obra_sem_fornecedor_e_produto(params[:q]).page params[:page]
  #     elsif params[:q][:obra_id_eq] == "" and params[:q][:produto_id_eq] != "" and params[:q][:data_saida_eq] != "" || params[:q][:data_vencimento_saida_eq] != ""
  #       @estoques = Estoque::EstoqueMovimento.search_saida_data_produto_sem_fornecedor_e_obra(params[:q]).page params[:page]
  #     elsif params[:q][:obra_id_eq] != "" and params[:q][:produto_id_eq] != "" and params[:q][:data_saida_eq] == "" || params[:q][:data_vencimento_saida_eq] == ""
  #       @estoques = Estoque::EstoqueMovimento.search_saida_obra_produto_sem_fornecedor_e_data(params[:q]).page params[:page]
  #     elsif params[:q][:fornecedor_id_eq] == "" and params[:q][:obra_id_eq] != "" and params[:q][:produto_id_eq] != "" and params[:q][:data_entrada_eq] == "" || params[:q][:data_vencimento_entrada_eq] == ""
  #       @estoques = Estoque::EstoqueMovimento.search_saida_obra_produto_sem_data_e_fornecedor(params[:q]).page params[:page]
  #     end
  #   end
  # end

  # => Metodo para o filtro do Tipo de Movimento Devolucao para o Fornecedor
  def filtro_de_devolucao
    if params[:q][:tipo_movimentacao_eq].eql?("DEVOLUÇÃO PARA FORNECEDOR")
      if params[:q][:fornecedor_id_eq] == "" and params[:q][:obra_id_eq] == "" and params[:q][:produto_id_eq] == "" and params[:q][:data_entrada_eq] == "" || params[:q][:data_devolucao_eq] == ""
        @estoques = Estoque::EstoqueMovimento.search_movimento(params[:q]).page params[:page]
      elsif params[:q][:fornecedor_id_eq] != "" and params[:q][:obra_id_eq] != "" and params[:q][:produto_id_eq] != "" and params[:q][:data_entrada_eq] != "" || params[:q][:data_devolucao_eq] != ""
        @estoques = Estoque::EstoqueMovimento.search_devolucao_completa(params[:q]).page params[:page]
      elsif params[:q][:fornecedor_id_eq] != "" and params[:q][:obra_id_eq] == "" and params[:q][:produto_id_eq] == "" and params[:q][:data_entrada_eq] == "" || params[:q][:data_devolucao_eq] == ""
        @estoques = Estoque::EstoqueMovimento.search_devolucao_com_fornecedor_sem_obra_produto_e_data(params[:q]).page params[:page]
      elsif params[:q][:fornecedor_id_eq] == "" and params[:q][:obra_id_eq] != "" and params[:q][:produto_id_eq] == "" and params[:q][:data_entrada_eq] == "" || params[:q][:data_devolucao_eq] == ""
        @estoques = Estoque::EstoqueMovimento.search_devolucao_com_obra_sem_data_fornecedor_e_produto(params[:q]).page params[:page]
      elsif params[:q][:fornecedor_id_eq] == "" and params[:q][:obra_id_eq] == "" and params[:q][:produto_id_eq] != "" and params[:q][:data_entrada_eq] == "" || params[:q][:data_devolucao_eq] == ""
        @estoques = Estoque::EstoqueMovimento.search_devolucao_com_produto_sem_obra_data_e_fornecedor(params[:q]).page params[:page]
      elsif params[:q][:fornecedor_id_eq] != "" and params[:q][:obra_id_eq] != "" and params[:q][:produto_id_eq] == "" and params[:q][:data_entrada_eq] == "" || params[:q][:data_devolucao_eq] == ""
        @estoques = Estoque::EstoqueMovimento.search_devolucao_com_fornecedor_obra_e_sem_produto_e_data(params[:q]).page params[:page]
      elsif params[:q][:fornecedor_id_eq] != "" and params[:q][:obra_id_eq] == "" and params[:q][:produto_id_eq] != "" and params[:q][:data_entrada_eq] == "" || params[:q][:data_devolucao_eq] == ""
        @estoques = Estoque::EstoqueMovimento.search_devolucao_com_fornecedor_produto_sem_obra_e_data(params[:q]).page params[:page]
      elsif params[:q][:fornecedor_id_eq] != "" and params[:q][:obra_id_eq] != "" and params[:q][:produto_id_eq] != "" and params[:q][:data_entrada_eq] == "" || params[:q][:data_devolucao_eq] == ""
        @estoques = Estoque::EstoqueMovimento.search_devolucao_com_fornecedor_obra_produto_e_sem_data(params[:q]).page params[:page]
      elsif params[:q][:fornecedor_id_eq] == "" and params[:q][:obra_id_eq] == "" and params[:q][:produto_id_eq] == "" and params[:q][:data_entrada_eq] != "" || params[:q][:data_devolucao_eq] != ""
        @estoques = Estoque::EstoqueMovimento.search_devolucao_com_data_sem_fornecedor_obra_e_produto(params[:q]).page params[:page]
      elsif params[:q][:fornecedor_id_eq] != "" and params[:q][:obra_id_eq] == "" and params[:q][:produto_id_eq] == "" and params[:q][:data_entrada_eq] != "" || params[:q][:data_devolucao_eq] != ""
        @estoques = Estoque::EstoqueMovimento.search_devolucao_com_data_fornecedor_sem_obra_e_produto(params[:q]).page params[:page]
      elsif params[:q][:fornecedor_id_eq] == "" and params[:q][:obra_id_eq] != "" and params[:q][:produto_id_eq] == "" and params[:q][:data_entrada_eq] != "" || params[:q][:data_devolucao_eq] != ""
        @estoques = Estoque::EstoqueMovimento.search_devolucao_com_data_obra_sem_fornecedor_e_produto(params[:q]).page params[:page]
      elsif params[:q][:fornecedor_id_eq] == "" and params[:q][:obra_id_eq] == "" and params[:q][:produto_id_eq] != "" and params[:q][:data_entrada_eq] != "" || params[:q][:data_devolucao_eq] != ""
        @estoques = Estoque::EstoqueMovimento.search_devolucao_data_produto_sem_fornecedor_e_obra(params[:q]).page params[:page]
      elsif params[:q][:fornecedor_id_eq] != "" and params[:q][:obra_id_eq] != "" and params[:q][:produto_id_eq] == "" and params[:q][:data_entrada_eq] != "" || params[:q][:data_devolucao_eq] != ""
        @estoques = Estoque::EstoqueMovimento.search_devolucao_data_fornecedor_obra_sem_produto(params[:q]).page params[:page]
      elsif params[:q][:fornecedor_id_eq] != "" and params[:q][:obra_id_eq] == "" and params[:q][:produto_id_eq] != "" and params[:q][:data_entrada_eq] != "" || params[:q][:data_devolucao_eq] != ""
        @estoques = Estoque::EstoqueMovimento.search_devolucao_data_fornecedor_produto_sem_obra(params[:q]).page params[:page]
      elsif params[:q][:fornecedor_id_eq] == "" and params[:q][:obra_id_eq] != "" and params[:q][:produto_id_eq] != "" and params[:q][:data_entrada_eq] == "" || params[:q][:data_vencimento_entrada_eq] == ""
        @estoques = Estoque::EstoqueMovimento.search_devolucao_obra_produto_sem_data_e_fornecedor(params[:q]).page params[:page]
      end
    end
  end

  def filtro_de_transferencia_entrada
    if params[:q][:tipo_movimentacao_eq].eql?("TRANSFERÊNCIA DE ENTRADA")
      if params[:q][:obra_id_eq] == "" and params[:q][:produto_id_eq] == "" and params[:q][:data_entrada_eq] == "" || params[:q][:data_vencimento_entrada_eq] == ""
        @estoques = Estoque::EstoqueMovimento.search_transferencia_entrada(params[:q]).page params[:page]
      end

      if params[:q][:obra_id_eq] == "" and params[:q][:produto_id_eq] == "" and params[:q][:data_entrada_eq] != "" || params[:q][:data_vencimento_entrada_eq] != ""
        @estoques = Estoque::EstoqueMovimento.search_transferencia_com_data(params[:q]).page params[:page]
      end

      if params[:q][:obra_id_eq] != "" and params[:q][:produto_id_eq] == "" and params[:q][:data_entrada_eq] == "" || params[:q][:data_vencimento_entrada_eq] == ""
        @estoques = Estoque::EstoqueMovimento.search_transferencia_com_obra(params[:q]).page params[:page]
      end

      if params[:q][:obra_id_eq] == "" and params[:q][:produto_id_eq] != "" and params[:q][:data_entrada_eq] == "" || params[:q][:data_vencimento_entrada_eq] == ""
        @estoques = Estoque::EstoqueMovimento.search_transferencia_com_produto(params[:q]).page params[:page]
      end

      if params[:q][:obra_id_eq] != "" and params[:q][:produto_id_eq] == "" and params[:q][:data_entrada_eq] != "" || params[:q][:data_vencimento_entrada_eq] != ""
        @estoques = Estoque::EstoqueMovimento.search_transferencia_com_obra_e_data(params[:q]).page params[:page]
      end

      if params[:q][:obra_id_eq] != "" and params[:q][:produto_id_eq] != "" and params[:q][:data_entrada_eq] == "" || params[:q][:data_vencimento_entrada_eq] == ""
        @estoques = Estoque::EstoqueMovimento.search_transferencia_com_obra_e_produto(params[:q]).page params[:page]
      end

      if params[:q][:obra_id_eq] != "" and params[:q][:produto_id_eq] != "" and params[:q][:data_entrada_eq] != "" || params[:q][:data_vencimento_entrada_eq] != ""
        @estoques = Estoque::EstoqueMovimento.search_transferencia_entrada_completa(params[:q]).page params[:page]
      end
    end
  end

  def filtro_de_transferencia_saida
    if params[:q][:tipo_movimentacao_eq].eql?("TRANSFERÊNCIA DE SAÍDA")
      if params[:q][:obra_id_eq] == "" and params[:q][:produto_id_eq] == "" and params[:q][:data_entrada_eq] == "" || params[:q][:data_vencimento_entrada_eq] == ""
        @estoques = Estoque::EstoqueMovimento.search_transferencia_saida(params[:q]).page params[:page]
      end

      if params[:q][:obra_id_eq] == "" and params[:q][:produto_id_eq] == "" and params[:q][:data_entrada_eq] != "" || params[:q][:data_vencimento_entrada_eq] != ""
        @estoques = Estoque::EstoqueMovimento.search_transferencia_com_data(params[:q]).page params[:page]
      end

      if params[:q][:obra_id_eq] != "" and params[:q][:produto_id_eq] == "" and params[:q][:data_entrada_eq] == "" || params[:q][:data_vencimento_entrada_eq] == ""
        @estoques = Estoque::EstoqueMovimento.search_transferencia_com_obra(params[:q]).page params[:page]
      end

      if params[:q][:obra_id_eq] == "" and params[:q][:produto_id_eq] != "" and params[:q][:data_entrada_eq] == "" || params[:q][:data_vencimento_entrada_eq] == ""
        @estoques = Estoque::EstoqueMovimento.search_transferencia_com_produto(params[:q]).page params[:page]
      end

      if params[:q][:obra_id_eq] != "" and params[:q][:produto_id_eq] == "" and params[:q][:data_entrada_eq] != "" || params[:q][:data_vencimento_entrada_eq] != ""
        @estoques = Estoque::EstoqueMovimento.search_transferencia_com_obra_e_data(params[:q]).page params[:page]
      end

      if params[:q][:obra_id_eq] != "" and params[:q][:produto_id_eq] != "" and params[:q][:data_entrada_eq] == "" || params[:q][:data_vencimento_entrada_eq] == ""
        @estoques = Estoque::EstoqueMovimento.search_transferencia_com_obra_e_produto(params[:q]).page params[:page]
      end

      if params[:q][:obra_id_eq] != "" and params[:q][:produto_id_eq] != "" and params[:q][:data_entrada_eq] != "" || params[:q][:data_vencimento_entrada_eq] != ""
        @estoques = Estoque::EstoqueMovimento.search_transferencia_saida_completa(params[:q]).page params[:page]
      end
    end
  end

  private
    def build_resource
      @search_all = Estoque::Formatter.new(params[:q][:tipo_movimentacao_eq], @source)
    end
end
