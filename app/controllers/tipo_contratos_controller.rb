class TipoContratosController < BrantController
  before_action :set_tipo_contrato, only: [:show, :edit, :update, :destroy]

  respond_to :html

  def index
    # @tipo_contratos = policy_scope(Tipo_Contrato).page params[:page]
    session[:page] = params[:page]
    @search = TipoContrato.ransack(params[:q])
    @tipo_contratos = @search.result.order(:name).page(params[:page]).per(10)
    @search.build_condition if @search.conditions.empty?
    
   end


  def new
    @tipo_contrato = user.tipo_contratos.build
    authorize @tipo_contrato
    respond_with(@tipo_contrato)
  end

  def create
    @tipo_contrato = user.tipo_contratos.build(resource_params)
    if @tipo_contrato.save
      redirect_to new_tipo_contrato_path, notice: t("flash.actions.#{__method__}.notice", resource_name: t('activerecord.models.tipo_contrato'))
    else
      flash[:error] = t("flash.actions.#{__method__}.alert", resource_name: t('activerecord.models.tipo_contrato'))
      render :edit
    end
  end

  def show
  end

  def edit
  end

  def update
    
   
    
    @tipo_contrato.status=(resource_params[:_status])
    @tipo_contrato.user=(user) if !@tipo_contrato.user.present?

    if @tipo_contrato.update(resource_params)
      flash[:info] = t("flash.actions.#{__method__}.notice", resource_name: t('activerecord.models.tipo_contrato'))
      redirect_to tipo_contratos_url
    else
      flash[:error] = t("flash.actions.#{__method__}.alert", resource_name: t('activerecord.models.tipo_contrato'))
      render :edit
    end
  end

  def destroy
    if @tipo_contrato.destroy
      flash[:info] = t("flash.actions.#{__method__}.notice", resource_name: t('activerecord.models.tipo_contrato'))
    else
      flash[:error] = t("flash.actions.#{__method__}.alert", resource_name: t('activerecord.models.tipo_contrato'))
    end
    redirect_to tipo_contratos_url
  end

  private
    def set_tipo_contrato
      @tipo_contrato = TipoContrato.find(params[:id])
      authorize @tipo_contrato
    end

    def resource_params
      params.require(:tipo_contrato).permit(:name, :user_id, :_status)
    end
end
