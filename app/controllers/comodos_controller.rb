class ComodosController < BrantController
  before_action :set_comodo, only: [:show, :edit, :update, :destroy]

  respond_to :html

  def index
    if params[:q]
      @search = Comodo.search(params[:q])
      @comodos = @search.page(params[:page])
    else
      @comodos = Comodo.all.order(name: :asc).page(params[:page])
    end
  end

  def show
    respond_with(@comodo)
  end

  def new
    @comodo = Comodo.new
    respond_with(@comodo)
  end

  def edit
  end

  def create
    @comodo = Comodo.new(comodo_params)
    unless @comodo.save
      render :new
    else
      flash[:notice] = t("flash.actions.#{__method__}.notice", resource_name: @comodo.name) 
      redirect_to :back
    end
  end

  def update
    unless @comodo.update(comodo_params)
      render :edit
    else
      flash[:notice] = t("flash.actions.#{__method__}.success", resource_name: @comodo.name) 
      redirect_to comodos_path
    end
  end

  def destroy
    @comodo.destroy
  end

  private
    def set_comodo
      @comodo = Comodo.find(params[:id])
    end

    def comodo_params
      params.require(:comodo).permit(:name)
    end
end
