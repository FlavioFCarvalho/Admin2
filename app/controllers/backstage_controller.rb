class BackstageController < ApplicationController
  before_action :authenticate_admin!
  layout 'backstage'

  def pundit_user
    current_admin
  end
end
