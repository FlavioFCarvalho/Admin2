class ProdutosController < BrantController
  before_action :set_produto, only: [:show, :edit, :update, :destroy]

  respond_to :html

  def index
    session[:page] = params[:page]
    @search = Produto.ransack(params[:q])
    @produtos = @search.result.order(:nome).page(params[:page]).per(10)
    @search.build_condition if @search.conditions.empty?
  end

  def show
    respond_with(@produto)
  end

  def new
    @produto = user.produtos.build
    # @produto_categoria = @produto.produto_categorias.build
    authorize @produto
    respond_with(@produto)
  end

  def edit
    # @produto_categorias = @produto.produto_categorias
  end

  def create
    @produto = user.produtos.build(resource_params)
    if @produto.save
      session[:produto_id] = @produto.id
      redirect_to new_produto_categoria_path
    else
      flash[:error] = t("flash.actions.create.alert", resource_name: "Produto")
      render :new
    end
  end

  def update
    @produto.status=(resource_params[:_status])
    if @produto.update(resource_params)
      session[:produto_id] = @produto.id
      redirect_to new_produto_categoria_path
    else
      flash[:error] = t("flash.actions.update.alert", resource_name: "Produto")
      render :edit
    end
  end

  def destroy
    @produto.destroy
    respond_with(@produto)
  end

  private
    def set_produto
      @produto = Produto.find(params[:id])
      authorize @produto
    end

    def resource_params
      params.require(:produto).permit(:status,
                                      :_status,
                                      :nome,
                                      :valor,
                                      :unidade,
                                      :observacoes,
                                      :categoria_ids =>[])
    end
end
