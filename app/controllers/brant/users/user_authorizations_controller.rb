class Brant::Users::UserAuthorizationsController < BrantController
  include Wicked::Wizard

  steps :permission


  def show
    @user = User.find(session[:user_id] || params[:user_id])
    render_wizard
  end

  def update
    @user = User.find(session[:user_id] || params[:user_id])

    if params[:user_authorizations]
      @user.import_authorizations(params[:user_authorizations])
    end

    if @user.save
      respond_to &:json
    end
  end
end
