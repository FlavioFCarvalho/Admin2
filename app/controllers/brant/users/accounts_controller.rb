class Brant::Users::AccountsController < BrantController
  before_action :find_user, only: [:show, :edit, :update]
  def index
    @users = policy_scope(User)
  end

  def show
  end

  def new
    @user = User.new
    @user.build_profile
    authorize @user
  end

  def create
    @user = User.new(account_params)
    @user.add_role=(account_params[:type])
    if @user.save
      if @user.user?
        if @user.authorization_users.empty?
          session[:user_id] = @user.id
          redirect_to brant_users_user_authorizations_path, notice: t('flash.actions.create.notice', resource_name: @user.email)
        end
      else
        redirect_to brant_users_accounts_path, notice: t('flash.actions.create.notice', resource_name: @user.email)
      end
    else
      flash[:error] = t('flash.actions.create.error', resource_name: "Usuário")
      render :new
    end
  end

  def edit
  end

  def update
    @user.add_role=(account_params[:type])
    if update_account?
      redirect_to brant_users_accounts_path, notice: t('flash.actions.update.notice', resource_name: @user.email)
    else
      flash[:error] = t('flash.actions.update.error', resource_name: "Usuário")
      render :edit
    end
  end


  private
    def update_account?
      if password_blank?
        @user.update_without_password(account_params)
      else
        @user.update_attributes(account_params)
      end
      return true
    end


    def find_user
      @user = User.find(params[:id])
      authorize @user
    end

    def account_params
      params.require(:user).permit(:username, :email, :role, :type, :password, :password_confirmation, :profile_attributes => [:id, :user_id, :name, :_destroy])
    end

    def password_blank?
      params[:user][:password].blank? &&
      params[:user][:password_confirmation].blank?
    end
end
