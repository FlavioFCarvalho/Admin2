class Brant::Users::SessionsController < Devise::SessionsController
  layout 'user_session'

  def new
    super
    # redirect_to root_path and return
  end

  def create
    super
    # user = User.authenticate(params[:email], params[:password])
    # if user
    #   session[:user_id] = user.id
    #   flash.now[:notice] = "Logged in!"
    #   redirect_to root_url
    # else
    #   flash[:error] = "Invalid email or password"
    #   redirect_to brant_start_index_path, remote: true
    # end
  end


  protected
    def after_signed_in_path_for(resource)
      stored_location = stored_location_for(resource)
      if stored_location.match(index_path)
        stored_location
      else
        # ??
      end
    end
end
