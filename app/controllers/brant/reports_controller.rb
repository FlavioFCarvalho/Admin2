class Brant::ReportsController < BrantController

  def print_individual_cliente_fornecedor
    @report = ReportConfiguration.first
    unless @report.nil?
      @cliente_fornecedor = ClienteFornecedor.find(params[:id])
      respond_to do |format|
        format.html
        format.pdf do
          pdf = ClienteFornecedorIndividualPrint.new(@cliente_fornecedor, @report, view_context)
          send_data pdf.render, filename: "#{@cliente_fornecedor.pdf_file_name}", type: 'application/pdf', disposition: 'inline'
        end
      end
    else
      flash[:error] = t('helpers.reports.configuration_not_found')
    end
  end

  def print_page_cliente_fornecedor
    @report = ReportConfiguration.first
    unless @report.nil?
      respond_to do |format|
        format.html
        format.pdf do
          pdf = ClienteFornecedorPrintPage.new($page_cliente_fornecedores, @report, view_context)
          send_data pdf.render, filename: "Relatorio_cliente_fornecedor", type: 'application/pdf', disposition: 'inline'
        end
      end
    else
      flash[:error] = t('helpers.reports.configuration_not_found')
    end
  end

  def report_all_etapas
    @report = ReportConfiguration.first
    unless @report.nil?
      @obra_etapas = ItemObra.all.alphabetic_order
      respond_to do |format|
        format.html
        format.pdf do
          pdf = EtapaObraPdfCompleto.new(@obra_etapas, @report, view_context)
          send_data pdf.render, filename: "Relatorio_etapas_da_obra", type: 'application/pdf', disposition: 'inline', taget: :_blank
        end
      end
    else
      flash[:error] = t('helpers.reports.configuration_not_found')
    end
  end
end
