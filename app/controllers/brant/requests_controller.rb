class Brant::RequestsController < BrantController
  def find_resource
    resource = params[:resource]
    @resource = resource[:name].constantize.find(resource[:id])
  end

  def create_service_through_ajax
    @service = current_user.services.build(service_params)
    @service.save
  end

  def create_banco_through_ajax
    @banco = current_user.bancos.build(banco_params)
    @banco.save
  end

  def create_product_through_ajax
    @produto = current_user.produtos.build(produto_params)
    if params[:produto][:categoria]
      @produto.produto_categorias.build(categoria_id: params[:produto][:categoria][:id])
    end
    @produto.save
  end

  def create_comodo_through_ajax
    @comodo = current_user.comodos.build(comodo_params)
    @comodo.save
  end

  def create_work_through_ajax
    @obra = current_user.obras.build(obra_params)
    @obra.save
  end

  def create_categoria_through_ajax
    @categoria = current_user.categorias.build(categoria_params)
    @categoria.save
  end

  def collect_fornecedor_services
    @cliente_fornecedor_services = ClienteFornecedor.find(params[:id]).fornecedor_services
  end

  def collect_fornecedor_products
    @cliente_fornecedor_products = ClienteFornecedor.find(params[:id]).fornecedor_products
  end

  def collect_cliente_obras
    @cliente_fornecedor_obras = ClienteFornecedor.find(params[:id]).cliente_works
  end

  def collect_contas_bancarias
    @cliente_fornecedor_contas_bancaria = ClienteFornecedor.find(params[:id]).contas_bancaria
  end

  def destroy_cliente_obras
    @cliente_fornecedor = ClienteFornecedor.find(params[:id])
    @cliente_obra = @cliente_fornecedor.cliente_works.find(params[:fornecedor_service_id])
    @before_destroy_id = @cliente_obra.id
    if @cliente_obra.destroy
      @deleted = true
    else
      @deleted = false
    end
  end

  def destroy_fornecedor_services
    @cliente_fornecedor = ClienteFornecedor.find(params[:id])
    @fornecedor_service = @cliente_fornecedor.fornecedor_services.find(params[:fornecedor_service_id])
    @before_destroy_id = @fornecedor_service.id
    if @fornecedor_service.destroy
      @deleted = true
    else
      @deleted = false
    end
  end

  def destroy_fornecedor_products
    @cliente_fornecedor = ClienteFornecedor.find(params[:id])
    @fornecedor_product = @cliente_fornecedor.fornecedor_products.find(params[:fornecedor_product_id])
    @before_destroy_id = @fornecedor_product.id
    if @fornecedor_product.destroy
      @deleted = true
    else
      @deleted = false
    end
  end

  private
    def categoria_params
      params.require(:categoria).permit(:nome)
    end

    def service_params
      params.require(:service).permit(:name)
    end

    def banco_params
      params.require(:banco).permit(:nome)
    end

    def comodo_params
      params.require(:comodo).permit(:name)
    end

    def produto_params
      params.require(:produto).permit(:nome, :valor, :unidade, :observacoes)
    end

    def obra_params
      params.require(:obra).permit(:codigo, :nome_local, :user_id,
                                   :endereco_local, :bairro, :cep,
                                   :cidade_id, :estado_id, :endereco_obra,
                                   :quadra, :lote, :cei, :peticao, :aprovacao)
    end
end
