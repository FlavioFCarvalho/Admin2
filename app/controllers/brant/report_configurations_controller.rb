class Brant::ReportConfigurationsController < BrantController
  before_action :set_report_configuration, only: [:show, :edit, :update, :destroy]

  respond_to :html

  def index
    @report_configurations = ReportConfiguration.all
    if @report_configurations.empty?
      redirect_to new_brant_report_configuration_path
    else
      redirect_to brant_report_configuration_path(@report_configurations.first)
    end
  end

  def show
    respond_with(@report_configuration)
  end

  def new
    @report_configuration = ReportConfiguration.new
    respond_with(@report_configuration)
  end

  def edit
  end

  def create
    @report_configuration = ReportConfiguration.new(report_configuration_params)
    @report_configuration.save
    redirect_to brant_report_configuration_path(@report_configuration)

  end

  def update
    @report_configuration.update(report_configuration_params)
    redirect_to brant_report_configuration_path(@report_configuration)
  end

  private
    def set_report_configuration
      @report_configuration = ReportConfiguration.find(params[:id])
    end

    def report_configuration_params
      params.require(:report_configuration).permit(:company, :cnpj, :phone, :address, :state_id, :city_id, :email, :neighborhood, :picture)
    end
end
