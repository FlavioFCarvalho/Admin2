class ContasController < BrantController
  before_action :set_conta, only: [:show, :edit, :update, :destroy]

  respond_to :html

  def index
    @search = Conta.ransack(params[:q])
    @contas = @search.result.order(:banco_id,:agencia,:numero).page(params[:page]).per(10)
    @search.build_condition if @search.conditions.empty?
  end

  def show
    respond_with(@conta)
  end

  def new
    @conta = Conta.new
    respond_with(@conta)
  end

  def edit
  end

  def create
    @conta = Conta.new(conta_params)
    if @conta.save
      redirect_to new_conta_path, notice: t("flash.actions.#{__method__}.success_fem", resource_name: t('activerecord.models.account'))
    else
      flash[:error] = t("flash.actions.#{__method__}.alert_fem", resource_name: t('activerecord.models.account'))
      render :new
    end
  end

  def update
    if @conta.update(conta_params)
      flash[:info]= t("flash.actions.#{__method__}.success_fem", resource_name: t('activerecord.models.account'))
      redirect_to contas_url
    else
      flash[:error] = t("flash.actions.#{__method__}.alert_fem", resource_name: t('activerecord.models.account'))
      render :edit
    end
  end

  def destroy
    if @conta.destroy
      flash[:info] = t("flash.actions.#{__method__}.success_fem", resource_name: t('activerecord.models.account'))
    else
      flash[:error] = t("flash.actions.#{__method__}.alert_fem", resource_name: t('activerecord.models.account'))
    end
    redirect_to contas_url
  end

  private
    def set_conta
      @conta = Conta.find(params[:id])
    end

    def conta_params
      params.require(:conta).permit(:banco_id, :agencia, :numero, :tipo_conta,
                                    :operacao, :titular, :cpf_cnpj)
    end
end
