'''
  CONTROLLER PARA ITENS DAS ETAPAS DA OBRA
'''
class ItensObraController < BrantController
  before_action :set_item_obra, only: [:show, :edit, :update, :destroy]
  before_action :set_authorization, only: [:show, :edit, :destroy]

  respond_to :html

  def index
    session[:page] = params[:page]
    @search = ItemObra.ransack(params[:q])
    @itens_obra = @search.result.order(:item).page(params[:page]).per(10)
    @search.build_condition if @search.conditions.empty?
  end

  def show
    @sub_itens_obras=@item_obra.sub_itens_obras.page params[:page]
    respond_to do |format|
      format.html
      format.js { render :layout => false }
    end
  end

  def search
    if params[:sub_item_obra]
      @sub_itens_obras = SubItemObra.search(params[:sub_item_obra])
      respond_to &:js
    end
  end

  def search_itens
    if params[:item_obras]
      @itens_obra = ItemObra.search(params[:item_obras])
      respond_to &:js
    end
  end

  def new
    @item_obra = ItemObra.new
    @item_obra.sub_itens_obras.build
    respond_with(@item_obra)
  end

  def edit
    @item_obra.sub_itens_obras.build if @item_obra.sub_itens_obras.blank?
  end

  def create
    @item_obra = ItemObra.new(resource_params)
    if @item_obra.save
      redirect_to new_item_obra_path, notice: t("flash.actions.#{__method__}.success", resource_name: "Item Obra")
    else
      flash[:error] = t("flash.actions.#{__method__}.alert", resource_name: "Item Obra")
      render :new
    end
  end

  def update
    flash[:notice] =  t("flash.actions.update.success", resource_name: "Item Obra") if @item_obra.update(resource_params)
    respond_with(@item_obra)
  end

  def destroy
    flash[:info] =  t("flash.actions.destroy.success", resource_name: "Item Obra") if @item_obra.destroy
    redirect_to :back
  end

  private
    def set_authorization
      authorize @item_obra
    end

    def set_item_obra
      @item_obra = ItemObra.find(params[:id])
    end

    def resource_params
      params.require(:item_obra).permit(:item, :tipo_servico, sub_itens_obras_attributes: [:id,
                                                                                          :sub_tipo_servico,
                                                                                          :sub_item,
                                                                                          :unidade,
                                                                                          :item_obra_id,
                                                                                          :_destroy])
    end
end
