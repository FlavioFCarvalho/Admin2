class RelobpsController < BrantController
  def index
    if params[:obp]
      @account = Conta.find(params[:obp][:conta_bancaria])
      @payment_orders = Estoque::EstoqueMovimento.generate_order(params[:obp])
      @stock_jobs     = Obra.return_distinct_job_code(params[:obp])
      @configuration = ReportConfiguration.first
      respond_to do |format|
        format.html
        format.pdf do
          obp_report = RelObpPdf.new(@account, @payment_orders, @stock_jobs, @configuration, params[:obp][:data_vencimento], view_context)
          send_data obp_report.render, filename: "RELATORIO_ORDEM_BANCARIA_PAGAMENTO_#{_date_report_}", type: 'application/pdf', disposition: 'inline'
        end
      end
    end
  end

  private
    def _date_report_
      return Date.today.strftime("%Y%m%d")
    end
end
