class ObrasController < BrantController
  before_action :set_obra, only: [:show, :edit, :update, :destroy]

  respond_to :html

  def index
    session[:page] = params[:page]
    @search = Obra.ransack(params[:q])
    @obras = @search.result.where.not(nome_local: "BRANT").order(codigo: :asc).page(params[:page]).per(10)
    @search.build_condition if @search.conditions.empty?
  end

  def show
  end

  def new
    @obra = user.obras.build
    @comodo = @obra.comodos.build
    authorize @obra
    respond_with(@obra)
  end
  
  def edit
    @comodo = @obra.comodos.build
  end
  
  def create
    @obra = user.obras.build(resource_params)
    @obra.save
    respond_to &:json
  end
  
  def update
    @obra.update(resource_params)
    respond_to &:json
  end
  
  def destroy
    authorize @obra
    @old_obra_id=@obra.id
    @obra.destroy
    respond_to &:js
  end
  
  def destroy_comodo
    @obra = Obra.find(params[:id])
    @comodo_obra = ComodoObra.find_by(obra_id: @obra.id, comodo_id: params[:comodo_id])
    @old_comodo_obra_id=@comodo_obra.id
    @comodo_obra.destroy
    respond_to &:js
  end

  private
    def set_obra
      @obra = Obra.find(params[:id])
      authorize @obra
    end

    def resource_params
      params.require(:obra).permit(:status,
                                   :codigo,
                                   :nome_local,
                                   :endereco_local,
                                   :bairro,
                                   :cep,
                                   :estado_id,
                                   :cidade_id,
                                   :endereco_obra,
                                   :quadra,
                                   :lote,
                                   :cei,
                                   :peticao,
                                   :aprovacao,
                                   :user_id,
                                   comodo_obras_attributes: [:id, :comodo_id, :_destroy])
    end
end
