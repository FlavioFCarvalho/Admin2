class ClienteFornecedoresController < BrantController
  include ClienteFornecedorControllerConcern

  # before_action only: [:index] do
  #   get_query('ransack_search')
  # end

  def index
    cookies[:page_cliente_fornecedores] = nil
    session[:page] = params[:page]
    @search = ClienteFornecedor.ransack(params[:q])
    @cliente_fornecedores = @search.result.order("nome ASC").page(params[:page]).per(10)
    load_and_update_if_there_are_uncompleted_fields_for @cliente_fornecedores
    $page_cliente_fornecedores = @search.result.order("nome ASC")
    @search.build_condition if @search.conditions.empty?
  end

  def show
    respond_with(@cliente_fornecedor)
  end

  def new
    @cliente_fornecedor = user.cliente_fornecedores.build
     authorize @cliente_fornecedor
    respond_with(@cliente_fornecedor)
  end

  def edit
   
  end

  def create
    @cliente_fornecedor= user.cliente_fornecedores.build(resource_params)
    if @cliente_fornecedor.save
      redirect_to :back, notice: t("flash.actions.#{__method__}.notice", resource_name: t('activerecord.models.cliente'))
    else
      flash[:error] = t("flash.actions.#{__method__}.alert", resource_name: t('activerecord.models.cliente'))
      render :edit
    end 
  end

  def update
    @cliente_fornecedor.user=(current_user)
    if @cliente_fornecedor.update(resource_params)
      redirect_to :back, notice: t("flash.actions.#{__method__}.notice", resource_name: t('activerecord.models.cliente'))
    else
      flash[:error] = t("flash.actions.#{__method__}.alert", resource_name: t('activerecord.models.cliente'))
      render :edit
    end 
  end

  def destroy
    @cliente_fornecedor.destroy
    respond_with(@cliente_fornecedor)
  end

  def print_record

  end

  private
    def resource_params
      params.require(:cliente_fornecedor).permit(:id, :status, :tipo_pessoa, :dados,
                                                 :tipo_cliente, :tipo_fornecedor,
                                                 :tipo_investidor, :cliente_fornecedor,
                                                 :nome, :razao_social, :cpf, :cnpj, :rg,
                                                 :sexo, :endereco, :numero, :complemento,
                                                 :cep, :bairro, :cidade_id, :estado_id,
                                                 :contato1_nome, :contato1_ddd, :contato1_telefone,
                                                 :contato1_tipo_telefone, :contato1_email1,
                                                 :contato1_email2, :contato2_nome, :contato2_ddd,
                                                 :contato2_telefone, :contato2_tipo_telefone,
                                                 :contato2_email1, :contato2_email2, :contato3_nome,
                                                 :contato3_ddd, :contato3_telefone, :contato3_tipo_telefone,
                                                 :contato3_email1, :contato3_email2, :inscricao_estadual, 
                                                 :observacao, :data_nascimento)
    end
end
