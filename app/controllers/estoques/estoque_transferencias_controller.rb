'''
  Controller de Transferencias do Estoque
'''
# -*- coding: utf-8 -*-
class Estoques::EstoqueTransferenciasController < BrantController
  include ParcelamentoConcern
  include CurrentItem
  before_action :find_estoque
  before_action :set_item, only: [:create]
  respond_to :html

  def new
    verifica_itens_adicionados
    inicializa_campos
    session[:qtParcelas]= @estoque.parcelas
  end

  def create
    @estoque_transferencia = @item.add_transferencia_produto(@estoque.transferencia_tipo, estoque_transferencia_params)
    @estoque_transferencia.verify_last_stock
    @estoque_transferencia.add_quantity_to_estoque_transferencia
    if @estoque_transferencia.save
      @estoque_transferencia_parcelamentos = Estoque::EstoqueTransferencia.build_parcelamento_from_source(@estoque_transferencia, session[:qtParcelas])

      if @estoque_transferencia_parcelamentos
        @estoque = Estoque::EstoqueMovimento.find(@estoque_transferencia.estoque_movimento)
        _create_parcelas_
        flash[:notice]=t("flash.actions.#{__method__}.success", resource_name: "Transferência")
        redirect_to new_estoques_estoque_movimentaco_estoque_transferencia_url(@estoque_transferencia.estoque_movimento)
      end
    else
      flash[:error]=t("flash.actions.#{__method__}.alert", resource_name: "Transferência")
      @item = Item.find(session[:item_id])
      @obra_brant = Obra.find_by_codigo("BRANT")
      render :new
    end
  end

  def destroy
    @estoque_transferencia = Estoque::EstoqueTransferencia.find(params[:id])
    @estoque = Estoque::EstoqueMovimento.find(params[:estoque_movimentaco_id])
    @deleted_id =@estoque_transferencia.id
    
    if @estoque_transferencia.destroy
      @confirmed = true
    else
      @confirmed = false
    end
  end

  def update
    @estoque_transferencia = @estoque.estoque_transferencias.find(params[:estoque_estoque_transferencia][:id])
    @estoque_transferencia.verify_changes_on_main_fields(params[:estoque_estoque_transferencia])
    @estoque_transferencia.simula_valor_total_de_produtos(params[:estoque_estoque_transferencia][:valor_produto])

    if @estoque_transferencia.save
      @estoque_transferencia_parcelamentos = Estoque::EstoqueTransferencia.build_parcelamento_from_source(@estoque_transferencia, session[:qtParcelas])
      @estoque = Estoque::EstoqueMovimento.find(@estoque_transferencia.estoque_movimento)
      @estoque.valor_total_produto=0
      @estoque.total_estoque=0
      Estoque::EstoqueTransferencia.where(estoque_movimento_id: @estoque.id).each do |f|
        @estoque.valor_total_produto += f.valor_total.to_f
        @estoque.total_estoque       += f.valor_total.to_f
      end
      @estoque.save
      _create_parcelas_
      if @estoque_produto_parcelamentos
        flash[:notice] = t("flash.actions.#{__method__}.success", resource_name: "Produto")
        redirect_to new_estoques_estoque_movimentaco_estoque_transferencia_path(@estoque)
      end
    else
      flash[:error] = t("flash.actions.#{__method__}.alert", resource_name: "Produto")
      render 'new'
    end
  end

  private
    def verifica_itens_adicionados
      @estoque_transferencia = @estoque.estoque_transferencias.last
      if !@estoque_transferencia.nil?
        @item = @estoque_transferencia.item
        session[:item_id] = @item.id
      elsif session[:item_id] != nil
        @item = Item.find(session[:item_id])
      else
        @item = Item.create
        session[:item_id] = @item.id
      end
    end

    def inicializa_campos
      @estoque_transferencia = @estoque.estoque_transferencias.build
      @produto = @estoque.produtos.build
      @produto.categorias.build

      # => Em caso de Transferencia do tipo entrada
      # => Obra Brant é Carregada automaticamente
      @obra_brant = Obra.find_by_codigo("BRANT")
    end

    def find_estoque
      @estoque = Estoque::EstoqueMovimento.find(params[:estoque_movimentaco_id])
    end

    def estoque_transferencia_params
      params.require(:estoque_estoque_transferencia).permit(:id,
                                                            :estoque_movimento_id,
                                                            :produto_id,
                                                            :obra_anterior_id,
                                                            :obra_posterior_id,
                                                            :item_obra_id,
                                                            :sub_item_obra_id,
                                                            :quantidade_movimento,
                                                            :comodo_id,
                                                            :valor_produto,
                                                            :valor_total)
    end
end