#-*- coding: utf-8 -*-
class Estoques::EstoqueMovimentacoesController < EstoquesController
  include ParcelamentoConcern
  include FiltroEstoque

  before_action :find_estoque, only: [:show, :edit, :update, :unlock_through_validate_by]
  respond_to :html

  def index
    @status_validos = Estoque::EstoqueMovimento::STATUS_VALIDOS.map { |v| v.to_s }

    if params[:q]
      filtro_de_todos
      filtro_de_entrada
      filtro_de_devolucao
      filtro_de_transferencia_entrada
      filtro_de_transferencia_saida
    else
      @estoque_incompleto = Estoque::EstoqueMovimento.all
      @estoque_incompleto.each do |ei|
        if !ei.tipo_movimentacao.upcase.eql?("TRANSFERÊNCIA")
          ei.confronta_valores_de_produtos
        else
          ei.confronta_valores_de_transferencia
        end
      end
      @search = Estoque::EstoqueMovimento.ransack(params[:q])
      @estoques = @search.result(distinct: true).order_by_incompleto.page params[:page]
    end
  end

  def show
    session[:item_id] = nil
    if !@estoque.tipo_movimentacao.upcase.eql?("TRANSFERÊNCIA")
      @estoque_produtos = @estoque.estoque_produtos.page params[:page]
    else
      @estoque_transferencias = @estoque.estoque_transferencias.page params[:page]
    end
  end

  def new
    session[:item_id] = nil
    session[:qtParcelas]= nil 
    @estoque = user.estoques_movimentos.build
  end

  def edit
    session[:item_id] = nil
    session[:qtParcelas]= nil
    @estoque_movimento_parcelas = @estoque.estoque_movimento_parcelas
  end
  
  def create
    _start_
    if _successfully_created_
      session[:qtParcelas]= @estoque.parcelas
      _check_transfer_
      _create_parcelas_
    end
  end
  
  def update
    if _successfully_updated_
      _check_transfer_
      _create_parcelas_
      _reload_if_item_parcelas_
    end
  end

  def unlock_through_validate_by
    $total_produtos = @estoque.return_total_produtos if !@estoque.tipo_movimentacao.eql?("Transferência")
    $total_produtos = @estoque.return_total_produtos_transferencia if @estoque.tipo_movimentacao.eql?("Transferência")
    $total_estoque = 0
    if @estoque.valida_por.eql?("total_estoque")
      $total_estoque = @estoque.total_estoque.to_f
      if ($total_produtos < $total_estoque) or ($total_produtos > $total_estoque)
        $lock = true
      else
        $lock = false
      end
    elsif @estoque.valida_por.eql?("total_produtos")
      $total_estoque = @estoque.valor_total_produto.to_f
      if ($total_produtos < $total_estoque) or ($total_produtos > $total_estoque)
        $lock = true
      else
        $lock = false
      end
    end
    respond_to &:js
  end

  private
    def _start_
      @estoque = user.estoques_movimentos.build(resource_params)
    end

    def _successfully_created_
      @estoque.save
    end

    def _check_transfer_
      if @estoque.tipo_movimentacao.eql?("Transferência")
        @estoque.add_transferencia_tipo=resource_params[:transferencia_tipo]
        @estoque.save
      end
    end

    def _reload_if_item_parcelas_
      if !@estoque.tipo_movimentacao.eql?("Transferência")
        @estoque_produtos = @estoque.estoque_produtos
        unless @estoque_produtos.empty?
          @estoque_produtos.each do |ep|
            Estoque::EstoqueProduto.build_parcelamento_from_source(ep, @estoque.parcelas)
          end
        end
      else
        @estoque_transferencias = @estoque.estoque_transferencias
        unless @estoque_transferencias.empty?
          @estoque_transferencias.each do |et|
            Estoque::EstoqueTransferencia.build_parcelamento_from_source(et, @estoque.parcelas)
          end
        end
      end
    end

    def _successfully_updated_
      @estoque.update(resource_params)
      session[:qtParcelas]= @estoque.parcelas
    end

    def find_estoque
      @id = check_params
      @estoque = Estoque::EstoqueMovimento.find_by(id: @id.call(params[:id], params[:estoque_movimentaco_id]))
    end

    def resource_params
      params.require(:estoque_estoque_movimento).permit(:id,
                                                        :tipo_movimentacao,
                                                        :fornecedor_id,
                                                        :destinatario_tomador_id,
                                                        :user_id,
                                                        :nota_fiscal_entrada,
                                                        :numero_pedido,
                                                        :recibo,
                                                        :cupom_fiscal,
                                                        :numero_da_os,
                                                        :nota_fiscal_saida,
                                                        :data_entrada,
                                                        :data_vencimento_entrada,
                                                        :data_vencimento_saida,
                                                        :data_saida,
                                                        :data_devolucao,
                                                        :data_transferencia,
                                                        :justificativa_devolucao,
                                                        :transferencia_tipo,
                                                        :total_estoque,
                                                        :parcelas,
                                                        :periodo,
                                                        :valor_total_produto,
                                                        :acrescimo,
                                                        :boleto,
                                                        :frete,
                                                        :desconto,
                                                        :observacoes,
                                                        :valida_por,
                                                        :pronto_pagamento)
    end
end