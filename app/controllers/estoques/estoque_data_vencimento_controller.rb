#-*- coding: utf-8 -*-
class Estoques::EstoqueDataVencimentoController < EstoquesController
  def update
    @estoque_movimento_parcela = Estoque::EstoqueMovimentoParcela.find(params[:id])
    @estoque = @estoque_movimento_parcela.estoque_movimento
    @estoque_movimento_parcela.update_attributes(data_vencimento: params[:data_vencimento])

    if !@estoque.tipo_movimentacao.eql?("Transferência")
      @estoque_produtos = @estoque.estoque_produtos
      @estoque_produtos.each do |ep|
        ep.estoque_produto_parcelamentos.where(parcela: @estoque_movimento_parcela.parcela).each do |epp|
          epp.update_attributes(data_vencimento: params[:data_vencimento])
        end
      end
    else
      @estoque_transferencias = @estoque.estoque_transferencias
      @estoque_transferencias.each do |et|
        et.estoque_transferencia_parcelamentos.where(parcela: @estoque_movimento_parcela.parcela).each do |etp|
          etp.update_attributes(data_vencimento: params[:data_vencimento])
        end
      end
    end
  end
end
