#-*- coding: utf-8 -*-
class Estoques::EstoqueParcelamentosController < BrantController
  include ParcelamentoConcern
  
  before_action :find_estoque, only: [:index, :show, :new, :router, :edit, :editar_parcelamentos]
  before_action :find_estoque_produto, only: [:new, :edit, :router, :editar_parcelamentos]
  before_action :find_estoque_transferencia, only: [:new, :edit, :router, :editar_parcelamentos]

  respond_to :html, :json

  # => Metodo para visualizar parcelas de um item do Estoque se ouver mais de uma
  def index
    if params[:estoque_produto_id]
      @estoque_parcelamentos = Estoque::EstoqueParcelamento.where(estoque_movimento_id: params[:estoque_movimentaco_id], estoque_produto_id: params[:estoque_produto_id])
    end
    if params[:estoque_transferencia_id]
      @estoque_parcelamentos = Estoque::EstoqueParcelamento.where(estoque_movimento_id: params[:estoque_movimentaco_id], estoque_transferencia_id: params[:estoque_transferencia_id])
    end
  end

  def show
    index
  end

  def router
    unless @estoque_produto.nil?
      redirect_to estoques_estoque_movimentaco_parcelamentos_path(@estoque, estoque_produto_id: @estoque_produto)
    end

    unless @estoque_transferencia.nil?
      redirect_to estoques_estoque_movimentaco_parcelamentos_path(@estoque, estoque_transferencia_id: @estoque_transferencia)    
    end
  end

  # => Metodo para reinciar todos os valores do parcelamento do produto decorrente
  def reset_quotations
    @estoque_movimento= Estoque::EstoqueMovimento.find(params[:estoque_movimentaco_id])
    if params[:estoque_produto_id]
      @estoque_produto = @estoque_movimento.estoque_produtos.find_by(id: params[:estoque_produto_id])
      _valor_total = @estoque_produto.valor_total.to_f.round(2)
      @valor_parcial = Estoque::EstoqueParcelamento.divide_valor_proporcional(_valor_total, @estoque_movimento.parcelas)
      _total_parcial=(@valor_parcial.round(2) * @estoque_movimento.parcelas)
      
      if (_valor_total > _total_parcial)
        _diferenca_de=(_valor_total + _total_parcial)
      else
        _diferenca_de=(_total_parcial - _valor_total)
      end

      _valor_ultima_parcela=(@valor_parcial - _diferenca_de)
      if @estoque_produto.parcelas.eql?(@estoque_movimento.parcelas)
        Estoque::EstoqueParcelamento.where(estoque_movimento_id: @estoque_movimento, estoque_produto_id: @estoque_produto).delete_all
        for x in 1..@estoque_movimento.parcelas
          @estoque_produto.estoque_produto_parcelamentos.create(estoque_movimento_id: @estoque_movimento.id, valor: @valor_parcial)
        end
        @estoque_produto.estoque_produto_parcelamentos.last.update_attributes(valor: _valor_ultima_parcela)
      end
      @estoque_parcelamentos = Estoque::EstoqueParcelamento.where(estoque_movimento_id: params[:estoque_movimentaco_id], estoque_produto_id: params[:estoque_produto_id])
    end

    if params[:estoque_transferencia_id]
      @estoque_transferencia = @estoque_movimento.estoque_transferencias.find_by(id: params[:estoque_transferencia_id])
      Estoque::EstoqueTransferencia.build_parcelamento_from_source(@estoque_transferencia, @estoque_transferencia.estoque_movimento.parcelas)
      @estoque_parcelamentos = Estoque::EstoqueParcelamento.where(estoque_movimento_id: @estoque_transferencia.estoque_movimento_id, estoque_transferencia_id: @estoque_transferencia.id)
    end

    respond_to &:json
  end

  def change
    @estoque_parcelamento = Estoque::EstoqueParcelamento.find(params[:id])
    respond_to &:js
  end

  # => Metodo para destravar o valor da parcela decorrente
  def unlock_value
    @estoque_parcelamento = Estoque::EstoqueParcelamento.find(params[:id])
    @estoque_parcelamento.update_attributes(bloqueado: 0)
    redirect_to :back
  end

  # => Atualiza valores proporcionalmente segundo valor inicial da parcelas
  def update
    respond_to do |format|
      @estoque_parcelamento = Estoque::EstoqueParcelamento.find_by(id: params[:id])

      # => Atribuindo novo valor da parcela
      _novo_valor    = params[:estoque_estoque_parcelamento][:valor].to_money.to_f
      _valor_parcela = @estoque_parcelamento.valor.to_f

      # => Verificando se o parcelamento é só de Produto
      if !@estoque_parcelamento.estoque_produto.nil?
        success = _parcelamento_do_produto(_novo_valor, _valor_parcela)
        if success.eql? true
          format.json { render json: { redirect_url: estoques_estoque_movimentaco_router_parcelamento_path(@estoque_parcelamento.estoque_movimento, produto_id: @estoque_produto.id) } }
          format.json { respond_with_bip(@estoque_parcelamento) }
        else
          format.json { render json: { message: { 
                                    error: t('pages.helpers.messages.quotation_exagerated_error', resource_value: params[:estoque_estoque_parcelamento][:valor]),
                                    warning: t('pages.helpers.messages.quotation_warning_observation', resource_value: _valor_parcela.to_money.to_s),
                                    watchout_for_releases: t('pages.helpers.messages.watchout_for_releases')
                                  },
                                  redirect_url: estoques_estoque_movimentaco_router_parcelamento_path(@estoque_parcelamento.estoque_movimento, produto_id: @estoque_produto.id),
                                  novo_valor: _novo_valor.to_money.to_s,
                                  valor_anterior:  _valor_parcela.to_money.to_s 
                                } }
          format.json { respond_with_bip(@estoque_parcelamento) }
        end
      end

      if !@estoque_parcelamento.estoque_transferencia.nil?
        success = _parcelamento_da_transferencia(_novo_valor, _valor_parcela)
        if success.eql? true
          format.json { render json: { redirect_url: estoques_estoque_movimentaco_router_parcelamento_path(@estoque_parcelamento.estoque_movimento, transferencia_id: @estoque_transferencia.id) } }
          format.json { respond_with_bip(@estoque_parcelamento) }
        else
          format.json { render json: { message: { 
                                    error: t('pages.helpers.messages.quotation_exagerated_error', resource_value: params[:estoque_estoque_parcelamento][:valor]),
                                    warning: t('pages.helpers.messages.quotation_warning_observation', resource_value: _valor_parcela.to_money.to_s),
                                    watchout_for_releases: t('pages.helpers.messages.watchout_for_releases')
                                  },
                                  redirect_url: estoques_estoque_movimentaco_router_parcelamento_path(@estoque_parcelamento.estoque_movimento, transferencia_id: @estoque_transferencia.id),
                                  novo_valor: _novo_valor.to_money.to_s,
                                  valor_anterior:  _valor_parcela.to_money.to_s 
                                } }
          format.json { respond_with_bip(@estoque_parcelamento) }
        end
      end
    end
  end

  private
    # => Todo parcelamento exige que um movimento do estoque  exista para poder existir
    def find_estoque
      @estoque = Estoque::EstoqueMovimento.find(params[:estoque_movimentaco_id])
      session[:estoque_movimento] =@estoque.id
    end

    def find_estoque_produto
      @id = check_params
      @estoque_produto =Estoque::EstoqueProduto.find_by(id: @id.call(params[:estoque_produto_id], params[:produto_id]))
      session[:estoque_produto] =@estoque_produto.id if @estoque_produto
    end

    def find_estoque_transferencia
      @id = check_params
      @estoque_transferencia =Estoque::EstoqueTransferencia.find_by(id: @id.call(params[:estoque_transferencia_id], params[:transferencia_id]))
      session[:estoque_transferencia] = @estoque_transferencia.id if @estoque_transferencia
    end

    def find_parcelamento
      @estoque_parcelamento =Estoque::EstoqueParcelamento.find(params[:id])
    end

    def parcelamento_params
      params.require(:estoque_estoque_parcelamento).permit(:id, :estoque_movimento_id, :estoque_produto_id, :estoque_transferencia_id, :valor)
    end
end