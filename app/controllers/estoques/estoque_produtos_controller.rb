# -*- coding: utf-8 -*-
class Estoques::EstoqueProdutosController < BrantController
  include CurrentItem
  before_action :find_estoque
  respond_to :html

  def new
    verifica_itens_adicionados
    @estoque_produto = @estoque.estoque_produtos.build
    @produto = @estoque.produtos.build
    @produto.categorias.build
    session[:qtParcelas]= @estoque.parcelas
  end

  def create
    if resource_params[:id].eql?("")
      verifica_itens_adicionados
      @estoque_produto = @item.add_produto_to_estoque(resource_params)
      @estoque_produto.verify_last_stock
      @estoque_produto.add_or_remove_through_stock_moviment
      if @estoque_produto.save
        @estoque_produto.save_product_at_provider_if_not_exists
        @estoque_produto_parcelamentos = Estoque::EstoqueProduto.build_parcelamento_from_source(@estoque_produto, session[:qtParcelas])
        if @estoque_produto_parcelamentos 
          flash[:notice]=t("flash.actions.#{__method__}.success", resource_name: "Produto")
          redirect_to new_estoques_estoque_movimentaco_estoque_produto_path(@estoque)
        end
      else
        flash[:error]=t("flash.actions.#{__method__}.alert", resource_name: "Produto")
        @item = Item.find(session[:item_id])
        render :new
      end
    else
      update
    end
  end

  def update
    @estoque_produto = @estoque.estoque_produtos.find(resource_params[:id])
    @estoque_produto.verify_changes_on_main_fields(resource_params)
    @estoque_produto.simula_valor_total_de_produtos(resource_params[:valor_produto])

    if @estoque_produto.save
      @estoque_produto_parcelamentos = Estoque::EstoqueProduto.build_parcelamento_from_source(@estoque_produto, session[:qtParcelas])
      if @estoque_produto_parcelamentos
        flash[:notice] = t("flash.actions.#{__method__}.success", resource_name: "Produto")
        redirect_to new_estoques_estoque_movimentaco_estoque_produto_path(@estoque)
      end
    else
      flash[:error] = t("flash.actions.#{__method__}.alert", resource_name: "Produto")
      render 'new'
    end
  end

  def destroy
    @estoque_produto = Estoque::EstoqueProduto.find(params[:id])
    @estoque = Estoque::EstoqueMovimento.find(params[:estoque_movimentaco_id])
    @deleted_id =@estoque_produto.id
    
    if @estoque_produto.destroy
      @confirmed = true
    else
      @confirmed = false
    end
  end


  private
    def verifica_itens_adicionados
      @estoque_produto = @estoque.estoque_produtos.last
      if !@estoque_produto.nil?
        @item = @estoque_produto.item
        session[:item_id] = @item.id
      elsif session[:item_id] != nil
        @item = Item.find(session[:item_id])
      else
        @item = Item.create
        session[:item_id] = @item.id
      end
    end

    def find_estoque
      @estoque = Estoque::EstoqueMovimento.find(params[:estoque_movimentaco_id])
    end

    def resource_params
      params.require(:estoque_estoque_produto).permit(:id,
                                                      :produto_id,
                                                      :estoque_movimento_id,
                                                      :obra_id,
                                                      :comodo_id,
                                                      :item_obra_id,
                                                      :sub_item_obra_id,
                                                      :quantidade_movimento,
                                                      :quantidade_anterior,
                                                      :quantidade_atual,
                                                      :valor_produto,
                                                      :valor_total,
                                                      :observacoes)
    end
end