#-*- coding: utf-8 -*-
class Estoques::EstoqueMovimentoParcelasController < EstoquesController
  include EstoqueMovimentoConcern
  include ParcelamentoConcern

  before_action :find_dados_movimentacao

  # => Reseta parcelas a partir dos valores informados na pagina
  def reset
    @estoque.estoque_movimento_parcelas.delete_all
    _create_parcelas_
    @estoque.parcelas=(params[:parcelas].to_i)
    @estoque.total_estoque=(params[:valor_total].gsub(".","").to_f)
    @estoque.save
  end
  
  # => Reseta parcelas a partir dos valores originais antes de serem individualmente alterados
  def reload
    @estoque.estoque_movimento_parcelas.delete_all
    _create_parcelas_
  end

  def unlock_item
    @estoque_movimento_parcela.update_attributes(bloqueado: 0)
    redirect_to :back
  end

  def update
    respond_to do |format|
      # => Atribuindo novo valor da parcela
      _novo_valor    = params[:estoque_estoque_movimento_parcela][:valor].to_money.to_f
      _valor_parcela = @estoque_movimento_parcela.valor.to_f

      # => Descobrindo quantitativo de parcelas
      _parcelas_restantes = @estoque.estoque_movimento_parcelas.where.not(id: @estoque_movimento_parcela.id).where(bloqueado: false).count

      # => Atribuindo valores inicial para variaveis de calculo
      _valor_restante = 0
      _valores_bloqueados = 0
      _parcelas_bloqueadas = false

      # => Soma todos os valores nao alterados
      _valor_restante = _somatorio_valores_nao_bloqueados(@estoque.estoque_movimento_parcelas)
        
      # => Verifica se existem valores alterados
      _valores_bloqueados = _somatorio_valores_bloqueados(@estoque.estoque_movimento_parcelas)
      if _valores_bloqueados != 0
        _parcelas_bloqueadas = true
      end

      # => Descobrindo novo valor a partir do novo valor para a parcela atualmente alterada
      _novo_total = (_valor_restante - _novo_valor)

      # => Descobrindo os valores parciais do novo valor
      @valor_parcial  = Estoque::EstoqueParcelamento.divide_valor_proporcional(_novo_total, _parcelas_restantes)
      _total_parcial=(@valor_parcial.round(2) * _parcelas_restantes)
      
      # => Condições para descobrir a diferença da parcela
      if not _parcelas_bloqueadas.eql?(true)
        if _novo_valor < _valor_parcela
          _resultado_diferenca = _calcular_diferenca_com_valores_menores(_total_parcial, _novo_total)
        else
          _resultado_diferenca = _calcular_diferenca_com_valores_maiores(_total_parcial, _novo_total)
        end
      else
        _resultado_diferenca = (_total_parcial.to_f - _novo_total.to_f)
      end
      
      # => Calculando o valor da ultima parcela
      _valor_ultima_parcela=(@valor_parcial - _resultado_diferenca.round(2))

      if _novo_valor.to_f <= _valor_restante.to_f
        # => Momento em que os valores são devidamente atualizados
        @estoque_movimento_parcela.update_attributes(valor: _novo_valor, bloqueado: true)
        @estoque.estoque_movimento_parcelas.where.not(id: @estoque_movimento_parcela.id).where(bloqueado: false).each do |parcela|
          if parcela.eql?(@estoque.estoque_movimento_parcelas.where.not(id: @estoque_movimento_parcela.id).where(bloqueado: false).first) 
            parcela.update_attributes(valor: _valor_ultima_parcela)
          else
            parcela.update_attributes(valor: @valor_parcial)
          end
        end
        # format.html { redirect_to edit_estoques_estoque_movimentaco_url(@estoque) }
        format.json { render json: { redirect_url: edit_estoques_estoque_movimentaco_url(@estoque.id) } }
        format.json { respond_with_bip(@estoque_movimento_parcela) }
      else
        format.json { render json: { message: { 
                                      error: t('pages.helpers.messages.quotation_exagerated_error', resource_value: params[:estoque_estoque_movimento_parcela][:valor]),
                                      warning: t('pages.helpers.messages.quotation_warning_observation', resource_value: _valor_parcela.to_money.to_s),
                                      watchout_for_releases: t('pages.helpers.messages.watchout_for_releases')
                                    },
                                    redirect_url: edit_estoques_estoque_movimentaco_url(@estoque.id),
                                    novo_valor: _novo_valor.to_money.to_s,
                                    valor_anterior:  _valor_parcela.to_money.to_s 
                                  } }
        format.json { respond_with_bip(@estoque_movimento_parcela) }
      end
    end
  end

  private
    # => Localiza o estoque vinculado as parcelas
    def find_dados_movimentacao
      @id = check_params
      if params[:id]
        @estoque_movimento_parcela = Estoque::EstoqueMovimentoParcela.find(params[:id])
        @estoque = Estoque::EstoqueMovimento.find_by(id: @id.call(params[:estoque_movimento_id], @estoque_movimento_parcela.estoque_movimento.id))
      else
        @estoque = Estoque::EstoqueMovimento.find_by(id: @id.call(params[:estoque_movimento_id], params[:estoque_movimentaco_id]))
      end
    end
end