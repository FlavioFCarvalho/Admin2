class BrantController < ApplicationController
  include Pundit
  before_action :authenticate_user!

  #pundit rescue errors manager
  rescue_from Pundit::NotAuthorizedError, with: :user_not_authorized

  def pundit_user
    current_user
  end

  def current_ability
    @current_ability ||= Ability.new(current_user)
  end

  def check_params
    lambda do |*args|
      raise ArgumentError if args.empty? || args.size > 2
      arg1, arg2, arg3 = args
      return arg1 unless arg1.nil?
      return arg2 unless arg2.nil?
      return arg3 unless arg3.nil?
    end
  end

  protected
    def user_not_authorized
      render :file => "#{Rails.root}/public/422.html", status: 422, layout: false
    end
end
