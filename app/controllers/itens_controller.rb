class ItensController < ApplicationController
  before_action :set_item, only: [:show, :edit, :update, :destroy]
  rescue_from ActiveRecord::RecordNotFound, with: :invalid_item
  respond_to :html

  def index
    @itens = Item.all
    respond_with(@itens)
  end

  def show
    respond_with(@item)
  end

  def new
    @item = Item.new
    respond_with(@item)
  end

  def edit
  end

  def create
    @item = Item.new(item_params)
    flash[:notice] = 'Item was successfully created.' if @item.save
    respond_with(@item)
  end

  def update
    flash[:notice] = 'Item was successfully updated.' if @item.update(item_params)
    respond_with(@item)
  end

  def destroy
    @item.destroy
    respond_with(@item)
  end

  private
    def set_item
      @item = Item.find(params[:id])
    end

    def item_params
      params[:item]
    end

    def invalid_item
      logger.error "Attempt to access invalid item #{params[:id]}"
      redirect_to estoques_estoque_movimentacoes_url, notice: 'Invalid Item'
    end
end
