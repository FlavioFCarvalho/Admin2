class TiposController < BrantController
  before_action :set_tipo, only: [:show, :edit, :update, :destroy]

  respond_to :html

  def index
    session[:page] = params[:page]
    @search = Tipo.ransack(params[:q])
    @tipos = @search.result.order(:name).page(params[:page]).per(10)
    @search.build_condition if @search.conditions.empty?
  end

  def new
    @tipo = user.tipos.build
    authorize @tipo
    respond_with(@tipo)
  end

  def create
    @tipo = user.tipos.build(resource_params)
    if @tipo.save
      redirect_to new_tipo_path, notice: t("flash.actions.#{__method__}.notice", resource_name: t('activerecord.models.tipo'))
    else
      flash[:error] = t("flash.actions.#{__method__}.alert", resource_name: t('activerecord.models.tipo'))
      render :edit
    end
  end

  def show
  end

  def edit
  end

  def update
    @tipo.status=(resource_params[:_status])
    @tipo.user=(user) if !@tipo.user.present?

    if @tipo.update(resource_params)
      flash[:info] = t("flash.actions.#{__method__}.notice", resource_name: t('activerecord.models.tipo'))
      redirect_to tipos_url
    else
      flash[:error] = t("flash.actions.#{__method__}.alert", resource_name: t('activerecord.models.tipo'))
      render :edit
    end
  end

  def destroy
    if @tipo.destroy
      flash[:info] = t("flash.actions.#{__method__}.notice", resource_name: t('activerecord.models.tipo'))
    else
      flash[:error] = t("flash.actions.#{__method__}.alert", resource_name: t('activerecord.models.tipo'))
    end
    redirect_to tipos_url
  end

  private
    def set_tipo
      @tipo = Tipo.find(params[:id])
      authorize @tipo
    end

    def resource_params
      params.require(:tipo).permit(:name, :user_id, :_status)
    end
end