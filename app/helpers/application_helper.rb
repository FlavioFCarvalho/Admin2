module ApplicationHelper
  # => Link para adicionar campos (Ransack)
  def link_to_add_fields(name, f, type)
    new_object = f.object.send "build_#{type}"
    id = "new_#{type}"
    fields = f.send("#{type}_fields", new_object, child_index: id) do |builder|
      render(type.to_s + "_fields", f: builder)
    end
    link_to(name, '#', class: "search_add_fields", data: {id: id, fields: fields.gsub("\n", "")})
  end

  # => Cria div com mensagem de Erro
  def error_tag(model, attribute)
    if model.errors.has_key? attribute
      content_tag(
        :div,
          model.errors[attribute].first,
            class: 'error_message'
      )
    end
  end

  # => Altera a cor do label caso tenha mensagem de erro no form
  def label_error_tag(builder, field, model, attribute)
    if model.errors.has_key? attribute
      builder.label(field, class: 'error_message')
    else
      builder.label(field)
    end
  end
end
