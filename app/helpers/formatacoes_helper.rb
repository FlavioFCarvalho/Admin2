module FormatacoesHelper
  def data_formatada(data)
    data.strftime("%d/%m/%Y") if data.present?
  end

  def hora_formatada(hora)
    hora.strftime("%H:%M") if hora.present?
  end

  def data_e_hora_formatada(data, hora)
    if data.present? && hora.present?
      content_tag(:label) do
        data.strftime("%d/%m/%Y") + " - " + hora.strftime("%H:%M")
      end
    end
  end

  def cliente_ou_fornecedor?(resource)
    if resource == 'C'
      'Cliente'
    else
      if resource == 'F'
        'Fornecedor'
      else
        if resource == 'CF'
          'Cliente/Fornecedor'
        end
      end
    end
  end

  def translate_i18n_status(status)
    return t('enums.active')   if status.eql?("active")
    return t('enums.inactive') if status.eql?("inactive")
  end

  def translate_i18n_yes_or_no(answer)
    return t('enums._yes')   if answer.eql?(true)
    return t('enums._no') if answer.eql?(false)
  end

  def tipo_pessoa?(resource)
    if resource == 'Fisica'
      'Física'
    else
      if resource == 'Juridica'
        'Jurídica'
      end
    end
  end
end
