module EstoqueHelper
  def exibe_data_entrada(object)
    if object.data_entrada.nil?
      Date.today.strftime("%d/%m/%Y")
    else
      object.data_entrada.strftime("%d/%m/%Y")
    end
  end


  def exibe_data_transferencia(object)
    if object.data_transferencia.nil?
      Date.today.strftime("%d/%m/%Y")
    else
      object.data_transferencia.strftime("%d/%m/%Y")
    end
  end

  def exibe_data_vencimento_entrada(object)
    if !object.data_vencimento_entrada.nil?
      object.data_vencimento_entrada.strftime("%d/%m/%Y")
    end
  end

  def exibe_data_vencimento_saida(object)
    if !object.data_vencimento_saida.nil?
      object.data_vencimento_saida.strftime("%d/%m/%Y")
    end
  end

  def exibe_data_devolucao(object)
    if object.data_devolucao.nil?
      Date.today.strftime("%d/%m/%Y")
    else
      object.data_devolucao.strftime("%d/%m/%Y")
    end
  end

  def contador_de_itens_no_estoque(element, params)
    @contador = 0
    if params[:q].present?
      if params[:q][:tipo_movimentacao_eq] != "" and params[:q][:fornecedor_id_eq] == "" and params[:q][:obra_id_eq] == "" and params[:q][:produto_id_eq] == ""
        element.estoque_produtos.each do |item|
          @contador += item.quantidade_movimento
        end
      end

      if params[:q][:tipo_movimentacao_eq] != "" and params[:q][:fornecedor_id_eq] != "" and params[:q][:obra_id_eq] == "" and params[:q][:produto_id_eq] == ""
        element.estoque_produtos.each do |item|
          @contador += item.quantidade_movimento
        end
      end

      if params[:q][:tipo_movimentacao_eq] != "" and params[:q][:fornecedor_id_eq] != "" and params[:q][:obra_id_eq] != "" and params[:q][:produto_id_eq] == ""
        element.estoque_produtos.where(obra_id: params[:q][:obra_id_eq]).each do |item|
          @contador += item.quantidade_movimento
        end
      end

      if params[:q][:tipo_movimentacao_eq] != "" and params[:q][:fornecedor_id_eq] == "" and params[:q][:obra_id_eq] != "" and params[:q][:produto_id_eq] == ""
        element.estoque_produtos.where(obra_id: params[:q][:obra_id_eq]).each do |item|
          @contador += item.quantidade_movimento
        end
      end

      if params[:q][:tipo_movimentacao_eq] != "" and params[:q][:fornecedor_id_eq] == "" and params[:q][:produto_id_eq] != "" and params[:q][:obra_id_eq] == ""
        element.estoque_produtos.where(produto_id: params[:q][:produto_id_eq]).each do |item|
          @contador += item.quantidade_movimento
        end
      end

      if params[:q][:tipo_movimentacao_eq] != "" and params[:q][:fornecedor_id_eq] == "" and params[:q][:obra_id_eq] != "" and params[:q][:produto_id_eq] != ""
        element.estoque_produtos.where(obra_id: params[:q][:obra_id_eq]).where(produto_id: params[:q][:produto_id_eq]).each do |item|
          @contador += item.quantidade_movimento
        end
      end

      if params[:q][:tipo_movimentacao_eq] != "" and params[:q][:fornecedor_id_eq] != "" and params[:q][:obra_id_eq] != "" and params[:q][:produto_id_eq] != ""
        element.estoque_produtos.where(obra_id: params[:q][:obra_id_eq]).where(produto_id: params[:q][:produto_id_eq]).each do |item|
          @contador += item.quantidade_movimento
        end
      end

      if params[:q][:tipo_movimentacao_eq] != "" and params[:q][:fornecedor_id_eq] != "" and params[:q][:obra_id_eq] == "" and params[:q][:produto_id_eq] != ""
        element.estoque_produtos.where(produto_id: params[:q][:produto_id_eq]).each do |item|
          @contador += item.quantidade_movimento
        end
      end

      if params[:q][:tipo_movimentacao_eq] == "TRANSFERÊNCIA DE ENTRADA" || params[:q][:tipo_movimentacao_eq] == "TRANSFERÊNCIA DE SAÍDA"
        if params[:q][:fornecedor_id_eq] == "" and params[:q][:obra_id_eq] == "" and params[:q][:produto_id_eq] == ""
          element.estoque_transferencias.each do |item|
            @contador += item.quantidade_movimento
          end
        end

        if params[:q][:fornecedor_id_eq] == "" and params[:q][:obra_id_eq] != "" and params[:q][:produto_id_eq] == ""
          element.estoque_transferencias.where("estoque_estoque_transferencias.obra_anterior_id='#{params[:q][:obra_id_eq]}' OR estoque_estoque_transferencias.obra_posterior_id='#{params[:q][:obra_id_eq]}'").each do |item|
            @contador += item.quantidade_movimento
          end
        end

        if params[:q][:fornecedor_id_eq] == "" and params[:q][:obra_id_eq] == "" and params[:q][:produto_id_eq] != ""
          element.estoque_transferencias.where("estoque_estoque_transferencias.produto_id='#{params[:q][:produto_id_eq]}'").each do |item|
            @contador += item.quantidade_movimento
          end
        end

        if params[:q][:fornecedor_id_eq] == "" and params[:q][:obra_id_eq] != "" and params[:q][:produto_id_eq] != ""
          element.estoque_transferencias.where("estoque_estoque_transferencias.produto_id='#{params[:q][:produto_id_eq]}'").where("estoque_estoque_transferencias.obra_anterior_id = ? OR estoque_estoque_transferencias.obra_posterior_id = ?", "#{params[:q][:obra_id_eq]}" , "#{params[:q][:obra_id_eq]}").each do |item|
            @contador += item.quantidade_movimento
          end
        end
      end
    else
      if element.tipo_movimentacao.upcase.eql?("ENTRADA") or element.tipo_movimentacao.upcase.eql?("DEVOLUÇÃO PARA FORNECEDOR")
        element.estoque_produtos.each do |item|
          @contador += item.quantidade_movimento
        end
      end

      if element.tipo_movimentacao.upcase.eql?("TRANSFERÊNCIA")
        element.estoque_transferencias.each do |item|
          @contador += item.quantidade_movimento
        end
      end
    end
    @contador
  end

  def total_produtos(resource)
    total = 0
    resource.each do |produto|
      total += produto.valor_total.to_money.to_f
    end

    return number_to_currency total
  end
end
