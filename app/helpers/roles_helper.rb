module RolesHelper
  OptionsForRoles = Struct.new(:id, :description)

  def options_for_roles
    Roles::OPTIONS.map do |key, value|
      OptionsForRoles.new(key, value)
    end
  end
end
