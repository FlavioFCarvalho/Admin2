module UsersHelper
  def logged_in_user?
    @user = admin_signed_in? || user_signed_in?
  end

  def logged_user
    @user = current_admin || current_user
  end
end
