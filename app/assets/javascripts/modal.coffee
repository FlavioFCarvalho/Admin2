animation = (x) ->
  $('.modal .modal-dialog').attr 'class', 'modal-dialog  ' + x + '  animated'
  return

$('.modal.alert').on 'show.bs.modal', (e) ->
  animation "shake"
  return
$('.modal.alert').on 'hide.bs.modal', (e) ->
  animation "zoomOut"
  return
