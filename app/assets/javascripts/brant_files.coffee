#= require masks

$(".data_live_search").selectpicker ->
$(".data").datepicker({language: 'pt-BR'})
$('.popover_link').popover ->
  trigger: 'hover'

$('.dropdown-menu').on 'click', (event) ->
  if this == event.target
    event.stopPropagation()
  return

$(document).ready ->
  ### Activating Best In Place ###
  jQuery('.best_in_place').best_in_place()
