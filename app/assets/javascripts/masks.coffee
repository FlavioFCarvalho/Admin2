jQuery ->
  $('.campo_telefone').mask('(99) 9999-9999')
  $('.campo_celular').mask('(99) 99999-9999')
  $('.campo_rg').mask('99.999.999-9')
  $('.campo_cpf').mask('999.999.999-99')
  $('.campo_cnpj').mask('99.999.999/9999-99')
  $('.campo_cep').mask('99.999-999')
  $('.campo_dinheiro').maskMoney({showSymbol:false, decimal:",", thousands:"."})
  $('.campo_numerico').maskMoney({showSymbol:false, decimal:",", thousands:".", precision:1})
