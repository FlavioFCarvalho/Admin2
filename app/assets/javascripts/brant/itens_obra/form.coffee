#= require brant/clientes_&_fornecedores/_init_

# ----------------------------- START ------------------------------------
jQuery ->
  count = 0
  $("#item_obra_item").change ->
    $(".sub_item_value").val($(this).val())

  $('form').on 'cocoon:after-insert', (e, added_thing) ->
    added_thing.find('#sub_item_task_value').find('input').val($("#item_obra_item").val())
    return
# ----------------------------- END ---------------------------------------
