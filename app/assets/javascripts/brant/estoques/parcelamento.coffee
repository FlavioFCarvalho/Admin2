#= require brant/estoques/_init_
#= require jquery/jquery.maskMoney
#= require jquery/jquery.mask

jQuery ->
  estoque_movimento_id = $("#estoque_movimento_id").val()

  $('.parcelamento_link').popover ->
    trigger: 'hover'
  $('#produto_descricao').popover ->
    trigger: 'hover'

  $(document).on 'focus', '.in_place_editing', (event) ->
    $(this).maskMoney({showSymbol:false, decimal:",", thousands:"."})

  isJson = (str) ->
    try
      JSON.parse(str)
    catch e
      return false
    true

  warinig_message_builder= (mensagem, valor_informado, valor_anterior) ->
    "<div class: 'container-fluid'>
      <b>#{mensagem[0]}</b>
      <BR />
      <b>#{mensagem[1]}</b>
      <BR />
      <BR />
      <b class='blink_me' id='watchout_for_releases'>#{mensagem[2]}</b>
    </div>"

  $('.best_in_place').bind 'ajax:success', (request, response) ->
    if isJson(response)
      json = JSON.parse(response)
      if json != undefined
        if json.message != undefined
          $("#warning_message").html(warinig_message_builder([json.message.error, json.message.warning, json.message.watchout_for_releases], json.novo_valor, json.valor_anterior))
          $("#lock_page_modal").modal()
          $('#lock_page_modal').on 'hide.bs.modal', (event) ->
            jQuery('.best_in_place').best_in_place()
        else
          window.location.href = json.redirect_url

  $(".btn-send-values").click (event) ->
    $('#cancel_stock_moviment_data').prop('disabled', true)


  $("#btn_reload_quotations").click (event) ->
    event.preventDefault()
    estoque_movimentaco_id= $(this).data().estoqueMovimentoId
    $.ajax
      url: LOCALHOST + "estoques/estoque_movimentacoes/#{estoque_movimentaco_id}/parcelas/reiniciar-valores"
      type: 'POST'
      dataType: 'JSON'
      data:
        estoque_movimentaco_id: estoque_movimentaco_id
      success: (response) ->
        $("#progress_bar").fadeIn('slow')
        setTimeout (->
          $('.progress .progress-bar').progressbar({display_text: 'fill', use_percentage: true})
          setTimeout (->
            $.message({title: 'Aviso!',content: response.notify_message, position: "topright", color: "green"})
            setTimeout (->
              window.location.href=response.url
            ), 8500
          ), 10000
        ), 2

  $("#btn_resetar_parcelas_estoque_transferencia").click (event) ->
    event.preventDefault()
    estoque_movimentaco_id= $(this).data().estoqueMovimentoId
    estoque_transferencia_id= $(this).data().estoqueTransferenciaId
    $.ajax
      url: LOCALHOST + "estoques/estoque_movimentacoes/#{estoque_movimentaco_id}/resetar-parcelas"
      type: 'GET'
      dataType: 'JSON'
      data:
        estoque_movimentaco_id: estoque_movimentaco_id
        estoque_transferencia_id: estoque_transferencia_id
      success: (response) ->
        $("#progress_bar").fadeIn('slow')
        setTimeout (->
          $('.progress .progress-bar').progressbar({display_text: 'fill', use_percentage: true})
          setTimeout (->
            $.message({title: 'Aviso!',content: response.notify_message, position: "topright", color: "green"})
            setTimeout (->
              window.location.href=response.url
            ), 8500
          ), 10000
        ), 2000

  $("#btn_resetar_parcelas_estoque_produto").click (event) ->
    event.preventDefault()
    estoque_movimentaco_id= $(this).data().estoqueMovimentoId
    estoque_produto_id= $(this).data().estoqueProdutoId
    parcelas= $(this).data().parcelas
    $.ajax
      url: LOCALHOST + "estoques/estoque_movimentacoes/#{estoque_movimentaco_id}/resetar-parcelas"
      type: 'POST'
      dataType: 'JSON'
      data:
        estoque_movimentaco_id: estoque_movimentaco_id
        estoque_produto_id: estoque_produto_id
        parcelas: parcelas
      success: (response) ->
        $("#progress_bar").fadeIn('slow')
        setTimeout (->
          $('.progress .progress-bar').progressbar({display_text: 'fill', use_percentage: true})
          setTimeout (->
            window.location.href=response.url
          ), 8500
        ), 2000
