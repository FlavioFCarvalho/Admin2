#= require brant/estoques/_init_

jQuery ->
  $(".link_to_alter_payday_date").on 'click', (event)->
    event.preventDefault()
    link_to_data = $(this).data()
    $("#{link_to_data.modalId}").modal()
  
  $(".btn_send_data_to_controller").on 'click', (event)->
    event.preventDefault()
    link_to_data = $(this).data()
    _estoque_movimento_parcela_id_ = $(".estoque_movimento_parcela_id_#{link_to_data.id}").val()
    _estoque_movimento_parcela_data_vencimento_ = $(".estoque_movimento_parcela_data_vencimento_#{link_to_data.id}").val()
    $.ajax
      url: LOCALHOST + "estoques/data_vencimento/#{_estoque_movimento_parcela_id_}"
      type: 'PUT'
      dataType: 'JSON'
      data:
        id: _estoque_movimento_parcela_id_
        data_vencimento: _estoque_movimento_parcela_data_vencimento_
      success: (response) ->
        setTimeout (->
          $('.progress .progress-bar').progressbar({display_text: 'fill', use_percentage: true})
          setTimeout (->
            $(".modal .close").click()
            $.message({title: 'Aviso!',content: response.notify_message, position: "topright", color: "green"})
            setTimeout (->
              window.location.reload();
            ),5000
          ), 10000
        ), 1000
        

