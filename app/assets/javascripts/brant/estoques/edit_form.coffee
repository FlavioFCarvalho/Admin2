#= require brant/estoques/_init_

$(document).ready ->
  $(document).on 'focus', '.in_place_editing', (event) ->
    $(this).maskMoney({showSymbol:false, decimal:",", thousands:"."})
  
  if $("#estoque_estoque_movimento_acrescimo").val() == "" || $("#estoque_estoque_movimento_acrescimo").val() == " "
    $("#estoque_estoque_movimento_acrescimo").val("0,00")

  if $("#estoque_estoque_movimento_parcelas").val() > 1 
    $(".periodo_field_section").show('fast')
  
  if $(".estoque_movimento_parcelas_devolucao").val() > 1 
    $(".periodo_devolucao_field_section").show('fast')

jQuery ->
  _parcelasCache=$("#estoque_estoque_movimento_parcelas").val()
  ValorTotalEstoque= "0.00"
  valorTotalProduto= "0.00"
  parcelas=""
  confirmDialogOnPage=false
  _estoque_estoque_movimento_periodo = $(".estoque_periodo_select").val()

  if $("#estoque_estoque_movimento_tipo_movimentacao :selected").text() == "Entrada"
    $(".estoque_movimento_parcelas_entrada").blur ->
      qtParcelas     = $(this).val()
      if $(".estoque_movimento_total_estoque_entrada").val() != ""
        valor_total    = to_float($(".estoque_movimento_total_estoque_entrada").val())

      valorParcelado = (valor_total / qtParcelas)
      valorParcelado = accounting.formatMoney(remove_currency_symbol(valorParcelado), {
        symbol: "R$ - ",
        precision: 2,
        thousand: ".",
        decimal: ","
      })

    if $(".estoque_movimento_parcelas_entrada").val() != ""
      parcelas = $(".estoque_movimento_parcelas_entrada").val()

    $(".estoque_movimento_parcelas_entrada").blur ->
      if $(this).val() != ""
        parcelas = $(this).val()

  if $("#estoque_estoque_movimento_tipo_movimentacao :selected").text() == "Devolução para Fornecedor"
    $(".estoque_movimento_parcelas_devolucao").blur ->
      qtParcelas     = $(this).val()
      if $(".estoque_movimento_total_estoque_devolucao").val() != ""
        valor_total    = to_float($(".estoque_movimento_total_estoque_devolucao").val())

      valorParcelado = (valor_total / qtParcelas)
      valorParcelado = accounting.formatMoney(remove_currency_symbol(valorParcelado), {
        symbol: "R$ - ",
        precision: 2,
        thousand: ".",
        decimal: ","
      })

    if $(".estoque_movimento_parcelas_devolucao").val() != ""
      parcelas = $(".estoque_movimento_parcelas_devolucao").val()
  
    $(".estoque_movimento_parcelas_devolucao").blur ->
      if $(this).val() != ""
        parcelas = $(this).val()

  if $("#estoque_estoque_movimento_tipo_movimentacao :selected").text() == "Transferência"
    $(".estoque_movimento_parcelas_transferencia").blur ->
      qtParcelas     = $(this).val()
      if $(".estoque_movimento_total_estoque_transferencia").val() != ""
        valor_total    = to_float($(".estoque_movimento_total_estoque_transferencia").val())

      valorParcelado = (valor_total / qtParcelas)
      valorParcelado = accounting.formatMoney(remove_currency_symbol(valorParcelado), {
        symbol: "R$ - ",
        precision: 2,
        thousand: ".",
        decimal: ","
      })

    if $(".estoque_movimento_parcelas_transferencia").val() != ""
      parcelas = $(".estoque_movimento_parcelas_transferencia").val()
  
    $(".estoque_movimento_parcelas_transferencia").blur ->
      if $(this).val() != ""
        parcelas = $(this).val()

  $(".estoque_periodo_select").change ->
    _estoque_estoque_movimento_periodo = $(this).val()

  # ------------ LOGICAS PARA ENVIAR DADOS PARA O CONTROLLER -------------
  $(".edit_estoque_estoque_movimento").submit (event) ->
    event.preventDefault()
    $('#save_stock_moviment_data').prop('disabled', true)
    $('#cancel_stock_moviment_data').prop('disabled', true)
    $('#btn_voltar_estoque').prop('disabled', true)
    estoque_movimento_id = $("#estoque_estoque_movimento_id").val()
    _data_entrada=""
    _data_saida=""
    _data_vencimento_entrada=""
    _data_vencimento_saida=""
    _data_devolucao=""
    _boleto=""
    _frete = "0.00"
    _desconto="0.00"
    _acrescimo="0.00"
    _fornecedor_id = ""
    _destinatario_tomador_id=""
    _valida_por= ""
    _pronto_pagamento=""

    if $("#estoque_estoque_movimento_tipo_movimentacao :selected").val() == "Entrada"
      if $(".estoque_entrada_total_produtos").val() != " " and $(".estoque_entrada_total_produtos").val() != ""
        valorTotalProduto = to_float($(".estoque_entrada_total_produtos").val())
      if $(".estoque_movimento_total_estoque_entrada").val() != " " and $(".estoque_movimento_total_estoque_entrada").val() != ""
        ValorTotalEstoque = $(".estoque_movimento_total_estoque_entrada").val().replace(".", "").replace(",", ".")
      if $("#estoque_entrada_frete").val() != ""
        _frete = to_float($("#estoque_entrada_frete").val())
      if $("#estoque_fornecedor_entrada :selected").val() != ""
        _fornecedor_id = $("#estoque_fornecedor_entrada :selected").val()
      if $("#estoque_destinatario_tomador_entrada :selected").val() != ""
        _destinatario_tomador_id = $("#estoque_destinatario_tomador_entrada :selected").val()
      if $("#estoque_estoque_movimento_valida_por").is(":checked")
        _valida_por = "total_estoque"
      else
        _valida_por = "total_produtos"
      if $("#estoque_estoque_movimento_pronto_pagamento").is(":checked")
        _pronto_pagamento="Não"
      else
        _pronto_pagamento="Sim"
      _data_entrada=$("#estoque_estoque_movimento_data_entrada").val()
      _data_vencimento_entrada=$("#estoque_data_vencimento_entrada").val()
      _boleto = $("#estoque_entrada_boleto").val()


    if $("#estoque_estoque_movimento_tipo_movimentacao :selected").val() == "Devolução para Fornecedor"
      if $("#estoque_fornecedor_devolucao :selected").val() != ""
        _fornecedor_id = $("#estoque_fornecedor_devolucao  :selected").val()
      if $(".estoque_devolucao_total_produtos").val() != " " and $(".estoque_devolucao_total_produtos").val() != ""
        valorTotalProduto = to_float($(".estoque_devolucao_total_produtos").val())
      if $(".estoque_movimento_total_estoque_devolucao").val() != " " or $(".estoque_movimento_total_estoque_devolucao").val() != ""
        ValorTotalEstoque=  $(".estoque_movimento_total_estoque_devolucao").val().replace(".", "").replace(",", ".")
      if $("#estoque_devolucao_frete").val() != ""
        _frete = to_float($("#estoque_devolucao_frete").val())
      if $("#estoque_estoque_movimento_devolucao_valida_por").is(":checked")
        _valida_por = "total_estoque"
      else
        _valida_por = "total_produtos"
      if $("#estoque_estoque_movimento_devolucao_pronto_pagamento").is(":checked")
        _pronto_pagamento="Não"
      else
        _pronto_pagamento="Sim"
      _boleto = $("#estoque_devolucao_boleto").val()
      _data_devolucao=$("#estoque_estoque_movimento_data_devolucao").val()

    if $("#estoque_estoque_movimento_tipo_movimentacao :selected").val() == "Transferência"
      if $("#estoque_estoque_movimento_transferencia_valida_por").is(":checked")
        _valida_por = "total_estoque"
      else
        _valida_por = "total_produtos"
      if $(".estoque_transferencia_total_produtos").val() != " " and $(".estoque_transferencia_total_produtos").val() != ""
        valorTotalProduto = to_float($(".estoque_transferencia_total_produtos").val())
      if $(".estoque_movimento_total_estoque_transferencia").val() != " " or $(".estoque_movimento_total_estoque_transferencia").val() != ""
        ValorTotalEstoque = $(".estoque_movimento_total_estoque_transferencia").val().replace(".", "").replace(",", ".")
      if $("#estoque_transferencia_frete").val() != ""
        _frete = to_float($("#estoque_transferencia_frete").val())
      if $("#estoque_estoque_movimento_transferencia_tipo").is(":checked")
        _transferencia_tipo = true
      else
        _transferencia_tipo = false
      if $("#estoque_estoque_movimento_transferencia_pronto_pagamento").is(":checked")
        _pronto_pagamento="Não"
      else
        _pronto_pagamento="Sim"
      _data_transferencia=$("#estoque_estoque_movimento_data_transferencia").val()

    if $("#estoque_estoque_movimento_acrescimo").val() != ""
      _acrescimo = $("#estoque_estoque_movimento_acrescimo").val()

    if $("#estoque_estoque_movimento_desconto").val() != ""
      _desconto = to_float($("#estoque_estoque_movimento_desconto").val())

    $.ajax
      url: LOCALHOST + "/estoques/estoque_movimentacoes/#{estoque_movimento_id}"
      type: 'PUT'
      dataType: 'JSON'
      data:
        estoque_estoque_movimento:
          tipo_movimentacao: $("#estoque_estoque_movimento_tipo_movimentacao :selected").val()
          fornecedor_id: _fornecedor_id
          destinatario_tomador_id: _destinatario_tomador_id
          nota_fiscal_entrada: $("#estoque_estoque_movimento_nota_fiscal_entrada").val()
          numero_pedido: $("#estoque_estoque_movimento_numero_pedido").val()
          numero_da_os: $("#estoque_estoque_movimento_numero_da_os").val()
          nota_fiscal_saida: $("#estoque_estoque_movimento_numero_da_os").val()
          recibo: $("#estoque_estoque_movimento_recibo").val()
          cupom_fiscal: $("#estoque_estoque_movimento_cupom_fiscal").val()
          data_entrada: _data_entrada
          data_saida: _data_saida
          data_vencimento_entrada: _data_vencimento_entrada
          data_vencimento_saida: _data_vencimento_saida
          data_devolucao: _data_devolucao
          justificativa_devolucao: $("#estoque_estoque_movimento_justificativa_devolucao").val()
          transferencia_tipo: _transferencia_tipo
          data_transferencia: _data_transferencia
          parcelas: parcelas
          valor_total_produto: valorTotalProduto
          total_estoque: ValorTotalEstoque
          acrescimo: _acrescimo
          desconto: _desconto
          boleto: _boleto
          frete: _frete
          periodo: _estoque_estoque_movimento_periodo
          observacoes: $("#estoque_estoque_movimento_observacoes").val()
          valida_por: _valida_por
          pronto_pagamento: _pronto_pagamento
      success: (response) ->
        if response.valid==false
          $(".required_content_box").fadeIn("500")
          $("ol.required_fields_messages").empty()
          $("ol.required_fields_messages").append(collect_errors_messages(response))
          $('#save_stock_moviment_data').prop('disabled', false)
          body = $('html, body')
          body.stop().animate { scrollTop: 0 }, 500, 'swing', ->
            return
        else
          $("#progress_bar").fadeIn('slow')
          setTimeout (->
            $('.progress .progress-bar').progressbar({display_text: 'fill', use_percentage: true})
            setTimeout (->
              $.message({title: 'Aviso!',content: response.notify_message, position: "topright", color: "green"})
              setTimeout (->
                window.location.href=response.url
              ), 8500
            ), 10000
          ), 2000
