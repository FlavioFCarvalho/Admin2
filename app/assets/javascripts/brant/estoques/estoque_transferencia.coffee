#= require brant/estoques/_init_

jQuery ->
  # => logica para calcular valor total do produto pela quantidade (TRANSFERENCIA)
  $("#estoque_estoque_transferencia_quantidade_movimento").change ->
    _quantidade = $(this).val()
    _produto_valor =$('#estoque_estoque_transferencia_valor_produto').val()
    _valor = to_float(_produto_valor)
    _valor_total = (_quantidade * _valor)

    _valor_total = accounting.formatMoney(remove_currency_symbol(_valor_total), {
      symbol: "",
      precision: 2,
      thousand: ".",
      decimal: ","
    })
    $("#estoque_estoque_transferencia_valor_total").val(_valor_total)

  # => logica para calcular valor total do produto pelo valor (TRANSFERENCIA)
  $("#estoque_estoque_transferencia_valor_produto").blur ->
    _valor = $(this).val()
    _valor = to_float(_valor)
    _quantidade = $("#estoque_estoque_transferencia_quantidade_movimento").val()
    _valor_total = (_quantidade * _valor)
    _valor_total = accounting.formatMoney(remove_currency_symbol(_valor_total), {
      symbol: "",
      precision: 2,
      thousand: ".",
      decimal: ","
    })
    $("#estoque_estoque_transferencia_valor_total").val(_valor_total)

  $(".btn_link_edit_estoque_transferencia").click (event) ->
    event.preventDefault()
    estoque_transferencia = $(this).data()

    valorProdutoFormatado = accounting.formatMoney(remove_currency_symbol(estoque_transferencia.valorProduto), {
      symbol: "",
      precision: 2,
      thousand: ".",
      decimal: ","
    })

    valorTotalFormatado = accounting.formatMoney(remove_currency_symbol(estoque_transferencia.valorTotal), {
      symbol: "",
      precision: 2,
      thousand: ".",
      decimal: ","
    })

    sub_etapas = $('#estoque_estoque_transferencia_sub_item_obra_id').html()

    $("#estoque_estoque_transferencia_produto_id").val(estoque_transferencia.produtoId)
    $("#estoque_estoque_transferencia_obra_anterior_id").val(estoque_transferencia.obraAnteriorId)
    $("#estoque_estoque_transferencia_obra_posterior_id").val(estoque_transferencia.obraPosteriorId)

    $("#estoque_estoque_transferencia_item_obra_id").val(estoque_transferencia.itemObraId)
    $(".data_live_search").selectpicker('refresh')
    
    if $('#estoque_estoque_transferencia_item_obra_id :selected').val() != ""
      $('#estoque_estoque_produto_sub_item_obra_id').prop 'disabled', false
      etapa = $('#estoque_estoque_transferencia_item_obra_id :selected').text()
      escaped_etapas = etapa.replace(/([ #;&,.+*~\':"!^$[\]()=>|\/@])/g, '\\$1')
      options = $(sub_etapas).filter('optgroup[label=\'' + escaped_etapas + '\']').html()
      $('#estoque_estoque_transferencia_sub_item_obra_id').html ""
      $('#estoque_estoque_transferencia_sub_item_obra_id').append("<option value=''>Por Favor selecione</option>")
      $('#estoque_estoque_transferencia_sub_item_obra_id').append options
      $('#estoque_estoque_transferencia_sub_item_obra_id').prop 'disabled', false
      $("#estoque_estoque_transferencia_sub_item_obra_id").val(estoque_transferencia.subItemObraId)

    $("#estoque_estoque_transferencia_quantidade_movimento").val(estoque_transferencia.quantidadeMovimento)
    $("#estoque_estoque_transferencia_valor_produto").val(valorProdutoFormatado)
    $("#estoque_estoque_transferencia_valor_total").val(valorTotalFormatado)
    $("#estoque_estoque_transferencia_comodo_id").val(estoque_transferencia.comodoId)

    $("#estoque_estoque_transferencia_estoque_movimento_id").val(estoque_transferencia.estoqueMovimentoId)
    $("#tr_estoque_transferencia_produto_id_#{estoque_transferencia.estoqueTransferenciaId}").fadeOut(500)
    $(".btn_link_edit_estoque_transferencia").fadeOut(500)
    $(".btn_submit_estoque_transferencia").hide()
    $("#btn_estoque_transferencia_area").append("<a href='#' id='btn_editar_estoque_transferencia' class='btn btn-success' data-estoque-transferencia-id='#{estoque_transferencia.estoqueTransferenciaId}'>"+
                                          "<i class='fa fa-edit'></i>Alterar Dados</a>")

  $(document).on 'click', '#btn_editar_estoque_transferencia', (event) ->
    event.preventDefault()
    estoque_movimentaco_id=$("#estoque_estoque_transferencia_estoque_movimento_id").val()
    produto_id=$("#estoque_estoque_transferencia_produto_id :selected").val()
    estoque_transferencia_id = $(this).data().estoqueTransferenciaId
    $.ajax
      url: LOCALHOST + "estoques/estoque_movimentacoes/#{estoque_movimentaco_id}/estoque_transferencias/#{estoque_transferencia_id}"
      type: 'PUT'
      dataType: 'JSON'
      data:
        estoque_movimentaco_id: estoque_movimentaco_id
        estoque_estoque_transferencia:
          id: estoque_transferencia_id
          estoque_movimento_id: estoque_movimentaco_id
          produto_id: produto_id
          comodo_id: $("#estoque_estoque_transferencia_comodo_id :selected").val()
          obra_id: $("#estoque_estoque_transferencia_obra_id :selected").val()
          item_obra_id: $("#estoque_estoque_transferencia_item_obra_id :selected").val()
          sub_item_obra_id: $("#estoque_estoque_transferencia_sub_item_obra_id :selected").val()
          quantidade_movimento: $("#estoque_estoque_transferencia_quantidade_movimento").val()
          valor_produto: to_float($("#estoque_estoque_transferencia_valor_produto").val())
          valor_total: to_float($("#estoque_estoque_transferencia_valor_total").val())
      success: (response) ->
        window.location.href=response.url