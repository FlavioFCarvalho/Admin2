#= require brant/estoques/_init_

jQuery ->
  # --------------------- LOGICA DE CATEGORIA ----------------------
  # => Logica para fazer aparecer campos para nova categoria no sistema
  $(document).on 'click', '#btn_new_categoria', (event) ->
    event.preventDefault()
    $("#categoria_fields").slideDown('fast')

  # => Logica para fazer cancelar cadastro de nova categoria no sistema
  $(document).on 'click', '#btn_cancelar_categoria', (event) ->
    event.preventDefault()
    $("#categoria_nome").val('')
    $("#categoria_fields").slideUp('fast')

  # => logica para salvar nova categoria
  $(document).on 'click', '#btn_salvar_categoria', (event) ->
    event.preventDefault()
    $.ajax
      url: LOCALHOST + "resquests/create_categoria_through_ajax"
      type: 'GET'
      dataType: 'JSON'
      data:
        categoria:
          nome: $("#categoria_nome").val()
      success: (response) ->
        if response.valid == true
          $.notify(response.message, "success")
          $("#categoria_fields").slideUp('fast')
          $("#categoria_nome").val("")
          $("#select_categoria").populate(response.value, response.name)
          $("#select_categoria").selectpicker('refresh')
        if response.valid == false
          $.notify(response.message, "error")
  # -----------------FIM DA LOGICA DE CATEGORIA --------------------
