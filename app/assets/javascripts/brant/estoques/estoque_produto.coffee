#= require brant/estoques/_init_

jQuery ->
  $(".btn_link_edit_estoque_produto").click (event) ->
    event.preventDefault()
    estoque_produto = $(this).data()

    valorProdutoFormatado = accounting.formatMoney(remove_currency_symbol(estoque_produto.valorProduto), {
      symbol: "",
      precision: 2,
      thousand: ".",
      decimal: ","
    })

    valorTotalFormatado = accounting.formatMoney(remove_currency_symbol(estoque_produto.valorTotal), {
      symbol: "",
      precision: 2,
      thousand: ".",
      decimal: ","
    })
    
    sub_etapas = $('#estoque_estoque_produto_sub_item_obra_id').html()

    $("#estoque_estoque_produto_produto_id").val(estoque_produto.produtoId)
    $("#estoque_estoque_produto_obra_id").val(estoque_produto.obraId)
    $("#estoque_estoque_produto_item_obra_id").val(estoque_produto.itemObraId)
    
    if $('#estoque_estoque_produto_item_obra_id :selected').val() != ""
      $('#estoque_estoque_produto_sub_item_obra_id').prop 'disabled', false
      etapa = $('#estoque_estoque_produto_item_obra_id :selected').text()
      escaped_etapas = etapa.replace(/([ #;&,.+*~\':"!^$[\]()=>|\/@])/g, '\\$1')
      options = $(sub_etapas).filter('optgroup[label=\'' + escaped_etapas + '\']').html()
      $('#estoque_estoque_produto_sub_item_obra_id').html ""
      $('#estoque_estoque_produto_sub_item_obra_id').append("<option value=''>Por Favor selecione</option>")
      $('#estoque_estoque_produto_sub_item_obra_id').append options
      $('#estoque_estoque_produto_sub_item_obra_id').prop 'disabled', false
      $("#estoque_estoque_produto_sub_item_obra_id").val(estoque_produto.subItemObraId)
      $(".data_live_search").selectpicker('refresh')

    $("#estoque_estoque_produto_id").val(estoque_produto.estoqueProdutoId)
    $("#estoque_estoque_produto_quantidade_movimento").val(estoque_produto.quantidadeMovimento)
    $("#estoque_estoque_produto_valor_produto").val(valorProdutoFormatado)
    $("#estoque_estoque_produto_valor_total").val(valorTotalFormatado)
    $("#estoque_estoque_produto_comodo_id").val(estoque_produto.comodoId)
    $('.data_live_search').selectpicker('refresh')
    $("#estoque_estoque_produto_estoque_movimento_id").val(estoque_produto.estoqueMovimentoId)
    $("#estoque_estoque_produto_observacoes").val(estoque_produto.observacoes)
    $("#tr_estoque_produto_id_#{estoque_produto.estoqueProdutoId}").fadeOut(500)
    $(".btn_link_edit_estoque_produto").fadeOut(500)
    $("#btn_save_estoque_produto").hide()
    $("#btn_estoque_produto_edit").fadeIn(500)
    $.message({title: 'Aviso!',content: "Entrando em modo de Edição", position: "topright", color: "yellow"})