jQuery ->
  $('#estoque_estoque_transferencia_item_obra_id').change ->
    etapa = $('#estoque_estoque_transferencia_item_obra_id :selected').text()
    escaped_etapas = etapa.replace(/([ #;&,.+*~\':"!^$[\]()=>|\/@])/g, '\\$1')
    options = $(sub_etapas).filter('optgroup[label=\'' + escaped_etapas + '\']').html()
    if options
      $('#estoque_estoque_transferencia_sub_item_obra_id').html options
      $('#estoque_estoque_transferencia_sub_item_obra_id').prop 'disabled', false
      return
    else
      $('#estoque_estoque_transferencia_sub_item_obra_id').empty()
      $('#estoque_estoque_transferencia_sub_item_obra_id').prop 'disabled', true
      return

  sub_etapas = $('#estoque_estoque_transferencia_sub_item_obra_id').html()
  if $('#estoque_estoque_transferencia_item_obra_id').val() != ""
    $('#estoque_estoque_transferencia_sub_item_obra_id').prop 'disabled', false
    estado = $('#estoque_estoque_transferencia_item_obra_id :selected').text()
    escaped_etapas = estado.replace(/([ #;&,.+*~\':"!^$[\]()=>|\/@])/g, '\\$1')
    options = $(sub_etapas).filter('optgroup[label=\'' + escaped_etapas + '\']').html()
    $('#estoque_estoque_transferencia_sub_item_obra_id').html options
  else
    $('#estoque_estoque_transferencia_sub_item_obra_id').prop 'disabled', true
