#= require brant/estoques/_init_
#= require brant/estoques/_categoria
jQuery ->
  # => Variavel de Controle
  _produtos_count = 0

  # --------------------- LOGICA DE PRODUTO ------------------------
  # => logica para liberar campos do produto na aba estoque
  $(document).on 'click', '#btn_new_product_on_estoque_form', (event) ->
    event.preventDefault()
    $("#estoque_estoque_produtos_quantidade").prop( "disabled", true )
    $("#estoque_estoque_produtos_valor").prop( "disabled", true )
    $("#estoque_estoque_produtos_valor_total").prop( "disabled", true )
    $("#btn_add_produto_no_estoque").fadeOut('fast')
    $("#estoque_produto_fields").slideDown('fast')

  # => logica para cancelar novo produto e esconder os campos do produto
  $(document).on 'click', '#btn_cancelar_produto', (event) ->
    event.preventDefault()
    $("#estoque_estoque_produtos_quantidade").prop( "disabled", false )
    $("#estoque_estoque_produtos_valor").prop( "disabled", false )
    $("#estoque_estoque_produtos_valor_total").prop( "disabled", false )
    $("#btn_add_produto_no_estoque").fadeIn('fast')
    $("#estoque_produto_fields").slideUp('fast')

  # => logica para salvar novo produto
  $(document).on 'click', '#btn_salvar_produto', (event) ->
    event.preventDefault()
    $.ajax
      url: LOCALHOST + "resquests/create_product_through_ajax"
      type: 'GET'
      dataType: 'JSON'
      data:
        produto:
          nome: $("#produto_nome").val()
          valor: $("#produto_valor").val()
          unidade: $("#produto_unidade").val()
          observacoes: $("#produto_observacoes").val()
          categoria:
            id:   $("#select_categoria :selected").val()
            nome: $("#select_categoria :selected").text()
      success: (response) ->
        if response.valid == true
          $.notify(response.message, "success")
          $("#categoria_fields").slideUp('fast')
          $("#produto_nome").val("")
          $("#produto_valor").val("")
          $("#produto_unidade").val("")
          $("#produto_observacoes").val("")
          $("#estoque_estoque_produto_produto_id").populate(response.value, response.name)
          $("#estoque_estoque_produto_produto_id").selectpicker('refresh')
          $("#estoque_estoque_produtos_quantidade").prop( "disabled", false )
          $("#estoque_estoque_produtos_valor").prop( "disabled", false )
          $("#estoque_estoque_produtos_valor_total").prop( "disabled", false )
          $("#btn_add_produto_no_estoque").fadeIn('fast')
          $("#estoque_produto_fields").slideUp('fast')
        if response.valid == false
          $.notify(response.message, "error")

  # => logica para retornar o valor do Produto
  $("#estoque_estoque_produto_produto_id").change ->
    $.ajax
      url: LOCALHOST + "resquests/find_resource"
      type: 'GET'
      dataType: 'JSON'
      data:
        resource:
          name: "Produto"
          id: $("#estoque_estoque_produto_produto_id :selected").val()
      success: (response) ->
        $("#estoque_estoque_produto_valor_produto").val(response.value)
        $("#estoque_estoque_produto_valor_total").val(response.value)

  # => logica para calcular valor total do produto pela quantidade
  $("#estoque_estoque_produto_quantidade_movimento").change ->
    _quantidade = $(this).val()
    _produto_valor =$('#estoque_estoque_produto_valor_produto').val()
    _valor = to_float(_produto_valor)
    _valor_total = (_quantidade * _valor)
    _valor_total = accounting.formatMoney(remove_currency_symbol(_valor_total), {
      symbol: "",
      precision: 2,
      thousand: ".",
      decimal: ","
    })
    $("#estoque_estoque_produto_valor_total").val(_valor_total)

  # => logica para calcular valor total do produto pelo valor
  $("#estoque_estoque_produto_valor_produto").blur ->
    _valor = $(this).val()
    _valor = to_float(_valor)
    _quantidade = $("#estoque_estoque_produto_quantidade_movimento").val()
    _valor_total = (_quantidade * _valor)
    _valor_total = accounting.formatMoney(remove_currency_symbol(_valor_total), {
      symbol: "",
      precision: 2,
      thousand: ".",
      decimal: ","
    })
    $("#estoque_estoque_produto_valor_total").val(_valor_total)

  # --------------------- FIM DA LOGICA DE PRODUTO -----------------------
