jQuery ->
  if $("#q_tipo_movimentacao_eq").val() == "ENTRADA"
    $("#dt_entrada").show()
    $("#dt_vencimento_entrada").show()
    $("#dt_vencimento_saida").hide()
    $("#dt_saida").hide()
    $("#dt_devolucao").hide()

  $("#q_tipo_movimentacao_eq").change ->
    if $(this).val() == ""
      $("#dt_entrada").hide()
      $("#dt_vencimento_entrada").hide()
      $("#dt_devolucao").hide()
      $("#q_data_entrada_eq").val("")
      $("#q_data_devolucao_eq").val("")
      $("#q_data_vencimento_entrada_eq").val("")
      $("#q_data_vencimento_entrada_eq").hide()
    if $(this).val() == "TODOS"
      $("#dt_entrada").show()
      $("#dt_vencimento_entrada").show()
      $("#dt_devolucao").hide()
      $("#q_data_entrada_eq").val("")
      $("#q_data_devolucao_eq").val("")
      $("#q_data_vencimento_entrada_eq").val("")
    if $(this).val() == "ENTRADA"
      $("#dt_entrada").show()
      $("#dt_vencimento_entrada").show()
      $("#dt_vencimento_saida").hide()
      $("#dt_saida").hide()
      $("#dt_devolucao").hide()
      $("#q_data_saida_eq").val("")
      $("#q_data_vencimento_saida_eq").val("")
      $("#q_data_devolucao_eq").val("")
      $("#q_data_vencimento_entrada_eq").show()
    # if $(this).val() == "SAÍDA"
    #   $("#dt_entrada").hide()
    #   $("#dt_saida").show()
    #   $("#dt_vencimento_entrada").hide()
    #   $("#dt_vencimento_saida").show()
    #   $("#dt_devolucao").hide()
    #   $("#q_data_entrada_eq").val("")
    #   $("#q_data_vencimento_entrada_eq").val("")
    #   $("#q_data_devolucao_eq").val("")
    if $(this).val() == "DEVOLUÇÃO PARA FORNECEDOR"
      $("#dt_vencimento_entrada").hide()
      $("#dt_vencimento_saida").hide()
      $("#dt_saida").hide()
      $("#dt_entrada").show()
      $("#dt_devolucao").show()
      $("#q_data_entrada_eq").val("")
      $("#q_data_saida_eq").val("")
      $("#q_data_vencimento_entrada_eq").val("")
      $("#q_data_vencimento_saida_eq").val("")
    if $(this).val() == "TRANSFERÊNCIA DE ENTRADA"
      $("#dt_entrada").show()
      $("#dt_vencimento_entrada").show()
      $("#dt_devolucao").hide()
      $("#q_data_entrada_eq").val("")
      $("#q_data_devolucao_eq").val("")
      $("#q_data_vencimento_entrada_eq").val("")
      $("#q_data_vencimento_entrada_eq").show()
    if $(this).val() == "TRANSFERÊNCIA DE SAÍDA"
      $("#dt_entrada").show()
      $("#dt_vencimento_entrada").show()
      $("#dt_devolucao").hide()
      $("#q_data_entrada_eq").val("")
      $("#q_data_devolucao_eq").val("")
      $("#q_data_vencimento_entrada_eq").val("")
      $("#q_data_vencimento_entrada_eq").show()

  $("#btn-estoque-filtro").on 'click', (event) ->
    event.preventDefault()
    _messages = []
    if $("#q_tipo_movimentacao_eq").val() == "TODOS"
      if $("#q_obra_id_eq").val() == "" and $("#q_produto_id_eq").val() == "" and $("#q_fornecedor_id_eq").val() == ""
        _messages.push("<li>Fornecedor ou Obra ou Produdo devem ser selecionada para o Filtro TODOS</li>")
      if $("#q_data_entrada_eq").val() == "" and $("#q_data_vencimento_entrada_eq").val() == ""
        _messages.push("<li>Datas devem ser preenchidas para qualquer filtro</li>")
      if _messages.length > 0
        BootstrapDialog.show
          type: BootstrapDialog.TYPE_DANGER
          title: 'AVISO!'
          message: _messages
          closable: true
          draggable: true
      else
        $('form#estoque_filtro').unbind('submit').submit()
    else
      $('form#estoque_filtro').unbind('submit').submit()


