#= require notifyjs_rails
# -----------------------BEFORE - START ---------------------------------

$(document).ready ->
  window.LOCALHOST= window.location.origin + '/'
  window._uncompleted_fields = false
  window._estoque_estoque_movimento_periodo = 0
  window._produtos = []

  $(".fornecedor_id").selectpicker ->
  $(".data_live_search").selectpicker ->
  $(".data").datepicker language: 'pt-BR'

  # => Lógica para popular componentes do tipo select
  $.fn.populate = (value, name) ->
    $(this).append("<option value='#{value}'>#{name}</option>")
    return

  # => Lógica para Formata valor
  window.format_value = (x) ->
    x.toFixed(2).replace(',', '').replace '.', ','

  # => Lógica para Converte para float
  window.to_float = (x) ->
    parseFloat x.replace('.', '').replace(',', '.')

  # => Lógica para remover simbologia da Moeda Local
  window.remove_currency_symbol = (x) ->
    x.toString().replace("-", "")

  # => Lógica para coletar e montar mensagens de erro
  window.collect_errors_messages=(resource) ->
    i=0
    _error_messages_=[]
    while i <= resource.messages.length
      if resource.messages[i]!= undefined
        _error_messages_.push("<li style='color: rgb(140, 18, 15); font-weight: bolder;'>#{resource.messages[i]}</li>")
      i++
    return _error_messages_
