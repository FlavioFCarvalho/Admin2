jQuery ->
  comodos = $('#estoque_estoque_transferencia_comodo_id').html()
  $('#estoque_estoque_transferencia_obra_anterior_id').change ->
    obra = $('#estoque_estoque_transferencia_obra_anterior_id :selected').text()
    escaped_comodos = obra.replace(/([ #;&,.+*~\':"!^$[\]()=>|\/@])/g, '\\$1')
    options = $(comodos).filter('optgroup[label=\'' + escaped_comodos + '\']').html()
    if options
      $('#estoque_estoque_transferencia_comodo_id').html ""
      $('#estoque_estoque_transferencia_comodo_id').append("<option value=''>Por Favor selecione</option>")
      $('#estoque_estoque_transferencia_comodo_id').append options
      $('#estoque_estoque_transferencia_comodo_id').prop 'disabled', false
      $(".data_live_search").selectpicker('refresh')
      return
    else
      $('#estoque_estoque_transferencia_comodo_id').empty()
      $(".data_live_search").selectpicker('refresh')
      $('#estoque_estoque_transferencia_comodo_id').prop 'disabled', true
      return
  
  if $('#estoque_estoque_transferencia_obra_anterior_id').val() != ""
    $('#estoque_estoque_transferencia_comodo_id').prop 'disabled', false
    obra = $('#estoque_estoque_transferencia_obra_anterior_id :selected').text()
    escaped_comodos = obra.replace(/([ #;&,.+*~\':"!^$[\]()=>|\/@])/g, '\\$1')
    options = $(comodos).filter('optgroup[label=\'' + escaped_comodos + '\']').html()
    $('#estoque_estoque_transferencia_comodo_id').html ""
    $('#estoque_estoque_transferencia_comodo_id').append("<option value=''>Por Favor selecione</option>")
    $('#estoque_estoque_transferencia_comodo_id').append options
    $(".data_live_search").selectpicker('refresh')
  else
    $(".data_live_search").selectpicker('refresh')
    $('#estoque_estoque_transferencia_comodo_id').prop 'disabled', true
  
  

  
