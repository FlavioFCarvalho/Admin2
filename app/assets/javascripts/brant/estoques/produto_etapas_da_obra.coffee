jQuery ->
  $('#estoque_estoque_produto_item_obra_id').change ->
    etapa = $('#estoque_estoque_produto_item_obra_id :selected').text()
    escaped_etapas = etapa.replace(/([ #;&,.+*~\':"!^$[\]()=>|\/@])/g, '\\$1')
    options = $(sub_etapas).filter('optgroup[label=\'' + escaped_etapas + '\']').html()
    if options
      $('#estoque_estoque_produto_sub_item_obra_id').html ""
      $('#estoque_estoque_produto_sub_item_obra_id').append("<option value=''>Por Favor selecione</option>")
      $('#estoque_estoque_produto_sub_item_obra_id').append options
      $('#estoque_estoque_produto_sub_item_obra_id').prop 'disabled', false
      $(".data_live_search").selectpicker('refresh')
      return
    else
      $('#estoque_estoque_produto_sub_item_obra_id').empty()
      $('#estoque_estoque_produto_sub_item_obra_id').prop 'disabled', true
      return

  sub_etapas = $('#estoque_estoque_produto_sub_item_obra_id').html()
  if $('#estoque_estoque_produto_item_obra_id :selected').val() != ""
    $('#estoque_estoque_produto_sub_item_obra_id').prop 'disabled', false
    etapa = $('#estoque_estoque_produto_item_obra_id :selected').text()
    escaped_etapas = etapa.replace(/([ #;&,.+*~\':"!^$[\]()=>|\/@])/g, '\\$1')
    options = $(sub_etapas).filter('optgroup[label=\'' + escaped_etapas + '\']').html()
    $('#estoque_estoque_produto_sub_item_obra_id').html ""
    $('#estoque_estoque_produto_sub_item_obra_id').append("<option value=''>Por Favor selecione</option>")
    $('#estoque_estoque_produto_sub_item_obra_id').append options
    $('#estoque_estoque_produto_sub_item_obra_id').prop 'disabled', false
    $(".data_live_search").selectpicker('refresh')
  else
    $('#estoque_estoque_produto_sub_item_obra_id').prop 'disabled', true
