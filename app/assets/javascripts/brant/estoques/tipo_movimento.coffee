#= require brant/estoques/_init_
jQuery ->
  $("#estoque_estoque_movimento_valida_por").bootstrapToggle({
    on: "Valor Total"
    off: "Total de Produtos"
    size: "normal"
    onstyle: "success"
    offstyle: "default"
    width: "245px"
  })

  $("#estoque_estoque_movimento_pronto_pagamento").bootstrapToggle({
    on: "Não"
    off: "Sim"
    size: "normal"
    onstyle: "success"
    offstyle: "default"
    width: "100px"
  })

  $("#estoque_estoque_movimento_devolucao_pronto_pagamento").bootstrapToggle({
    on: "Não"
    off: "Sim"
    size: "normal"
    onstyle: "success"
    offstyle: "default"
    width: "100px"
  })

  $("#estoque_estoque_movimento_transferencia_pronto_pagamento").bootstrapToggle({
    on: "Não"
    off: "Sim"
    size: "normal"
    onstyle: "success"
    offstyle: "default"
    width: "150px"
  })

  $("#estoque_estoque_movimento_devolucao_valida_por").bootstrapToggle({
    on: "Valor Total"
    off: "Total de Produtos"
    size: "normal"
    onstyle: "success"
    offstyle: "default"
    width: "245px"
  })

  $("#estoque_estoque_movimento_transferencia_valida_por").bootstrapToggle({
    on: "Valor Total"
    off: "Total de Produtos"
    size: "normal"
    onstyle: "success"
    offstyle: "default"
    width: "150px"
  })
  
  $("#estoque_estoque_movimento_transferencia_tipo").bootstrapToggle({
    on: "Entrada"
    off: "Saída"
    size: "normal"
    onstyle: "success"
    offstyle: "default"
    width: "150px"
  })

  # => Logica para Exibição dos campos dos tipos de movimentos detectados
  if $("#estoque_estoque_movimento_tipo_movimentacao").val() == "Entrada"
    $("#entrada").slideDown('slow')
    $("#transferencia").slideUp('slow')
    $("#devolucao").slideUp('slow')
  if $("#estoque_estoque_movimento_tipo_movimentacao").val() == "Transferência"
    $("#entrada").slideUp('slow')
    $("#devolucao").slideUp('slow')
    $("#transferencia").slideDown('slow')
  if $("#estoque_estoque_movimento_tipo_movimentacao").val() == "Devolução para Fornecedor"
    $("#entrada").slideUp('slow')
    $("#transferencia").slideUp('slow')
    $("#devolucao").slideDown('slow')

  # => Logica para troca dos campos dos tipos de Movimentos
  $("#estoque_estoque_movimento_tipo_movimentacao").change ->
    if $(this).val() == "" || $(this).text() == "Selecione"
      $("#entrada").fadeOut(500)
      $("#transferencia").fadeOut(500)
      $("#devolucao").fadeOut(500)
      if $(".estoque_movimento_parcelas_entrada").val() == "1"
          $(".estoque_periodo_select").val("Semanal")
          $(".estoque_periodo_select").hide()
          _estoque_estoque_movimento_periodo = $(".estoque_periodo_select :selected").val()
    else
      if $(this).val() == "Entrada"
        if $(".estoque_movimento_parcelas_entrada").val() == "1"
          $(".estoque_periodo_select").val("Semanal")
        $("#entrada").slideDown('slow')
        $("#transferencia").slideUp('slow')
        $("#devolucao").slideUp('slow')
      if $(this).val() =="Devolução para Fornecedor"
        $("#entrada").slideUp('slow')
        $("#transferencia").slideUp('slow')
        $("#devolucao").slideDown('slow')
      if $(this).val() =="Transferência"
        $("#entrada").slideUp('slow')
        $("#devolucao").slideUp('slow')
        $("#transferencia").slideDown('slow')

  $(".estoque_entrada_total_produtos").blur ->
    valor_total_produto = to_float($(this).val())
    acrescimo   = 0.0
    desconto    = 0.0
    valor_total = 0.0
    frete       = 0.0

    if $("#estoque_estoque_movimento_acrescimo").val() != ""
      acrescimo = to_float($("#estoque_estoque_movimento_acrescimo").val())
    if $("#estoque_estoque_movimento_desconto").val() != ""
      desconto  = to_float($("#estoque_estoque_movimento_desconto").val())

    if $("#estoque_estoque_movimento_tipo_movimentacao :selected").text() == "Entrada"
      if $("#estoque_entrada_frete").val() != ""
        frete = to_float($("#estoque_entrada_frete").val())
    
      valor_total = valor_total + valor_total_produto + acrescimo - desconto + frete

      valorformatado = accounting.formatMoney(remove_currency_symbol(valor_total), {
        symbol: "",
        precision: 2,
        thousand: ".",
        decimal: ","
      })
      $(".estoque_movimento_total_estoque_entrada").val(valorformatado)
  
  $(".estoque_transferencia_total_produtos").blur ->
    valor_total_produto = to_float($(this).val())
    acrescimo   = 0.0
    desconto    = 0.0
    valor_total = 0.0
    frete       = 0.0

    if $("#estoque_estoque_movimento_tipo_movimentacao :selected").text() == "Transferência"
      if $("#estoque_transferencia_frete").val() != "" or $("#estoque_transferencia_frete").val() != " "
        frete = to_float($("#estoque_transferencia_frete").val())
    
      valor_total = valor_total + valor_total_produto + acrescimo - desconto + frete

      valorformatado = accounting.formatMoney(remove_currency_symbol(valor_total), {
        symbol: "",
        precision: 2,
        thousand: ".",
        decimal: ","
      })
      $(".estoque_movimento_total_estoque_transferencia").val(valorformatado)

  $(".estoque_devolucao_total_produtos").blur ->
    valor_total_produto = to_float($(this).val())
    acrescimo   = 0.0
    desconto    = 0.0
    valor_total = 0.0
    frete       = 0.0
    
    if $("#estoque_estoque_movimento_tipo_movimentacao :selected").text() == "Devolução para Fornecedor"
      if $("#estoque_devolucao_frete").val() != ""
        frete = to_float($("#estoque_devolucao_frete").val())
      valor_total = valor_total + valor_total_produto + acrescimo - desconto + frete

      valorformatado = accounting.formatMoney(remove_currency_symbol(valor_total), {
        symbol: "",
        precision: 2,
        thousand: ".",
        decimal: ","
      })
      $(".estoque_movimento_total_estoque_devolucao").val(valorformatado)
    if $("#estoque_estoque_movimento_tipo_movimentacao :selected").text() == "Transferência"
      $(".estoque_movimento_total_estoque_transferencia").val(valorformatado)

  $("#estoque_estoque_movimento_acrescimo").blur ->
    valor_acrescimo = to_float($(this).val())
    valor_total_produto = 0.0
    desconto            = 0.0
    valor_total         = 0.0
    frete               = 0.0
    
    if $("#estoque_entrada_frete").val() != ""
      frete = to_float($("#estoque_entrada_frete").val())
    if $("#estoque_estoque_movimento_valor_total_produto").val() != ""
      valor_total_produto = to_float($("#estoque_estoque_movimento_valor_total_produto").val())
    if $("#estoque_estoque_movimento_desconto").val() != ""
      desconto  = to_float($("#estoque_estoque_movimento_desconto").val())

    valor_total = valor_total + valor_total_produto + valor_acrescimo - desconto + frete

    valorformatado = accounting.formatMoney(remove_currency_symbol(valor_total), {
      symbol: "",
      precision: 2,
      thousand: ".",
      decimal: ","
    })
    if $("#estoque_estoque_movimento_tipo_movimentacao :selected").text() == "Entrada"
      $(".estoque_movimento_total_estoque_entrada").val(valorformatado)
    if $("#estoque_estoque_movimento_tipo_movimentacao :selected").text() == "Devolução para Fornecedor"
      $(".estoque_movimento_total_estoque_devolucao").val(valorformatado)
    if $("#estoque_estoque_movimento_tipo_movimentacao :selected").text() == "Transferência"
      $(".estoque_movimento_total_estoque_transferencia").val(valorformatado)

  $("#estoque_estoque_movimento_desconto").blur ->
    desconto = to_float($(this).val())
    valor_total_produto   = 0.0
    valor_acrescimo    = 0.0
    valor_total = 0.0
    
    frete = 0.0
    if $("#estoque_entrada_frete").val() != ""
      frete = to_float($("#estoque_entrada_frete").val())
    if $("#estoque_estoque_movimento_valor_total_produto").val() != ""
      valor_total_produto = to_float($("#estoque_estoque_movimento_valor_total_produto").val())
    if $("#estoque_estoque_movimento_acrescimo").val() != ""
      valor_acrescimo  = to_float($("#estoque_estoque_movimento_acrescimo").val())

    valor_total = valor_total + valor_total_produto + valor_acrescimo - desconto + frete

    valorformatado = accounting.formatMoney(remove_currency_symbol(valor_total), {
      symbol: "",
      precision: 2,
      thousand: ".",
      decimal: ","
    })

    if $("#estoque_estoque_movimento_tipo_movimentacao :selected").text() == "Entrada"
      $(".estoque_movimento_total_estoque_entrada").val(valorformatado)
    if $("#estoque_estoque_movimento_tipo_movimentacao :selected").text() == "Devolução para Fornecedor"
      $(".estoque_movimento_total_estoque_devolucao").val(valorformatado)
    if $("#estoque_estoque_movimento_tipo_movimentacao :selected").text() == "Transferência"
      $(".estoque_movimento_total_estoque_transferencia").val(valorformatado)
  
  $("#estoque_entrada_frete").blur ->
    frete = to_float($(this).val())
    valor_total_produto = 0.0
    valor_acrescimo     = 0.0
    valor_total         = 0.0  
    desconto            = 0.0  

    if $("#estoque_estoque_movimento_desconto").val() != ""
      desconto = to_float($("#estoque_estoque_movimento_desconto").val())
    if $(".estoque_entrada_total_produtos").val() != ""
      valor_total_produto = to_float($(".estoque_entrada_total_produtos").val())
    if $("#estoque_estoque_movimento_acrescimo").val() != ""
      valor_acrescimo = to_float($("#estoque_estoque_movimento_acrescimo").val())

    valor_total = valor_total + valor_total_produto + valor_acrescimo - desconto + frete

    valorformatado = accounting.formatMoney(remove_currency_symbol(valor_total), {
      symbol: "",
      precision: 2,
      thousand: ".",
      decimal: ","
    })

    if $("#estoque_estoque_movimento_tipo_movimentacao :selected").text() == "Entrada"
      $(".estoque_movimento_total_estoque_entrada").val(valorformatado)

  $("#estoque_devolucao_frete").blur ->
    frete = to_float($(this).val())
    valor_total_produto = 0.0
    valor_acrescimo     = 0.0
    valor_total         = 0.0  
    desconto            = 0.0  

    if $(".estoque_devolucao_total_produtos").val() != ""
      valor_total_produto = to_float($(".estoque_devolucao_total_produtos").val())
    if $("#estoque_estoque_movimento_acrescimo").val() != ""
      valor_acrescimo = to_float($("#estoque_estoque_movimento_acrescimo").val())

    valor_total = valor_total + valor_total_produto + valor_acrescimo - desconto + frete

    valorformatado = accounting.formatMoney(remove_currency_symbol(valor_total), {
      symbol: "",
      precision: 2,
      thousand: ".",
      decimal: ","
    })

    if $("#estoque_estoque_movimento_tipo_movimentacao :selected").text() == "Devolução para Fornecedor"
      $(".estoque_movimento_total_estoque_devolucao").val(valorformatado)

  $("#estoque_transferencia_frete").blur ->
    frete = to_float($(this).val())
    total_produtos  = 0
    valor_acrescimo = 0
    total_estoque   = 0
    desconto        = 0

    if $(".estoque_transferencia_total_produtos").val() != "" and $(".estoque_transferencia_total_produtos").val() != " "
      valor_total_produto = to_float($(".estoque_transferencia_total_produtos").val())

    total_estoque = total_estoque + valor_total_produto + valor_acrescimo - desconto + frete

    valorformatado = accounting.formatMoney(total_estoque, {
      symbol: "",
      precision: 2,
      thousand: ".",
      decimal: ","
    })

    if $("#estoque_estoque_movimento_tipo_movimentacao :selected").text() == "Transferência"
      $(".estoque_movimento_total_estoque_transferencia").val(valorformatado)
  

  $("#estoque_estoque_movimento_parcelas").blur ->
    if $(this).val() > "1"
      $(".periodo_field_section").show('fast')
    if $(this).val() == "1"
      $(".periodo_field_section").fadeOut(500)

  $(".estoque_movimento_parcelas_devolucao").blur ->
    if $(this).val() > "1"
      $(".periodo_devolucao_field_section").show('fast')
    if $(this).val() == "1"
      $(".periodo_devolucao_field_section").fadeOut(500)

  $(".estoque_movimento_parcelas_transferencia").blur ->
    if $(this).val() > "1"
      $(".periodo_transferencia_field_section").show('fast')
    if $(this).val() == "1"
      $(".periodo_transferencia_field_section").fadeOut(500)


  