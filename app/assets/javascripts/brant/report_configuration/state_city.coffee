#= require jquery/jquery-3.2.1.min

# ----------------------------- START ------------------------------------
jQuery ->
  cidades = undefined
  estado = undefined
  escaped_estados = undefined
  options = undefined

  if $('.simple_form.new_report_configuration').length
    if $('#report_configuration_state_id').length
      $('#report_configuration_state_id').val("19")

  if $('.simple_form.edit_report_configuration').length
    if $('#report_configuration_state_id').length
      $('#report_configuration_state_id').val("19")

  $('#report_configuration_state_id').change ->
    estado = $('#report_configuration_state_id :selected').text()
    escaped_estados = estado.replace(/([ #;&,.+*~\':"!^$[\]()=>|\/@])/g, '\\$1')
    options = $(cidades).filter('optgroup[label=\'' + escaped_estados + '\']').html()
    if options
      $('#report_configuration_city_id').html options
      $('#report_configuration_city_id').prop 'disabled', false
      return
    else
      $('#report_configuration_city_id').empty()
      $('#report_configuration_city_id').prop 'disabled', true
      return

  cidades = $('#report_configuration_city_id').html()
  if $('#report_configuration_state_id').val() != ""
    if $('#report_configuration_state_id').val() == "19"
      $('#report_configuration_city_id').val("3633")
    else
      $('#report_configuration_city_id').prop 'disabled', false
      estado = $('#report_configuration_state_id :selected').text()
      escaped_estados = estado.replace(/([ #;&,.+*~\':"!^$[\]()=>|\/@])/g, '\\$1')
      options = $(cidades).filter('optgroup[label=\'' + escaped_estados + '\']').html()
      $('#report_configuration_city_id').html options
  else
    $('#report_configuration_city_id').prop 'disabled', true
  return
