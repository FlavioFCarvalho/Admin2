#= require brant/clientes_&_fornecedores/_init_
# require bootbox
#= require global/delete
# ----------------------------- START ------------------------------------
jQuery ->
  $("#cliente_fornecedor_name_products").selectpicker ->
  $("#txt_new_product_name").val("")
  $("#txt_new_product_value").val("")
  $('#select_new_product_unity :nth-child(1)').prop('selected', true)
  $("#txt_new_product_observation").val("")

  # => Faz Surgir os campos para criar um novo produto
  $("#add_product_association_btn").click (event) ->
    event.preventDefault()
    $(this).fadeOut("slow")
    $("#hide_divproduct_field").slideDown( "slow" )

  # => Remove o formulário de criacao de um novo produto
  $("#delete_txt_new_product").click (event) ->
    event.preventDefault()
    $("#add_product_association_btn").fadeIn("slow")
    $("#hide_divproduct_field").slideUp( "slow" )
    $("#txt_new_product_name").val("")
    $("#txt_new_product_value").val("")
    $('#select_new_product_unity :nth-child(1)').prop('selected', true)
    $("#txt_new_product_observation").val("")

  # => Envia dados para o controller de Requests para criar um novo Produto
  $("#create_new_product").click (event) ->
    event.preventDefault()
    if $("#txt_new_product_name").val() == ""
      $.notify("O nome do produto deve ser preenchido.", "error")
    if $("#txt_new_product_value").val() == ""
      $.notify("O valor do produto deve ser preenchido.", "error")
    if $('#select_new_product_unity :selected').val() == ""
      $.notify("A Unidade deve ser seleciona.", "error")
    else
      $.ajax
        url: LOCALHOST + 'resquests/create_product_through_ajax'
        type: 'get'
        dataType: 'JSON'
        data:
          produto:
            nome:       $("#txt_new_product_name").val()
            valor:      $("#txt_new_product_value").val()
            unidade:    $('#select_new_product_unity :selected').val()
            observacao: $('#select_new_product_observation').val()
        success: (response) ->
          if response.valid == true
            $.notify(response.message, "success")
            $("#hide_divproduct_field").slideUp( "slow" )
            $("#add_product_association_btn").fadeIn("slow")
            $("#txt_new_product_name").val("")
            $("#txt_new_product_value").val("")
            $('#select_new_product_unity :nth-child(1)').prop('selected', true)
            $("#txt_new_product_observation").val("")
            $('#cliente_fornecedor_name_products').populate(response.value, response.name)
            $('#cliente_fornecedor_name_products').selectpicker('refresh')
          if response.valid == false
            $.notify(response.message, "error")

  $("#select_product_association_btn").click (event) ->
    event.preventDefault()
    product_id = $('#cliente_fornecedor_name_products :selected').val()
    if product_id == ""
      bootbox.alert("<i style='color: rgb(208, 39, 39)'><b>Selecione</b> um Produto</i>")
    else
      _products_.push
        'product_id' : product_id
      generate_table("Produto", product_id)

  # => Retorna todos os produtos vinculados ao cliente_fornecedor
  if $('.edit_cliente_fornecedor').length
    cliente_fornecedor_id = $("#cliente_fornecedor_id").val()
    $.ajax
      url: LOCALHOST + "resquests/collect_fornecedor_products"
      type: 'GET'
      dataType: 'JSON'
      data:
        id: cliente_fornecedor_id
      success: (response) ->
        i=0
        while i <= response.length
          if response[i] != undefined
            $("#produtos_content").append(
              "<tr id='tr_fornecedor_service_id_#{response[i].id}'>" +
                "<td>#{response[i].name}</td>" +
                "<td>#{response[i].status}</td>" +
                "<td class='center'>" +
                  "<a href='#' id='btn_remove_product' title='Remover Produto' data-fornecedor-id='#{response[i].fornecedor_id}' data-fornecedor-product-id='#{response[i].id}'>" +
                    "<i class='fa fa-trash' aria-hidden='true' style='color: #AF2222; font-size: 16pt'></i>" +
                  "</a>" +
                "</td>" +
              "</tr>")
          i++

  # => Deleta produtos não salvo
  $(document).on "click", "#delete_preview_produtos_data", (event) ->
    event.preventDefault()
    data_product_id = $(this).data().id
    $("#tr_produtos_id_#{data_product_id}").remove()
    index = _products_.indexOf(data_product_id)
    _products_.splice(index, data_product_id)

  $(document).on "click", "#btn_remove_product", (event) ->
    event.preventDefault()
    fornecedorProductId = $(this).data().fornecedorProductId
    fornecedorId = $(this).data().fornecedorId
    bootbox.dialog
      message: "Deseja mesmo remover este item?"
      class: 'class-confirm-box'
      className: "my-modal"
      buttons:
        success:
          label: "SIM!"
          className: "btn-success pull-left"
          callback: ->
            $.ajax
              url: LOCALHOST + "resquests/find_fornecedor_product_and_destroy"
              type: 'GET'
              dataType: 'JSON'
              data:
                id: fornecedorId
                fornecedor_product_id: fornecedorProductId
              success: (response) ->
                $("#tr_fornecedor_service_id_#{fornecedorProductId}").fadeOut(500)
                $.notify(response.message, "success")
        chickenout:
          label: "NÃO"
          className: "btn-danger pull-right"

# ----------------------------- END ------------------------------------
