#= require brant/clientes_&_fornecedores/_init_
#= require brant/clientes_&_fornecedores/_page_load
#= require brant/clientes_&_fornecedores/tipos/_cliente
#= require brant/clientes_&_fornecedores/tipos/_fornecedor
#= require brant/clientes_&_fornecedores/tipos/_investidor


# ----------------------------------------- START ------------------------------------
jQuery ->
  #=> Verificando Nome
  $("#cliente_fornecedor_nome").blur ->
    $("ol.required_fields_messages").empty()
    if $("#cliente_fornecedor_nome").val() != ""
      _uncompleted_fields_messages_['nome'] = ""
      $("ol.required_fields_messages").append(uncompleted_fields_display_messages(_uncompleted_fields_messages_))
    else
      $("#cliente_fornecedor_nome").addClass('required_field')
      _uncompleted_fields_messages_['nome'] = "Nome - Deve ser preenchido"
      $("ol.required_fields_messages").append(uncompleted_fields_display_messages(_uncompleted_fields_messages_))

  # => Verificando Razao Social
  $("#cliente_fornecedor_razao_social").blur ->
    $("ol.required_fields_messages").empty()
    if $("#cliente_fornecedor_razao_social").val() != "" || $("#cliente_fornecedor_razao_social").val() != "-"
      _uncompleted_fields_messages_['razao_social'] = ""
      $("ol.required_fields_messages").append(uncompleted_fields_display_messages(_uncompleted_fields_messages_))
    if $("#cliente_fornecedor_razao_social").val() == "" || $("#cliente_fornecedor_razao_social").val() == "-"
      _uncompleted_fields_messages_['razao_social'] = "Razão Social - Deve ser preenchido"
      $("ol.required_fields_messages").append(uncompleted_fields_display_messages(_uncompleted_fields_messages_))

  # => Verificando Inscrição Estadual
  $("#cliente_fornecedor_inscricao_estadual").change ->
    $("ol.required_fields_messages").empty()
    if $("#cliente_fornecedor_inscricao_estadual").val() != "" || $("#cliente_fornecedor_inscricao_estadual").val() != "-"
      _uncompleted_fields_messages_['inscricao_estadual'] = ""
    if $("#cliente_fornecedor_inscricao_estadual").val() == "" || $("#cliente_fornecedor_inscricao_estadual").val() == "-"
      _uncompleted_fields_messages_['inscricao_estadual'] = "Inscrição Estadual - Deve ser preenchido"
    $("ol.required_fields_messages").append(uncompleted_fields_display_messages(_uncompleted_fields_messages_))

  # => Verificando Razao CNPJ
  $("#cliente_fornecedor_cnpj").change ->
    $("ol.required_fields_messages").empty()
    if $("#cliente_fornecedor_cnpj").val() != "" || $("#cliente_fornecedor_cnpj").val() != "-"
      _uncompleted_fields_messages_['cnpj'] = ""
    if $("#cliente_fornecedor_cnpj").val() == "" || $("#cliente_fornecedor_cnpj").val() == "-"
      _uncompleted_fields_messages_['cnpj'] = "CNPJ - Deve ser preenchido"
    $("ol.required_fields_messages").append(uncompleted_fields_display_messages(_uncompleted_fields_messages_))

  # => Verificando Razao RG
  $("#cliente_fornecedor_rg").change ->
    $("ol.required_fields_messages").empty()
    if $("#cliente_fornecedor_rg").val() != ""
      _uncompleted_fields_messages_['rg'] = ""
      $("ol.required_fields_messages").append(uncompleted_fields_display_messages(_uncompleted_fields_messages_))

  # => Verificando Razao CPF
  $("#cliente_fornecedor_cpf").change ->
    $("ol.required_fields_messages").empty()
    if $("#cliente_fornecedor_cpf").val() != ""
      _uncompleted_fields_messages_['cpf'] = ""
      $("ol.required_fields_messages").append(uncompleted_fields_display_messages(_uncompleted_fields_messages_))

  $('.edit_cliente_fornecedor').submit (event) ->
    event.preventDefault()
    cliente_fornecedor_id = $("#cliente_fornecedor_id").val()
    $.ajax
      url: LOCALHOST + "cliente_fornecedores/#{cliente_fornecedor_id}"
      type: 'PUT'
      dataType: 'JSON'
      data:
        cliente_fornecedor:
          id: cliente_fornecedor_id
          status: $("#status").find( "input:checked" ).val()
          tipo_cliente: $("#cliente_fornecedor_tipo_cliente :selected").val()
          tipo_fornecedor: $("#cliente_fornecedor_tipo_fornecedor :selected").val()
          tipo_investidor: $("#cliente_fornecedor_tipo_investidor :selected").val()
          tipo_pessoa: $("#cliente_fornecedor_tipo_pessoa :selected").val()
          cliente_fornecedor: $("#cliente_fornecedor_cliente_fornecedor :selected").val()
          nome: $("#cliente_fornecedor_nome").val()
          razao_social: $("#cliente_fornecedor_razao_social").val()
          inscricao_estadual: $("#cliente_fornecedor_inscricao_estadual").val()
          cpf:  $("#cliente_fornecedor_cpf").val()
          cnpj: $("#cliente_fornecedor_cnpj").val()
          rg:   $("#cliente_fornecedor_rg").val()
          sexo: $("#cliente_fornecedor_sexo").val()
          endereco: $("#cliente_fornecedor_endereco").val()
          numero: $("#cliente_fornecedor_numero").val()
          complemento: $("#cliente_fornecedor_complemento").val()
          cep: $("#cliente_fornecedor_cep").val()
          bairro: $("#cliente_fornecedor_bairro").val()
          observacao: $("#cliente_fornecedor_observacao").val()
          cidade_id: $("#cliente_fornecedor_cidade_id :selected").val()
          estado_id: $("#cliente_fornecedor_estado_id :selected").val()
          contato1_nome: $("#cliente_fornecedor_contato1_nome").val()
          contato1_ddd:  $("#cliente_fornecedor_contato1_ddd").val()
          contato1_telefone: $("#cliente_fornecedor_contato1_telefone").val()
          contato1_tipo_telefone: $("#cliente_fornecedor_contato1_tipo_telefone").val()
          contato1_email1: $("#cliente_fornecedor_contato1_email1").val()
          contato1_email2: $("#cliente_fornecedor_contato1_email2").val()
          contato2_nome:   $("#cliente_fornecedor_contato2_nome").val()
          contato2_ddd:    $("#cliente_fornecedor_contato2_ddd").val()
          contato2_telefone: $("#cliente_fornecedor_contato2_telefone").val()
          contato2_tipo_telefone: $("#cliente_fornecedor_contato2_tipo_telefone").val()
          contato2_email1: $("#cliente_fornecedor_contato2_email1").val()
          contato2_email2: $("#cliente_fornecedor_contato2_email2").val()
          contato3_nome: $("#cliente_fornecedor_contato3_nome").val()
          contato3_ddd: $("#cliente_fornecedor_contato3_ddd").val()
          contato3_telefone: $("#cliente_fornecedor_contato3_telefone").val()
          contato3_tipo_telefone: $("#cliente_fornecedor_contato3_tipo_telefone").val()
          contato3_email1: $("#cliente_fornecedor_contato3_email1").val()
          contato3_email2: $("#cliente_fornecedor_contato3_email2").val()
        fornecedor_servicos: _services_
        fornecedor_produtos: _products_
        cliente_obras: _works_
        contas_bancaria: _bank_accounts_
      success: (response) ->
        if response.valid==false
          $(".required_content_box").fadeIn("500")
          $("ol.required_fields_messages").empty()
          $("ol.required_fields_messages").append(collect_errors_messages(response))
          body = $('html, body')
          body.stop().animate { scrollTop: 0 }, 500, 'swing', ->
            return
        else
          setTimeout (->
            $.notify(response.notify_message, "success")
          ), 2000
          setTimeout (->
            window.location.href=response.url
          ), 5000
# ----------------------------- END ------------------------------------
