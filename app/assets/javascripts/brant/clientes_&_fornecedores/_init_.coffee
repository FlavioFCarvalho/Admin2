#= require jquery/jquery-3.2.1.min
#= require jquery/jquery-ui
#= require jquery_ujs
#= require notifyjs_rails

$(document).ready ->
  # => VARIAVEIS DISPONIVEIS EM TODO O FORMULARIO
  window.LOCALHOST          =window.location.origin + '/'
  window._services_         =[]
  window._products_         =[]
  window._works_            =[]
  window._bank_accounts_    =[]
  window._tipo_pessoa_      = ""
  window._uncompleted_fields = false
  window._uncompleted_fields_messages_ ={ nome: '', rg: '', cpf: '', cnpj: '', razao_social: '', inscricao_estadual: '', telefone: '', aba_cliente_obra: '', aba_investidor_obra: '', aba_servico: '', aba_produto: '', tipo_cliente: '', tipo_fornecedor: '', tipo_investidor: ''}

  # => Função para popular componentes do tipo select
  $.fn.populate = (value, name) ->
    $(this).append("<option value='#{value}'>#{name}</option>")
    return

  # => Caso esteja disabled evita a entrada automatica na tab {ABA}
  $('li').click (event) ->
    if $(this).hasClass('disabled')
      return false
    return

  # => Formata as mensagens de erro
  window.collect_errors_messages=(resource) ->
    i=0
    _error_messages_=[]
    while i <= resource.messages.length
      if resource.messages[i]!= undefined
        _error_messages_.push("<li style='color: rgb(140, 18, 15); font-weight: bolder;'>#{resource.messages[i]}</li>")
      i++
    return _error_messages_

  # => gerador de tabela
  window.generate_table=(resource_name, resource_id) ->
    $.ajax
      url: LOCALHOST + 'resquests/find_resource'
      type: 'GET'
      dataType: 'JSON'
      data:
        resource:
          name: resource_name
          id: resource_id
      success: (response) ->
        $("##{response.class_name}_content").append(
          "<tr id='tr_#{response.class_name}_id_#{response.id}'>" +
            "<td>#{response.name}</td>" +
            "<td>#{response.status.label}</td>" +
            "<td><center><a href='#' id='delete_preview_#{response.class_name}_data' data-id='#{response.id}'><i class='fa fa-times-circle fa-2x'></i></a></center></td>" +
          "</tr>")

  # => gerador de tabela
  window.generate_table_obras=(resource_name, resource_id) ->
    $.ajax
      url: LOCALHOST + 'resquests/find_resource'
      type: 'GET'
      dataType: 'JSON'
      data:
        resource:
          name: resource_name
          id: resource_id
      success: (response) ->
        console.log response
        $("##{response.class_name}_content").append(
          "<tr id='tr_#{response.class_name}_id_#{response.id}'>" +
            "<td>#{response.codigo}</td>" +
            "<td>#{response.nome_local}</td>" +
            "<td>#{response.status.label}</td>" +
            "<td><center><a href='#' id='delete_preview_#{response.class_name}_data' data-id='#{response.id}'><i class='fa fa-times-circle fa-2x'></i></a></center></td>" +
          "</tr>")

  $.fn.dialog_confirm = (message, callback) ->
    options = message: message
    options.buttons =
      cancel:
        label: 'No'
        className: 'btn-default'
        callback: (result) ->
          callback false
          return
      main:
        label: 'Yes'
        className: 'btn-primary'
        callback: (result) ->
          callback true
          return
    bootbox.dialog options
    return

  window.uncompleted_fields_display_messages = (source) ->
    display = ""
    # => Verificando campos
    if source['nome'] != ''
      display = "<li style='color: #850C0C; font-weight: bolder'>#{source['nome']}</li>"
    if source['rg'] != ''
      display = "<li style='color: #850C0C; font-weight: bolder'>#{source['rg']}</li>"
    if source['cpf'] != ''
      display = display.concat("<li style='color: #850C0C; font-weight: bolder'>#{source['cpf']}</li>")
    if source['cnpj'] != ''
      display = display.concat("<li style='color: #850C0C; font-weight: bolder'>#{source['cnpj']}</li>")
    if source['razao_social'] != ''
      display = display.concat("<li style='color: #850C0C; font-weight: bolder'>#{source['razao_social']}</li>")
    if source['inscricao_estadual'] != ''
      display = display.concat("<li style='color: #850C0C; font-weight: bolder'>#{source['inscricao_estadual']}</li>")

    # => Tipo Cliente
    if source['tipo_cliente'] != '' && source['tipo_cliente'] != 'NAO' && source['tipo_cliente'] != 'SIM' && source['tipo_cliente'] != 'Tipo Cliente - selecionado'
      display = display.concat("<li style='color: #850C0C; font-weight: bolder'>#{source['tipo_cliente']}</li>")
    if source['tipo_cliente'] == 'SIM' || source['tipo_cliente'] == 'Tipo Cliente - selecionado'
      source['tipo_cliente'] = 'Tipo Cliente - selecionado'
      display = display.concat("<li style='color: #560000; font-weight: bolder'>#{source['tipo_cliente']}</li>")
      if source['aba_cliente_obra'] != ''
        display = display.concat("<ul><li style='color: #991919; font-weight: bolder'>#{source['aba_cliente_obra']}</li></ul>")

    # => Tipo Fornecedor
    if source['tipo_fornecedor'] != '' && (source['tipo_fornecedor'] != 'NAO' && source['tipo_fornecedor'] != 'SIM' && source['tipo_fornecedor'] != 'Tipo Fornecedor - selecionado')
      display = display.concat("<li style='color: #991919; font-weight: bolder'>#{source['tipo_fornecedor']}</li>")
    if source['tipo_fornecedor'] == 'SIM' || source['tipo_fornecedor'] == 'Tipo Fornecedor - selecionado'
      source['tipo_fornecedor'] = 'Tipo Fornecedor - selecionado'
      display = display.concat("<li style='color: #560000; font-weight: bolder'>#{source['tipo_fornecedor']}</li>")
      if source['aba_servico'] != ''
        display = display.concat("<ul><li style='color: #991919; font-weight: bolder'>#{source['aba_servico']}</li></ul>")
      if source['aba_produto'] != ''
        display = display.concat("<ul><li style='color: #991919; font-weight: bolder'>#{source['aba_produto']}</li></ul>")

    # => Tipo Investidor
    if source['tipo_investidor'] != '' && source['tipo_investidor'] != 'NAO' && source['tipo_investidor'] != 'SIM' && source['tipo_investidor'] != 'Tipo Investidor - selecionado'
      display = display.concat("<li style='color: #850C0C; font-weight: bolder'>#{source['tipo_investidor']}</li>")
    if source['tipo_investidor'] == 'SIM' || source['tipo_investidor'] == 'Tipo Investidor - selecionado'
      source['tipo_investidor'] = 'Tipo Investidor - selecionado'
      display = display.concat("<li style='color: #560000; font-weight: bolder'>#{source['tipo_investidor']}</li>")
      if source['aba_investidor_obra'] != ''
        display = display.concat("<ul><li style='color: #991919; font-weight: bolder'>#{source['aba_investidor_obra']}</li></ul>")

    return display
