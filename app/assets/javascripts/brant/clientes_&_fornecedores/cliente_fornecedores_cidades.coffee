#= require jquery/jquery-3.2.1.min

# ----------------------------- START ------------------------------------
jQuery ->
  cidades = undefined
  estado = undefined
  escaped_estados = undefined
  options = undefined

  if $('.simple_form.new_cliente_fornecedor#new_cliente_fornecedor').length
    if $('#cliente_fornecedor_estado_id').length
      $('#cliente_fornecedor_estado_id').val("19")

  $('#cliente_fornecedor_estado_id').change ->
    estado = $('#cliente_fornecedor_estado_id :selected').text()
    escaped_estados = estado.replace(/([ #;&,.+*~\':"!^$[\]()=>|\/@])/g, '\\$1')
    options = $(cidades).filter('optgroup[label=\'' + escaped_estados + '\']').html()
    if options
      $('#cliente_fornecedor_cidade_id').html options
      $('#cliente_fornecedor_cidade_id').prop 'disabled', false
      return
    else
      $('#cliente_fornecedor_cidade_id').empty()
      $('#cliente_fornecedor_cidade_id').prop 'disabled', true
      return

  cidades = $('#cliente_fornecedor_cidade_id').html()
  if $('#cliente_fornecedor_estado_id').val() != ""
    if $('#cliente_fornecedor_estado_id').val() == "19"
      $('#cliente_fornecedor_cidade_id').val("3633")
    else
      $('#cliente_fornecedor_cidade_id').prop 'disabled', false
      estado = $('#cliente_fornecedor_estado_id :selected').text()
      escaped_estados = estado.replace(/([ #;&,.+*~\':"!^$[\]()=>|\/@])/g, '\\$1')
      options = $(cidades).filter('optgroup[label=\'' + escaped_estados + '\']').html()
      $('#cliente_fornecedor_cidade_id').html options
  else
    $('#cliente_fornecedor_cidade_id').prop 'disabled', true
  return
# ----------------------------- END ------------------------------------
