#= require jquery/jquery-3.2.1.min

# ----------------------------- START ------------------------------------
jQuery ->
  $('.new_cliente_fornecedor, .edit_cliente_fornecedor').ready ->
    $('div#cliente_fornecedor_pessoa_fisica').fadeOut()
    if $('#cliente_fornecedor_tipo_pessoa').val() == 'Fisica'
      $('div#cliente_fornecedor_pessoa_juridica').hide()
      $('div#cliente_fornecedor_pessoa_fisica').slideDown 250
    else if $('#cliente_fornecedor_tipo_pessoa').val() == 'Juridica'
      $('div#cliente_fornecedor_pessoa_fisica').hide()
      $('div#cliente_fornecedor_pessoa_juridica').slideDown 250

  $('#cliente_fornecedor_tipo_pessoa').change ->
    if $('#cliente_fornecedor_tipo_pessoa option:selected').val() == 'Fisica'
      $('div#cliente_fornecedor_pessoa_fisica').fadeIn 250
      $('div#cliente_fornecedor_pessoa_juridica').hide()
    else if $('#cliente_fornecedor_tipo_pessoa option:selected').val() == 'Juridica'
      $('div#cliente_fornecedor_pessoa_juridica').fadeIn 250
      $('div#cliente_fornecedor_pessoa_fisica').hide()
# ----------------------------- END ------------------------------------
