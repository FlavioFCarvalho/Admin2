(($) ->
  $ ->
    PageLoad.init()
    return
  PageLoad =
    init: ->
      aba_para_tipo_cliente=(response) ->
        if response.cliente.errors_found == true
          $("ol.required_fields_messages").empty()
          $(".required_content_box").fadeIn("500")
          if response.cliente.work_tab == true and response.cliente.tipo_cliente_founded == true and response.cliente.tipo_cliente == "Sim"
            $('li.work').removeClass("disabled")
            $("li.work a").addClass("required_tab")
            _uncompleted_fields_messages_['tipo_cliente']= "SIM"
            _uncompleted_fields_messages_['aba_cliente_obra'] = "Aba Obra - Verifique se ao menos existe uma Obra Cadastrada"
        else
          if response.cliente.work_tab == true
            $('li.work').removeClass("disabled")
            $("li.work a").removeClass("required_tab")

      aba_para_tipo_fornecedor=(response) ->
        if response.fornecedor.errors_found == true
          $("ol.required_fields_messages").empty()
          $(".required_content_box").fadeIn("500")
          if response.fornecedor.uncompleted_tab == true and response.fornecedor.tipo_fornecedor_founded == true and response.fornecedor.service_tab == true and response.fornecedor.product_tab == true and response.fornecedor.tipo_fornecedor == "Sim"
            _uncompleted_fields_messages_['tipo_fornecedor']= "SIM"
            _uncompleted_fields_messages_['aba_servico'] = "Aba Serviço - Verifique se ao menos existe um Serviço Cadastrado"
            _uncompleted_fields_messages_['aba_produto'] = "Aba Produto - Verifique se ao menos existe um Produto Cadastrado"
            $('li.service').removeClass("disabled")
            $('li.product').removeClass("disabled")
            $("li.service a").addClass("required_tab")
            $("li.product a").addClass("required_tab")
        else
          if response.fornecedor.service_tab == true and response.fornecedor.product_tab == true
            $('li.service').removeClass("disabled")
            $('li.product').removeClass("disabled")
            $("li.service a").removeClass("required_tab")
            $("li.product a").removeClass("required_tab")

      aba_para_tipo_investidor=(response) ->
        console.log response
        if response.investidor.errors_found == true
          $("ol.required_fields_messages").empty()
          $(".required_content_box").fadeIn("500")
          if response.investidor.work_tab == true and response.investidor.tipo_investidor_founded == true and response.investidor.tipo_investidor == "Sim"
            $('li.work').removeClass("disabled")
            $("li.work a").addClass("required_tab")
            _uncompleted_fields_messages_['tipo_investidor']= "SIM"
            _uncompleted_fields_messages_['aba_investidor_obra'] = "Aba Obra - Deve haver ao menos uma Obra Cadastrada"
        else
          if response.investidor.work_tab == true
            $('li.work').removeClass("disabled")
            $("li.work a").removeClass("required_tab")

      logica_de_validacao_dos_demais_campos= (response)->
        if response.cliente.uncompleted_fields == true
          if response.cliente.tipo_cliente_founded == false
            _uncompleted_fields_messages_['tipo_cliente'] = "Tipo Cliente - Deve ser selecionado"
        if response.fornecedor.uncompleted_fields == true
          if response.fornecedor.tipo_fornecedor_founded == false
            _uncompleted_fields_messages_['tipo_fornecedor'] = "Tipo Fornecedor - Deve ser selecionado"
        if response.investidor.uncompleted_fields == true
          if response.investidor.tipo_investidor_founded == false
            _uncompleted_fields_messages_['tipo_investidor'] = "Tipo Investidor - Deve ser selecionado"
        if response.nome == '-' || response.nome == '' || response.nome == null
          _uncompleted_fields_messages_['nome'] = "Nome - Deve ser preenchido"

      # => Ajax Request para trazar informação do Objeto Requisitado
      $.ajax
        url: LOCALHOST + 'resquests/find_resource'
        type: 'GET'
        dataType: 'JSON'
        data:
          resource:
            name: "ClienteFornecedor"
            id: $("#cliente_fornecedor_id").val()
        success: (response) ->
          _uncompleted_fields = response.uncompleted_fields
          count = 0
          _tipo_pessoa_= response.tipo_pessoa

          # => LOGICA DE ABLITACAO DA ABA OBRA PARA TIPO CLIENTE
          aba_para_tipo_cliente(response)

          # => LOGICA DE ABLITACAO DA ABA SERVICOS E PRODUTOS PARA TIPO FORNECEDOR
          aba_para_tipo_fornecedor(response)

          # => LOGICA DE ABLITACAO DA ABA OBRA PARA TIPO INVESTIDOR
          aba_para_tipo_investidor(response)

          # => LOGICA DE VALIDACAO PARA DEMAIS CAMPOS DO FORMULARIO
          logica_de_validacao_dos_demais_campos(response)

          $("ol.required_fields_messages").append(uncompleted_fields_display_messages(_uncompleted_fields_messages_))
      return
  return
) jQuery
