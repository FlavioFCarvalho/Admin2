#= require brant/clientes_&_fornecedores/_init_

# ----------------------------- START ------------------------------------
jQuery ->
  $("#cliente_fornecedor_name_works").selectpicker ->
  # -----------------START CIDADE & ESTADOS ------------------------------
  options = undefined
  cidades = $('#select_work_cidade_id').html()
  if $('#new_cliente_fornecedor').length
    if $('#select_work_estado_id').length
      $('#select_work_estado_id').val("19")

  $('#select_work_estado_id').change ->
    estado = $('#select_work_estado_id :selected').text()
    escaped_estados = estado.replace(/([ #;&,.+*~\':"!^$[\]()=>|\/@])/g, '\\$1')
    options = $(cidades).filter('optgroup[label=\'' + escaped_estados + '\']').html()
    if options
      $('#select_work_cidade_id').html options
      $('#select_work_cidade_id').prop 'disabled', false
    else
      $('#select_work_cidade_id').empty()
      $('#select_work_cidade_id').prop 'disabled', true

  if $('#select_work_estado_id').val() != ""
    if $('#select_work_estado_id').val() == "19"
      $('#select_work_cidade_id').val("3633")
    else
      $('#select_work_cidade_id').prop 'disabled', false
      estado = $('#select_work_estado_id :selected').text()
      escaped_estados = estado.replace(/([ #;&,.+*~\':"!^$[\]()=>|\/@])/g, '\\$1')
      options = $(cidades).filter('optgroup[label=\'' + escaped_estados + '\']').html()
      $('#select_work_cidade_id').html options
  else
    $('#select_work_cidade_id').prop 'disabled', true

  # -----------------END CIDADE & ESTADOS ---------------------------------

  if $("#cliente_fornecedor_cliente_fornecedor :selected").val() == "Cliente"
    $('li.work').removeClass("disabled")

  # => Faz Surgir os campos para criar um novo produto
  $("#add_work_association_btn").click (event) ->
    event.preventDefault()
    $(this).fadeOut("slow")
    $("#hide_divwork_field").slideDown( "slow" )

  $("#delete_blk_new_work").click (event) ->
    event.preventDefault()
    $("#add_work_association_btn").fadeIn("slow")
    $("#hide_divwork_field").slideUp( "slow" )
    $("#txt_new_work_codigo").val("")
    $("#txt_new_work_name").val("")
    $("#txt_new_work_local_address").val("")
    $("#txt_new_work_bairro").val("")
    $("#txt_new_work_aprovacao").val("")
    $("#txt_new_work_cep").val("")
    $('#select_work_estado_id :nth-child(1)').prop('selected', true)
    $('#select_work_cidade_id :nth-child(1)').prop('selected', true)
    $("#txt_new_work_address").val("")
    $("#txt_new_work_quadra").val("")
    $("#txt_new_work_lote").val("")
    $("#txt_new_work_CEI").val("")
    $("#txt_new_work_peticao").val("")

  # => Envia dados para o controller de Requests para criar um nova Obra
  $("#create_new_work").click (event) ->
    event.preventDefault()
    error_messages=[]
    if $("#txt_new_work_codigo").val() == ""
      error_messages.push "<li>O código da obra deve ser preenchido.</li>"
    if $("#txt_new_work_name").val() == ""
      error_messages.push "<li>O nome da obra deve ser preenchido.</li>"
    if $("#txt_new_work_local_address").val() == ""
      error_messages.push "<li>O endereço do local deve ser preenchido.</li>"
    if $("#txt_new_work_bairro").val() == ""
      error_messages.push "<li>O bairro deve ser preenchido.</li>"
    if $("#txt_new_work_cep").val() == ""
      error_messages.push "<li>O Cep deve ser preenchido.</li>"
    if $('#select_work_estado_id :selected').val() == ""
      error_messages.push "<li>O estado deve ser selecionado.</li>"
    if $('#select_work_cidade_id :selected').val() == ""
      error_messages.push "<li>A cidade deve ser selecionada.</li>"
    if $("#txt_new_work_address").val() == ""
      error_messages.push "<li>O endereço deve ser preenchido.</li>"
    if $("#txt_new_work_quadra").val() == ""
      error_messages.push "<li>A quadra deve ser preenchido.</li>"
    if $("#txt_new_work_lote").val() == ""
      error_messages.push "<li>O lote deve ser preenchido.</li>"
    # if $("#txt_new_work_CEI").val() == ""
    #   error_messages.push "<li>O CEI deve ser preenchido.</li>"
    # if $("#txt_new_work_peticao").val() == ""
    #   error_messages.push "<li>A petição deve ser preenchido.</li>"
    if error_messages.length !=0
      BootstrapDialog.show
        type: BootstrapDialog.TYPE_DEFAULT
        title: 'Erros Encontrados: Para prosseguir resolva os seguintes problemas'
        message: error_messages
        closable: false
        buttons: [ {
          label: 'Fechar'
          action: (dialogRef) ->
            dialogRef.close()
            error_messages = []
            false
        } ]
    else
      $.ajax
        url: LOCALHOST + 'resquests/create_work_through_ajax'
        type: 'get'
        dataType: 'JSON'
        data:
          obra:
            codigo: $("#txt_new_work_codigo").val()
            nome_local: $("#txt_new_work_name").val()
            endereco_local: $("#txt_new_work_local_address").val()
            bairro: $("#txt_new_work_bairro").val()
            cep: $("#txt_new_work_cep").val()
            cidade_id: $('#select_work_cidade_id :selected').val()
            estado_id: $('#select_work_estado_id :selected').val()
            endereco_obra: $("#txt_new_work_address").val()
            quadra: $("#txt_new_work_quadra").val()
            lote: $("#txt_new_work_lote").val()
            cei: $("#txt_new_work_CEI").val()
            peticao: $("#txt_new_work_peticao").val()
            aprovacao: $("#txt_new_work_aprovacao").val()
        success: (response) ->
          if response.valid == true
            $.notify(response.message, "success")
            $("#hide_divwork_field").slideUp( "slow" )
            $("#add_work_association_btn").fadeIn("slow")
            $("#txt_new_work_codigo").val("")
            $("#txt_new_work_name").val("")
            $("#txt_new_work_local_address").val("")
            $("#txt_new_work_bairro").val("")
            $("#txt_new_work_aprovacao").val("")
            $("#txt_new_work_cep").val("")
            $("#select_work_estado_id :nth-child(1)").prop('selected', true)
            $("#select_work_cidade_id :nth-child(1)").prop('selected', true)
            $("#txt_new_work_address").val("")
            $("#txt_new_work_quadra").val("")
            $("#txt_new_work_lote").val("")
            $("#txt_new_work_CEI").val("")
            $("#txt_new_work_peticao").val("")
            $("#cliente_fornecedor_name_works").populate(response.value, response.codigo)
            $('#cliente_fornecedor_name_works').selectpicker('refresh')
            $('#cliente_fornecedor_name_works').val(response.value)
          if response.valid == false
            $.notify(response.message, "error")

  $("#select_work_association_btn").click (event) ->
    event.preventDefault()
    obra_id = $('#cliente_fornecedor_name_works :selected').val()
    if obra_id == ""
      bootbox.alert("<i style='color: rgb(208, 39, 39)'><b>Selecione</b> uma Obra</i>")
    else
      _works_.push
        'obra_id' : obra_id
      generate_table_obras("Obra", obra_id)

  # => Deleta obra não salvo
  $(document).on "click", "#delete_preview_obras_data", (event) ->
    event.preventDefault()
    data_work_id = $(this).data().id
    $("#tr_obras_id_#{data_work_id}").remove()
    index = _works_.indexOf(data_work_id)
    _works_.splice(index, data_work_id)

  if $('.edit_cliente_fornecedor').length
    cliente_fornecedor_id = $("#cliente_fornecedor_id").val()
    $.ajax
      url: LOCALHOST + "resquests/collect_cliente_obras"
      type: 'GET'
      dataType: 'JSON'
      data:
        id: cliente_fornecedor_id
      success: (response) ->
        i=0
        while i <= response.length
          if response[i] != undefined
            $("#obras_content").append(
              "<tr id='tr_obras_id_#{response[i].id}'>" +
                "<td>#{response[i].codigo}</td>" +
                "<td>#{response[i].name}</td>" +
                "<td>#{response[i].status}</td>" +
                "<td class='center'>" +
                  "<a href='#' id='btn_remove_obra' title='Remover Obra' data-cliente-id='#{response[i].cliente_id}' data-cliente-obra-id='#{response[i].id}'>" +
                    "<i class='fa fa-trash' aria-hidden='true' style='color: #AF2222; font-size: 16pt'></i>" +
                  "</a>" +
                "</td>" +
              "</tr>")
          i++

  $(document).on "click", "#btn_remove_obra", (event) ->
    event.preventDefault()
    # myCustomConfirmBox "Deseja mesmo remover este item?", ->

    clienteObraId = $(this).data().clienteObraId
    clienteId = $(this).data().clienteId
    bootbox.dialog
      message: "Deseja mesmo remover este item?"
      class: 'class-confirm-box'
      className: "my-modal"
      buttons:
        success:
          label: "SIM!"
          className: "btn-success pull-left"
          callback: ->
            $.ajax
              url: LOCALHOST + "resquests/find_client_obra_and_destroy"
              type: 'GET'
              dataType: 'JSON'
              data:
                id: clienteId
                fornecedor_service_id: clienteObraId
              success: (response) ->
                $("#tr_obras_id_#{clienteObraId}").fadeOut(500)
                $.notify(response.message, "success")
        chickenout:
          label: "NÃO"
          className: "btn-danger pull-right"
# ----------------------------- END --------------------------------------
