#= require brant/clientes_&_fornecedores/_init_

# ----------------------------- START ------------------------------------
jQuery ->
  $('#new_cliente_fornecedor').submit (event) ->
    event.preventDefault()
    $.ajax
      url: LOCALHOST + 'cliente_fornecedores'
      type: 'POST'
      dataType: 'JSON'
      data:
        cliente_fornecedor:
          tipo_pessoa: $("#cliente_fornecedor_tipo_pessoa :selected").val()
          tipo_cliente: $("#cliente_fornecedor_tipo_cliente :selected").val()
          tipo_fornecedor: $("#cliente_fornecedor_tipo_fornecedor :selected").val()
          tipo_investidor: $("#cliente_fornecedor_tipo_investidor :selected").val()
          cliente_fornecedor: $("#cliente_fornecedor_cliente_fornecedor :selected").val()
          nome: $("#cliente_fornecedor_nome").val()
          razao_social: $("#cliente_fornecedor_razao_social").val()
          inscricao_estadual: $("#cliente_fornecedor_inscricao_estadual").val()
          cpf:  $("#cliente_fornecedor_cpf").val()
          cnpj: $("#cliente_fornecedor_cnpj").val()
          rg:   $("#cliente_fornecedor_rg").val()
          sexo: $("#cliente_fornecedor_sexo").val()
          endereco: $("#cliente_fornecedor_endereco").val()
          numero: $("#cliente_fornecedor_numero").val()
          complemento: $("#cliente_fornecedor_complemento").val()
          cep: $("#cliente_fornecedor_cep").val()
          bairro: $("#cliente_fornecedor_bairro").val()
          observacao: $("#cliente_fornecedor_observacao").val()
          cidade_id: $("#cliente_fornecedor_cidade_id :selected").val()
          estado_id: $("#cliente_fornecedor_estado_id :selected").val()
          contato1_nome: $("#cliente_fornecedor_contato1_nome").val()
          contato1_ddd:  $("#cliente_fornecedor_contato1_ddd").val()
          contato1_telefone: $("#cliente_fornecedor_contato1_telefone").val()
          contato1_tipo_telefone: $("#cliente_fornecedor_contato1_tipo_telefone").val()
          contato1_email1: $("#cliente_fornecedor_contato1_email1").val()
          contato1_email2: $("#cliente_fornecedor_contato1_email2").val()
          contato2_nome:   $("#cliente_fornecedor_contato2_nome").val()
          contato2_ddd:    $("#cliente_fornecedor_contato2_ddd").val()
          contato2_telefone: $("#cliente_fornecedor_contato2_telefone").val()
          contato2_tipo_telefone: $("#cliente_fornecedor_contato2_tipo_telefone").val()
          contato2_email1: $("#cliente_fornecedor_contato2_email1").val()
          contato2_email2: $("#cliente_fornecedor_contato2_email2").val()
          contato3_nome: $("#cliente_fornecedor_contato3_nome").val()
          contato3_ddd: $("#cliente_fornecedor_contato3_ddd").val()
          contato3_telefone: $("#cliente_fornecedor_contato3_telefone").val()
          contato3_tipo_telefone: $("#cliente_fornecedor_contato3_tipo_telefone").val()
          contato3_email1: $("#cliente_fornecedor_contato3_email1").val()
          contato3_email2: $("#cliente_fornecedor_contato3_email2").val()
        fornecedor_servicos: _services_
        fornecedor_produtos: _products_
        cliente_obras: _works_
        contas_bancaria: _bank_accounts_
      success: (response) ->
        if response.valid==false
          $(".required_content_box").fadeIn("500")
          $("ol.required_fields_messages").empty()
          $("ol.required_fields_messages").append(collect_errors_messages(response))
          body = $('html, body')
          body.stop().animate { scrollTop: 0 }, 500, 'swing', ->
            return
          # BootstrapDialog.show
          #   type: BootstrapDialog.TYPE_DEFAULT
          #   title: 'Erros Encontrados: Para prosseguir resolva os seguintes problemas'
          #   message: collect_errors_messages(response)
          #   closable: false
          #   buttons: [ {
          #     label: 'Fechar'
          #     action: (dialogRef) ->
          #       dialogRef.close()
          #       error_messages = []
          #       false
          #   } ]
        else
          setTimeout (->
            $.notify(response.notify_message, "success")
          ), 2000
          setTimeout (->
            window.location.href = LOCALHOST + 'cliente_fornecedores/new'
          ), 5000

  # => Verificando Tipo Cliente
  $("select#cliente_fornecedor_tipo_cliente").change ->
    $("ol.required_fields_messages").empty()
    if $(this).val() != ""
      if $("#cliente_fornecedor_tipo_cliente :selected").text() == "Sim"
        _uncompleted_fields_messages_['aba_obra'] = "Aba Obra - Deve haver ao menos uma Obra Cadastrada"
        $("li.work a").addClass("required_tab")
        $('li.work').removeClass("disabled")
        $("ol.required_fields_messages").append(uncompleted_fields_display_messages(_uncompleted_fields_messages_))
        if $("#cliente_fornecedor_tipo_fornecedor :selected").text() == "Sim"
          $('li.service').removeClass("disabled")
          $('li.product').removeClass("disabled")
        else
          $('li.service').addClass("disabled")
          $('li.product').addClass("disabled")
      if $("#cliente_fornecedor_tipo_cliente :selected").text() == "Não"
        _uncompleted_fields_messages_['tipo_cliente']= "NAO"
        $('li.work').addClass("disabled")
        if $("#cliente_fornecedor_tipo_fornecedor :selected").text() == "Sim"
          $('li.service').removeClass("disabled")
          $('li.product').removeClass("disabled")
        else
          $('li.service').addClass("disabled")
          $('li.product').addClass("disabled")
        $("ol.required_fields_messages").append(uncompleted_fields_display_messages(_uncompleted_fields_messages_))
        _uncompleted_fields_messages_['tipo_cliente'] = ""
        _uncompleted_fields_messages_['aba_obra']     = ""
    else
      $('li.work').addClass("disabled")
      _uncompleted_fields_messages_['tipo_cliente'] = "Tipo Cliente - Deve ser selecionado"
      $("ol.required_fields_messages").append(uncompleted_fields_display_messages(_uncompleted_fields_messages_))

  # => Verificando Tipo Fornecedor
  $("select#cliente_fornecedor_tipo_fornecedor").change ->
    $("ol.required_fields_messages").empty()
    if $(this).val() != ""
      if $("#cliente_fornecedor_tipo_fornecedor :selected").text() == "Sim"
        _uncompleted_fields_messages_['aba_servico'] = "Aba Serviços - Deve haver ao menos um Serviço Cadastrado"
        _uncompleted_fields_messages_['aba_produto'] = "Aba Produto - Deve haver ao menos uma Produto Cadastrada"
        $('li.service').removeClass("disabled")
        $('li.product').removeClass("disabled")
        $("li.service a").addClass("required_tab")
        $("li.product a").addClass("required_tab")
        $("ol.required_fields_messages").append(uncompleted_fields_display_messages(_uncompleted_fields_messages_))
        if $("#cliente_fornecedor_tipo_cliente :selected").text() == "Sim"
          $('li.work').removeClass("disabled")
      if $("#cliente_fornecedor_tipo_fornecedor :selected").text() == "Não"
        _uncompleted_fields_messages_['tipo_fornecedor']= "NAO"
        $('li.service').addClass("disabled")
        $('li.product').addClass("disabled")
        if $("#cliente_fornecedor_tipo_cliente :selected").text() == "Sim"
          $('li.work').removeClass("disabled")
        else
          $('li.work').addClass("disabled")
        $("ol.required_fields_messages").append(uncompleted_fields_display_messages(_uncompleted_fields_messages_))
        _uncompleted_fields_messages_['tipo_fornecedor'] = ""
        _uncompleted_fields_messages_['aba_servico'] = ""
        _uncompleted_fields_messages_['aba_produto'] = ""
    else
      $('li.service').addClass("disabled")
      $('li.product').addClass("disabled")
      _uncompleted_fields_messages_['tipo_fornecedor'] = "Tipo Fornecedor - Deve ser selecionado"
      _uncompleted_fields_messages_['aba_servico'] = "Aba Serviços - Deve haver ao menos um Serviço Cadastrado"
      _uncompleted_fields_messages_['aba_produto'] = "Aba Produtos - Deve haver ao menos um Produto Cadastrado"
      $("ol.required_fields_messages").append(uncompleted_fields_display_messages(_uncompleted_fields_messages_))

  # => Verificando Tipo Investidor
  $("select#cliente_fornecedor_tipo_investidor").change ->
    $("ol.required_fields_messages").empty()
    if $(this).val() != ""
      if $("#cliente_fornecedor_tipo_investidor :selected").text() == "Sim" ||$("#cliente_fornecedor_tipo_investidor :selected").text() == "Não"
        _uncompleted_fields_messages_['tipo_investidor']= ""
    else
      _uncompleted_fields_messages_['tipo_investidor']= "Tipo Investidor - Deve ser selecionado"
      $("ol.required_fields_messages").append(uncompleted_fields_display_messages(_uncompleted_fields_messages_))

# ----------------------------- END ------------------------------------
