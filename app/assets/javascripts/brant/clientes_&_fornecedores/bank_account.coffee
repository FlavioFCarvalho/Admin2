#= require brant/clientes_&_fornecedores/_init_
#= require bootbox
#= require global/delete
# ----------------------------- START ------------------------------------
jQuery ->
  number = 0
  editing = false
  conta_id =0

  $('#txt_new_bank_account_agency').val("")
  $("#select_new_bank_account_bank :nth-child(1)").prop('selected', true)
  $('#txt_new_bank_account_account').val("")
  $("#select_new_bank_account_tipo :nth-child(1)").prop('selected', true)

  $(document).on "click", "#add_new_bank", (event) ->
    event.preventDefault()
    $("#account_data").fadeOut(500)
    $("#new_bank").fadeIn(500)

  $(document).on "click", "#delete_txt_new_bank", (event) ->
    event.preventDefault()
    $("#new_bank").fadeOut(500)
    $("#account_data").fadeIn(500)
    $("#txt_new_bank").val("")

  # --------------------  Adicionar novo Banco ------------------------
  $(document).on "click", "#btn_save_new_bank", (event) ->
    event.preventDefault()
    if $("#txt_new_bank").val() == ""
      bootbox.alert("O nome do banco deve ser preenchido.")
    else
      $.ajax
        url: LOCALHOST + 'resquests/create_banco_through_ajax'
        type: 'get'
        dataType: 'JSON'
        data:
          banco:
            nome: $("#txt_new_bank").val()
        success: (response) ->
          if response.valid == true
            $.notify(response.message, "success")
            $("#new_bank").fadeOut(500)
            $("#account_data").fadeIn(500)
            $("#txt_new_bank").val("")
            $('#select_new_bank_account_bank').populate(response.value, response.name)
          if response.valid == false
            $.notify(response.message, "error")
  # ---------------------------------------------------------------------------------------
  #------------------------ Coletar Dados da nova Conta -----------------------------------
  $("#btn_create_new_bank_account").on 'click', (event) ->
      event.preventDefault()
      agencia = $('#txt_new_bank_account_agency').val()
      banco   = $('#select_new_bank_account_bank :selected')
      conta   = $('#txt_new_bank_account_account').val()
      tipo    = $('#select_new_bank_account_tipo :selected').val()
      id      = $("#hidden_id_account").val()

      if banco == ""
        bootbox.alert("<i style='color: rgb(208, 39, 39)'><b>Selecione</b> um banco</i>")
      else
        _bank_accounts_.push
          'agencia': agencia
          'banco_id':   banco.val()
          'conta':   conta
          'tipo':   tipo
          'banco_nome': banco.text()
          'id': id
        generate_account_table(agencia, banco.text(), conta, tipo)
        $('#txt_new_bank_account_agency').val("")
        $("#select_new_bank_account_bank :nth-child(1)").prop('selected', true)
        $('#txt_new_bank_account_account').val("")
        $("#select_new_bank_account_tipo :nth-child(1)").prop('selected', true)
        number += 1
  # ---------------------------------------------------------------------------------------
  generate_account_table=(agencia, banco, conta, tipo) ->
    $("#accounts_content").append(
      "<tr id='tr_account_#{number  }'>" +
        "<td>#{agencia}</td>" +
        "<td>#{banco}</td>" +
        "<td>#{conta}</td>" +
        "<td>#{tipo}</td>" +
        "<td><center><a href='#' id='delete_preview_account' data-number='#{number}'><i class='fa fa-times-circle fa-2x'></i></a></center></td>" +
      "</tr>")

  $(document).on "click", "#delete_preview_account", (event) ->
    event.preventDefault()
    datanumber = $(this).data().number
    _bank_accounts_.splice(datanumber, 1)
    $(this).closest("tr").fadeOut('fast')

  if $('.edit_cliente_fornecedor').length
    cliente_fornecedor_id = $("#cliente_fornecedor_id").val()
    $.ajax
      url: LOCALHOST + "resquests/collect_contas_bancarias"
      type: 'GET'
      dataType: 'JSON'
      data:
        id: cliente_fornecedor_id
      success: (response) ->
        i=0
        while i <= response.length
          if response[i] != undefined
            $("#accounts_content").append(
              "<tr>" +
                "<td>#{response[i].agencia}</td>" +
                "<td>#{response[i].banco_nome}</td>" +
                "<td>#{response[i].conta}</td>" +
                "<td>#{response[i].tipo}</td>" +
                "<td><center><a href='#' id='edit_account' data-account-id='#{response[i].id}'><i class='fa fa-pencil-square-o  fa-2x'></i></a></center></td>" +
                "<td><center><a href='#' id='inactvate_account' data-account-id='#{response[i].id}'><i class='fa fa-times-circle fa-2x'></i></a></center></td>" +
              "</tr>")
          i++

  $(document).on "click", "#edit_account", (event) ->
    event.preventDefault()
    link = $(this).data()
    $(this).closest("tr").fadeOut(500)
    $.ajax
      url: LOCALHOST + "resquests/find_resource"
      type: 'GET'
      dataType: 'JSON'
      data:
        resource:
          id: link.accountId
          name: "ContasBancaria"
      success: (response) ->
        editing = true
        $('#txt_new_bank_account_agency').val(response.agency)
        $("#select_new_bank_account_bank").val(response.bank_id)
        $('#txt_new_bank_account_account').val(response.account)
        $("#select_new_bank_account_tipo").val(response.account_type)
        $("#hidden_id_account").val(response.id)
# ----------------------------- END ------------------------------------
