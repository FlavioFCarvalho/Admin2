# => Verificando Tipo Cliente
$("select#cliente_fornecedor_tipo_cliente").change ->
  resposta =$("#cliente_fornecedor_tipo_cliente :selected").text()
  if resposta != ""
    if resposta == "Sim"
      $.ajax
        url: LOCALHOST + 'resquests/find_resource'
        type: 'GET'
        dataType: 'JSON'
        data:
          resource:
            name: "ClienteFornecedor"
            id: $("#cliente_fornecedor_id").val()
        success: (response) ->
          $("ol.required_fields_messages").empty()
          if resposta == "Sim"
            if response.cliente.tipo_cliente_founded == false and response.cliente.tipo_cliente == ""
              $(".required_content_box").fadeIn("500")
              _uncompleted_fields_messages_['tipo_cliente'] = "SIM"
              _uncompleted_fields_messages_['aba_cliente_obra'] = "Aba Obra - Verifique se ao menos existe uma Obra Cadastrada"
              $("ol.required_fields_messages").append(uncompleted_fields_display_messages(_uncompleted_fields_messages_))
              $('li.work').removeClass("disabled")
              $("li.work a").addClass("required_tab")
            if response.cliente.tipo_cliente_founded == true and response.cliente.tipo_cliente == "Sim"
              if response.cliente.uncompleted_tab == true and response.cliente.work_tab == true
                $(".required_content_box").fadeIn("500")
                _uncompleted_fields_messages_['tipo_cliente'] = "SIM"
                _uncompleted_fields_messages_['aba_cliente_obra'] = "Aba Obra - Verifique se ao menos existe uma Obra Cadastrada"
                $('li.work').removeClass("disabled")
                $("li.work a").addClass("required_tab")
                $("ol.required_fields_messages").append(uncompleted_fields_display_messages(_uncompleted_fields_messages_))
              else
                if response.cliente.work_tab == true and response.cliente.uncompleted_tab == false
                  _uncompleted_fields_messages_['tipo_cliente'] = ""
                  _uncompleted_fields_messages_['aba_cliente_obra'] = ""
                  $('li.work').removeClass("disabled")
                  $("ol.required_fields_messages").append(uncompleted_fields_display_messages(_uncompleted_fields_messages_))
            if response.cliente.tipo_cliente_founded == true and response.cliente.tipo_cliente == "Não"
              $('li.work').removeClass("disabled")
    if resposta == "Não"
      $(".required_content_box").fadeOut("500")
      $("ol.required_fields_messages").empty()
      _uncompleted_fields_messages_['tipo_cliente']= "NAO"
      $('li.work').addClass("disabled")
      $("ol.required_fields_messages").append(uncompleted_fields_display_messages(_uncompleted_fields_messages_))
      _uncompleted_fields_messages_['tipo_cliente'] = ""
      _uncompleted_fields_messages_['aba_cliente_obra'] = ""
      if $("#cliente_fornecedor_tipo_investidor :selected").text() == "Sim"
        $('li.work').removeClass("disabled")
    if resposta == "Selecione"
      $(".required_content_box").fadeIn("500")
      $("ol.required_fields_messages").empty()
      $('li.work').addClass("disabled")
      _uncompleted_fields_messages_['tipo_cliente'] = "Tipo Cliente - Deve ser selecionado"
      _uncompleted_fields_messages_['aba_cliente_obra'] = "Aba Obras - Verifique se ao menos existe uma Obra Cadastrada"
      $("ol.required_fields_messages").append(uncompleted_fields_display_messages(_uncompleted_fields_messages_))
      if $("#cliente_fornecedor_tipo_investidor :selected").text() == "Sim"
        $('li.work').removeClass("disabled")
