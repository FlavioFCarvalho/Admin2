# => Verificando Tipo Cliente
$("select#cliente_fornecedor_tipo_investidor").change ->
  resposta =$("#cliente_fornecedor_tipo_investidor :selected").text()
  if resposta != ""
    if resposta == "Sim"
      $.ajax
        url: LOCALHOST + 'resquests/find_resource'
        type: 'GET'
        dataType: 'JSON'
        data:
          resource:
            name: "ClienteFornecedor"
            id: $("#cliente_fornecedor_id").val()
        success: (response) ->
          $("ol.required_fields_messages").empty()
          if resposta == "Sim"
            if response.investidor.tipo_investidor_founded == true and response.investidor.tipo_investidor == "Não"
              _uncompleted_fields_messages_['tipo_investidor'] = "SIM"
              _uncompleted_fields_messages_['aba_investidor_obra'] = "Aba Obra - Verifique se ao menos existe uma Obra Cadastrada"
            if response.investidor.tipo_investidor_founded == false and response.investidor.tipo_investidor == ""
              $(".required_content_box").fadeIn("500")
              _uncompleted_fields_messages_['tipo_investidor'] = "SIM"
              _uncompleted_fields_messages_['aba_investidor_obra'] = "Aba Obra - Verifique se ao menos existe uma Obra Cadastrada"
              $('li.work').removeClass("disabled")
              $("li.work a").addClass("required_tab")
            if response.investidor.tipo_investidor_founded == true and response.investidor.tipo_investidor == "Sim"
              if response.investidor.uncompleted_tab == true and response.investidor.work_tab == true
                $(".required_content_box").fadeIn("500")
                _uncompleted_fields_messages_['tipo_investidor'] = "SIM"
                _uncompleted_fields_messages_['aba_investidor_obra'] = "Aba Obra - Verifique se ao menos existe uma Obra Cadastrada"
                $('li.work').removeClass("disabled")
                $("li.work a").addClass("required_tab")
              else
                if response.investidor.work_tab == true and response.investidor.uncompleted_tab == false
                  _uncompleted_fields_messages_['tipo_investidor'] = ""
                  _uncompleted_fields_messages_['aba_investidor_obra'] = ""
                  $('li.work').removeClass("disabled")
            $("ol.required_fields_messages").append(uncompleted_fields_display_messages(_uncompleted_fields_messages_))
    if resposta == "Não"
      $("ol.required_fields_messages").empty()
      _uncompleted_fields_messages_['tipo_investidor']= "NAO"
      $('li.work').addClass("disabled")
      $("ol.required_fields_messages").append(uncompleted_fields_display_messages(_uncompleted_fields_messages_))
      _uncompleted_fields_messages_['tipo_investidor'] = ""
      _uncompleted_fields_messages_['aba_investidor_obra'] = ""
      if $("#cliente_fornecedor_tipo_cliente :selected").text() == "Sim"
        $('li.work').removeClass("disabled")
    if resposta == "Selecione"
      $(".required_content_box").fadeIn("500")
      $("ol.required_fields_messages").empty()
      $('li.work').addClass("disabled")
      _uncompleted_fields_messages_['tipo_investidor'] = "Tipo Investidor - Deve ser selecionado"
      _uncompleted_fields_messages_['aba_investidor_obra'] = "Aba Obras - Verifique se ao menos existe uma Obra Cadastrada"
      $("ol.required_fields_messages").append(uncompleted_fields_display_messages(_uncompleted_fields_messages_))
      if $("#cliente_fornecedor_tipo_cliente :selected").text() == "Sim"
        $('li.work').removeClass("disabled")
