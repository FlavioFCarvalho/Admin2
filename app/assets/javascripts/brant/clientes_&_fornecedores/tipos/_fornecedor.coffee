valida_tipo_nao_encontrado= ->
  # _uncompleted_fields_messages_['aba_servico'] = "Aba Serviços - Deve haver ao menos um Serviço Cadastrado"
  # _uncompleted_fields_messages_['aba_produto'] = "Aba Produto - Deve haver ao menos uma Produto Cadastrado"
  # _uncompleted_fields_messages_['tipo_fornecedor']= "SIM"
  $('li.service').removeClass("disabled")
  $('li.product').removeClass("disabled")
  $("li.service a").addClass("required_tab")
  $("li.product a").addClass("required_tab")

# => Verificando Tipo Fornecedor
$("select#cliente_fornecedor_tipo_fornecedor").change ->
  resposta =$("#cliente_fornecedor_tipo_fornecedor :selected").text()
  if resposta != ""
    if resposta == "Sim"
      _uncompleted_fields_messages_['tipo_fornecedor'] = ""
      $.ajax
        url: LOCALHOST + 'resquests/find_resource'
        type: 'GET'
        dataType: 'JSON'
        data:
          resource:
            name: "ClienteFornecedor"
            id: $("#cliente_fornecedor_id").val()
        success: (response) ->
          $("ol.required_fields_messages").empty()
          if resposta == "Sim"
            if response.fornecedor.tipo_fornecedor_founded == true and response.fornecedor.tipo_fornecedor == "Não"
              # $(".required_content_box").fadeIn("500")
              # _uncompleted_fields_messages_['tipo_fornecedor'] = "SIM"
              # _uncompleted_fields_messages_['aba_servico'] = "Aba Serviços - Deve haver ao menos um Serviço Cadastrado"
              # _uncompleted_fields_messages_['aba_produto'] = "Aba Produto - Deve haver ao menos uma Produto Cadastrado"
              $('li.service').removeClass("disabled")
              $('li.product').removeClass("disabled")
              $("li.service a").addClass("required_tab")
              $("li.product a").addClass("required_tab")
            if response.fornecedor.tipo_fornecedor_founded == false
              # $(".required_content_box").fadeIn("500")
              valida_tipo_nao_encontrado()
            if response.fornecedor.tipo_fornecedor_founded == true
              if response.fornecedor.tipo_fornecedor == "Sim" and response.fornecedor.uncompleted_tab == true
                # $(".required_content_box").fadeIn("500")
                if response.fornecedor.service_tab == true and response.fornecedor.product_tab == true
                  valida_tipo_nao_encontrado()
              if response.fornecedor.tipo_fornecedor == "Sim" and response.fornecedor.uncompleted_tab == false
                # _uncompleted_fields_messages_['tipo_fornecedor']= ""
                # _uncompleted_fields_messages_['aba_servico']    = ""
                # _uncompleted_fields_messages_['aba_produto']    = ""
                $('li.service').removeClass("disabled")
                $('li.product').removeClass("disabled")
          $("ol.required_fields_messages").append(uncompleted_fields_display_messages(_uncompleted_fields_messages_))
    if resposta == "Não"
      $("ol.required_fields_messages").empty()
      # _uncompleted_fields_messages_['tipo_fornecedor']= "NAO"
      $('li.service').addClass("disabled")
      $('li.product').addClass("disabled")
      $("ol.required_fields_messages").append(uncompleted_fields_display_messages(_uncompleted_fields_messages_))
      # _uncompleted_fields_messages_['tipo_fornecedor'] = ""
      # _uncompleted_fields_messages_['aba_servico']     = ""
      # _uncompleted_fields_messages_['aba_produto']     = ""
  if resposta == "Selecione"
    $(".required_content_box").fadeIn("500")
    $("ol.required_fields_messages").empty()
    $('li.service').addClass("disabled")
    $('li.product').addClass("disabled")
    _uncompleted_fields_messages_['tipo_fornecedor']= "Tipo Fornecedor - Deve ser selecionado."
    # _uncompleted_fields_messages_['aba_servico']     = ""
    # _uncompleted_fields_messages_['aba_produto']     = ""
    $("ol.required_fields_messages").append(uncompleted_fields_display_messages(_uncompleted_fields_messages_))
