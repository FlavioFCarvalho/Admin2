#= require brant/clientes_&_fornecedores/_init_
#= require bootbox
#= require global/delete
# ----------------------------- START ------------------------------------
jQuery ->
  $("#cliente_fornecedor_name_services").selectpicker ->

  # => Em Caso de já vir selecionado Fornecedor
  # if $("#cliente_fornecedor_cliente_fornecedor :selected").val() == "Fornecedor"
  #   $('li.service').removeClass("disabled")
  #   $('li.product').removeClass("disabled")

  # => Abre bloco para criar um novo servico
  $("#add_service_association_btn").click (event) ->
    event.preventDefault()
    $(this).fadeOut("slow")
    $("#hide_div_field").slideDown( "slow" )

  # => Esconde bloco para criar um novo servico
  $("#delele_txt_new_service").click (event) ->
    event.preventDefault()
    $("#add_service_association_btn").fadeIn("slow")
    $("#hide_div_field").slideUp( "slow" )
    $("#txt_new_service_name").val("")

  # => Envia dados do novo servico para o controller
  $("#create_new_service").click (event) ->
    event.preventDefault()
    if $("#txt_new_service_name").val() == ""
      bootbox.alert("O nome do serviço deve ser preenchido.")
    else
      $.ajax
        url: LOCALHOST + 'resquests/create_service_through_ajax'
        type: 'get'
        dataType: 'JSON'
        data:
          service:
            name: $("#txt_new_service_name").val()
        success: (response) ->
          if response.valid == true
            $.notify(response.message, "success")
            $("#hide_div_field").slideUp( "slow" )
            $("#txt_new_service_name").val("")
            $("#add_service_association_btn").fadeIn("slow")
            $('#cliente_fornecedor_name_services').populate(response.value, response.name)
            $('#cliente_fornecedor_name_services').selectpicker('refresh')
          if response.valid == false
            $.notify(response.message, "error")

  # => Seleciona servico do select e adiciona na GRID
  $("#select_service_association_btn").on 'click', (event) ->
    event.preventDefault()
    service_id = $('#cliente_fornecedor_name_services :selected').val()
    if service_id == ""
      bootbox.alert("<i style='color: rgb(208, 39, 39)'><b>Selecione</b> um serviço</i>")
    else
      _services_.push
        'service_id' : service_id
      generate_table("Service",service_id)

  if $('.edit_cliente_fornecedor').length
    cliente_fornecedor_id = $("#cliente_fornecedor_id").val()
    $.ajax
      url: LOCALHOST + "resquests/collect_fornecedor_services"
      type: 'GET'
      dataType: 'JSON'
      data:
        id: cliente_fornecedor_id
      success: (response) ->
        i=0
        while i <= response.length
          if response[i] != undefined
            $("#services_content").append(
              "<tr id='tr_fornecedor_service_id_#{response[i].id}'>" +
                "<td>#{response[i].name}</td>" +
                "<td>#{response[i].status}</td>" +
                "<td class='center'>" +
                  "<a href='#' id='btn_remove_service' title='Remover Serviço' data-fornecedor-id='#{response[i].fornecedor_id}' data-fornecedor-service-id='#{response[i].id}'>" +
                    "<i class='fa fa-trash' aria-hidden='true' style='color: #AF2222; font-size: 16pt'></i>" +
                  "</a>" +
                "</td>" +
              "</tr>")
          i++

  # => Deleta servico não salvo
  $(document).on "click", "#delete_preview_services_data", (event) ->
    event.preventDefault()
    data_service_id = $(this).data().id
    $("#tr_services_id_#{data_service_id}").remove()
    index = _services_.indexOf(data_service_id)
    _services_.splice(index, data_service_id)

  $(document).on "click", "#btn_remove_service", (event) ->
    event.preventDefault()
    # myCustomConfirmBox "Deseja mesmo remover este item?", ->

    fornecedorServiceId = $(this).data().fornecedorServiceId
    fornecedorId = $(this).data().fornecedorId
    bootbox.dialog
      message: "Deseja mesmo remover este item?"
      class: 'class-confirm-box'
      className: "my-modal"
      buttons:
        success:
          label: "SIM!"
          className: "btn-success pull-left"
          callback: ->
            $.ajax
              url: LOCALHOST + "resquests/find_fornecedor_service_and_destroy"
              type: 'GET'
              dataType: 'JSON'
              data:
                id: fornecedorId
                fornecedor_service_id: fornecedorServiceId
              success: (response) ->
                $("#tr_fornecedor_service_id_#{fornecedorServiceId}").fadeOut(500)
                $.notify(response.message, "success")
        chickenout:
          label: "NÃO"
          className: "btn-danger pull-right"

# ----------------------------- END ------------------------------------
