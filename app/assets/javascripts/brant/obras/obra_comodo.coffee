$(document).ready ->
  window.LOCALHOST= window.location.origin + '/'
  window.obra_comodos = []
  window.counter = 0

  $('#btn_to_add_comodo_association').on 'click', (event) ->
    if $("#obra_comodo_id :selected").val() != ""
      comodo_id = $("#obra_comodo_id :selected").val()
      comodo_nome= $("#obra_comodo_id :selected").text()
      obra_comodos.push
        comodo_id: comodo_id
      build_table_obra_comodos(counter, comodo_nome, comodo_id)
      counter += 1
    else
      $("#form_list_messages").html("<li>Comodo deve ser selecionado.</li>")
      $("#form_messages_modal").modal()

  window.build_table_obra_comodos=(index, comodo_nome, comodo_id) ->
    $("#tbody_comodo_obra").append("<tr id='tr_comodo_obra_id_#{index}' class='animated zoomIn'>
                                    <td><span style='color: #862424; font-weight: bolder;'>#{comodo_nome}</span></td>
                                    <td><div class='pull-right'>
                                      <a href='#' id='link_to_remove_obra_comodo_association', data-tr-id='#{index}'>
                                        <span class='fa fa-trash fa-2x'></span>
                                      </a>
                                    </div</td>
                                  </tr>")
    build_hidden_fields_obra_comodos(index, comodo_id)
  
  build_hidden_fields_obra_comodos=(index, comodo_id) ->
    $("#tr_comodo_obra_id_#{index}").append("<td><input type='hidden' id='obra_comodo_obras_attributes_#{index}_comodo_id' name='obra[comodo_obras_attributes][#{index}][comodo_id]', value='#{comodo_id}'/></td>")

  $(document).on 'click', "#link_to_remove_obra_comodo_association", (event) ->
    event.preventDefault()
    index = $(this).data().trId
    $("#tr_comodo_obra_id_#{index}").fadeOut(500).remove()
    $("#obra_comodo_obras_attributes_#{index}_comodo_id").remove()
    obra_comodos[index]=''

  send_request_to_controller=(_url_, _type_)->
    $.ajax
        url: LOCALHOST + _url_
        type: _type_
        dataType: 'JSON'
        data:
          obra:
            codigo: $("#obra_codigo").val()
            nome_local: $("#obra_nome_local").val()
            endereco_local: $("#obra_endereco_local").val()
            bairro: $("#obra_bairro").val()
            cep: $("#obra_cep").val()
            estado_id: $("#obra_estado_id").val()
            cidade_id: $("#obra_cidade_id").val()
            endereco_obra: $("#obra_endereco_obra").val()
            quadra: $("#obra_quadra").val()
            lote: $("#obra_lote").val()
            cei: $("#obra_cei").val()
            peticao: $("#obra_peticao").val()
            aprovacao: $("#obra_aprovacao").val()
            comodo_obras_attributes: obra_comodos
        success: (response)->
          if response.valid == true
            $("#progress_bar").fadeIn('slow')
            setTimeout (->
              $('.progress .progress-bar').progressbar({display_text: 'fill', use_percentage: true})
              setTimeout (->
                $.message({title: 'Aviso!',content: response.notify_message, position: "topright", color: "yellow"})
                setTimeout (->
                  window.location.href=response.url
                ), 8500
              ), 10000
            ), 2000
          else
            form_messages_manager(response.messages)
            $("#form_messages_modal").modal()

  form_messages_manager=(messages)->
    errors_counter = 0
    console.log(messages.length)
    while errors_counter <= messages.length
      if messages[errors_counter] != undefined
        $("#form_list_messages").append("<li>#{messages[errors_counter]}</li>")
      errors_counter += 1
    return

  $(document).submit (event) ->
    event.preventDefault()
    if $(".new_obra").length == 1
      send_request_to_controller("obras", "POST")

    if $(".edit_obra").length == 1
      obra_id=$("#obra_id").val()
      send_request_to_controller("obras/#{obra_id}", "PUT")
