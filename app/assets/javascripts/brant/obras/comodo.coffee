$(document).ready ->
  $.fn.populate = (value, name) ->
    $(this).append("<option value='#{value}'>#{name}</option>")
    return

  $("#btn_to_create_new_comodo").click (event) ->
    event.preventDefault()
    $("#comodo_obra_added").fadeOut(500).slideUp()
    $("#comodo_form").slideDown()
    $("#btn_submit_obra").prop(disabled: true)
    $("#btn_finish_obra").prop(disabled: true)
    $("#obra_comodo_id").prop(disabled: true)

  $("#btn_cancelar_comodo").click (event) ->
    event.preventDefault()
    $("#comodo_form").fadeOut(500).slideUp()
    $("#comodo_obra_added").fadeIn('slow')
    $("#btn_submit_obra").prop(disabled: false)
    $("#btn_finish_obra").prop(disabled: false)
    $("#obra_comodo_id").prop(disabled: false)

  $("#btn_salvar_comodo").click (event) ->
    event.preventDefault()
    $.ajax
      url: LOCALHOST + "resquests/create_comodo_through_ajax"
      type: 'GET'
      dataType: 'JSON'
      data:
        comodo:
          name: $("#comodo_name").val()
      success: (response) ->
        if response.valid == true
          $("#comodo_form").fadeOut(500).slideUp()
          $("#comodo_obra_added").fadeIn('slow')
          $("#btn_submit_obra").prop(disabled: false)
          $("#btn_finish_obra").prop(disabled: false)
          $("#obra_comodo_id").prop(disabled: false)
          $("#obra_comodo_id").populate(response.value, response.name)
          $("#obra_comodo_id  ").selectpicker('refresh')
          $.message({title: 'Sucesso!',content: response.message, position: "topright", color: "green"})

          obra_comodos.push
            comodo_id: response.value
          build_table_obra_comodos(counter, response.name, response.value)
          counter += 1
        else
          $.message({title: 'Erro!',content: response.message, position: "topright", color: "red"})
