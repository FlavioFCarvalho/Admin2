#= require jquery/jquery-3.2.1.min

# ----------------------------- START ------------------------------------
jQuery ->
  cidades = undefined
  estado = undefined
  escaped_estados = undefined
  options = undefined

  if $('.simple_form.new_obra#new_obra').length
    if $('#obra_estado_id').length
      $('#obra_estado_id').val("19")

  $('#obra_estado_id').change ->
    estado = $('#obra_estado_id :selected').text()
    escaped_estados = estado.replace(/([ #;&,.+*~\':"!^$[\]()=>|\/@])/g, '\\$1')
    options = $(cidades).filter('optgroup[label=\'' + escaped_estados + '\']').html()
    if options
      $('#obra_cidade_id').html options
      $('#obra_cidade_id').prop 'disabled', false
      return
    else
      $('#obra_cidade_id').empty()
      $('#obra_cidade_id').prop 'disabled', true
      return

  cidades = $('#obra_cidade_id').html()
  if $('#obra_estado_id').val() != ""
    if $('#obra_estado_id').val() == "19"
      $('#obra_cidade_id').val("3633")
    else
      $('#obra_cidade_id').prop 'disabled', false
      estado = $('#obra_estado_id :selected').text()
      escaped_estados = estado.replace(/([ #;&,.+*~\':"!^$[\]()=>|\/@])/g, '\\$1')
      options = $(cidades).filter('optgroup[label=\'' + escaped_estados + '\']').html()
      $('#obra_cidade_id').html options
  else
    $('#obra_cidade_id').prop 'disabled', true
  return
# ----------------------------- END ------------------------------------
