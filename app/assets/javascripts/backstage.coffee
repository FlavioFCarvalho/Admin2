#= require admin/jquery/jquery.min
#= require jquery_ujs
#= require admin/bootstrap/bootstrap.min
#= require admin/metisMenu/metisMenu.min
#= require admin/sb-admin-2.min
#= require bootbox
#= require bootstrap-switch
#= require notifyjs_rails


#= require global/messages
#= require global/users
#= require global/delete
