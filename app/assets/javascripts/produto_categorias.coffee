$(document).ready ->
  URL_BASE = window.location.origin + '/'
  dados_categorias = []

  # inserir produto_categoria
  $('#salvar_produto_categorias').click (event) ->
    event.preventDefault()
    # validando campos
    error_messages = []
    if $('#produto_categoria_categoria_id option:selected').val() == ''
      error_messages.push('<li>Categoria deve ser selecionada</li>')
    # verifica se existem erros
    if error_messages.length != 0
      # montando mensagem de erro
      return BootstrapDialog.show
        type: BootstrapDialog.TYPE_DANGER
        title: 'Erros Encontrados: Para prosseguir resolva os seguintes problemas'
        message: error_messages
        closable: false
        buttons: [ {
          label: 'Fechar'
          action: (dialogRef) ->
            dialogRef.close()
            error_messages = []
            false
        } ]
    else
      $('form#new_produto_categoria.simple_form.new_produto_categoria').unbind('submit').submit()
      $('#produto_categoria_categoria_id').val('')
    return
