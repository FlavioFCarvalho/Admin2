$('document').ready ->
  flashInfo = $('.flash').first()
  if flashInfo.length
    type = flashInfo.data('type')
    message = flashInfo.data('message')
    if type == 'success'
      # $.notify(message, "success")
      $.message({title: 'Aviso!',content: message, position: "topright", color: "green"})
    if type == 'error' || type == 'alert'
      # $.notify(message, "error")
      $.message({title: 'Aviso!',content: message, position: "topright", color: "red"})
    if type == 'warning'
      # $.notify(message, "warn")
      $.message({title: 'Aviso!',content: message, position: "topright", color: "yellow"})
    if type == 'info'
      # $.notify(message, "info")
      $.message({title: 'Aviso!',content: message, position: "topright", color: "blue"})
  return
