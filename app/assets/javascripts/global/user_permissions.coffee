#= require jquery
#= require jquery_ujs

LOCALHOST = window.location.origin + '/'
counter= 0
user_id= 0

if $('#user_authorization_form').length
  counter = $("#authorizations_count").text()
  user_id = $("#user_id").text()

collect_authorizations= ->
  user_authorizations=[]
  x=1
  # => While necessario para percorrer todas as permissoes
  # estejam elas marcadas ou não!
  c = 0
  r = 0
  u = 0
  d = 0
  while x <= counter
    # => Localizando a Div da permissao
    index = $("#indice_externo_#{x}")
    if index.val() != undefined
      #=> Localizando o id da permissao
      authorization_id = index.find("input").closest("#user_authorization_id").val()

      # => Localizando as permissões de CRUD
      inputs = index.find("input:checked")

      c = inputs.closest("#user_create").val()
      r = inputs.closest("#user_retrieve").val()
      u = inputs.closest("#user_update").val()
      d = inputs.closest("#user_delete").val()

      if index.find("input:checked").closest("#user_create").val() == undefined
        c = "0"
      if index.find("input:checked").closest("#user_retrieve").val() == undefined
        r = "0"
      if index.find("input:checked").closest("#user_update").val() == undefined
        u = "0"
      if index.find("input:checked").closest("#user_delete").val() == undefined
        d = "0"

      # => Adicionando dados das permissoes do usuario no Hash
      user_authorizations.push
        'authorization_id' : authorization_id
        'create'    : c
        'retrieve'  : r
        'update'    : u
        'delete'    : d
    x++
  return user_authorizations

ajax_request = (resource_params, PATH) ->
  $.ajax
    url: LOCALHOST + PATH
    type: 'PUT'
    dataType: 'JSON'
    data:
      user_authorizations: resource_params
      user_id: user_id
    success: (response) ->
      if response.valid.success == true
        setTimeout (->
           $.notify(response.messages.success, "info")
        ), 2000
        setTimeout (->
          window.location.href="/brant/users/accounts"
        ), 5000
      else
        i =0
        setTimeout (->
          toastr.error(response.messages.alert, "Erro!", {timeOut: 5000})
        ), 2000
        while i <= response.errors.length
          if response.errors[i] != undefined
            toastr.error(response.errors[i].erro, "Erro!")
          i++


$('#user_authorization_form').submit (event) ->
  event.preventDefault()
  data_usuario = []
  inputs = ""

  # => AJAX request para enviar dados ao controller
  PATH = "brant/users/user_authorizations/permission"

  # console.log data_usuario_permissoes
  ajax_request(collect_authorizations(), PATH)
