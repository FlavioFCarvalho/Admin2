#= require jquery3
#= require best_in_place
#= require jquery_ujs
#= require jquery-ui
#= require jquery.purr
#= require jquery/jquery.json.min
#= require best_in_place.jquery-ui
#= require best_in_place.purr
#= require notifyjs_rails
#= require bootstrap
#= require bootstrap/popper.min.js
#= require bootbox
#= require cocoon
#= require bootstrap-switch
#= require bootstrap-toggle

#= require accounting/accounting
#= require jQuery-message/jquery.message.min

#= require bootstrap/bootstrap-progressbar.min
#= require bootstrap/bootstrap-dialog
#= require bootstrap-select/bootstrap-select
#= require bootstrap-select/i18n/defaults-pt_BR
#= require jquery/jquery.maskMoney
#= require jquery/jquery.mask

#= require datepicker/bootstrap-datepicker
#= require datepicker/bootstrap-datepicker.pt-BR.min

#= require brant_files
#= require global/users
#= require global/delete
#= require global/messages
#= require global/ransack
# require global/cocoon

#= require produto_categorias

#= require cocoon
#= require modal

# require global/ransack
#= require produto_categorias

#= require brant/estoques/parcelamento