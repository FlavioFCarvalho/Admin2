require "prawn"
# -*- coding: utf-8 -*-
class ClienteFornecedorIndividualPrint < Template
  include ActionView::Helpers::SanitizeHelper

  def initialize(resource, report, view)
    super(report, view)
    @resource   = resource
    @report     = report
    @title      = @resource.pdf_file_name

    send_pdf
  end

  def send_pdf
    print_first_page
    # start_new_page
    # print_second_page
  end

  def print_first_page
    header
    bounding_box([6, 450], width: 520) do
      text "Dados do #{@title}", size: 13, align: :left, style: :bold
      line_to [50,50]

      if @resource.tipo_pessoa.eql?("Juridica")
        table fields_for_resource_juridic_person do
          self.header = false
          self.width = 520
          row(0..row_length).columns(0..13).borders = []
        end
      else
        table fields_for_resource do
          self.header = false
          self.width = 520
          row(0..row_length).columns(0..13).borders = []
        end
      end
    end
    footer
   end

  # def print_second_page
  #   header
  #   text "Contas bancárias", size: 15, align: :center, style: :bold
  #   bounding_box([20, 450], width: 999, height: 350) do
  #     table banck_accounts_resource do
  #       row(0).font_style = :bold
  #       row(0..row_length).align = :center
  #       self.width  = 750
  #       self.header = true
  #       style(row(0), background_color: 'EAEBFE')
  #     end
  #   end
  #   footer
  # end

  def fields_for_resource_juridic_person
    [[{:content=>"Status: #{show_field(@resource.status)}", :colspan=>2}],
    [{:content=> "Tipo Pessoa: #{show_field(@resource.tipo_pessoa)}", :colspan=>2}],
    [{:content=> "CNPJ: #{show_field(@resource.cnpj)}", :colspan=>2}],
    [{:content=> "Nome: #{show_field(@resource.nome)}", :colspan=>2}],
    [{:content=> "Razão Social: #{show_field(@resource.razao_social)}", :colspan=>2}],
    [{:content=> "Sexo: #{@resource.sexo}", :colspan=>2}],
    [{:content=> "Endereço: #{show_field(@resource.endereco)}", :colspan=>2}],
    [{:content=> "Bairro: #{show_field(@resource.bairro)}", :colspan=>2}],
    [{:content=> "CEP: #{show_field(@resource.cep)}", :colspan=>2}],
    [{:content=> "Cidade: #{show_field(@resource.cidade)}", :colspan=>2}],
    [{:content=> "Estado: #{show_field(@resource.estado)}", :colspan=>2}],
    [{:content=> "Inscrição Estadual: #{show_field(@resource.inscricao_estadual)}", :colspan=>2}],
    [{:content=> "Numero: #{show_field(@resource.numero)}", :colspan=>2}],
    [{:content=> "Telefone: #{show_field(@resource.phone)}", :colspan=>2}]]
  end

  def fields_for_resource
    [[{:content=> "Status: #{show_field(@resource.status)}", :colspan=>2}],
    [{:content=> "Tipo Pessoa: #{show_field(@resource.tipo_pessoa)}", :colspan=>2}],
    [:content=> "RG: #{show_field(@resource.rg)}", :colspan=>2],
    [:content=> "CPF: #{show_field(@resource.cpf)}", :colspan=>2],
    [{:content=> "Nome: #{show_field(@resource.nome)}", :colspan=>2}],
    [{:content=> "Razão Social: #{show_field(@resource.razao_social)}", :colspan=>2}],
    [{:content=> "Sexo: #{@resource.sexo}", :colspan=>2}],
    [{:content=> "Endereço: #{show_field(@resource.endereco)}", :colspan=>2}],
    [{:content=> "Bairro: #{show_field(@resource.bairro)}", :colspan=>2}],
    [{:content=> "CEP: #{show_field(@resource.cep)}", :colspan=>2}],
    [{:content=> "Cidade: #{show_field(@resource.cidade)}", :colspan=>2}],
    [{:content=> "Estado: #{show_field(@resource.estado)}", :colspan=>2}],
    [{:content=> "Inscrição Estadual: #{show_field(@resource.inscricao_estadual)}", :colspan=>2}],
    [{:content=> "Numero: #{show_field(@resource.numero)}", :colspan=>2}],
    [{:content=> "Telefone: #{show_field(@resource.phone)}", :colspan=>2}]]
  end

  def banck_accounts_resource
    [['Código', 'Agência', 'Conta', 'Banco', 'Tipo']] + @resource.contas_bancaria.map do |resource|
      [resource.id, resource.agencia, resource.conta, resource.banco_nome, resource.tipo]
    end
  end
end
