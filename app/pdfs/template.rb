require 'open-uri'

class Template <  Prawn::Document
  def page_layout
    options = {
      page_size: 'A4',
      page_layout: :landscape,
      top_margin: 20
    }
  end

  def initialize(report, view)
    super(page_layout)
    @report = report
    @view     = view

    @address      = @report.address
    @neighborhood = @report.neighborhood
    @city_state   = @report.city_state
    @cnpj         = @report.cnpj
    @phone        = @report.phone

    date_formatter
  end

  def date_formatter
    @date = Time.zone.now.strftime('%d/%m/%Y')
    @hour = Time.zone.now.strftime('%H:%M')
  end

  def header
    repeat(:all) do
      bounding_box([0, bounds.top], width: 200, height: 40) do
        image ("public/images/LOGO.jpg"), width: 130, height: 35, position: 20, vposition: 3
      end

      move_down 10

      bounding_box([620, bounds.top], width: 395, height: 40) do
        move_down 3.5
        text I18n.t("helpers.reports.emission", date: @date, hour: @hour), style: :bold
      end
    end
  end

  def footer
    repeat(:all) do
      bounding_box([150, 40], width: 520, height: 40) do
        text "CNPJ: #{@cnpj} - Telefone: #{@phone}", align: :center
        text "#{@address}, #{@neighborhood}, #{@city_state}", align: :center
        text "E-mail:#{@report.email}", align: :center
      end
    end
  end

  def show_field(field)
    if field.present?
      return field
    else
      return "Não informado"
    end
  end
end
