require "prawn"
# -*- coding: utf-8 -*-
class ClienteFornecedorPrintPage < Template
  include ActionView::Helpers::SanitizeHelper

  def initialize(resource, report, view)
    super(report, view)
    @resource   = resource
    @report     = report

    self.font_size = 10
    send_pdf
  end

  def send_pdf
    header
    print_page
    footer
  end

  def print_page
    bounding_box([20, 450], width: 999, height: 350) do
      # text "Dados do #{@title}", size: 10, align: :left, style: :bold
      table fields_for_resource do
        row(0).font_style = :bold
        row(0..row_length).align = :center
        self.width  = 750
        self.header = true
        style(row(0), background_color: 'EAEBFE')
        # row(0..row_length).columns(0..9).borders = [:bottom]
      end
    end
  end

  def fields_for_resource
    [['Código', 'Status', 'Tipo Pessoa', 'Nome', 'E-Mail', 'Telefone']] + @resource.map do |resource|
      [resource.id, resource.status, resource.tipo_pessoa, resource.nome, resource.email, resource.phone]
    end
  end
end
