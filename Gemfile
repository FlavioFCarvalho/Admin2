source 'https://rubygems.org'

git_source(:github) do |repo_name|
  repo_name = "#{repo_name}/#{repo_name}" unless repo_name.include?("/")
  "https://github.com/#{repo_name}.git"
end

gem 'rails', '5.0.0.1'
gem 'mysql2', '>= 0.3.18', '< 0.5'
gem 'puma', '~> 3.9', '>= 3.9.1'
gem 'foreman', '~> 0.84.0'
gem 'sass-rails', '~> 5.0'
gem 'uglifier', '>= 1.3.0'
gem 'coffee-rails', '~> 4.2'
gem 'cocoon', '~> 1.2', '>= 1.2.10'
gem 'jbuilder', '~> 2.5'
gem 'responders', '~> 2.4'
gem "bcrypt"
gem 'devise', '4.3.0'
gem 'devise-async', '0.7.0'
gem 'devise-encryptable', '0.2.0'
gem 'devise-i18n', '~> 1.1', '>= 1.1.2'
gem "brazilian-rails"
gem 'kaminari', '~> 1.0.1'
gem "rolify"
gem "pundit"
gem 'cancancan'
gem 'enum_help'
gem 'annotate', '2.6.5'
gem 'sidekiq', '~> 5.0'
gem 'nokogiri', '~> 1.8'
gem 'rack', '~> 2.0.1'
gem 'simple_form', '~> 3.5.0'
gem 'ransack', '~> 1.8.2'
gem "wicked", "~>  1.3.0"
gem 'wannabe_bool'
gem 'haml-rails', '~> 1.0'
gem 'jquery-rails'
gem 'jquery-ui-rails'
gem 'redis'
gem 'money-rails'
gem 'dotenv-rails', '~> 2.2', '>= 2.2.1'
gem 'bootstrap-kaminari-views'
gem 'notifyjs_rails', '~> 0.0.2'
gem "bootstrap-switch-rails"
gem 'bootstrap-sass', '~> 3.3.6'
gem 'font-awesome-rails', '~> 4.7', '>= 4.7.0.2'
gem 'prawn', '~> 2.1.0'
gem 'prawn-table', '~> 0.2.2'
gem "paperclip", "~> 5.0.0"
gem 'tzinfo-data', platforms: [:mingw, :mswin, :x64_mingw, :jruby]
gem 'bootstrap-toggle-rails'
gem 'best_in_place', '~> 3.0.1'


group :development do
  gem 'xray-rails'
  gem 'better_errors', '~> 2.1', '>= 2.1.1'
  gem 'web-console', '>= 3.3.0'
  gem 'listen', '>= 3.0.5', '< 3.2'
  gem 'lerolero_generator', '~> 1.0', '>= 1.0.1'
  gem 'spring'
  gem 'spring-watcher-listen', '~> 2.0.0'
end

group :development, :test do
  gem 'byebug', platforms: [:mri, :mingw, :x64_mingw]
  gem 'pry'
  gem 'pry-rails'
  gem 'rspec', '~> 3.6'
  gem 'rspec-rails', '~> 3.6'
  gem 'factory_girl_rails'
  gem 'faker', '~> 1.6', '>= 1.6.6'
  gem "binding_of_caller"
  gem 'capybara', '~> 2.13'
  gem 'selenium-webdriver'
end

source 'https://rails-assets.org' do
  gem 'rails-assets-bootbox'
end

group :development do
  gem 'capistrano', '~> 3.8', '>= 3.8.1'
  gem 'capistrano-rails', '~> 1.2', '>= 1.2.3'
  gem 'capistrano-bundler', '~> 1.2'
  gem 'capistrano-rbenv', '~> 2.1', '>= 2.1.1'
  gem 'capistrano-puma', '~> 0.2.3'
  gem 'capistrano3-puma', '~> 3.1'
  gem 'capistrano3-nginx', '~> 2.1', '>= 2.1.6'
  gem 'capistrano-upload-config', '~> 0.7.0'
end
